#ifndef VISITORSUPPORTED_H
#define VISITORSUPPORTED_H

#include "visitor.h"

class VisitorSupported
{
public:

    virtual void accept(Visitor & visitor) = 0;
};

#endif // VISITORSUPPORTED_H
