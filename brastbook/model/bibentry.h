#ifndef BIBENTRY_H
#define BIBENTRY_H

#include <cstdlib>
#include <iostream>

#include <QMetaType>
#include <QtGlobal>
#include <QString>

#include <QDebug>

#include "visitorsupported.h"

class BibEntryTest;

class BibEntry : public VisitorSupported
{
public:
    friend BibEntryTest;

    static const int DEFAULT;      /**< Dummy index */
    static const int NOT_EXISTS;   /**< Index for non-existing entry */

    static QString initStatement();
    static QString insertStatement();

    static const int BIBENTRYTYPES_NUM;
    static const int BET_ARTICLE;
    static const int BET_BOOK;
    static const int BET_BOOKLET;
    static const int BET_CONFERENCE;
    static const int BET_INBOOK;
    static const int BET_INCOLLECTION;
    static const int BET_INPROCEEDINGS;
    static const int BET_MANUAL;
    static const int BET_MASTERTHESIS;
    static const int BET_MISC;
    static const int BET_PHDTHESIS;
    static const int BET_PROCEEDINGS;
    static const int BET_TECHREPORT;
    static const int BET_UNPUBLISHED;

    static const char * const BIBENTRY_TYPE[];
    static const char * const BIBENTRY_FIELD[];
    static const char * const BIBENTRY_FIELD_TYPE[];
    static const char * const BIBENTRY_FIELD_PROPERTY[];

    enum {
        ID = 0
      , ENTRYTYPE
      , HASH
      , BIBKEY
      , ADDRESS
      , ANNOTE
      , AUTHOR
      , BOOKTITLE
      , CHAPTER
      , CROSSREF
      , EDITION
      , EDITOR
      , HOWPUBLISHED
      , INSTITUTION
      , JOURNAL
      , KEY
      , MONTH
      , NOTE
      , NUMBER
      , ORGANIZATION
      , PAGES
      , PUBLISHER
      , SCHOOL
      , SERIES
      , TITLE
      , TYPE
      , VOLUME
      , YEAR
      , DOI
      , ISSN
      , ISBN
      , URL
      , DATE
      , TIMESTAMP
      , LANGUAGE
      , OWNER
      , EDITEDBY
      , EDITEDAT
      , ABSTRACT
    };

    static const int COL_NUM;


    static const char * const DEFAULT_PRESETS[];

    BibEntry(int id = BibEntry::DEFAULT);

    int getID() const { return id; }
    void setID(const int id) { this->id = id; }

    /**
     * Realisation of the Visitor pattern
     */
    virtual void accept(Visitor & visitor) override { visitor.processBibEntry(*this); };

    const QString & getField(int fieldNum) const;
    void setField(int fieldNum, const QString & value);
    const int getEntryType() const { return entryType; }
    void setEntryType(const int type);

    const QStringList & getTextNamedGroups() const { return textNamedGroups; }
    void setTextNamedGroups(QStringList & groups) { textNamedGroups = groups; }

    const QList<int> getShelfIDs() const { return shelfIDs.values(); }
    void clearShelfIDs() { shelfIDs.clear(); }
    void addShelfID(int id) { shelfIDs.insert(id); }
    bool removeShelfID(int id) { return shelfIDs.remove(id); }

    const QStringList & getFiles() const { return files; }
    const QString & getFile(int num) const;
    void clearFiles() { files.clear(); }
    void addFile(const QString & file) { files.append(file); }

    void setHash();

//    friend std::ostream& operator<< (std::ostream& os, const BibEntry & entry);
//    friend QDebug operator<< (QDebug dbg, const BibEntry & entry);
    friend bool operator==(const BibEntry& lhs, const BibEntry& rhs);
    friend bool operator!=(const BibEntry& lhs, const BibEntry& rhs);

private:
    int id;
    int entryType;

    QStringList bibField;


    QStringList textNamedGroups; /**< For JabRef import */
    QSet<int> shelfIDs; /**< IDs of shelfs this entry belongs to */
    QStringList files; /**< list of paths to the associated files */
};


Q_DECLARE_METATYPE(BibEntry)

#endif // BIBENTRY_H
