#include <QObject>
#include <QCryptographicHash>

#include <exception>
#include "bibentry.h"


const int BibEntry::DEFAULT = -1;
const int BibEntry::NOT_EXISTS = -2;


const int BibEntry::BIBENTRYTYPES_NUM          = 14;
const int BibEntry::BET_ARTICLE         = 0;
const int BibEntry::BET_BOOK            = 1;
const int BibEntry::BET_BOOKLET         = 2;
const int BibEntry::BET_CONFERENCE      = 3;
const int BibEntry::BET_INBOOK          = 4;
const int BibEntry::BET_INCOLLECTION    = 5;
const int BibEntry::BET_INPROCEEDINGS   = 6;
const int BibEntry::BET_MANUAL          = 7;
const int BibEntry::BET_MASTERTHESIS    = 8;
const int BibEntry::BET_MISC            = 9;
const int BibEntry::BET_PHDTHESIS       = 10;
const int BibEntry::BET_PROCEEDINGS     = 11;
const int BibEntry::BET_TECHREPORT      = 12;
const int BibEntry::BET_UNPUBLISHED     = 13;


const int BibEntry::COL_NUM    = BibEntry::ABSTRACT + 1; //39;

BibEntry::BibEntry(int id) :
    id(id)
{
    for (int i = 0; i < COL_NUM; i ++) {
        bibField.append("");
    }
    entryType = 0;
}

const char * const BibEntry::DEFAULT_PRESETS[] = {
      "<p>{{author}}</p>\n<p style=\"font-style: italic;\">{{title}}</p>\n<p>{{journal}}, <span style=\"font-weight: bold;\">{{volume}}</span>, {{pages}} ({{year}})</p>\n<p><a href=\"{{url}}\">{{url}}</a> DOI: {{doi}}</p>\n<p><span style=\"font-weight: bold;\">Abstract:</span>&nbsp;{{abstract}}</p>\n<p><span style=\"font-weight: bold;\">Note:</span>&nbsp;{{note}}</p>\n<p><span style=\"font-weight: bold;\">Shelfs:</span>{{shelfs}}</p>\n"
    , "<p>{{author}}</p>\n<p style=\"font-style: italic;\">{{title}}</p>\n<p>Ed. by {{editor}}, {{publisher}} ({{year}})</p>\n<p><a href=\"{{url}}\">{{url}}</a> DOI: {{doi}}</p>\n<p><span style=\"font-weight: bold;\">Abstract:</span>&nbsp;{{abstract}}</p>\n<p><span style=\"font-weight: bold;\">Note:</span>&nbsp;{{note}}</p>\n<p><span style=\"font-weight: bold;\">Shelfs:</span>{{shelfs}}</p>\n"
    , "{{author}}<br/>\n<it>{{title}}</it><br/>\n{{note}} ({{year}})\n"
    , "<p>{{author}}</p>\n<p style=\"font-style: italic;\">{{title}}</p>\n<p>In: {{booktitle}}</p>\n<p>P. {{pages}} ({{year}}), <a href=\"{{url}}\">{{url}}</a> DOI: {{doi}}</p>\n<p><span style=\"font-weight: bold;\">Shelfs:</span>{{shelfs}}</p>\n"
    , "<p>{{author}}</p>\n<p style=\"font-style: italic;\">{{title}}</p>\n<p>In: {{booktitle}}</p>\n<p>P. {{pages}} ({{year}}), <a href=\"{{url}}\">{{url}}</a> DOI: {{doi}}</p>\n<p><span style=\"font-weight: bold;\">Shelfs:</span>{{shelfs}}</p>\n"
    , "<p>{{author}}</p>\n<p style=\"font-style: italic;\">{{title}}</p>\n<p>In: {{booktitle}}</p>\n<p>P. {{pages}} ({{year}}), <a href=\"{{url}}\">{{url}}</a> DOI: {{doi}}</p>\n<p><span style=\"font-weight: bold;\">Shelfs:</span>{{shelfs}}</p>\n"
    , "<p>{{author}}</p>\n<p style=\"font-style: italic;\">{{title}}</p>\n<p>In: {{booktitle}}</p>\n<p>P. {{pages}} ({{year}}), <a href=\"{{url}}\">{{url}}</a> DOI: {{doi}}</p>\n<p><span style=\"font-weight: bold;\">Shelfs:</span>{{shelfs}}</p>\n"
    , "{{author}}<br/>\n<it>{{title}}</it><br/>\n{{note}} ({{year}})\n"
    , "{{author}}<br/>\n<it>{{title}}</it><br/>\n{{note}} ({{year}})\n"
    , "{{author}}<br/>\n<it>{{title}}</it><br/>\n{{note}} ({{year}})\n"
    , "{{author}}<br/>\n<it>{{title}}</it><br/>\n{{note}} ({{year}})\n"
    , "{{author}}<br/>\n<it>{{title}}</it><br/>\n{{note}} ({{year}})\n"
    , "{{author}}<br/>\n<it>{{title}}</it><br/>\n{{note}} ({{year}})\n"
    , "{{author}}<br/>\n<it>{{title}}</it><br/>\n{{note}} ({{year}})\n"
};

const char * const BibEntry::BIBENTRY_TYPE[] = {
      "article"
    , "book"
    , "booklet"
    , "conference"
    , "inbook"
    , "incollection"
    , "inproceedings"
    , "manual"
    , "masterthesis"
    , "misc"
    , "phdthesis"
    , "proceedings"
    , "techreport"
    , "unpublished"
};

const char * const BibEntry::BIBENTRY_FIELD[] = {
      "id"
    , "entrytype"
    , "hash"
    , "bibkey"
    , "address"
    , "annote"
    , "author"
    , "booktitle"
    , "chapter"
    , "crossref"
    , "edition"
    , "editor"
    , "howpublished"
    , "institution"
    , "journal"
    , "key"
    , "month"
    , "note"
    , "number"
    , "organization"
    , "pages"
    , "publisher"
    , "school"
    , "series"
    , "title"
    , "type"
    , "volume"
    , "year"
    , "doi"
    , "issn"
    , "isbn"
    , "url"
    , "date"
    , "timestamp"
    , "language"
    , "owner"
    , "editedby"
    , "editedat"
    , "abstract"
};

const char * const BibEntry::BIBENTRY_FIELD_TYPE[] = {
      "INTEGER"
    , "INTEGER"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
    , "TEXT"
};

const char * const BibEntry::BIBENTRY_FIELD_PROPERTY[] = {
      "PRIMARY KEY"
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
    , ""
};

QString BibEntry::initStatement() {
    QString str = ("CREATE TABLE bibitems (");
    str += QString(BIBENTRY_FIELD[0]) + " " + BIBENTRY_FIELD_TYPE[0] + " " + BIBENTRY_FIELD_PROPERTY[0];
    for (int i = 1; i < COL_NUM; i ++) {
        str += QString(", ") + BIBENTRY_FIELD[i] + " " + BIBENTRY_FIELD_TYPE[i] + " " + BIBENTRY_FIELD_PROPERTY[i];
    }
    str += ")";
    return str;
}

QString BibEntry::insertStatement() {
    QString str = "INSERT INTO bibitems (";
    str += BIBENTRY_FIELD[1];
    for (int i = 2; i < COL_NUM; i ++) {
        str += ", ";
        str += BIBENTRY_FIELD[i];
    }
    str += ") VALUES (?";
    for (int i = 2; i < COL_NUM; i ++) {
        str += ", ?";
    }
    str += ")";

    return str;
}

const QString & BibEntry::getField(int fieldNum) const {
    if (fieldNum < 0 || fieldNum >= COL_NUM) {
        throw std::runtime_error(QObject::tr("Internal error: Wrong column number %1!").arg(fieldNum).toStdString());
    }

    return bibField[fieldNum];
}

void BibEntry::setField(int fieldNum, const QString &value) {
    if (fieldNum < 0 || fieldNum >= COL_NUM) {
        throw std::runtime_error(QObject::tr("Internal error: Wrong column number %1!").arg(fieldNum).toStdString());
    }

    bibField[fieldNum] = value;
}

const QString & BibEntry::getFile(int num) const {
    if (num < 0 || num >= files.count()) {
        throw std::runtime_error(QObject::tr("Internal error: Wrong file number %1!").arg(num).toStdString());
    }

    return files[num];
}

void BibEntry::setEntryType(const int type) {
    if (type < 0 || type >= BIBENTRYTYPES_NUM) {
        throw std::runtime_error(QObject::tr("Internal error: Wrong entry type %1!").arg(type).toStdString());
    }

    entryType = type;
}

//std::ostream& operator<< (std::ostream& os, const BibEntry& entry) {
//    os << "@" << BibEntry::BIBENTRY_TYPE[entry.getEntryType()] << "(" << entry.bibField[BibEntry::BIBKEY].toStdString() << ") {\n"
//        << "  title:     " << entry.bibField[BibEntry::TITLE].toStdString()
//        << "\n  author(s): " << entry.bibField[BibEntry::AUTHOR].toStdString()
//        << "\n  doi:       " << entry.bibField[BibEntry::DOI].toStdString()
//        << "\n}";
//    return os;
//}

//QDebug operator<< (QDebug dbg, const BibEntry & entry) {
//    dbg.nospace() << entry;
//    return dbg.maybeSpace();
//}

bool operator==(const BibEntry& lhs, const BibEntry& rhs) {
    if (lhs.getEntryType() != rhs.getEntryType()) {
        return false;
    }

    for (int i = BibEntry::BIBKEY; i < BibEntry::COL_NUM; i ++) {
        if ((i != BibEntry::EDITEDBY) && (i != BibEntry::EDITEDAT) && (lhs.getField(i).simplified() != rhs.getField(i).simplified())) {
            return false;
        }
    }

    return true;
}

bool operator!=(const BibEntry& lhs, const BibEntry& rhs) {
    return !(lhs == rhs);
}

void BibEntry::setHash() {

    QString str = "";
    for (int i = BibEntry::BIBKEY; i < BibEntry::COL_NUM; i ++) {
        str += getField(i);
    }

    setField(BibEntry::HASH, QCryptographicHash::hash(str.toLatin1(), QCryptographicHash::Md5).toHex());
}
