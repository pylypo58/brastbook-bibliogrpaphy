#ifndef MODEL_H
#define MODEL_H

#include <QtGlobal>

#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QStandardItemModel>
#include <QSqlTableModel>
#include <QSqlQueryModel>
#include <QSortFilterProxyModel>
#include <QHash>

#include "bibentry.h"
#include "shelfentry.h"
#include "qshelfmodel.h"
#include "dbvars.h"

class Model : public QObject
{
    Q_OBJECT
public:
    static const QString DB_ENTRIES_TABLE;
    static const QString DB_SHELFS_TABLE;
    static const QString DB_ENTRY_SHELF_TABLE;
    static const QString DB_FILES_TABLE;

    /**
     * @brief Model constructor
     * @param path2database the path to SQLite database
     * @param columns the list of comma-separated columns to
     *          show in the associated view of bibliographical entries
     * @param isNew the flag indicating is the existing database opening or not
     */
    Model(QString path2database, QString columns, bool isNew = false);
    ~Model();

    // general getters
    DBVars & getDBVars() { return DBVars::getInstance(); }


//    const QString & getPath2Database() { return path2database; }
////    QAbstractItemModel * getBibEntriesModel() { return bibEntriesModelSort; }
    QAbstractItemModel * getBibEntriesModel() { return shelfModel; }
    QAbstractItemModel * getShelfsModel() { return bookcaseTreeModel; }
//    const QString & getOwner() { return owner; }
//    const QString & getEmail() { return email; }
//    const QStringList & getPresets() { return presets; }
//    const QString & getFilePath() { return filePath; }

    // getters for bibliography
    BibEntry getBibEntry(const int id);
    BibEntry getBibEntry(const QString & hash, bool throwIfNotFound = true);
    int getAllBibEntriesNum();
    int getBibEntriesNumAtShelf(const QString shelfName = "");
    int getBibEntriesNumAtShelf(const ShelfEntry & shelf);
    BibEntry getCurrentBibEntry() { return currentEntry; }
    BibEntry getRightClickedEntry() {
        BibEntry tmp = rightClickedEntry;
        rightClickedEntry.setID(BibEntry::NOT_EXISTS);
        return tmp;
    }

    // getters for shelfs
    QList<ShelfEntry> getAllShelfs();
//    ShelfEntry getShelfEntry(const QString shelfName = "");
    ShelfEntry getShelfEntry(const int id);
    QList<ShelfEntry> getShelfsForEntry(const int id);
    QList<ShelfEntry> getShelfsForEntry(const BibEntry & entry);
    QList<BibEntry> getEntriesAtShelf(const int shelf_id);
    int getDefaultShelfId() { return DBVars::getInstance().getDefaultShelfId(); }
    int getRootShelfId() { return DBVars::getInstance().getRootShelfId(); }
    ShelfEntry getCurrentShelf() { return currentShelf; }
    ShelfEntry getRightClickedShelf() {
        ShelfEntry tmp = rightClickedShelf;
        rightClickedShelf.setID(ShelfEntry::NOT_EXITS);
        return tmp;
    }

    // setters
    void setOwner(const QString owner) { DBVars::getInstance().setOwner(owner); }
    void setEmail(const QString email) { DBVars::getInstance().setEmail(email); }
    void setPresets(const QStringList presets) { DBVars::getInstance().setPresets(presets); }
    void setFilePath(const QString filePath) { DBVars::getInstance().setFilePath(filePath); }
    void setEntryViewColumns(QString columns);
    void setDefaultShelfId(int id) { DBVars::getInstance().setDefaultShelfId(id); }

    // general actions
    void updateBookcaseView();
    void populateAllShelfs();
    void updateShelfView(const ShelfEntry shelf, bool useLastQuery = false);
    void updateShelfViewBySearch(const QString & searchCondition, const int shelfId);
    const QList<BibEntry> getEntriesBySearch(const QString searchCondition);
    bool isBibKeyPresent(const QString bibkey);

    // actions on bibliographical entries
    void addBibEntry(const BibEntry & bibEntry, const int shelf_id_to_update = ShelfEntry::DEFAULT);
    void updateBibEntry(const BibEntry bibEntry, const ShelfEntry currentShelf);
    void deleteBibEntry(const BibEntry & bibEntry, const ShelfEntry & currentShelf);

    // actions on shelfs
    void addShelf(const ShelfEntry & shelfEntry);
    void updateShelf(ShelfEntry shelf);
    void deleteShelf(const ShelfEntry shelf);
    void clearShelf(const ShelfEntry shelf);

signals:
    void allShelfsAreChanged();
    void shelfViewIsChanged();
    void shelfIsChanged(ShelfEntry shelf);

public slots:
    void setCurrentShelf(const ShelfEntry entry) { currentShelf = entry; }
    void setCurrentBibEntry(const BibEntry entry) { currentEntry = entry; }
    void setRightClickedShelf(const ShelfEntry entry) { rightClickedShelf = entry; }
    void setRightClickedBibEntry(const BibEntry entry) { rightClickedEntry = entry; }

private:
    const QString DB_SHELFS_SELECT = QString("SELECT %1.name FROM %1 ORDER BY %1.name ASC").arg(DB_SHELFS_TABLE);
    const QString DB_DEFAULT_SHELF_SELECT = QString("SELECT __cols__ FROM %1").arg(DB_ENTRIES_TABLE);

    void initNewDatabase();
    void openDatabase();
    int getShelfId(const QString & name, bool throwIfNotFound = true);

//    QString path2database;
    QSqlDatabase db;

    QSqlQueryModel * bibEntriesModel;
    QSortFilterProxyModel * bibEntriesModelSort;
    QShelfModel * shelfModel;

    QSqlQueryModel * bookcaseModel;
    QStandardItemModel * bookcaseTreeModel;
    QHash<int, QStandardItem *> allShelfs;
    QStandardItem * rootShelfItem;

    QString lastShelfQuery;

    QString entryViewColumns;
//    QStringList presets;
//    QString owner;
//    QString email;
//    QString filePath;

//    int rootShelfId;
//    int defaultShelfId;

    ShelfEntry currentShelf;
    BibEntry currentEntry;
    ShelfEntry rightClickedShelf;
    BibEntry rightClickedEntry;
};

#endif // MODEL_H
