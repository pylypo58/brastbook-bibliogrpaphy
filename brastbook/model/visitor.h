#ifndef VISITOR_H
#define VISITOR_H

class BibEntry;
class ShelfEntry;

class Visitor
{
public:
    virtual void processBibEntry(const BibEntry & entry) = 0;
    virtual void processShelf(const ShelfEntry & entry) = 0;
};

#endif // VISITOR_H
