#include <exception>
#include <QDebug>
#include <QDir>
#include <QSqlRecord>

#include "model.h"
#include "../global.h"


const QString DB_DRIVER("QSQLITE");

const QString Model::DB_ENTRIES_TABLE = "bibitems";
const QString Model::DB_SHELFS_TABLE = "shelfs";
const QString Model::DB_ENTRY_SHELF_TABLE = "entry_shelf";
const QString Model::DB_FILES_TABLE = "files";

Model::Model(QString path2database, QString columns, bool isNew) :
//    path2database(path2database)
   bibEntriesModel(nullptr)
  , shelfModel(nullptr)
  , bookcaseModel(nullptr)
  , bookcaseTreeModel(nullptr)
  , rightClickedShelf(ShelfEntry::NOT_EXITS)
  , rightClickedEntry(BibEntry::NOT_EXISTS)
{
    DBVars::getInstance().path2database = path2database;

    if (QSqlDatabase::isDriverAvailable(DB_DRIVER)) {
        db = QSqlDatabase::addDatabase(DB_DRIVER);
    } else {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << "Cannot load SQLite driver!";
        throw std::runtime_error(QObject::tr("Cannot load SQLite driver!").toStdString());
    }

    if (isNew) {
        // no database, going to create a new one
        initNewDatabase();
    } else {
        // try to open the given one
        openDatabase();
    }

    setEntryViewColumns(columns);
    bibEntriesModel = new QSqlQueryModel(nullptr);
    QString selquery = DB_DEFAULT_SHELF_SELECT;
    lastShelfQuery = selquery.replace("__cols__", DB_ENTRIES_TABLE + ".id," + entryViewColumns);
    bibEntriesModel->setQuery(lastShelfQuery);    
    while (bibEntriesModel->canFetchMore()) {
        bibEntriesModel->fetchMore();
    }
    bibEntriesModelSort = new QSortFilterProxyModel(nullptr);
    bibEntriesModelSort->setSourceModel(bibEntriesModel);

    bookcaseModel = new QSqlQueryModel(nullptr);
    bookcaseModel->setQuery(DB_SHELFS_SELECT);
    bookcaseTreeModel = new QStandardItemModel(nullptr);
    populateAllShelfs();


    QSqlQuery query(db);
    query.prepare(QString("SELECT id FROM %1 WHERE name='---'").arg(DB_SHELFS_TABLE));
    query.exec();
    if (query.next()) {
        DBVars::getInstance().rootShelfId = query.value(0).toInt();
    } else {
//        throw std::runtime_error(QObject::tr("Cannot get root shelf id!").toStdString());
        DBVars::getInstance().rootShelfId = -1;
    }
    DBVars::getInstance().defaultShelfId = DBVars::getInstance().getRootShelfId();

    setCurrentShelf(getShelfEntry(DBVars::getInstance().getRootShelfId()));
    shelfModel = new QShelfModel(bibEntriesModelSort, DBVars::getInstance().getRootShelfId());
    setEntryViewColumns(columns);
}

Model::~Model() {
    if (shelfModel) {
        delete shelfModel;
    }
    if (bibEntriesModelSort) {
        delete bibEntriesModelSort;
    }
    if (bibEntriesModel) {
        delete bibEntriesModel;
    }
    if (bookcaseModel) {
        delete bookcaseModel;
    }
    if (bookcaseTreeModel) {
        delete bookcaseTreeModel;
    }
}

void Model::populateAllShelfs() {
    allShelfs.clear();
    bookcaseTreeModel->clear();

    QSqlQuery query(db);

    query.prepare(QString("SELECT id,name,hash,parent,shared,repo_file,files_path,external_db_path"
                          " FROM %1 ORDER BY parent ASC, name ASC").arg(DB_SHELFS_TABLE));
    query.exec();
    QList<QStandardItem*> listItems;
    bool isRoot = false;
    rootShelfItem = nullptr;
    while (query.next()) {
        ShelfEntry shelf(query.value(0).toInt());
        shelf.name = query.value(1).toString();
        shelf.hash = query.value(2).toString();
        shelf.parent = query.value(3).toInt();
        shelf.shared = query.value(4).toBool();
        shelf.repo_file = query.value(5).toString();
        shelf.files_path = query.value(6).toString();
        shelf.external_db_path = query.value(7).toString();

        if (shelf.hash == ShelfEntry::ROOT_HASH) {
            shelf.name = tr("Root");
            isRoot = true;
        }

        QStandardItem * item = new QStandardItem(shelf.name);
        item->setData(QVariant::fromValue(shelf), Qt::UserRole);
        item->setEditable(false);
        allShelfs.insert(shelf.getID(), item);
        listItems.append(item);
        if (isRoot) {
            rootShelfItem = item;
            isRoot = false;
        }
    }

    bookcaseTreeModel->appendRow(rootShelfItem);
    foreach (auto item, listItems) {
        if (item != rootShelfItem) {
            ShelfEntry shelf = item->data(Qt::UserRole).value<ShelfEntry>();
            if (shelf.parent <= 0) {
                rootShelfItem->appendRow(item);
            } else {
                QStandardItem * parent = allShelfs.value(shelf.parent);
                parent->appendRow(item);
            }
        }
    }

    emit allShelfsAreChanged();
}

void Model::initNewDatabase() {
    openDatabase();

    QSqlQuery query(db);

    // create table for all entries
    if (!query.exec(BibEntry::initStatement())) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot initialize bibitems table!").toStdString());
    }

    // create table for shelfs
    if (!query.exec(QString("CREATE TABLE %1 ("
                      "id INTEGER PRIMARY KEY"
                    ", name TEXT NOT NULL"
                    ", hash TEXT NOT NULL UNIQUE"
                    ", parent INT"
                    ", shared INT"
                    ", repo_file TEXT"
                    ", files_path TEXT"
                    ", external_db_path TEXT"
                      ")").arg(DB_SHELFS_TABLE))) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot initialize shelfs table!").toStdString());
    }    
    // define the "global" shell for all entries
    if (!query.exec(QString("INSERT INTO %1 (name, hash, parent, shared, repo_file, files_path)"
                    "VALUES ('---', '%2', -1, 0, '', '')").arg(DB_SHELFS_TABLE).arg(ShelfEntry::ROOT_HASH))) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot initialize shelfs table!").toStdString());
    }

    // create table for connection between entries and shelfs
    if (!query.exec(QString("CREATE TABLE %1 ("
                      "id INTEGER PRIMARY KEY"
                    ", entry_id INT"
                    ", shelf_id INT"
                      ")").arg(DB_ENTRY_SHELF_TABLE))) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot initialize entry_shelf table!").toStdString());
    }

    // create table for files
    if (!query.exec(QString("CREATE TABLE %1 ("
                      "id INTEGER PRIMARY KEY"
                    ", path TEXT"
                    ", entry_id INT"
                    ", display_order INT"
                      ")").arg(DB_FILES_TABLE))) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot initialize files table!").toStdString());
    }
}

void Model::openDatabase() {
    db.setDatabaseName(DBVars::getInstance().path2database);

    if (!db.open()) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot open the database '%1'!").arg(DBVars::getInstance().path2database).toStdString());
    }
}

void Model::addBibEntry(const BibEntry &bibEntry, const int shelf_id_to_update) {
    if (bibEntry.getField(BibEntry::HASH).length() == 0) {
//        qWarning() << __FILE__ << ":" << __LINE__ << ":" << bibEntry;
        throw std::runtime_error(QObject::tr("Internal error: Empty hash!").toStdString());
    }

    if (!db.transaction()) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot initialize entry_shelf table!").toStdString());
    }


    QSqlQuery query(db);
    query.prepare(BibEntry::insertStatement());
    query.addBindValue(bibEntry.getEntryType());
    for (int i = BibEntry::HASH; i < BibEntry::COL_NUM; i ++) {
        query.addBindValue(bibEntry.getField(i));
    }

    if (!query.exec()) {
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot save entry!").toStdString());
    }


    if (!db.commit()) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        throw std::runtime_error(QObject::tr("Database commit failed!").toStdString());
        return;
    }
    if (!db.transaction()) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot initialize entry_shelf table!").toStdString());
    }


    // get id of the last record
    int entry_id = bibEntriesModel->query().lastInsertId().toInt();

    const QStringList files = bibEntry.getFiles();
    for (int i = 0; i < files.length(); i ++) {
        query.prepare(QString("INSERT INTO %1 (path, entry_id, display_order) VALUES (?, ?, ?)").arg(DB_FILES_TABLE));
        query.addBindValue(files[i]);
        query.addBindValue(entry_id);
        query.addBindValue(i);
        if (!query.exec()) {
            if (!db.rollback()) {
                qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
                throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
            }
            qWarning() << db.lastError().text();
            qWarning() << "Cannot bind entry and file " << files[i];
        }
    }


    const QStringList textNamedGroups = bibEntry.getTextNamedGroups();
    const QList<int> shelfIDs = bibEntry.getShelfIDs();
    if (textNamedGroups.length() > 0) {
        foreach (auto grp, textNamedGroups) {
            // add group if not exists
            ShelfEntry shelfEntry;
            shelfEntry.name = grp;

            // get id of the shelf
            int shelf_id = getShelfId(grp, false);
            if (shelf_id == ShelfEntry::NOT_EXITS) {
                shelfEntry.setHash();
                shelfEntry.parent = -1;
                shelfEntry.shared = 0;
                shelfEntry.repo_file = "";
                shelfEntry.files_path = "";
                query.prepare(QString("INSERT INTO %1 (name, hash, parent, shared, repo_file, files_path) VALUES (?, ?, ?, ?, ?, ?)").arg(DB_SHELFS_TABLE));
                query.addBindValue(shelfEntry.name);
                query.addBindValue(shelfEntry.hash);
                query.addBindValue(shelfEntry.parent);
                query.addBindValue(shelfEntry.shared);
                query.addBindValue(shelfEntry.repo_file);
                query.addBindValue(shelfEntry.files_path);

                if (!query.exec()) {
                    if (!db.rollback()) {
                        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
                        throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
                    }
                    qWarning() << db.lastError().text();
                    qWarning() << "Cannot add group:" << grp;
                }
                shelf_id = getShelfId(grp);
            }

            // update record-shelf table
            query.prepare(QString("INSERT INTO %1 (entry_id, shelf_id) VALUES (?, ?)").arg(DB_ENTRY_SHELF_TABLE));
            query.addBindValue(entry_id);
            query.addBindValue(shelf_id);
            if (!query.exec()) {
                if (!db.rollback()) {
                    qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
                    throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
                }
                qWarning() << db.lastError().text();
                qWarning() << "Cannot bind entry and shelf";
            }
        }
    } else if (shelfIDs.size() > 0) {
        // add new associated shelfs
        for (int i = 0; i < shelfIDs.size(); i ++) {
            query.prepare(QString("INSERT INTO %1 (entry_id, shelf_id) VALUES (?, ?)").arg(DB_ENTRY_SHELF_TABLE));
            query.addBindValue(entry_id);
            query.addBindValue(shelfIDs[i]);

            if (!query.exec()) {
                if (!db.rollback()) {
                    qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
                    throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
                }
                qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
                throw std::runtime_error(QObject::tr("Cannot add 'new' associated groups for the entry!").toStdString());
            }
        }
    }


    if (!db.commit()) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        throw std::runtime_error(QObject::tr("Database commit failed!").toStdString());
        return;
    }

    if (shelf_id_to_update > ShelfEntry::NOT_EXITS) {
        updateShelfView(shelf_id_to_update, true);
    }
}

void Model::addShelf(const ShelfEntry &shelfEntry) {
    if (!db.transaction()) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot initialize entry_shelf table!").toStdString());
    }

    QSqlQuery query(db);
    query.prepare(QString("INSERT INTO %1 (name, hash, parent, shared, repo_file, files_path, external_db_path) VALUES (?, ?, ?, ?, ?, ?, ?)").arg(DB_SHELFS_TABLE));
    query.addBindValue(shelfEntry.name);
    query.addBindValue(shelfEntry.hash);
    query.addBindValue(shelfEntry.parent);
    query.addBindValue(shelfEntry.shared);
    query.addBindValue(shelfEntry.repo_file);
    query.addBindValue(shelfEntry.files_path);
    query.addBindValue(shelfEntry.external_db_path);

    if (!query.exec()) {
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot add shelf! Do you already have one with the same name?").toStdString());
    }
    if (!db.commit()) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        throw std::runtime_error(QObject::tr("Database commit failed!").toStdString());
        return;
    }

    updateBookcaseView();

    populateAllShelfs();
}

void Model::updateBookcaseView() {
    bookcaseModel->clear();
    bookcaseModel->setQuery(DB_SHELFS_SELECT);

    emit shelfViewIsChanged();
}

int Model::getShelfId(const QString &name, bool throwIfNotFound) {
    QSqlQuery query(db);
    int shelf_id = -1;
    query.prepare("SELECT id FROM shelfs WHERE ( name = ? )");
    query.addBindValue(name);
    query.exec();
    if (query.first()) {
        shelf_id = query.value(0).toInt();
    } else {
        if (throwIfNotFound) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Cannot get group's '%1' id!").arg(name).toStdString());
        } else {
            return ShelfEntry::NOT_EXITS;
        }
    }

    return shelf_id;
}

int Model::getAllBibEntriesNum() {
    QSqlQuery query(db);

    query.prepare(QString("SELECT COUNT(id) FROM %1").arg(DB_ENTRIES_TABLE));
    query.exec();
    if (query.first()) {
        return query.value(0).toInt();
    } else {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot retrive number of entries!").toStdString());
    }
}


int Model::getBibEntriesNumAtShelf(const QString shelfName) {
    QSqlQuery query(db);
    if (shelfName.length() == 0) {
        return getAllBibEntriesNum();
    } else {
        int shelf_id = getShelfId(shelfName);

        query.prepare(QString("SELECT COUNT(%1.id) FROM %1 LEFT JOIN %2 ON %2.shelf_id = %3 WHERE %1.id = %2.entry_id").arg(DB_ENTRIES_TABLE).arg(DB_ENTRY_SHELF_TABLE).arg(shelf_id));
        query.exec();
        if (query.first()) {
            return query.value(0).toInt();
        } else {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Cannot retrive number of entries in shelf '%1'!").arg(shelfName).toStdString());
        }
    }
}

int Model::getBibEntriesNumAtShelf(const ShelfEntry &shelf) {
    QSqlQuery query(db);
    if (shelf.hash == ShelfEntry::ROOT_HASH) {
        return getAllBibEntriesNum();
    } else {
        query.prepare(QString("SELECT COUNT(%1.id) FROM %1 LEFT JOIN %2 ON %2.shelf_id = %3 WHERE %1.id = %2.entry_id").arg(DB_ENTRIES_TABLE).arg(DB_ENTRY_SHELF_TABLE).arg(shelf.getID()));
        query.exec();
        if (query.first()) {
            return query.value(0).toInt();
        } else {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Cannot retrive number of entries in shelf '%1'!").arg(shelf.name).toStdString());
        }
    }
}

ShelfEntry Model::getShelfEntry(const int id) {
    int entriesNum;
    int shelf_id = id;

    if (id == DBVars::getInstance().rootShelfId) {
        return ShelfEntry(DBVars::getInstance().rootShelfId);
    }

    QSqlQuery query(db);

    query.prepare(QString("SELECT COUNT(%1.id) FROM %1 LEFT JOIN %2 ON %2.shelf_id = %3 WHERE %1.id = %2.entry_id").arg(DB_ENTRIES_TABLE).arg(DB_ENTRY_SHELF_TABLE).arg(shelf_id));
    query.exec();
    if (query.first()) {
        entriesNum = query.value(0).toInt();
    } else {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot retrive number of entries in shelf with id'%1'!").arg(shelf_id).toStdString());
    }


    ShelfEntry shelf(shelf_id, entriesNum);
    query.prepare(QString("SELECT hash, name, parent, shared, repo_file, files_path, external_db_path FROM %1 WHERE id = ?").arg(DB_SHELFS_TABLE));
    query.addBindValue(shelf_id);
    query.exec();
    if (query.first()) {
        shelf.hash   = query.value(0).toString();
        shelf.name   = query.value(1).toString();
        shelf.parent = query.value(2).toInt();
        shelf.shared = query.value(3).toBool();
        shelf.repo_file = query.value(4).toString();
        shelf.files_path = query.value(5).toString();
        shelf.external_db_path = query.value(6).toString();
    } else {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot fetch shelf with id '%1'!").arg(shelf_id).toStdString());
    }


    return shelf;
}

BibEntry Model::getBibEntry(const int id) {
    QSqlQuery query(db);

    QString querystr = "SELECT id";
    for (int i = BibEntry::ENTRYTYPE; i < BibEntry::COL_NUM; i ++) {
        querystr += ",";
        querystr += BibEntry::BIBENTRY_FIELD[i];
    }
    querystr += QString(" FROM %1 WHERE id = %2").arg(DB_ENTRIES_TABLE).arg(id);


    BibEntry entry(id);
    query.prepare(querystr);
    query.exec();
    if (query.first()) {
        entry.setID(query.value(0).toInt());
        entry.setEntryType(query.value(1).toInt());
        for (int i = BibEntry::HASH; i < BibEntry::COL_NUM; i ++) {
            entry.setField(i, query.value(i).toString());
        }
    } else {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot fetch entry with id '%1'!").arg(id).toStdString());
    }

    query.prepare(QString("SELECT %1.id FROM %1 "
                            "LEFT JOIN %2 ON %1.id = %2.shelf_id "
                            "WHERE %2.entry_id = %3"
                           ).arg(DB_SHELFS_TABLE).arg(DB_ENTRY_SHELF_TABLE).arg(entry.getID()));

    if (query.exec()) {
        entry.clearShelfIDs();
        while (query.next()) {
            entry.addShelfID(query.value(0).toInt());
        }
    }


    query.prepare(QString("SELECT path FROM %1 WHERE entry_id = %2 ORDER BY display_order ASC, path ASC").arg(DB_FILES_TABLE).arg(entry.getID()));
    if (query.exec()) {
        entry.clearFiles();
        while (query.next()) {
            entry.addFile(query.value(0).toString());
        }
    }

    return entry;
}

BibEntry Model::getBibEntry(const QString &hash, bool throwIfNotFound) {

    BibEntry entry;
    QString querystr = "SELECT id";
    for (int i = BibEntry::ENTRYTYPE; i < BibEntry::COL_NUM; i ++) {
        querystr += ",";
        querystr += BibEntry::BIBENTRY_FIELD[i];
    }
    querystr += QString(" FROM %1 WHERE hash = '%2'").arg(DB_ENTRIES_TABLE).arg(hash);
//    query.prepare(QString("SELECT id,bibkey,entrytype,address,annote,author,booktitle,chapter,crossref,edition,editor,howpublished,institution,journal,key,month,note,number,organization,pages,publisher,school,series,title,type,volume,year,doi,issn,isbn,url,date,timestamp,language,owner,editedby,editedat,abstract FROM %1 WHERE hash = '%2'").arg(DB_ENTRIES_TABLE).arg(hash));

    QSqlQuery query(db);
    query.prepare(querystr);
    query.exec();
    if (query.first()) {
        entry.setID(query.value(0).toInt());
        entry.setEntryType(query.value(1).toInt());
        for (int i = BibEntry::HASH; i < BibEntry::COL_NUM; i ++) {
            entry.setField(i, query.value(i).toString());
        }
    } else {
        if (throwIfNotFound) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Cannot fetch entry with hash '%1'!").arg(hash).toStdString());
        } else {
            return BibEntry(BibEntry::NOT_EXISTS);
        }
    }

    query.prepare(QString("SELECT %1.id FROM %1 "
                            "LEFT JOIN %2 ON %1.id = %2.shelf_id "
                            "WHERE %2.entry_id = %3"
                           ).arg(DB_SHELFS_TABLE).arg(DB_ENTRY_SHELF_TABLE).arg(entry.getID()));

    if (query.exec()) {
        entry.clearShelfIDs();
        while (query.next()) {
            entry.addShelfID(query.value(0).toInt());
        }
    }


    query.prepare(QString("SELECT path FROM %1 WHERE entry_id = %2 ORDER BY display_order ASC, path ASC").arg(DB_FILES_TABLE).arg(entry.getID()));
    if (query.exec()) {
        entry.clearFiles();
        while (query.next()) {
            entry.addFile(query.value(0).toString());
        }
    }

    return entry;
}

//void Model::updateShelfView(const QString shelfName, bool useLastQuery) {
//    bibEntriesModel->clear();

//    if (useLastQuery) {
//        bibEntriesModel->setQuery(lastShelfQuery);
//    }
//    else {
//        if (shelfName.length() == 0) {
//            QString selquery = DB_DEFAULT_SHELF_SELECT;
//            selquery.replace("__cols__", DB_ENTRIES_TABLE + ".id," + entryViewColumns);
//            lastShelfQuery = selquery;
//            bibEntriesModel->setQuery(lastShelfQuery);
//        } else{
//            int shelfId = getShelfId(shelfName);
//            lastShelfQuery = QString("SELECT %1.id,%2 FROM %1 LEFT JOIN %3 ON %3.shelf_id = %4 WHERE %1.id = %3.entry_id").arg(DB_ENTRIES_TABLE).arg(entryViewColumns).arg(DB_ENTRY_SHELF_TABLE).arg(shelfId);
//            bibEntriesModel->setQuery(lastShelfQuery);
//        }
//    }
//}


void Model::updateShelfView(const ShelfEntry shelf, bool useLastQuery) {
    bibEntriesModel->clear();
    int shelfId = shelf.getID();

    if (useLastQuery) {
        bibEntriesModel->setQuery(lastShelfQuery);
    } else {
        if (shelfId <= DBVars::getInstance().getRootShelfId()) {
            QString selquery = DB_DEFAULT_SHELF_SELECT;
            lastShelfQuery = selquery.replace("__cols__", DB_ENTRIES_TABLE + ".id," + entryViewColumns);
            bibEntriesModel->setQuery(lastShelfQuery);
        } else{
            lastShelfQuery = QString("SELECT %1.id,%2 FROM %1 LEFT JOIN %3 ON %3.shelf_id = %4 WHERE %1.id = %3.entry_id").arg(DB_ENTRIES_TABLE).arg(entryViewColumns).arg(DB_ENTRY_SHELF_TABLE).arg(shelfId);
            bibEntriesModel->setQuery(lastShelfQuery);
        }
    }

    while (bibEntriesModel->canFetchMore()) {
        bibEntriesModel->fetchMore();
    }

    emit shelfIsChanged(shelf);
}

void Model::updateShelfViewBySearch(const QString & searchCondition, const int shelfId) {
    bibEntriesModel->clear();
    if (shelfId <= DBVars::getInstance().getRootShelfId()) {
        QString selquery = DB_DEFAULT_SHELF_SELECT;
        bibEntriesModel->setQuery(selquery.replace("__cols__", DB_ENTRIES_TABLE + ".id," + entryViewColumns));
        selquery += QString(" WHERE (%1)").arg(searchCondition);

//        qWarning() << selquery;
        lastShelfQuery = selquery;
        bibEntriesModel->setQuery(lastShelfQuery);
    } else{
        lastShelfQuery = QString("SELECT %1.id,%2 FROM %1 LEFT JOIN %3 ON %3.shelf_id = %4 WHERE %1.id = %3.entry_id AND (%5)").arg(DB_ENTRIES_TABLE).arg(entryViewColumns).arg(DB_ENTRY_SHELF_TABLE).arg(shelfId).arg(searchCondition);
        bibEntriesModel->setQuery(lastShelfQuery);
    }
}

const QList<BibEntry> Model::getEntriesBySearch(const QString searchCondition) {
    QList<BibEntry> entries;
    QSqlQuery query(db);

    query.prepare(searchCondition);
    query.exec();
    while (query.next()) {
        BibEntry entry(query.value(0).toInt());

        entry.setEntryType(query.value(1).toInt());
        for (int i = BibEntry::HASH; i < BibEntry::COL_NUM; i ++) {
            entry.setField(i, query.value(i).toString());
        }

//        entry.setField(BibEntry::BIBKEY, query.value(1).toString());
//        entry.setEntryType(query.value(2).toInt());
//        entry.setField(BibEntry::ADDRESS, query.value(3).toString());
//        entry.setField(BibEntry::ANNOTE, query.value(4).toString());
//        entry.setField(BibEntry::AUTHOR, query.value(5).toString());
//        entry.setField(BibEntry::BOOKTITLE, query.value(6).toString());
//        entry.setField(BibEntry::CHAPTER, query.value(7).toString());
//        entry.setField(BibEntry::CROSSREF, query.value(8).toString());
//        entry.setField(BibEntry::EDITION, query.value(9).toString());
//        entry.setField(BibEntry::EDITOR, query.value(10).toString());
//        entry.setField(BibEntry::HOWPUBLISHED, query.value(11).toString());
//        entry.setField(BibEntry::INSTITUTION, query.value(12).toString());
//        entry.setField(BibEntry::JOURNAL, query.value(13).toString());
//        entry.setField(BibEntry::KEY, query.value(14).toString());
//        entry.setField(BibEntry::MONTH, query.value(15).toString());
//        entry.setField(BibEntry::NOTE, query.value(16).toString());
//        entry.setField(BibEntry::NUMBER, query.value(17).toString());
//        entry.setField(BibEntry::ORGANIZATION, query.value(18).toString());
//        entry.setField(BibEntry::PAGES, query.value(19).toString());
//        entry.setField(BibEntry::PUBLISHER, query.value(20).toString());
//        entry.setField(BibEntry::SCHOOL, query.value(21).toString());
//        entry.setField(BibEntry::SERIES, query.value(22).toString());
//        entry.setField(BibEntry::TITLE, query.value(23).toString());
//        entry.setField(BibEntry::TYPE, query.value(24).toString());
//        entry.setField(BibEntry::VOLUME, query.value(25).toString());
//        entry.setField(BibEntry::YEAR, query.value(26).toString());
//        entry.setField(BibEntry::DOI, query.value(27).toString());
//        entry.setField(BibEntry::ISSN, query.value(28).toString());
//        entry.setField(BibEntry::ISBN, query.value(29).toString());
//        entry.setField(BibEntry::URL, query.value(30).toString());
//        entry.setField(BibEntry::DATE, query.value(31).toString());
//        entry.setField(BibEntry::TIMESTAMP, query.value(32).toString());
//        entry.setField(BibEntry::LANGUAGE, query.value(33).toString());
//        entry.setField(BibEntry::OWNER, query.value(34).toString());
//        entry.setField(BibEntry::EDITEDBY, query.value(35).toString());
//        entry.setField(BibEntry::EDITEDAT, query.value(36).toString());
//        entry.setField(BibEntry::ABSTRACT, query.value(37).toString());

        QSqlQuery query2(db);
        query2.prepare(QString("SELECT %1.id FROM %1 "
                                "LEFT JOIN %2 ON %1.id = %2.shelf_id "
                                "WHERE %2.entry_id = %3"
                               ).arg(DB_SHELFS_TABLE).arg(DB_ENTRY_SHELF_TABLE).arg(entry.getID()));

        if (query2.exec()) {
            entry.clearShelfIDs();
            while (query2.next()) {
                entry.addShelfID(query2.value(0).toInt());
            }
        }


        query2.prepare(QString("SELECT path FROM %1 WHERE entry_id = %2 ORDER BY display_order ASC, path ASC").arg(DB_FILES_TABLE).arg(entry.getID()));
        if (query2.exec()) {
            entry.clearFiles();
            while (query2.next()) {
                entry.addFile(query2.value(0).toString());
            }
        }

        entries.append(entry);
    }

    return entries;
}

void Model::updateShelf(ShelfEntry shelf) {
    ShelfEntry oldShelf = getShelfEntry(shelf.getID());

    if (!db.transaction()) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot initialize entry_shelf table!").toStdString());
    }

    QSqlQuery query(db);

    query.prepare(QString("UPDATE %1 SET"
                        "  name = ?"
                        ", hash = ?"
                        ", parent = ?"
                        ", shared = ?"
                        ", repo_file = ?"
                        ", files_path = ?"
                        ", external_db_path = ?"
                        " WHERE id = %2").arg(DB_SHELFS_TABLE).arg(shelf.getID()));
    query.addBindValue(shelf.name);
    query.addBindValue(shelf.hash);
    query.addBindValue(shelf.parent);
    query.addBindValue(shelf.shared);
    query.addBindValue(shelf.repo_file);
    query.addBindValue(shelf.files_path);
    query.addBindValue(shelf.external_db_path);

    if (!query.exec()) {
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot update shelf with id '%1'!").arg(shelf.getID()).toStdString());
    }
    if (!db.commit()) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        throw std::runtime_error(QObject::tr("Database commit failed!").toStdString());
    }

//    QStandardItem * item = allShelfs.value(shelf.getID());
//    item->setData(QVariant::fromValue(shelf), Qt::UserRole);
//    item->setText(shelf.name);

    updateBookcaseView();
    populateAllShelfs();
}

void Model::updateBibEntry(const BibEntry bibEntry, const ShelfEntry currentShelf) {
    if (!db.transaction()) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot initialize entry_shelf table!").toStdString());
    }

    QSqlQuery query(db);

    // update bibentry itself
    query.prepare(QString("UPDATE %1 SET "
                            "  bibkey           = ?"
                            ", entrytype        = ?"
                            ", address          = ?"
                            ", annote           = ?"
                            ", author           = ?"
                            ", booktitle        = ?"
                            ", chapter          = ?"
                            ", crossref         = ?"
                            ", edition          = ?"
                            ", editor           = ?"
                            ", howpublished     = ?"
                            ", institution      = ?"
                            ", journal          = ?"
                            ", key              = ?"
                            ", month            = ?"
                            ", note             = ?"
                            ", number           = ?"
                            ", organization     = ?"
                            ", pages            = ?"
                            ", publisher        = ?"
                            ", school           = ?"
                            ", series           = ?"
                            ", title            = ?"
                            ", type             = ?"
                            ", volume           = ?"
                            ", year             = ?"
                            ", doi              = ?"
                            ", issn             = ?"
                            ", isbn             = ?"
                            ", url              = ?"
                            ", date             = ?"
                            ", timestamp        = ?"
                            ", language         = ?"
                            ", owner            = ?"
                            ", editedby         = ?"
                            ", editedat         = ?"
                            ", abstract         = ?"
                            " WHERE id = %2").arg(DB_ENTRIES_TABLE).arg(bibEntry.getID()));
    query.addBindValue(bibEntry.getField(BibEntry::BIBKEY));
    query.addBindValue(bibEntry.getEntryType());
    query.addBindValue(bibEntry.getField(BibEntry::ADDRESS));
    query.addBindValue(bibEntry.getField(BibEntry::ANNOTE));
    query.addBindValue(bibEntry.getField(BibEntry::AUTHOR));
    query.addBindValue(bibEntry.getField(BibEntry::BOOKTITLE));
    query.addBindValue(bibEntry.getField(BibEntry::CHAPTER));
    query.addBindValue(bibEntry.getField(BibEntry::CROSSREF));
    query.addBindValue(bibEntry.getField(BibEntry::EDITION));
    query.addBindValue(bibEntry.getField(BibEntry::EDITOR));
    query.addBindValue(bibEntry.getField(BibEntry::HOWPUBLISHED));
    query.addBindValue(bibEntry.getField(BibEntry::INSTITUTION));
    query.addBindValue(bibEntry.getField(BibEntry::JOURNAL));
    query.addBindValue(bibEntry.getField(BibEntry::KEY));
    query.addBindValue(bibEntry.getField(BibEntry::MONTH));
    query.addBindValue(bibEntry.getField(BibEntry::NOTE));
    query.addBindValue(bibEntry.getField(BibEntry::NUMBER));
    query.addBindValue(bibEntry.getField(BibEntry::ORGANIZATION));
    query.addBindValue(bibEntry.getField(BibEntry::PAGES));
    query.addBindValue(bibEntry.getField(BibEntry::PUBLISHER));
    query.addBindValue(bibEntry.getField(BibEntry::SCHOOL));
    query.addBindValue(bibEntry.getField(BibEntry::SERIES));
    query.addBindValue(bibEntry.getField(BibEntry::TITLE));
    query.addBindValue(bibEntry.getField(BibEntry::TYPE));
    query.addBindValue(bibEntry.getField(BibEntry::VOLUME));
    query.addBindValue(bibEntry.getField(BibEntry::YEAR));
    query.addBindValue(bibEntry.getField(BibEntry::DOI));
    query.addBindValue(bibEntry.getField(BibEntry::ISSN));
    query.addBindValue(bibEntry.getField(BibEntry::ISBN));
    query.addBindValue(bibEntry.getField(BibEntry::URL));
    query.addBindValue(bibEntry.getField(BibEntry::DATE));
    query.addBindValue(bibEntry.getField(BibEntry::TIMESTAMP));
    query.addBindValue(bibEntry.getField(BibEntry::LANGUAGE));
    query.addBindValue(bibEntry.getField(BibEntry::OWNER));
    query.addBindValue(bibEntry.getField(BibEntry::EDITEDBY));
    query.addBindValue(bibEntry.getField(BibEntry::EDITEDAT));
    query.addBindValue(bibEntry.getField(BibEntry::ABSTRACT));

    if (!query.exec()) {
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot save entry!").toStdString());
    }

    // remove all associated files
    query.prepare(QString("DELETE FROM %1 WHERE entry_id = %2").arg(DB_FILES_TABLE).arg(bibEntry.getID()));
    if (!query.exec()) {
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot remove 'old' associated files for the entry!").toStdString());
    }

    // add new associated files
    const QStringList files = bibEntry.getFiles();
    for (int i = 0; i < files.length(); i ++) {
        query.prepare(QString("INSERT INTO %1 (path, entry_id, display_order) VALUES (?, ?, ?)").arg(DB_FILES_TABLE));
        query.addBindValue(files[i]);
        query.addBindValue(bibEntry.getID());
        query.addBindValue(i);

        if (!query.exec()) {
            if (!db.rollback()) {
                qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
                throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
            }
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Cannot add 'new' associated files for the entry!").toStdString());
        }
    }

    // remove all associated shelfs
    query.prepare(QString("DELETE FROM %1 WHERE entry_id = %2").arg(DB_ENTRY_SHELF_TABLE).arg(bibEntry.getID()));
    if (!query.exec()) {
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot remove 'old' associated shelfs for the entry!").toStdString());
    }

    // add new associated shelfs
    const QList<int> shelfIDs = bibEntry.getShelfIDs();
    for (int i = 0; i < shelfIDs.size(); i ++) {
        query.prepare(QString("INSERT INTO %1 (entry_id, shelf_id) VALUES (?, ?)").arg(DB_ENTRY_SHELF_TABLE));
        query.addBindValue(bibEntry.getID());
        query.addBindValue(shelfIDs[i]);

        if (!query.exec()) {
            if (!db.rollback()) {
                qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
                throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
            }
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Cannot add 'new' associated groups for the entry!").toStdString());
        }
    }

    if (!db.commit()) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        throw std::runtime_error(QObject::tr("Database commit failed!").toStdString());
        return;
    }

    updateShelfView(currentShelf.getID(), true);
}

QList<ShelfEntry> Model::getShelfsForEntry(const int id) {
    QList<ShelfEntry> shelfs;
    QSqlQuery query(db);

    query.prepare(QString("SELECT %1.id,%1.name,%1.parent FROM %1 "
                            "LEFT JOIN %2 ON %1.id = %2.shelf_id "
                            "WHERE %2.entry_id = %3"
                           ).arg(DB_SHELFS_TABLE).arg(DB_ENTRY_SHELF_TABLE).arg(id));

    query.exec();
    while (query.next()) {
        ShelfEntry shelf(query.value(0).toInt());
        shelf.name = query.value(1).toString();
        shelf.parent = query.value(2).toInt();

        shelfs.append(shelf);
    }

    return shelfs;
}

QList<ShelfEntry> Model::getShelfsForEntry(const BibEntry & entry) {
    QList<ShelfEntry> shelfs;
    QSqlQuery query(db);

    foreach (int shelfId, entry.getShelfIDs()) {
        query.prepare(QString("SELECT name,hash,parent,shared,repo_file,files_path,external_db_path FROM %1 "
                                "WHERE id = %2"
                               ).arg(DB_SHELFS_TABLE).arg(shelfId));
        query.exec();
        if (query.next()) {
            ShelfEntry shelf(shelfId);
            shelf.name = query.value(0).toString();
            shelf.hash = query.value(1).toString();
            shelf.parent = query.value(2).toInt();
            shelf.shared = query.value(3).toBool();
            shelf.repo_file = query.value(4).toString();
            shelf.files_path = query.value(5).toString();
            shelf.external_db_path = query.value(6).toString();

            shelfs.append(shelf);
        }
    }

    return shelfs;
}

void Model::deleteBibEntry(const BibEntry &bibEntry, const ShelfEntry & currentShelf) {
    if (!db.transaction()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Cannot start transaction!").toStdString());
        }

    QSqlQuery query(db);

    query.prepare(QString("DELETE FROM %1 WHERE id = %2").arg(DB_ENTRIES_TABLE).arg(bibEntry.getID()));
    if (!query.exec()) {
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot delete entry!").toStdString());
    }

    query.prepare(QString("DELETE FROM %1 WHERE entry_id = %2").arg(DB_ENTRY_SHELF_TABLE).arg(bibEntry.getID()));
    if (!query.exec()) {
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot delete entry!").toStdString());
    }

    query.prepare(QString("DELETE FROM %1 WHERE entry_id = %2").arg(DB_FILES_TABLE).arg(bibEntry.getID()));
    if (!query.exec()) {
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot delete entry!").toStdString());
    }

    if (!db.commit()) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        throw std::runtime_error(QObject::tr("Database commit failed!").toStdString());
        return;
    }

    if (currentShelf.getID() != ShelfEntry::NOT_EXITS) {
        updateShelfView(currentShelf.getID(), true);
    }
}

void Model::deleteShelf(const ShelfEntry shelf) {
    QStandardItem * item = allShelfs.value(shelf.getID());
    for (int i = 0; i < item->rowCount(); i ++) {
        QStandardItem * child = item->child(i);
        ShelfEntry subshelf = child->data(Qt::UserRole).value<ShelfEntry>();
        subshelf.parent = shelf.parent;
        updateShelf(subshelf);
    }

    if (!db.transaction()) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot start transaction!").toStdString());
    }

    QSqlQuery query(db);

    query.prepare(QString("DELETE FROM %1 WHERE id = %2").arg(DB_SHELFS_TABLE).arg(shelf.getID()));
    if (!query.exec()) {
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot delete entry!").toStdString());
    }

    query.prepare(QString("DELETE FROM %1 WHERE shelf_id = %2").arg(DB_ENTRY_SHELF_TABLE).arg(shelf.getID()));
    if (!query.exec()) {
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot delete entry!").toStdString());
    }

    if (!db.commit()) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        throw std::runtime_error(QObject::tr("Database commit failed!").toStdString());
        return;
    }

    updateShelfView(ShelfEntry::DEFAULT, true);
    updateBookcaseView();

    populateAllShelfs();
}

void Model::clearShelf(const ShelfEntry shelf) {
    if (!db.transaction()) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot start transaction!").toStdString());
    }

    QSqlQuery query(db);

    query.prepare(QString("DELETE FROM %1 WHERE shelf_id = %2").arg(DB_ENTRY_SHELF_TABLE).arg(shelf.getID()));

    if (!query.exec()) {
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        throw std::runtime_error(QObject::tr("Cannot delete entry!").toStdString());
    }

    if (!db.commit()) {
        qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
        if (!db.rollback()) {
            qWarning() << __FILE__ << ":" << __LINE__ << ":" << db.lastError().text();
            throw std::runtime_error(QObject::tr("Database rollback failed!").toStdString());
        }
        throw std::runtime_error(QObject::tr("Database commit failed!").toStdString());
        return;
    }

    updateShelfView(shelf.getID());
    updateBookcaseView();

    populateAllShelfs();
}

QList<BibEntry> Model::getEntriesAtShelf(const int shelf_id) {
    QList<BibEntry> bibEntries;
    QSqlQuery query(db);

    QString querystr = "SELECT " + DB_ENTRIES_TABLE + ".id";
    for (int i = BibEntry::ENTRYTYPE; i < BibEntry::COL_NUM; i ++) {
        querystr += "," + DB_ENTRIES_TABLE + "." + BibEntry::BIBENTRY_FIELD[i];
    }
    querystr += " FROM " + DB_ENTRIES_TABLE;
    querystr += QString(" LEFT JOIN %2 ON %1.id = %2.entry_id WHERE %2.shelf_id = %3 ORDER BY %1.timestamp ASC").arg(DB_ENTRIES_TABLE).arg(DB_ENTRY_SHELF_TABLE).arg(shelf_id);

    query.prepare(querystr);
    query.exec();
    while (query.next()) {
        int entry_id = query.value(0).toInt();
        BibEntry entry(entry_id);
        entry.setEntryType(query.value(1).toInt());
        for (int i = BibEntry::HASH; i < BibEntry::COL_NUM; i ++) {
            entry.setField(i, query.value(i).toString());
        }

        QSqlQuery filesQuery(db);
        filesQuery.prepare(QString("SELECT path FROM %1 WHERE entry_id = %2 ORDER BY display_order ASC, path ASC").arg(DB_FILES_TABLE).arg(entry.getID()));
        if (filesQuery.exec()) {
            entry.clearFiles();
            while (filesQuery.next()) {
                entry.addFile(filesQuery.value(0).toString());
            }
        }

        bibEntries.append(entry);
    }

    return bibEntries;
}

QList<ShelfEntry> Model::getAllShelfs() {
    QList<ShelfEntry> shelfEntries;
    QSqlQuery query(db);

    query.prepare(QString("SELECT id,name,hash,parent,shared,repo_file,files_path,external_db_path FROM %1").arg(DB_SHELFS_TABLE));
    query.exec();
    while (query.next()) {
        int shelf_id = query.value(0).toInt();
        ShelfEntry entry(shelf_id);
        entry.name = query.value(1).toString();
        entry.hash = query.value(2).toString();
        entry.parent = query.value(3).toInt();
        entry.shared = query.value(4).toBool();
        entry.repo_file = query.value(5).toString();
        entry.files_path = query.value(6).toString();
        entry.external_db_path = query.value(7).toString();

        shelfEntries.append(entry);
    }

    return shelfEntries;
}

bool Model::isBibKeyPresent(const QString bibkey) {
    QSqlQuery query(db);

    query.prepare(QString("SELECT COUNT(id) FROM %1 WHERE bibkey='%2'").arg(DB_ENTRIES_TABLE).arg(bibkey));
    query.exec();
    int count = 0;
    while (query.next()) {
        count = query.value(0).toInt();
    }
    if (count < 1) {
        return false;
    } else {
        return true;
    }
}

void Model::setEntryViewColumns(QString columns) {
    entryViewColumns = DB_ENTRIES_TABLE + "." + columns.replace(",", "," + DB_ENTRIES_TABLE + "."); // DB_ENTRIES_TABLE + "." +

    const QStringList strings = entryViewColumns.split(",");
    int entrytypeColumn = -1;
    for (int i = 0; i < strings.size(); i ++) {
        if (strings[i].toLower() == DB_ENTRIES_TABLE + ".""entrytype") {
            entrytypeColumn = i;
            break;
        }
    }

    if (shelfModel) {
        if (entrytypeColumn >= 0) {
            shelfModel->setEntryTypeColumn(entrytypeColumn);
        } else {
            shelfModel->unsetEntryTypeColumn();
        }
    }
}
