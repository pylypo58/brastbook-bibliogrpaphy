#include <QDebug>
#include "qshelfmodel.h"
#include "bibentry.h"

const int QShelfModel::NO_COLUMN = -10;

QShelfModel::QShelfModel(QAbstractItemModel * source, int rootShelfId, QObject *parent) :
    QIdentityProxyModel(parent)
  , entrytypeColumn(NO_COLUMN)
  , rootShelfId(rootShelfId)
{
    this->setSourceModel(source);
}

QVariant QShelfModel::data(const QModelIndex &index, int role) const {
    if (role != Qt::DisplayRole) {
        return QIdentityProxyModel::data(index, role);
    }
//        qWarning() << index.column() << "/" << entrytypeColumn << ":" << QIdentityProxyModel::data(index, role).toString() << " - " << entrytypeColumn;

    if (index.column() == entrytypeColumn) {
        return BibEntry::BIBENTRY_TYPE[QIdentityProxyModel::data(index, role).toInt()];
    } else {
        return QIdentityProxyModel::data(index, role);
    }
}
