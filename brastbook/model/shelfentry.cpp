#include <QCryptographicHash>
#include <QDateTime>
#include <QThread>

#include "shelfentry.h"

const QString ShelfEntry::ROOT_HASH = "000";

const int ShelfEntry::DEFAULT = -1;
const int ShelfEntry::NOT_EXITS = -2;

ShelfEntry::ShelfEntry() :
      name("")
    , parent(0)
    , shared(false)
    , repo_file("")
    , files_path("")
    , external_db_path("")
    , id(ShelfEntry::DEFAULT)
    , entriesNum(ShelfEntry::DEFAULT)
{

}


ShelfEntry::ShelfEntry(int id, int entriesNum) :
      name("")
    , parent(0)
    , shared(false)
    , repo_file("")
    , files_path("")
    , external_db_path("")
    , id(id)
    , entriesNum(entriesNum)
{

}


void ShelfEntry::setHash() {
    QThread::msleep(100);
    hash = QCryptographicHash::hash((name + QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss.zzz")).toLatin1(), QCryptographicHash::Md5).toHex();
}


bool operator==(const ShelfEntry& lhs, const ShelfEntry& rhs) {
    return lhs.getID() == rhs.getID();
}

bool operator!=(const ShelfEntry& lhs, const ShelfEntry& rhs) {
    return !(lhs == rhs);
}
