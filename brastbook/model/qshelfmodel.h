#ifndef QSHELFMODEL_H
#define QSHELFMODEL_H

#include <QSqlDatabase>
#include <QIdentityProxyModel>

#include "dbvars.h"

class QShelfModel : public QIdentityProxyModel
{
public:
    static const int NO_COLUMN;

    QShelfModel(QAbstractItemModel * source, int rootShelfId, QObject *parent = nullptr);

    QVariant data(const QModelIndex &index, int role) const override;

    void setEntryTypeColumn(int col) { entrytypeColumn = col + 1; }
    void unsetEntryTypeColumn() { entrytypeColumn = NO_COLUMN; }

    int getRootShelfId() { return rootShelfId; }
private:
    int entrytypeColumn;
    int rootShelfId;

    QSqlDatabase * db;
};

#endif // QSHELFMODEL_H
