#ifndef DBVARS_H
#define DBVARS_H

#include <QString>
#include <QStringList>

class Model;

class DBVars
{
public:

    static DBVars & getInstance() {
        static DBVars instance;

        return instance;
    }

    const QString & getOwner() { return owner; }
    const QString & getEmail() { return email; }
    const QStringList & getPresets() { return presets; }
    const QString & getFilePath() { return filePath; }
    int getDefaultShelfId() { return defaultShelfId; }
    int getRootShelfId() { return rootShelfId; }

    void setOwner(const QString owner) { this->owner = owner; }
    void setEmail(const QString email) { this->email = email; }
    void setPresets(const QStringList presets) { this->presets = presets; }
    void setFilePath(const QString filePath) { this->filePath = filePath; }
    void setEntryViewColumns(QString columns);
    void setDefaultShelfId(int id) { defaultShelfId = id; }



private:
    friend Model;

    DBVars() {};
//    DBVars(DBVars const &);
//    void operator = (DBVars const&);

    QString path2database;

    QStringList presets;
    QString owner;
    QString email;
    QString filePath;

    int rootShelfId;
    int defaultShelfId;

public:
//    DBVars(DBVars const&) = delete;
//    void operator = (DBVars const&) = delete;
};

#endif // DBVARS_H
