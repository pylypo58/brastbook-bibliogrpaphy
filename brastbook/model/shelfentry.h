#ifndef SHELFENTRY_H
#define SHELFENTRY_H

#include <QMetaType>
#include <QString>

#include "visitorsupported.h"

class ShelfEntry : public VisitorSupported
{
public:
    static const int DEFAULT; /**< Dummy index */
    static const int NOT_EXITS; /**< Index for non-existing entry */
    static const QString ROOT_HASH;
    ShelfEntry();
    ShelfEntry(int id, int entriesNum = ShelfEntry::DEFAULT);
    ShelfEntry(const ShelfEntry & ) = default;

    /**
     * Realisation of the Visitor pattern
     */
    virtual void accept(Visitor & visitor) override { visitor.processShelf(*this); };

    QString name;
    QString hash;
    int parent;
    bool shared;
    QString repo_file;
    QString files_path;
    QString external_db_path;

    /**
     * Returns number of bibliographical entries in this shelf
     */
    int getEntriesNum() const { return entriesNum; }

    int getID() const { return id; }
    void setID(const int id) { this->id = id; }

    void setHash();

    friend bool operator==(const ShelfEntry& lhs, const ShelfEntry& rhs);
    friend bool operator!=(const ShelfEntry& lhs, const ShelfEntry& rhs);

private:
    int id;
    int entriesNum;
};


Q_DECLARE_METATYPE(ShelfEntry)

#endif // SHELFENTRY_H
