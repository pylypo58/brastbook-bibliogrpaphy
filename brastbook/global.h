#ifndef GLOBAL_H
#define GLOBAL_H

#include <QString>
#include <QDebug>

#include "version.h"

const QString APP_NAME = "BraStBook";

//#define QDBG ( qDebug()   << "(" << (__FILE__) << ":" << (__LINE__) << ")" )
//#define QWRN ( qWarning() << "(" << (__FILE__) << ":" << (__LINE__) << ")" )
//#define qWarning() QMessageLogger(__FILE__, __LINE__, Q_FUNC_INFO).debug()

const QString ALL_SHELFS_SUBS = "---";
const QString FILE_EXTENSION = "shelf";
const QString INI_CHECKED_STATE = "_c";


#endif // GLOBAL_H
