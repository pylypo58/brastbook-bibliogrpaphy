#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "../model/model.h"
#include "shelfcontroller.h"
#include "import/importer.h"
#include "visitor/syncinfo.h"


class Controller : public QObject
{
    Q_OBJECT
public:
    Controller();
    ~Controller();

    static QString getFirstAuthorSurname(const QString authors);
    static QString getJournalAbbreviation(const QString journal, const int nchars = 3);
    static QString removeSymbols(QString str, bool leaveDash = true);

    void initComponents(QString path2database, QString columns, bool isNew = false);
    void importData(Importer & importer, const QString preset, int shelf_id = ShelfEntry::DEFAULT);

    Model * getModel() { return model; }
    ShelfController * getShelfController() { return shelfController; }

    QString getBibKey(const BibEntry & entry, const QString preset) const;
    QString getHash(const BibEntry & entry) const;

    void updateAssociatedDB(ShelfEntry & shelfEntry);
    void exportShelf2File(ShelfEntry &shelfEntry, QString &fileName, QString &header);
    void search(const QString & text2search, const QStringList & columns, const bool filter, const int shelf_id);
    const QList<BibEntry> searchSimilar(const BibEntry & entry);

    void replaceUTF(BibEntry * entry);

    QString replaceSymbolsByList(QString str);

private:
    Model * model;

    ShelfController * shelfController;
};

#endif // CONTROLLER_H
