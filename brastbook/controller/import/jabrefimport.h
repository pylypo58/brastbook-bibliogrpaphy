#ifndef JABREFIMPORT_H
#define JABREFIMPORT_H

#include "importer.h"

class JabrefImportTest;

class JabrefImport : public Importer
{
public:
    JabrefImport(QString fileName);

    void process();

private:
    bool getNextEntry(QString & data, int * pos, QString * entry);
    BibEntry parseEntry(QString & entry);

    friend class JabrefImportTest;
};

#endif // JABREFIMPORT_H
