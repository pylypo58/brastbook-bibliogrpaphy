#include "jabrefimport.h"

#include <exception>

#include <QDebug>
#include <QFile>

#include <QtGlobal>
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#include <QRegExp>
#else
#include <QRegularExpression>
#endif

JabrefImport::JabrefImport(QString fileName) : Importer(fileName)
{
}

void JabrefImport::process() {
    QFile file(this->identifier);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        throw std::runtime_error(QObject::tr("Cannot process '%1'").arg(this->identifier).toStdString());
    }
    QTextStream in(&file);
    QString sdata = in.readAll();

    QString entry;
    int pos = 0;
    int importednum = 0;
    while (getNextEntry(sdata, &pos, &entry)) {
        BibEntry e = parseEntry(entry);
        if (e.getID() != BibEntry::NOT_EXISTS) {
            entries.append(e);
            importednum ++;
            if (importednum % 100 == 0) {
                qWarning() << QObject::tr("Jabref/BibTeX Import: %1 entries are imported").arg(importednum);
            }
        }
    }

    this->isProcessed = true;
}

bool JabrefImport::getNextEntry(QString &data, int * pos, QString * entry) {
    int pos1 = data.indexOf('@', *pos);
    if (pos1 < 0) {
        return false;
    }

    int pos2 = data.indexOf('{', pos1);
    // find respective }
    int openclosed = 1;
    int pos3 = pos2 + 1;
    while (pos3 < data.length() && openclosed > 0) {
        if (data[pos3] == '{') {
            openclosed ++;
        }
        else if (data[pos3] == '}') {
            openclosed --;
        }
        pos3 ++;
    }
    *entry = data.mid(pos1, pos3 - pos1);
    *pos = pos3;
    return true;
}

BibEntry JabrefImport::parseEntry(QString &entry) {
    BibEntry be;
    int pos = entry.indexOf('{');
    QString entrytype = entry.mid(1, pos - 1).trimmed().toLower();
    if (entrytype == "article") {
        be.setEntryType(BibEntry::BET_ARTICLE);
    } else if (entrytype == "book") {
        be.setEntryType(BibEntry::BET_BOOK);
    } else if (entrytype == "booklet") {
        be.setEntryType(BibEntry::BET_BOOKLET);
    } else if (entrytype == "conference") {
        be.setEntryType(BibEntry::BET_CONFERENCE);
    } else if (entrytype == "inbook") {
        be.setEntryType(BibEntry::BET_INBOOK);
    } else if (entrytype == "incollection") {
        be.setEntryType(BibEntry::BET_INCOLLECTION);
    } else if (entrytype == "inproceedings") {
        be.setEntryType(BibEntry::BET_INPROCEEDINGS);
    } else if (entrytype == "manual") {
        be.setEntryType(BibEntry::BET_MANUAL);
    } else if (entrytype == "masterthesis") {
        be.setEntryType(BibEntry::BET_MASTERTHESIS);
    } else if (entrytype == "misc") {
        be.setEntryType(BibEntry::BET_MISC);
    } else if (entrytype == "phdthesis") {
        be.setEntryType(BibEntry::BET_PHDTHESIS);
    } else if (entrytype == "proceedings") {
        be.setEntryType(BibEntry::BET_PROCEEDINGS);
    } else if (entrytype == "techreport") {
        be.setEntryType(BibEntry::BET_TECHREPORT);
    } else if (entrytype == "unpublished") {
        be.setEntryType(BibEntry::BET_UNPUBLISHED);
    }
    pos += 1;
    int pos2 = entry.indexOf(',', pos);
    be.setField(BibEntry::BIBKEY, entry.mid(pos, pos2 - pos).trimmed());
    pos = pos2 + 1;

    int eqpos = entry.indexOf('=', pos);
    if (eqpos < 0) {
        qWarning() << "Something wrong with the entry";
        be.setID(BibEntry::NOT_EXISTS);
        return be;
    }
    do {
        QString key = entry.mid(pos, eqpos - pos).trimmed().toLower();
        QString val;

        int commapos = entry.indexOf(',', pos);
        int bracepos = entry.indexOf('{', pos);
        int quotepos = entry.indexOf('"', pos);

        if (commapos < 0) {
            commapos = entry.length() + 1;
        }
        if (bracepos < 0) {
            bracepos = entry.length() + 1;
        }
        if (quotepos < 0) {
            quotepos = entry.length() + 1;
        }

        if (commapos < bracepos && commapos < quotepos) {
            // key = value ,
            val = entry.mid(eqpos + 1, commapos - eqpos - 1).trimmed();
            pos = commapos + 1;
        } else if (quotepos < commapos && quotepos < bracepos) {
            // key = " value " ,
            int pos3;
            pos3 = quotepos + 1;
            while (pos3 < entry.length()) {
                if (entry[pos3] == '"') {
                    if (entry[pos3-1] == '\\') {
                        continue;
                    } else {
                        break;
                    }
                }
                pos3 ++;
            }
            val = entry.mid(quotepos + 1, pos3 - quotepos - 1).trimmed();
            pos = entry.indexOf(',', pos3);
            if (pos >= 0) {
                pos ++;
            } else {
                pos = entry.length();
            }
        } else if (bracepos < quotepos && bracepos < commapos) {
            // key = { value },
            // find respective }
            int openclosed = 1;
            int pos3;
            pos3 = bracepos + 1;
            while (pos3 < entry.length() && openclosed > 0) {
                if (entry[pos3] == '{') {
                    openclosed ++;
                }
                else if (entry[pos3] == '}') {
                    openclosed --;
                }
                pos3 ++;
            }
            val = entry.mid(bracepos + 1, pos3 - bracepos - 2).trimmed();
            pos = entry.indexOf(',', pos3);
            if (pos >= 0) {
                pos ++;
            } else {
                pos = entry.length();
            }
        } else {
            // key = value -- last entry
            pos = entry.indexOf('}', pos);
            val = entry.mid(eqpos + 1, pos - eqpos - 2).trimmed();
            pos ++;
        }

        if (key == "address") {
            be.setField(BibEntry::ADDRESS, val);
        } else if (key == "annote") {
            be.setField(BibEntry::ANNOTE, val);
        } else if (key == "author") {
            be.setField(BibEntry::AUTHOR, val);
        } else if (key == "booktitle") {
            be.setField(BibEntry::BOOKTITLE, val);
        } else if (key == "chapter") {
            be.setField(BibEntry::CHAPTER, val);
        } else if (key == "crossref") {
            be.setField(BibEntry::CROSSREF, val);
        } else if (key == "edition") {
            be.setField(BibEntry::EDITION, val);
        } else if (key == "editor") {
            be.setField(BibEntry::EDITOR, val);
        } else if (key == "howpublished") {
            be.setField(BibEntry::HOWPUBLISHED, val);
        } else if (key == "journal") {
            be.setField(BibEntry::JOURNAL, val);
        } else if (key == "institution") {
            be.setField(BibEntry::INSTITUTION, val);
        } else if (key == "key") {
            be.setField(BibEntry::KEY, val);
        } else if (key == "month") {
            be.setField(BibEntry::MONTH, val);
        } else if (key == "note") {
            be.setField(BibEntry::NOTE, val);
        } else if (key == "number") {
            be.setField(BibEntry::NUMBER, val);
        } else if (key == "issue") {
            be.setField(BibEntry::NUMBER, val);
        } else if (key == "organization") {
            be.setField(BibEntry::ORGANIZATION, val);
        } else if (key == "pages") {
            be.setField(BibEntry::PAGES, val);
        } else if (key == "publisher") {
            be.setField(BibEntry::PUBLISHER, val);
        } else if (key == "school") {
            be.setField(BibEntry::SCHOOL, val);
        } else if (key == "series") {
            be.setField(BibEntry::SERIES, val);
        } else if (key == "title") {
            be.setField(BibEntry::TITLE, val);
        } else if (key == "type") {
            be.setField(BibEntry::TYPE, val);
        } else if (key == "volume") {
            be.setField(BibEntry::VOLUME, val);
        } else if (key == "year") {
            be.setField(BibEntry::YEAR, val);
        } else if (key == "doi") {
            be.setField(BibEntry::DOI, val);
        } else if (key == "issn") {
            be.setField(BibEntry::ISSN, val);
        } else if (key == "isbn") {
            be.setField(BibEntry::ISBN, val);
        } else if (key == "url") {
            be.setField(BibEntry::URL, val);
        } else if (key == "date") {
            be.setField(BibEntry::DATE, val);
        } else if (key == "timestamp") {
            be.setField(BibEntry::TIMESTAMP, val);
        } else if (key == "language") {
            be.setField(BibEntry::LANGUAGE, val);
        } else if (key == "owner") {
            be.setField(BibEntry::OWNER, val);
//        } else if (key == "editedby") {
//            be.setField(BibEntry::EDITEDBY, val);
//        } else if (key == "editedat") {
//            be.setField(BibEntry::EDITEDAT, val);
        } else if (key == "abstract") {
            be.setField(BibEntry::ABSTRACT, val);
        } else if (key == "groups") {
            QStringList textNamedGroups = val.split(",");
            for (int i = 0; i < textNamedGroups.length(); i ++) {
                textNamedGroups[i] = textNamedGroups[i].trimmed();
            }
            be.setTextNamedGroups(textNamedGroups);
        } else if (key == "file") {
            be.clearFiles();
            QStringList files = val.split(";");
            for (int i = 0; i < files.length(); i ++) {
                QStringList tmp = files[i].split(":");
                if (tmp.length() == 3) {
                    // the string structure is :path/to/file.pdf:MIMETYPE
                    files[i] = tmp[1];
                } else if (tmp.length() == 4) {
                    files[i] = tmp[1];
                } else {
                    qWarning() << "Unexpected structure of the 'file' field in the entry " << be.getField(BibEntry::BIBKEY);
                }
                be.addFile(files[i]);
            }
        };

        eqpos = entry.indexOf('=', pos);
        if (eqpos < 0) {
            eqpos = entry.length() + 1;
        }
    } while (pos < entry.length() && eqpos < entry.length());

    return be;
}
