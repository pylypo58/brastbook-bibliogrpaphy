#include <exception>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QUrlQuery>
#include "doiimport.h"

DOIImport::DOIImport(QString doi, QString email) :
    Importer(doi)
  , email(email)
{

}

void DOIImport::process() {
    QNetworkAccessManager nam;
    connect(&nam, SIGNAL(finished(QNetworkReply *)), this, SLOT(getReply(QNetworkReply *)));
    // https://api.crossref.org/works/10.1038/s41586-018-0770-2?mailto=o.pylypovskyi@gmail.com
    // https://api.crossref.org/works?filter=doi:10.1038/s41586-018-0770-2&select=DOI,title,author&mailto=o.pylypovskyi@gmail.com
    QNetworkRequest rec(QUrl(QString("https://api.crossref.org/works/%1?mailto=%2").arg(identifier).arg(email)));
    reply = nam.get(rec);
    loop.exec();
}

void DOIImport::processEntries(QString str) {
    QJsonDocument doc = QJsonDocument::fromJson(str.toUtf8());
    if (doc.isObject()) {
        QJsonObject jobj = doc.object();
        if (!jobj.contains("status") && !jobj.contains("message-type")) {
            throw std::runtime_error(QObject::tr("Cannot parse Crossref response!").toStdString());
        }
        if (jobj.value("status").toString().toLower() == "ok" && jobj.value("message-type").toString().toLower() == "work") {
            BibEntry entry;
            QJsonObject msg = jobj["message"].toObject();
            if (msg.contains("publisher")) {
                entry.setField(BibEntry::PUBLISHER, msg["publisher"].toString());
            }
            if (msg.contains("volume")) {
                entry.setField(BibEntry::VOLUME, msg["volume"].toString());
            }
            if (msg.contains("issue")) {
                entry.setField(BibEntry::NUMBER, msg["issue"].toString());
            }
            if (msg.contains("publisher-location")) {
                entry.setField(BibEntry::ADDRESS, msg["publisher-location"].toString());
            }
            if (msg.contains("DOI")) {
                entry.setField(BibEntry::DOI, msg["DOI"].toString());
            }
            if (msg.contains("type")) {
                QString type = msg["type"].toString().toLower();
                if (type == "journal-article") {
                    entry.setEntryType(BibEntry::BET_ARTICLE);
                } else if (type == "book") {
                    entry.setEntryType(BibEntry::BET_BOOK);
                } else if (type == "reference-entry") {
                    entry.setEntryType(BibEntry::BET_INBOOK);
                } else if (type == "book-chapter") {
                    entry.setEntryType(BibEntry::BET_INBOOK);
                } else if (type == "proceedings-article") {
                    entry.setEntryType(BibEntry::BET_INPROCEEDINGS);
                } else {
                    // default
                    entry.setEntryType(BibEntry::BET_MISC);
                }
            }
            if (msg.contains("page")) {
                entry.setField(BibEntry::PAGES, msg["page"].toString());
            } else if (msg.contains("article-number")) {
                entry.setField(BibEntry::PAGES, msg["article-number"].toString());
            }
            if (msg.contains("language")) {
                entry.setField(BibEntry::LANGUAGE, msg["language"].toString());
            }
            if (msg.contains("URL")) {
                entry.setField(BibEntry::URL, msg["URL"].toString());
            }
            if (msg.contains("title")) {
                if (msg["title"].isArray()) {
                    QJsonArray tmp = msg["title"].toArray();
                    if (tmp.count() > 0) {
                        entry.setField(BibEntry::TITLE, tmp.at(0).toString());
                    }
                }
            }
            if (msg.contains("author")) {
                if (msg["author"].isArray()) {
                    QString author = "";
                    QJsonArray tmp = msg["author"].toArray();
                    for (int i = 0; i < tmp.count(); i ++) {
                        QJsonValue auv = tmp.at(i);
                        if (auv.isObject()) {
                            QJsonObject auo = auv.toObject();
                            if (auo.contains("family") && auo.contains("given")) {
                                if (author.length() > 0) {
                                    author += " and ";
                                }
                                author += auo["family"].toString() + ", " + auo["given"].toString();
                            }
                        }

                    }
                    entry.setField(BibEntry::AUTHOR, author);
                }
            }
            if (msg.contains("editor")) {
                if (msg["editor"].isArray()) {
                    QString editor = "";
                    QJsonArray tmp = msg["editor"].toArray();
                    for (int i = 0; i < tmp.count(); i ++) {
                        QJsonValue auv = tmp.at(i);
                        if (auv.isObject()) {
                            QJsonObject auo = auv.toObject();
                            if (auo.contains("family") && auo.contains("given")) {
                                if (editor.length() > 0) {
                                    editor += " and ";
                                }
                                editor += auo["family"].toString() + ", " + auo["given"].toString();
                            }
                        }
                    }
                    entry.setField(BibEntry::EDITOR, editor);
                }
            }
            if (msg.contains("container-title")) {
                if (msg["container-title"].isArray()) {
                    QJsonArray tmp = msg["container-title"].toArray();
                    if (tmp.count() > 0) {                        
                        if (entry.getEntryType() == BibEntry::BET_INBOOK) {
                            entry.setField(BibEntry::BOOKTITLE, tmp.at(0).toString());
                        } else if (entry.getEntryType() == BibEntry::BET_INBOOK) {
                            entry.setField(BibEntry::BOOKTITLE, tmp.at(0).toString());
                        } else if (entry.getEntryType() == BibEntry::BET_INPROCEEDINGS) {
                            entry.setField(BibEntry::BOOKTITLE, tmp.at(0).toString());
                        } else {
                            // default
                            entry.setField(BibEntry::JOURNAL, tmp.at(0).toString());
                        }
                    }
                }
            }
            if (msg.contains("ISBN")) {
                if (msg["ISSN"].isArray()) {
                    QJsonArray tmp = msg["ISBN"].toArray();
                    if (tmp.count() > 0) {
                        entry.setField(BibEntry::ISBN, tmp.at(0).toString());
                    }
                }
            }
            if (msg.contains("ISSN")) {
                if (msg["ISSN"].isArray()) {
                    QJsonArray tmp = msg["ISSN"].toArray();
                    if (tmp.count() > 0) {
                        entry.setField(BibEntry::ISSN, tmp.at(0).toString());
                    }
                }
            }
            if (msg.contains("published-print")) {
                if (msg["published-print"].isObject()) {
                    QJsonObject tmp1 = msg["published-print"].toObject();
                    if (tmp1.contains("date-parts") && tmp1["date-parts"].isArray()) {
                        QJsonArray tmp2 = tmp1["date-parts"].toArray();
                        if (tmp2.count() > 0) {
                            if (tmp2.at(0).isArray()) {
                                QString date = "";
                                QJsonArray tmp3 = tmp2.at(0).toArray();
                                qWarning() << tmp3.at(0).toDouble();
                                if (tmp3.count() > 0) {
                                    entry.setField(BibEntry::YEAR, QString::number(tmp3.at(0).toDouble()));
                                    date = QString::number(tmp3.at(0).toDouble());
                                }
                                if (tmp3.count() > 1) {
                                    date += "-" + QString::number(tmp3.at(1).toDouble());
                                }
                                if (tmp3.count() > 2) {
                                    date += "-" + QString::number(tmp3.at(2).toDouble());
                                }
                                entry.setField(BibEntry::DATE, date);
                            }
                        }
                    }
                }
            } else if (msg.contains("published-online")) {
                if (msg["published-online"].isObject()) {
                    QJsonObject tmp1 = msg["published-online"].toObject();
                    if (tmp1.contains("date-parts") && tmp1["date-parts"].isArray()) {
                        QJsonArray tmp2 = tmp1["date-parts"].toArray();
                        if (tmp2.count() > 0) {
                            if (tmp2.at(0).isArray()) {
                                QString date = "";
                                QJsonArray tmp3 = tmp2.at(0).toArray();
                                qWarning() << tmp3.at(0).toDouble();
                                if (tmp3.count() > 0) {
                                    entry.setField(BibEntry::YEAR, QString::number(tmp3.at(0).toDouble()));
                                    date = QString::number(tmp3.at(0).toDouble());
                                }
                                if (tmp3.count() > 1) {
                                    date += "-" + QString::number(tmp3.at(1).toDouble());
                                }
                                if (tmp3.count() > 2) {
                                    date += "-" + QString::number(tmp3.at(2).toDouble());
                                }
                                entry.setField(BibEntry::DATE, date);
                            }
                        }
                    }
                }
            } else if (msg.contains("published")) {
                if (msg["published"].isObject()) {
                    QJsonObject tmp1 = msg["published"].toObject();
                    if (tmp1.contains("date-parts") && tmp1["date-parts"].isArray()) {
                        QJsonArray tmp2 = tmp1["date-parts"].toArray();
                        if (tmp2.count() > 0) {
                            if (tmp2.at(0).isArray()) {
                                QString date = "";
                                QJsonArray tmp3 = tmp2.at(0).toArray();
                                if (tmp3.count() > 0) {
                                    entry.setField(BibEntry::YEAR, QString::number(tmp3.at(0).toDouble()));
                                    date = QString::number(tmp3.at(0).toDouble());
                                }
                                if (tmp3.count() > 1) {
                                    date += "-" + QString::number(tmp3.at(1).toDouble());
                                }
                                if (tmp3.count() > 2) {
                                    date += "-" + QString::number(tmp3.at(2).toDouble());
                                }
                                entry.setField(BibEntry::DATE, date);
                            }
                        }
                    }
                }
            }

            entries.append(entry);
        } else {
            throw std::runtime_error(QObject::tr("Cannot parse Crossref result!").toStdString());
        }
    }
}

void DOIImport::getReply(QNetworkReply * reply) {
    if (reply && reply->error() == QNetworkReply::NoError) {

        processEntries(reply->readAll());

        this->isProcessed = true;
        loop.exit();
        reply->deleteLater();
        emit responseIsObtained(true);
    } else {
        this->isProcessed = false;
        loop.exit();
        if (reply) {
            reply->deleteLater();
        }
        emit responseIsObtained(false);
    }
}
