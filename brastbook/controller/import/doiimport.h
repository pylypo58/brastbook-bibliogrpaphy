#ifndef DOIIMPORT_H
#define DOIIMPORT_H

#include <QWidget>
#include <QtNetwork/QNetworkAccessManager>
#include <QNetworkReply>
#include <QEventLoop>

#include "importer.h"

class DOIImport : public Importer
{
    Q_OBJECT
public:
    DOIImport(QString doi, QString email);

    void process();

private slots:
    void getReply(QNetworkReply * reply);

private:
    void processEntries(QString str);
    QString email;

    QNetworkReply * reply;    
    QEventLoop loop;
};

#endif // DOIIMPORT_H
