#include <exception>

#include <QDebug>
#include <QFile>
#include <QString>
#include <QtGlobal>
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#include <QRegExp>
#else
#include <QRegularExpression>
#endif

#include "risimport.h"

RISImport::RISImport(QString fileName) : Importer(fileName)
{

}

void RISImport::process() {
    QFile file(this->identifier);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        throw std::runtime_error(QObject::tr("Cannot process '%1'").arg(this->identifier).toStdString());
    }
    QTextStream in(&file);
    QString sdata = in.readAll();

    QString entry;
    int pos = 0;
    int importednum = 0;
    while (getNextEntry(sdata, &pos, &entry)) {
        bool flag;
        BibEntry e = parseEntry(entry, &flag);
        if (flag) {
            entries.append(e);
            importednum ++;
            if (importednum % 100 == 0) {
                qWarning() << QObject::tr("RIS Import: %1 entries are imported").arg(importednum);
            }
        }
    }

    this->isProcessed = true;
}

bool RISImport::getNextEntry(QString & data, int * pos, QString * entry) {
    int pos1 = data.indexOf("TY", *pos);
    if (pos1 < 0) {
        return false;
    }

    int pos2 = data.indexOf("ER", pos1 + 1);
    if (pos2 < 0) {
        return false;
    }

    *entry = data.mid(pos1, pos2 - pos1);
    *pos = pos2;
    return true;
}

BibEntry RISImport::parseEntry(QString & entry, bool * flag) {
    BibEntry be;

    QString authors = "";
    QString pages = "";

    int pos = 0;
    while (pos >= 0 && pos < entry.length()) {
        int retpos = entry.indexOf('\n', pos);
        QString substr = entry.mid(pos, retpos - pos).simplified().trimmed();
        pos = retpos + 1;

        QString field = substr.mid(0, 2).toUpper();
        int dashpos = substr.indexOf('-');
        QString value = substr.mid(dashpos + 1).trimmed();
        if (field == "TY") {

        } else if (field == "ER") {

        } else if (field == "AU") {
            if (authors.length() > 0) {
                authors += " and ";
            }
            authors += value;
        } else if (field == "PY") {
            be.setField(BibEntry::YEAR, value);
        } else if (field == "DA") {
            be.setField(BibEntry::DATE, value);
        } else if (field == "TI") {
            be.setField(BibEntry::TITLE, value);
        } else if (field == "JO") {
            be.setField(BibEntry::JOURNAL, value);
        } else if (field == "SP") {
            pages = value + pages;
        } else if (field == "EP") {
            pages += "-" + value;
        } else if (field == "VL") {
            be.setField(BibEntry::VOLUME, value);
        } else if (field == "IS") {
            be.setField(BibEntry::NUMBER, value);
        } else if (field == "AB") {
            be.setField(BibEntry::ABSTRACT, value);
        } else if (field == "SN") {
            be.setField(BibEntry::ISSN, value);
        } else if (field == "UR") {
            be.setField(BibEntry::URL, value);
        } else if (field == "DO") {
            be.setField(BibEntry::DOI, value);
        } else if (field == "ID") {
            be.setField(BibEntry::BIBKEY, value);
        } else if (field == "ED") {
            be.setField(BibEntry::EDITOR, value);
        } else if (field == "ET") {
            be.setField(BibEntry::EDITION, value);
        } else if (field == "LA") {
            be.setField(BibEntry::LANGUAGE, value);
//        } else if (field == "M3") {
//            be.setField(BibEntry::YEAR entrytype, value);
        } else if (field == "PB") {
            be.setField(BibEntry::PUBLISHER, value);
        } else if (field == "PP") {
            be.setField(BibEntry::ADDRESS, value);
        }
    }

    be.setField(BibEntry::AUTHOR, authors);
    be.setField(BibEntry::PAGES, pages);
    be.setEntryType(BibEntry::BET_ARTICLE);
    *flag = true;
    return be;
}
