#ifndef RISIMPORT_H
#define RISIMPORT_H

#include "importer.h"


class RISImport : public Importer
{
public:
    RISImport(QString fileName);

    void process();

private:
    bool getNextEntry(QString & data, int * pos, QString * entry);
    BibEntry parseEntry(QString & entry, bool * flag);
};

#endif // RISIMPORT_H
