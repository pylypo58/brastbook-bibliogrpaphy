#ifndef IMPORTER_H
#define IMPORTER_H

#include <QObject>
#include <QString>
#include <QList>

#include "../../model/bibentry.h"

class Importer : public QObject
{
    Q_OBJECT
public:
    Importer(QString identifier);
    QList<BibEntry> & getProcessed();

    virtual void process() = 0;

    bool isFinished() { return isProcessed; }

signals:
    void responseIsObtained(bool result);

protected:
    QString identifier;
    bool isProcessed;
    QList<BibEntry> entries;
};

#endif // IMPORTER_H
