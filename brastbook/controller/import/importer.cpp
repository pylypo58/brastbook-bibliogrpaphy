#include <QObject>
#include <exception>
#include "importer.h"

Importer::Importer(QString fileName):
    identifier(fileName)
  , isProcessed(false)
{

}


QList<BibEntry> & Importer::getProcessed() {
    if (isProcessed) {
        return entries;
    }
    else {
        throw std::runtime_error(QObject::tr("Calling of 'getParsed' before 'parse'!").toStdString());
    }
};
