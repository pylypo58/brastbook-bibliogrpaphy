#include <QFile>
#include <exception>
#include <iostream>

#include "exportbibtex.h"
#include "../../model/bibentry.h"
#include "bibentrytobibtex.h"

ExportBibTeX::ExportBibTeX(Model * model, QString fileName, QString header) :
    model(model)
  , fileName(fileName)
  , header(header)
  , exportFiles(false)
  , exportOwner(false)
  , exportTimestamp(false)
  , makeNumberIssue(false)
  , makeDOI2URL(false)
{

}

void ExportBibTeX::processShelf(const ShelfEntry &entry) {
    QList<BibEntry> bibEntries = model->getEntriesAtShelf(entry.getID());

    QFile fid(fileName);

    if (!fid.open(QIODevice::WriteOnly | QIODevice::Text)) {
        throw std::runtime_error(QObject::tr("Cannot open external library '%1' for writing!").arg(fileName).toStdString());
    }

    QTextStream out(&fid);

    out << "% Created automatically by BraStBook" << Qt::endl;
    out << "% Any manual changes in this file could be overwritten!" << Qt::endl << Qt::endl;

    BibEntryToBibTeX bib2texconverter;
    bib2texconverter.setDOI2URL(makeDOI2URL);
    bib2texconverter.setExportOwner(exportOwner);
    bib2texconverter.setExportTimestamp(exportTimestamp);
    bib2texconverter.setExportAbstract(exportAbstract);
    bib2texconverter.setExprortFiles(exportFiles);
    bib2texconverter.setNumberIssue(makeNumberIssue);
    bib2texconverter.setExportLanguage(exportLanguage);

    if (header.length() > 0) {
        QString header1 = "% " + header;
        header1.replace("\n", "\n% ");
        out << header1 << Qt::endl << Qt::endl;
    }

    for (int i = 0; i < bibEntries.length(); i ++) {
        bibEntries[i].accept(bib2texconverter);
        out << bib2texconverter.toString();
    }

    fid.close();
}
