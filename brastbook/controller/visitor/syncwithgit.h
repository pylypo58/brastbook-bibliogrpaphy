#ifndef SYNCWITHGIT_H
#define SYNCWITHGIT_H

#include <QString>
#include <QHash>
#include <QProgressDialog>

#include "../../model/visitor.h"
#include "../../model/model.h"
#include "syncinfo.h"

class SyncWithGit : public Visitor
{
public:
    enum PullResult {
        PR_UP_TO_DATE,
        PR_UPDATED
    };

    enum Status {
        SYNC_UP_TO_DATE,
        SYNC_UPDATED,
        SYNC_FAILED
    };

    SyncWithGit(Model * model, ShelfEntry currentShelfInView, const SyncInfo & syncInfo);
    ~SyncWithGit();

    virtual void processBibEntry(const BibEntry & entry) {};
    virtual void processShelf(const ShelfEntry & entry);

    Status getStatus() { return status; }

private:
    QString bibEntry2Base64(const BibEntry & entry);
    BibEntry base642bibEntry(const QString str);
    QHash<QString, BibEntry> list2hash(const QList<BibEntry> & list);

    void writeGitFile();
    QHash<QString, BibEntry> readGitFile();
    void compare();

    void commitRepo();
    PullResult pullRepo();
    bool pushRepo();
    void findRemoved();

    Model * model;
    ShelfEntry currentShelfInView;
    ShelfEntry currentShelfProcessing;
    QString fileName;

    SyncInfo syncInfo;

    QHash<QString, BibEntry> localBibEntries;
    QHash<QString, BibEntry> oldRemoteBibEntries;
    QHash<QString, BibEntry> remoteBibEntries;
    QHash<QString, BibEntry> locallyRemovedBibEntries;
    QHash<QString, BibEntry> remotelyRemovedBibEntries;

    Status status;
    QProgressDialog * progress;
};

#endif // SYNCWITHGIT_H
