#ifndef BIBENTRYTOBIBTEX_H
#define BIBENTRYTOBIBTEX_H

#include <QString>

#include "../../model/visitor.h"
#include "../../model/bibentry.h"

class BibEntryToBibTeX : public Visitor
{
public:
    BibEntryToBibTeX();

    virtual void processBibEntry(const BibEntry & entry);
    virtual void processShelf(const ShelfEntry & entry) {};

    void setExprortFiles(bool b) { exportFiles = b; }
    void setExportOwner(bool b) { exportOwner = b; }
    void setExportTimestamp(bool b) { exportTimestamp = b; }
    void setExportAbstract(bool b) { exportAbstract = b; }
    void setNumberIssue(bool b) { makeNumberIssue = b; }
    void setDOI2URL(bool b) { makeDOI2URL = b; }
    void setExportLanguage(bool b) { exportLanguage = b; }

    QString toString() { return str; }

private:
    QString str;

    bool exportFiles;
    bool exportOwner;
    bool exportTimestamp;
    bool exportAbstract;
    bool makeNumberIssue;
    bool makeDOI2URL;
    bool exportLanguage;
};

#endif // BIBENTRYTOBIBTEX_H
