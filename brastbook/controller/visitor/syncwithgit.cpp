#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QProcess>
#include <QDateTime>
#include <QTextStream>
#include <QMessageBox>

#include <exception>
#include <cstdlib>

#include "syncwithgit.h"
#include "../../view/compareentries/compareentriesdialog.h"

SyncWithGit::SyncWithGit(Model * model, ShelfEntry currentShelfInView, const SyncInfo & syncInfo) :
    model(model)
  , currentShelfInView(currentShelfInView)
  , syncInfo(syncInfo)
  , progress(nullptr)
{
    status = Status::SYNC_FAILED;
}

SyncWithGit::~SyncWithGit() {
    if (progress) {
        delete progress;
    }
}

void SyncWithGit::processShelf(const ShelfEntry &entry) {
    currentShelfProcessing = entry;
    if (currentShelfProcessing.shared) {
        fileName = QDir::cleanPath(DBVars::getInstance().getFilePath() + QDir::separator() + entry.repo_file);
    } else {
        throw std::runtime_error(QObject::tr("The shelf '%1' is not shared!").arg(currentShelfProcessing.name).toStdString());
    }

    progress = new QProgressDialog(QObject::tr("Syncing with remote..."), QObject::tr("Abort"), 0, 5);
    progress->setWindowModality(Qt::WindowModal);
    progress->show();
    if (!QFile::exists(fileName)) {
        // initialization
        localBibEntries = list2hash(model->getEntriesAtShelf(currentShelfProcessing.getID())); progress->setValue(1);

        writeGitFile(); progress->setValue(2);
        commitRepo(); progress->setValue(3);
        pushRepo(); progress->setValue(4);
    } else {
        // work with existing repo
        do {
            progress->setMaximum(5); progress->setValue(1);
            localBibEntries = list2hash(model->getEntriesAtShelf(currentShelfProcessing.getID())); progress->setValue(2);
            oldRemoteBibEntries = readGitFile();
            pullRepo();
            remoteBibEntries = readGitFile();
            findRemoved();
            progress->setMaximum(remoteBibEntries.size() + localBibEntries.size());
            compare();
            writeGitFile();
            commitRepo();
            // try to push (if not, pull, read again, compare...)
        } while (!pushRepo());
    }

    status = Status::SYNC_UPDATED;
    progress->setValue(progress->maximum());
}

void SyncWithGit::findRemoved() {
    if (localBibEntries.size() == 0) {
        // no entries at the local side, this is a fresh sync
        return;
    }

    // checking for the locally removed entries
    foreach (auto entry, oldRemoteBibEntries) {
        QString hash = entry.getField(BibEntry::HASH);
        if (!localBibEntries.contains(hash)) {
            // 'entry' has been removed at the local side
            // being present at the remote from the last sync

            if (remoteBibEntries.contains(hash)) {
                // entry still present remotely, removing
                remoteBibEntries.remove(hash);
            }

//            locallyRemovedBibEntries.insert(hash, entry);
        }
    }

    // checking for the remotely removed entries
    foreach (auto entry, oldRemoteBibEntries) {
        QString hash = entry.getField(BibEntry::HASH);
        if (!remoteBibEntries.contains(hash)) {
            // 'entry' has been removed at the remote side
            // being present at the remote from the last sync

            if (localBibEntries.contains(hash)) {
                // the remotely removed entry is still present locally
                if (QMessageBox::question(nullptr, QObject::tr("Removed entry"), QObject::tr("Entry with bibkey %1 is removed from the shelf '%2' remotely. Confirm?").arg(entry.getField(BibEntry::BIBKEY)).arg(currentShelfProcessing.name), QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes) {
                    BibEntry localEntry2remove = localBibEntries[hash];
                    localEntry2remove.removeShelfID(currentShelfProcessing.getID());
                    model->updateBibEntry(localEntry2remove, currentShelfInView);
                    localBibEntries.remove(hash);
                }
            } else {
                // the remotely removed entry is not found locally, do nothing
            }

//            remotelyRemovedBibEntries.insert(hash, entry);
        }
    }

}

QString SyncWithGit::bibEntry2Base64(const BibEntry &entry) {
    QString str;

    str = QString::number(entry.getEntryType()).toUtf8().toBase64();
    str += "," + entry.getField(BibEntry::HASH);

    for (int i = BibEntry::BIBKEY; i < BibEntry::COL_NUM; i ++) {
        str += "," + entry.getField(i).toUtf8().toBase64();
    }

    str += ",";
    const QStringList files = entry.getFiles();
    if (files.length() > 0) {
        str += files[0].toUtf8().toBase64();
    }
    for (int i = 1; i < files.length(); i ++) {
        str += "_" + files[i].toUtf8().toBase64();
    }

    return str;
}

BibEntry SyncWithGit::base642bibEntry(const QString str) {
    BibEntry entry;

    QStringList sfield = str.split(',');
    if (sfield.count() != BibEntry::COL_NUM) {
        throw std::runtime_error(QObject::tr("Internal error: Wrong structure of the git file!").toStdString());
    }
    entry.setField(BibEntry::HASH, sfield[1]);

    entry.setEntryType(QString(QByteArray::fromBase64(sfield[0].toLatin1())).toInt());
    for (int i = BibEntry::BIBKEY; i < BibEntry::COL_NUM; i ++) {
        entry.setField(i, QByteArray::fromBase64(sfield[2 + i - BibEntry::BIBKEY].toLatin1()));
    }

    QStringList files = sfield[BibEntry::COL_NUM-1].split('_');
    for (int i = 0; i < files.length(); i ++){
        entry.addFile(QByteArray::fromBase64(files[i].toLatin1()));
    }

    return entry;
}

void SyncWithGit::writeGitFile() {
    QFile fid(fileName);

    if (!fid.open(QIODevice::WriteOnly | QIODevice::Text)) {
        throw std::runtime_error(QObject::tr("Cannot open external library '%1' for writing!").arg(fileName).toStdString());
    }

    QTextStream out(&fid);

    foreach (auto entry, localBibEntries) {
        out << bibEntry2Base64(entry);
//        Qt::endl(out);
        out << "\n";
    }
}

QHash<QString, BibEntry> SyncWithGit::readGitFile() {
    QFile fid(fileName);

    if (!fid.open(QIODevice::ReadOnly | QIODevice::Text)) {
        throw std::runtime_error(QObject::tr("Cannot open external library '%1' for writing!").arg(fileName).toStdString());
    }

    QTextStream in(&fid);

    QHash<QString, BibEntry> bibentries;
    while (!in.atEnd()) {
        QString line = in.readLine();
        BibEntry entry = base642bibEntry(line);
        bibentries.insert(entry.getField(BibEntry::HASH), entry);
    }

    return bibentries;
}

void SyncWithGit::commitRepo() {
    QFileInfo fi(fileName);

    QProcess process;
    process.setProcessChannelMode(QProcess::MergedChannels);
    process.setWorkingDirectory(fi.dir().path());

    qWarning() << "@ Working directory: " << fi.dir().path();
    qWarning() << "@ Git command: " << syncInfo.command << " add *";
    process.start(syncInfo.command, QStringList() << "add" << "*");

    if (!process.waitForFinished()) {
        throw std::runtime_error(process.errorString().toStdString());
    }
    QTextStream(stdout) << "Git add:";
    QTextStream(stdout) << process.readAll();

    process.start(syncInfo.command, QStringList() << syncInfo.commit.split(' ') << "-m" << syncInfo.commitMsg);
//    process.start(syncInfo.command, QStringList() << syncInfo.commit.split(' ') << "-m" << "\"shelf update\"");

    if (!process.waitForFinished()) {
        throw std::runtime_error(process.errorString().toStdString());
    }
    QTextStream(stdout) << "Git commit:";
    QTextStream(stdout) << process.readAll();
}

SyncWithGit::PullResult SyncWithGit::pullRepo() {
    QFileInfo fi(fileName);

    QProcess process;
    process.setProcessChannelMode(QProcess::MergedChannels);
    process.setWorkingDirectory(fi.dir().path());

    qWarning() << "@ Working directory: " << fi.dir().path();
    qWarning() << "@ Git command: " << syncInfo.command << syncInfo.pull;
    process.start(syncInfo.command, syncInfo.pull.split(' '));

    if (!process.waitForFinished()) {
        throw std::runtime_error(process.errorString().toStdString());
    }
    QTextStream(stdout) << "Git pull:";
    QString result = process.readAll();
    QTextStream(stdout) << result;

    if (result.indexOf("up to date") == -1) {
        return PR_UPDATED;
    } else {
        return PR_UP_TO_DATE;
    }
}

bool SyncWithGit::pushRepo() {
    QFileInfo fi(fileName);

    QProcess process;
    process.setProcessChannelMode(QProcess::MergedChannels);
    process.setWorkingDirectory(fi.dir().path());

    qWarning() << "@ Working directory: " << fi.dir().path();
    qWarning() << "@ Git command: " << syncInfo.command << syncInfo.push;
    process.start(syncInfo.command, syncInfo.push.split(' '));

    if (!process.waitForFinished()) {
        throw std::runtime_error(process.errorString().toStdString());
    }

    QString result = process.readAll();
    QTextStream(stdout) << "Git push:";
    QTextStream(stdout) << result;

    if (result.indexOf("error") != -1) {
        return false;
    } else {
        return true;
    }
}

QHash<QString, BibEntry> SyncWithGit::list2hash(const QList<BibEntry> &list) {
    QHash<QString, BibEntry> hash;

    foreach (auto & element , list) {
        hash.insert(element.getField(BibEntry::HASH), element);
    }

    return hash;
}

void SyncWithGit::compare() {
//    progress.setWindowModality(Qt::WindowModal);

    int counter = 0;

//    // removing locally removed entries at the remote side
//    foreach (auto entry, locallyRemovedBibEntries) {
//        QString hash = entry.getField(BibEntry::HASH);
//        if (remoteBibEntries.contains(hash)) {
//            // entry still present remote, removing
//            remoteBibEntries.remove(hash);
//        }
//    }

//    // removing remotely removed entries at the local side
//    foreach (auto entry, remotelyRemovedBibEntries) {
//        QString hash = entry.getField(BibEntry::HASH);
//        if (localBibEntries.contains(hash)) {
//            if (QMessageBox::question(nullptr, QObject::tr("Removed entry"), QObject::tr("Entry with bibkey %1 is removed from the shelf '%2' remotely. Confirm?").arg(entry.getField(BibEntry::BIBKEY).arg(currentShelfProcessing.name)), QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes) {
//                BibEntry localEntry2remove = localBibEntries[hash];
//                localEntry2remove.removeShelfID(currentShelfProcessing.getID());
//                model->updateBibEntry(localEntry2remove, currentShelfInView);
//                localBibEntries.remove(hash);
//            }
//        }
//    }

//    // checking for removed entries at the remote side
//    foreach (auto entry, remoteBibEntries) {
//        counter ++;
//        QString hash = entry.getField(BibEntry::HASH);
//        if (!remoteBibEntries.contains(hash)) {
//            // 'entry' has been removed at the remote side
//            if (localBibEntries.contains(hash)) {
//                // if 'entry' is still present locally, ask for confirmation
//                if (QMessageBox::question(nullptr, QObject::tr("Removed entry"), QObject::tr("Entry with bibkey %1 is removed from the shelf '%2' remotely. Confirm?").arg(entry.getField(BibEntry::BIBKEY).arg(currentShelfProcessing.name)), QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes) {
//                    BibEntry entryLocal = localBibEntries[hash];
//                    entryLocal.removeShelfID(currentShelfProcessing.getID());
//                    model->updateBibEntry(entryLocal, currentShelfInView);
//                }
//            }
//        }
//        if (counter % 100 == 0) {
//            progress->setValue(counter);
//        }
//        if (progress->wasCanceled()) {
//            throw std::runtime_error("Syncing has been aborted!");
//        }
//    }

    CompareEntriesDialog::SyncAction saction = CompareEntriesDialog::NO_SYNC;

    // adding new entries
//    localBibEntries = list2hash(model->getEntriesAtShelf(currentShelfProcessing.getID()));
    foreach (auto entry, remoteBibEntries) {
        counter ++;
        entry.addShelfID(currentShelfProcessing.getID());

        BibEntry localEntry;
        try {
            QString hash = entry.getField(BibEntry::HASH);
            localEntry = model->getBibEntry(hash, false);
            int localEntryID = localEntry.getID();

            if (localEntryID == BibEntry::NOT_EXISTS) {
                if (!locallyRemovedBibEntries.contains(hash)) {
                    // local entry not found and it was not removed locally, adding
                    BibEntry copyEntry = entry;

                    copyEntry.addShelfID(currentShelfInView.getID());
                    copyEntry.setField(BibEntry::EDITEDBY, DBVars::getInstance().getOwner());
                    copyEntry.setField(BibEntry::EDITEDAT, QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss"));
                    model->addBibEntry(copyEntry, currentShelfInView.getID());
                }
            } else if (localEntry == entry) {
                // local entry is found and coincides with existing one
                // may be outside the shared shelf

//                const QList<int> localEntryShelfIDs = localEntry.getShelfIDs();
//                if (!localEntryShelfIDs.contains(currentShelfProcessing.getID())) {
//                    // local entry exists, just not in the current shelf
                    localEntry.addShelfID(currentShelfProcessing.getID());
                    model->updateBibEntry(localEntry, currentShelfInView);
//                }
            } else {
                // local entry found and it significantly differ from the existing one
                // should be compared

                if (saction == CompareEntriesDialog::NO_SYNC) {
                    CompareEntriesDialog compareentries(localEntry, entry, model, CompareEntriesDialog::LEAVE_TWO, CompareEntriesDialog::SHOW_SYNC_ACITONS);
                    compareentries.setTitles("Local", "Remote");
                    compareentries.exec();

                    if (compareentries.result() == QDialog::Accepted) {

                        saction = compareentries.getSyncAction();

                        if (saction == CompareEntriesDialog::NO_SYNC) {
                            // making merge
                            localEntry = compareentries.getChosenEntry();
                            localEntry.setID(localEntryID);
                            localEntry.addShelfID(currentShelfProcessing.getID());
                            model->updateBibEntry(localEntry, currentShelfInView);
                        }

                    } else {
                        // save both, i.e. add remote one
                        BibEntry copyEntry = entry;

                        copyEntry.addShelfID(currentShelfProcessing.getID());
                        copyEntry.setHash();
                        copyEntry.setField(BibEntry::EDITEDBY, DBVars::getInstance().getOwner());
                        copyEntry.setField(BibEntry::EDITEDAT, QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss"));
                        model->addBibEntry(copyEntry, currentShelfInView.getID());
                    }
                }

                if (saction == CompareEntriesDialog::TAKE_ALL_LOCAL) {
                    // the only local versions are requested, no merge
                } else if (saction == CompareEntriesDialog::TAKE_ALL_REMOTE) {
                    // use remote instead of local version
                    entry.setID(localEntry.getID());
                    foreach (auto groupId, localEntry.getShelfIDs()) {
                        entry.addShelfID(groupId);
                    }
                    model->updateBibEntry(entry, currentShelfInView);
                }
            }

        }  catch (...) {
        }

        if (counter % 100 == 0) {
            progress->setValue(counter);
        }
        if (progress->wasCanceled()) {
            throw std::runtime_error("Syncing has been aborted!");
        }
    }
    localBibEntries = list2hash(model->getEntriesAtShelf(currentShelfProcessing.getID()));
}
