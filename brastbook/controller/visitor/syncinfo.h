#ifndef SYNCINFO_H
#define SYNCINFO_H

#include <QString>

struct SyncInfo {
    QString command;
    QString commit;
    QString commitMsg;
    QString pull;
    QString push;
};

#endif // SYNCINFO_H
