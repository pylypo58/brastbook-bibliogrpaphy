#include <QFileInfo>
#include "bibentrytobibtex.h"

BibEntryToBibTeX::BibEntryToBibTeX() :
      exportFiles(false)
    , exportOwner(false)
    , exportTimestamp(false)
    , makeNumberIssue(false)
    , makeDOI2URL(false)
{

}

void BibEntryToBibTeX::processBibEntry(const BibEntry &entry) {
    str = "@";
    str += BibEntry::BIBENTRY_TYPE[entry.getEntryType()];
    str += "{" + entry.getField(BibEntry::BIBKEY) + ",\n";

    if (entry.getField(BibEntry::ADDRESS).length() > 0) {
        str += "address = {" + entry.getField(BibEntry::ADDRESS) + "},\n";
    }
    if (entry.getField(BibEntry::ANNOTE).length() > 0) {
        str += "annote = {" + entry.getField(BibEntry::ANNOTE) + "},\n";
    }
    if (entry.getField(BibEntry::AUTHOR).length() > 0) {
        str += "author = {" + entry.getField(BibEntry::AUTHOR) + "},\n";
    }
    if (entry.getField(BibEntry::BOOKTITLE).length() > 0) {
        str += "booktitle = {" + entry.getField(BibEntry::BOOKTITLE) + "},\n";
    }
    if (entry.getField(BibEntry::CHAPTER).length() > 0) {
        str += "chapter = {" + entry.getField(BibEntry::CHAPTER) + "},\n";
    }
    if (entry.getField(BibEntry::CROSSREF).length() > 0) {
        str += "crossref = {" + entry.getField(BibEntry::CROSSREF) + "},\n";
    }
    if (entry.getField(BibEntry::DATE).length() > 0) {
        str += "date = {" + entry.getField(BibEntry::DATE) + "},\n";
    }
    if (entry.getField(BibEntry::DOI).length() > 0) {
        str += "doi = {" + entry.getField(BibEntry::DOI) + "},\n";
    }
    if (entry.getField(BibEntry::EDITEDAT).length() > 0 && exportTimestamp) {
        str += "editedat = {" + entry.getField(BibEntry::EDITEDAT) + "},\n";
    }
    if (entry.getField(BibEntry::EDITEDBY).length() > 0 && exportOwner) {
        str += "editedby = {" + entry.getField(BibEntry::EDITEDBY) + "},\n";
    }
    if (entry.getField(BibEntry::EDITION).length() > 0) {
        str += "edition = {" + entry.getField(BibEntry::EDITION) + "},\n";
    }
    if (entry.getField(BibEntry::EDITOR).length() > 0) {
        str += "editor = {" + entry.getField(BibEntry::EDITOR) + "},\n";
    }
    if (entry.getField(BibEntry::HOWPUBLISHED).length() > 0) {
        str += "howpublished = {" + entry.getField(BibEntry::HOWPUBLISHED) + "},\n";
    }
    if (entry.getField(BibEntry::INSTITUTION).length() > 0) {
        str += "institution = {" + entry.getField(BibEntry::INSTITUTION) + "},\n";
    }
    if (entry.getField(BibEntry::ISBN).length() > 0) {
        str += "isbn = {" + entry.getField(BibEntry::ISBN) + "},\n";
    }
    if (entry.getField(BibEntry::ISSN).length() > 0) {
        str += "issn = {" + entry.getField(BibEntry::ISSN) + "},\n";
    }
    if (entry.getField(BibEntry::NUMBER).length() > 0 && makeNumberIssue) {
        str += "issue = {" + entry.getField(BibEntry::NUMBER) + "},\n";
    }
    if (entry.getField(BibEntry::JOURNAL).length() > 0) {
        str += "journal = {" + entry.getField(BibEntry::JOURNAL) + "},\n";
    }
//    if (entry.key.length() > 0) {
//        str += "key = {" + entry.key + "},\n";
//    }
    if (exportLanguage && entry.getField(BibEntry::LANGUAGE).length() > 0) {
        str += "language = {" + entry.getField(BibEntry::LANGUAGE) + "},\n";
    }
    if (entry.getField(BibEntry::MONTH).length() > 0) {
        str += "month = {" + entry.getField(BibEntry::MONTH) + "},\n";
    }
    if (entry.getField(BibEntry::NOTE).length() > 0) {
        str += "note = {" + entry.getField(BibEntry::NOTE) + "},\n";
    }
    if (entry.getField(BibEntry::NUMBER).length() > 0) {
        str += "number = {" + entry.getField(BibEntry::NUMBER) + "},\n";
    }
    if (entry.getField(BibEntry::ORGANIZATION).length() > 0) {
        str += "organization = {" + entry.getField(BibEntry::ORGANIZATION) + "},\n";
    }
    if (entry.getField(BibEntry::OWNER).length() > 0 && exportOwner) {
        str += "owner = {" + entry.getField(BibEntry::OWNER) + "},\n";
    }
    if (entry.getField(BibEntry::PAGES).length() > 0) {
        str += "pages = {" + entry.getField(BibEntry::PAGES) + "},\n";
    }
    if (entry.getField(BibEntry::PUBLISHER).length() > 0) {
        str += "publisher = {" + entry.getField(BibEntry::PUBLISHER) + "},\n";
    }
    if (entry.getField(BibEntry::SCHOOL).length() > 0) {
        str += "school = {" + entry.getField(BibEntry::SCHOOL) + "},\n";
    }
    if (entry.getField(BibEntry::SERIES).length() > 0) {
        str += "series = {" + entry.getField(BibEntry::SERIES) + "},\n";
    }
    if (entry.getField(BibEntry::TIMESTAMP).length() > 0 && exportTimestamp) {
        str += "timestamp = {" + entry.getField(BibEntry::TIMESTAMP) + "},\n";
    }
    if (entry.getField(BibEntry::TITLE).length() > 0) {
        str += "title = {" + entry.getField(BibEntry::TITLE) + "},\n";
    }
    if (entry.getField(BibEntry::TYPE).length() > 0) {
        str += "type = {" + entry.getField(BibEntry::TYPE) + "},\n";
    }
    if (entry.getField(BibEntry::URL).length() > 0) {
        str += "url = {" + entry.getField(BibEntry::URL) + "},\n";
    } else {
        if (entry.getField(BibEntry::DOI).length() > 0) {
            str += "url = {https://doi.org/" + entry.getField(BibEntry::DOI) + "},\n";
        }
    }
    if (entry.getField(BibEntry::VOLUME).length() > 0) {
        str += "volume = {" + entry.getField(BibEntry::VOLUME) + "},\n";
    }
    if (entry.getField(BibEntry::YEAR).length() > 0) {
        str += "year = {" + entry.getField(BibEntry::YEAR) + "},\n";
    }
    if (entry.getField(BibEntry::ABSTRACT).length() > 0 && exportAbstract) {
        str += "abstract = {" + entry.getField(BibEntry::ABSTRACT) + "},\n";
    }
    const QStringList files = entry.getFiles();
    if (exportFiles && files.length() > 0) {
        QFileInfo fi(files[0]);
        str += "file = {:" + files[0] + ":" + fi.suffix();
        for (int i = 1; i < files.length(); i ++) {
            fi.setFile(files[i]);
            str += ";:" + files[i] + ":" + fi.suffix();
        }
        str += "},\n";
    }

    str += "}\n\n";
}
