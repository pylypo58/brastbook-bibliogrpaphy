#include <QRegularExpression>
#include <QDateTime>
#include <QMessageBox>
#include <QFileInfo>
#include <QSettings>
#include <QCryptographicHash>
#include <QProgressDialog>
#include <QDir>

#include "controller.h"
#include "visitor/exportbibtex.h"
// #include "visitor/syncwithgit.h"
#include "textprocessing.h"

Controller::Controller() :
    model(nullptr)
  , shelfController(nullptr)
{

}

Controller::~Controller() {
    if (!model) {
        delete model;
        model = nullptr;
    }

    if (!shelfController) {
        delete shelfController;
        shelfController = nullptr;
    }
}

void Controller::initComponents(QString path2database, QString columns, bool isNew) {
    model = new Model(path2database, columns, isNew);

    shelfController = new ShelfController(model);
}

void Controller::importData(Importer &importer, const QString preset, int shelf_id) {


    importer.process();
    emit model->getBibEntriesModel()->layoutAboutToBeChanged();
    QList<BibEntry> entry = importer.getProcessed();

    QProgressDialog progress(tr("Importing entries..."), tr("Abort import"), 0, entry.length());
    progress.setWindowModality(Qt::WindowModal);

    for (int i = 0; i < entry.length(); i ++) {
        if (shelf_id != ShelfEntry::DEFAULT) {
            entry[i].addShelfID(shelf_id);
        }
        if (model->getDefaultShelfId() != model->getRootShelfId()) {
            entry[i].addShelfID(model->getDefaultShelfId());
        }
        entry[i].setField(BibEntry::BIBKEY, getBibKey(entry[i], preset));
        entry[i].setHash();
        if (entry[i].getField(BibEntry::TIMESTAMP).length() == 0) {
            entry[i].setField(BibEntry::TIMESTAMP, QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss"));
        }
        if (entry[i].getField(BibEntry::OWNER).length() == 0) {
            entry[i].setField(BibEntry::OWNER, DBVars::getInstance().getOwner());
        }


        replaceUTF(&(entry[i]));
        entry[i].setField(BibEntry::BOOKTITLE, replaceSymbolsByList(entry[i].getField(BibEntry::BOOKTITLE)));
        entry[i].setField(BibEntry::TITLE, replaceSymbolsByList(entry[i].getField(BibEntry::TITLE)));
        model->addBibEntry(entry[i]);
        if (i % 100 == 0) {
            progress.setValue(i);
        }
        if (progress.wasCanceled()) {
            break;
        }
    }

    shelfController->updateShelfView(shelf_id);
    model->updateBookcaseView();
    model->populateAllShelfs();
}

void Controller::replaceUTF(BibEntry * entry) {
    QSettings settings;
    settings.beginGroup("General");
    bool replaceUTF = settings.value("ReplaceUTF", true).toBool();
    settings.endGroup();

    if (replaceUTF) {
        for (int j = BibEntry::BIBKEY; j < BibEntry::COL_NUM; j ++) {
            entry->setField(j, TextProcessing::replaceUTFbyLaTeX(entry->getField(j)));
        }
    }
}

QString Controller::getBibKey(const BibEntry &entry, const QString preset) const {
    QString result = preset;

    QString authorSurname;
    if (entry.getField(BibEntry::AUTHOR).length() > 0) {
        authorSurname = Controller::getFirstAuthorSurname(entry.getField(BibEntry::AUTHOR));
    } else if (entry.getField(BibEntry::EDITOR).length() > 0) {
        authorSurname = Controller::getFirstAuthorSurname(entry.getField(BibEntry::EDITOR));
    } else {
        qWarning() << "No author or editor";
        authorSurname = "";
    }
    result.replace("{{author}}", authorSurname);
    result.replace("{{yyyy}}", entry.getField(BibEntry::YEAR));
    result.replace("{{yy}}", entry.getField(BibEntry::YEAR).right(2));
    if (entry.getField(BibEntry::JOURNAL).length() > 0) {
        result.replace("{{JRN}}", Controller::getJournalAbbreviation(entry.getField(BibEntry::JOURNAL)));
    } else if (entry.getField(BibEntry::BOOKTITLE).length() > 0) {
        result.replace("{{JRN}}", Controller::getJournalAbbreviation(entry.getField(BibEntry::BOOKTITLE)));
    } else {
        result.replace("{{JRN}}", "");
    }

    result = TextProcessing::replaceUTFbyLaTeX(result);
    result.replace("-", "");
    auto it = std::remove_if(result.begin(), result.end(), [](const QChar& c){ return !c.isLetterOrNumber();});
    result.chop(std::distance(it, result.end()));

    if (result == entry.getField(BibEntry::BIBKEY)) {
        return result;
    }

    if (!model->isBibKeyPresent(result)) {
        return result;
    }

    char suffix = 'a';
    result += suffix;
    while (model->isBibKeyPresent(result) && suffix <= 'q') {
        suffix += 1;
        result.replace(result.length() - 1, 1, suffix);
    }
    if (suffix > 'q') {
        suffix = 'a';
        result += suffix;
        while (model->isBibKeyPresent(result) && suffix <= 'q') {
            suffix += 1;
#if QT_VERSION < QT_VERSION_CHECK(6,0,0)
            result.replace(result.length() - 1, suffix);
#else
            result.replace(result.length() - 1, 1, suffix);
#endif
        }
    }
    if (suffix > 'q') {
        qWarning() << __FILE__ << ":" << __LINE__ << "Bibkey problem";
    }

    return result;

}

QString Controller::getFirstAuthorSurname(const QString authors) {
    QString author1 = Controller::removeSymbols(authors.simplified());
    int nand = author1.indexOf(" and ");
    if (nand > 0) {
        author1 = author1.left(nand).trimmed();
    } else {
        author1 = author1.trimmed();
    }

    int ncomma = author1.indexOf(',');
    if (ncomma != -1) {
        author1 = author1.left(ncomma);
    } else {
        author1 = author1.split(' ').last();
    }


//    int ncomma = author1.indexOf(',');
//    int nspace = author1.indexOf(' ');
//    if (ncomma < nspace && ncomma != -1) {
//        author1 = author1.left(ncomma);
//    } else {
//        nspace = author1.lastIndexOf(' ');
//        author1 = author1.right(author1.length() - nspace - 1);
//    }

    return author1;
}

QString Controller::getJournalAbbreviation(const QString journal, const int nchars) {
    QString abbrev = "";
    QStringList journalWords = Controller::removeSymbols(journal.simplified()).split(' ');
    if (journalWords.length() == 1) {
        abbrev = journal.left(nchars);
    } else {
        for (int i = 0; i < journalWords.length(); i ++) {
            QString word = journalWords.at(i).toLower();
            if (word != "of"
                    && word != "and"
                    && word != "de"
                    && word != "the"
                    && word != "on"
                    && word != "in"
                    && word != "le"
                    ) {
                abbrev += journalWords.at(i).left(1);
            }
        }
        abbrev = abbrev.toUpper();
    }

//    int nspace = -1;
//    do {
//        abbrev += journal1.at(nspace + 1);
//        nspace = journal1.indexOf(' ', nspace + 1);
//    } while (nspace != -1);

//    if (abbrev.length() == 1) {
//        abbrev = journal1.left(nchars);
//    }

    return abbrev;
}

QString Controller::removeSymbols(QString str, bool leaveDash) {
    str.replace(QRegularExpression("[\\n|\\r|\\t]+"), "");

    str.replace('{', "");
    str.replace('}', "");
    str.replace('\\', "");
    str.replace('/', "");
    str.replace('\'', "");
    str.replace('"', "");
    str.replace('~', "");
    str.replace('!', "");
    str.replace('?', "");
    str.replace('@', "");
    str.replace('#', "");
    str.replace('$', "");
    str.replace('%', "");
    str.replace('^', "");
    str.replace('&', "");
    str.replace('*', "");
    str.replace('(', "");
    str.replace(')', "");
    if (!leaveDash) {
        str.replace('-', "");
    }
    str.replace('+', "");
    str.replace('<', "");
    str.replace('>', "");
    str.replace('`', "");
    str.replace('=', "");

    return str;
}

void Controller::updateAssociatedDB(ShelfEntry &shelfEntry) {
    if (shelfEntry.external_db_path.length() > 0) {
        QString path = QDir::cleanPath(DBVars::getInstance().getFilePath() + QDir::separator() + shelfEntry.external_db_path);

        Visitor * visitor;
        QFileInfo fi(path);
        if (fi.suffix().toLower() == "bib") {
            visitor = new ExportBibTeX(model, path);

            QSettings settings;
            settings.beginGroup("BibTeX");
            static_cast<ExportBibTeX *>(visitor)->setExprortFiles(settings.value("exportFiles", true).toBool());
            static_cast<ExportBibTeX *>(visitor)->setExportOwner(settings.value("exportOwner", true).toBool());
            static_cast<ExportBibTeX *>(visitor)->setExportTimestamp(settings.value("exportTimestamp", true).toBool());
            static_cast<ExportBibTeX *>(visitor)->setExportAbstract(settings.value("exportAbstract", true).toBool());
            static_cast<ExportBibTeX *>(visitor)->setNumberIssue(settings.value("makeNumberIssue", true).toBool());
            static_cast<ExportBibTeX *>(visitor)->setDOI2URL(settings.value("makeDOI2URL", true).toBool());
            static_cast<ExportBibTeX *>(visitor)->setExportLanguage(settings.value("exportLanguage", true).toBool());
            settings.endGroup();
        } else {
            qWarning() << QString("External database of '%1' format is not supported!").arg(fi.suffix());
            return;
        }

        shelfEntry.accept(*visitor);
    }
}

void Controller::exportShelf2File(ShelfEntry &shelfEntry, QString &fileName, QString &header) {
    if (fileName.length() > 0) {
        QString path = QDir::cleanPath(fileName);

        Visitor * visitor;
        QFileInfo fi(path);
        if (fi.suffix().toLower() == "bib") {
            visitor = new ExportBibTeX(model, path, header);

            QSettings settings;
            settings.beginGroup("BibTeX");
            static_cast<ExportBibTeX *>(visitor)->setExprortFiles(settings.value("exportFiles", true).toBool());
            static_cast<ExportBibTeX *>(visitor)->setExportOwner(settings.value("exportOwner", true).toBool());
            static_cast<ExportBibTeX *>(visitor)->setExportTimestamp(settings.value("exportTimestamp", true).toBool());
            static_cast<ExportBibTeX *>(visitor)->setExportAbstract(settings.value("exportAbstract", true).toBool());
            static_cast<ExportBibTeX *>(visitor)->setNumberIssue(settings.value("makeNumberIssue", true).toBool());
            static_cast<ExportBibTeX *>(visitor)->setDOI2URL(settings.value("makeDOI2URL", true).toBool());
            static_cast<ExportBibTeX *>(visitor)->setExportLanguage(settings.value("exportLanguage", true).toBool());
            settings.endGroup();
        } else {
            qWarning() << QString("External database of '%1' format is not supported!").arg(fi.suffix());
            return;
        }

        shelfEntry.accept(*visitor);
    }
}


void Controller::search(const QString & text2search, const QStringList & columns, const bool filter, const int shelf_id) {
    QString searchCondition = "";
    for (int i = 0; i < columns.size(); i ++) {
        if (searchCondition.length() > 0) {
            searchCondition += " OR ";
        }
        if (filter) {
            searchCondition += "REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(";
            searchCondition += "REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(";
            searchCondition += "REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(";
        }
        searchCondition += Model::DB_ENTRIES_TABLE + "." + columns.at(i);
        if (filter) {
            searchCondition += ", '\\i', ''), '\\o', ''), '\\v', ''), '\\u', ''), '\\r', ''), '\\d', ''), '\\.', ''), '\\b', ''), '\\=', ''), '\\l', ''), '\\k', ''), '\\c', ''), '\\~', '')";
            searchCondition += ", '\\H', ''), '\\^', ''), '\\\"', ''), '\\''', ''), '\\`', '')";
            searchCondition += ", '''', ''), '^', ''), '_', ''), '$', ''), '}', ''), '{', ''), '\\', '') ";
        }
        searchCondition += " LIKE '%" + text2search + "%' ";

    }
    model->updateShelfViewBySearch(searchCondition, shelf_id);
}

const QList<BibEntry> Controller::searchSimilar(const BibEntry & entry) {
    QList<BibEntry> found;

    QString searchCondition = "SELECT ";
    QString field;

    field = entry.getField(BibEntry::DOI);
    if (field.length() > 0) {
        searchCondition += BibEntry::BIBENTRY_FIELD[BibEntry::ID];
        for (int i = BibEntry::ID + 1; i < BibEntry::COL_NUM; i ++) {
            searchCondition += ",";
            searchCondition += BibEntry::BIBENTRY_FIELD[i];
        }
        searchCondition += " FROM " + Model::DB_ENTRIES_TABLE + " WHERE doi LIKE '%" + field + "%'";

//            id,bibkey,entrytype,address,annote,author,booktitle,chapter,crossref,edition,editor,howpublished,institution,journal,key,month,note,number,organization,pages,publisher,school,series,title,type,volume,year,doi,issn,isbn,url,date,timestamp,language,owner,editedby,editedat,abstract FROM " + Model::DB_ENTRIES_TABLE + " WHERE doi LIKE '%" + field + "%'";

        QList<BibEntry> entries = model->getEntriesBySearch(searchCondition);
        foreach (auto entry, entries) {
            found.append(entry);
        }
    }

    field = entry.getField(BibEntry::TITLE);
    if (field.length() > 0) {
        int dollar = field.indexOf('$');
        if (dollar != -1) {
            field = field.mid(0, dollar);
        }
        searchCondition += "SELECT id,bibkey,entrytype,address,annote,author,booktitle,chapter,crossref,edition,editor,howpublished,institution,journal,key,month,note,number,organization,pages,publisher,school,series,title,type,volume,year,doi,issn,isbn,url,date,timestamp,language,owner,editedby,editedat,abstract FROM " + Model::DB_ENTRIES_TABLE + " WHERE REPLACE(title, ' ', '') LIKE REPLACE('%" + field + "%', ' ', '')";

        QList<BibEntry> entries = model->getEntriesBySearch(searchCondition);
        int foundSize = found.size();
        foreach (auto entry, entries) {
            for (int i = 0; i < foundSize; i ++) {
                if (entry.getID() == found.at(i).getID()) {
                    continue;
                }
                if (entry.getField(BibEntry::VOLUME).simplified().length() > 0 && entry.getField(BibEntry::VOLUME).simplified() == found.at(i).getField(BibEntry::VOLUME).simplified()) {
                    continue;
                }
            }
            found.append(entry);
        }
    }

    field = entry.getField(BibEntry::BOOKTITLE);
    if (field.length() > 0) {
        int dollar = field.indexOf('$');
        if (dollar != -1) {
            field = field.mid(0, dollar);
        }
        searchCondition += "SELECT id,bibkey,entrytype,address,annote,author,booktitle,chapter,crossref,edition,editor,howpublished,institution,journal,key,month,note,number,organization,pages,publisher,school,series,title,type,volume,year,doi,issn,isbn,url,date,timestamp,language,owner,editedby,editedat,abstract FROM " + Model::DB_ENTRIES_TABLE + " WHERE REPLACE(booktitle, ' ', '') LIKE REPLACE('%" + field + "%', ' ', '')";

        QList<BibEntry> entries = model->getEntriesBySearch(searchCondition);
        int foundSize = found.size();
        foreach (auto entry, entries) {
            for (int i = 0; i < foundSize; i ++) {
                if (entry.getID() == found.at(i).getID()) {
                    continue;
                }
                if (entry.getField(BibEntry::VOLUME).simplified().length() > 0 && entry.getField(BibEntry::VOLUME).simplified() == found.at(i).getField(BibEntry::VOLUME).simplified()) {
                    continue;
                }
            }
            found.append(entry);
        }
    }

    field = entry.getField(BibEntry::CHAPTER);
    if (field.length() > 0) {
        int dollar = field.indexOf('$');
        if (dollar != -1) {
            field = field.mid(0, dollar);
        }
        searchCondition += "SELECT id,bibkey,entrytype,address,annote,author,booktitle,chapter,crossref,edition,editor,howpublished,institution,journal,key,month,note,number,organization,pages,publisher,school,series,title,type,volume,year,doi,issn,isbn,url,date,timestamp,language,owner,editedby,editedat,abstract FROM " + Model::DB_ENTRIES_TABLE + " WHERE REPLACE(chapter, ' ', '') LIKE REPLACE('%" + field + "%', ' ', '')";

        QList<BibEntry> entries = model->getEntriesBySearch(searchCondition);
        int foundSize = found.size();
        foreach (auto entry, entries) {
            for (int i = 0; i < foundSize; i ++) {
                if (entry.getID() == found.at(i).getID()) {
                    continue;
                }
                if (entry.getField(BibEntry::VOLUME).simplified().length() > 0 && entry.getField(BibEntry::VOLUME).simplified() == found.at(i).getField(BibEntry::VOLUME).simplified()) {
                    continue;
                }
            }
            found.append(entry);
        }
    }

    return found;
}

//QString Controller::getHash(const BibEntry &entry) const {
//    QString str = "";
//    for (int i = BibEntry::BIBKEY; i < BibEntry::COL_NUM; i ++) {
//        str += entry.getField(i);
//    }

//    return QString(QCryptographicHash::hash(str.toLatin1(), QCryptographicHash::Md5).toHex());
//}

QString Controller::replaceSymbolsByList(QString str) {
    QSettings settings;
    settings.beginGroup("Replacements");

    int repStringsNum = settings.value("repStringsNum").toInt();
    QStringList str2replace;
    QStringList strRepl;
    for (int i = 0; i < repStringsNum; i ++) {
        str2replace << settings.value("string2replace" + QString::number(i)).toString();
        strRepl << settings.value("stringReplacement" + QString::number(i)).toString();
    }
    settings.endGroup();

    QString str1 = str;
    for (int i = 0; i < repStringsNum; i ++) {
        // int p = str1.indexOf(QRegExp("[\\s\\W\\d]" + str2replace[i] + "[\\s\\W\\d]"));
        // str1 = str1.replace(QRegExp("[\\s\\W\\d]" + str2replace[i] + "[\\s\\W\\d]"), " " + strRepl[i] + " ");
        str1 = str1.replace(str2replace[i], strRepl[i]);
    }

    return str1.trimmed();
}
