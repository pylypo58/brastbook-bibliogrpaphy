#ifndef TEXTPROCESSING_H
#define TEXTPROCESSING_H

#include <QString>

#include "../model/model.h"
#include "../model/bibentry.h"
#include "../model/shelfentry.h"

class TextProcessingTest;

class TextProcessing
{
public:
    TextProcessing();

    static QString TeX2Normal(const QString &str);

    static QString replacePlaceholders(Model * model, const QString & str, const BibEntry & entry, bool surnameFirst, bool authorsNaturalOrder, bool abbreviate);

    static QString replaceUTFbyLaTeX(const QString & str);

private:
    struct AuthorName {
        QString firstNames;
        QString lastName;
    };

    static void replaceCycle(QString * str, QString * what1, QString * what2, QString * to);

    static AuthorName splitAuthorName(const QString & author);
    static QString abbreviateName(const QString & name);
    static QString prepareName(const QString & author, bool surnameFirst, bool abbreviate);
    static QString processAuthors(const QString & authors, bool surnameFirst, bool authorsNaturalOrder, bool abbreviate);
    
    friend class TextProcessingTest;
};

#endif // TEXTPROCESSING_H
