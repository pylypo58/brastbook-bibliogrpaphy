#include <QtGlobal>
#include "textprocessing.h"

TextProcessing::TextProcessing()
{

}

QString TextProcessing::TeX2Normal(const QString &str) {
    QString res = str.simplified();

    QString replaceWhat1, replaceWhat2, replaceTo;
    replaceWhat1 = "\\`{A}";
    replaceWhat2 = "\\`A";
    replaceTo = "&Agrave;";
    replaceCycle(&res, &replaceWhat1, &replaceWhat2, &replaceTo);
    replaceWhat1 = "\\'{A}";
    replaceWhat2 = "\\'A";
    replaceTo = "&Aacute;";
    replaceCycle(&res, &replaceWhat1, &replaceWhat2, &replaceTo);
    replaceWhat1 = "\\^{A}";
    replaceWhat2 = "\\^A";
    replaceTo = "&Acirc;";
    replaceCycle(&res, &replaceWhat1, &replaceWhat2, &replaceTo);

    replaceWhat1 = "\\\"{A}";
    replaceWhat2 = "\\\"A";
    replaceTo = "&Auml;";
    replaceCycle(&res, &replaceWhat1, &replaceWhat2, &replaceTo);
    replaceWhat1 = "\\~{A}";
    replaceWhat2 = "\\~A";
    replaceTo = "&Atilde;";
    replaceCycle(&res, &replaceWhat1, &replaceWhat2, &replaceTo);
    replaceWhat1 = "\\c{A}";
    replaceTo = "&Acedil;";
    replaceCycle(&res, &replaceWhat1, nullptr, &replaceTo);
    replaceWhat1 = "\\k{A}";
    replaceTo = "&Aogon;";
    replaceCycle(&res, &replaceWhat1, nullptr, &replaceTo);
    replaceWhat1 = "\\={A}";
    replaceWhat2 = "\\=A";
    replaceTo = "&Amacr;";
    replaceCycle(&res, &replaceWhat1, &replaceWhat2, &replaceTo);
    replaceWhat1 = "\\.{A}";
    replaceWhat2 = "\\.A";
    replaceTo = "&Adot;";
    replaceCycle(&res, &replaceWhat1, &replaceWhat2, &replaceTo);
    replaceWhat1 = "\\u{A}";
    replaceTo = "&Abreve;";
    replaceCycle(&res, &replaceWhat1, nullptr, &replaceTo);
    replaceWhat1 = "\\v{A}";
    replaceTo = "&Acaron;";
    replaceCycle(&res, &replaceWhat1, nullptr, &replaceTo);

    res.replace("\\&", "&amp;");
    res.replace("~", "&nbsp;");
    res.replace("--", "&ndash;");
    res.replace("---", "&mdash;");
    res.replace("\"--~", "&ndash;&nbsp;");
    res.replace("\"---~", "&mdash;&nbsp;");
    res.replace("\\l", "&lstrok;");
    res.replace("\\L", "&Lstrok;");
    res.replace("\\ss", "&szlig;");

    res.replace("\\{", "&lbrace;");
    res.replace("\\}", "&rbrace;");
    res.replace("{", "");
    res.replace("}", "");

    return res;
}


QString TextProcessing::replaceUTFbyLaTeX(const QString &str) {
    QString s = str;

    s.replace("Ä", "\\\"{A}");
    s.replace("À", "\\`{A}");
    s.replace("Á", "\\'{A}");
    s.replace("Â", "\\^{A}");
    s.replace("Č", "\\v{C}");
    s.replace("Ç", "\\c{C}");
    s.replace("Ď", "\\v{D}");
    s.replace("É", "\\'{E}");
    s.replace("Ê", "\\^{E}");
    s.replace("Ë", "\\\"{E}");
    s.replace("È ", "\\`{E}");
    s.replace("Ě", "\\v{E}");
    s.replace("Î", "\\^{I}");
    s.replace("Ì", "\\`{I}");
    s.replace("Ï", "\\\"{I}");
    s.replace("Í", "\\'{I}");
    s.replace("Ñ", "\\~{N}");
    s.replace("Ň", "\\v{N}");
    s.replace("Ö", "\\\"{O}");
    s.replace("Ô", "\\^{O}");
    s.replace("Ò", "\\`{O}");
    s.replace("Ó", "\\'{O}");
    s.replace("Š", "\\v{S}");
    s.replace("Ü", "\\\"{U}");
    s.replace("Û", "\\^{U}");
    s.replace("Ù", "\\`{U}");
    s.replace("Ú", "\\'{U}");
    s.replace("Ÿ", "\\\"{Y}");
    s.replace("Ž", "\\v{Z}");

    s.replace("ä", "\\\"{a}");
    s.replace("à", "\\`{a}");
    s.replace("á", "\\'{a}");
    s.replace("â", "\\^{a}");
    s.replace("č", "\\v{c}");
    s.replace("ç", "\\c{c}");
    //s.replace("ď", "\\v{d}");
    s.replace("é", "\\'{e}");
    s.replace("ê", "\\^{e}");
    s.replace("ë", "\\\"{e}");
    s.replace("è ", "\\`{e}");
    s.replace("ě", "\\v{e}");
    s.replace("î", "\\^{\\i}");
    s.replace("ì", "\\`{\\i}");
    s.replace("ï", "\\\"{\\i}");
    s.replace("í", "\\'{\\i}");
    s.replace("ñ", "\\~{n}");
    s.replace("ň", "\\v{n}");
    s.replace("ö", "\\\"{o}");
    s.replace("ô", "\\^{o}");
    s.replace("ò", "\\`{o}");
    s.replace("ó", "\\'{o}");
    s.replace("š", "\\v{s}");
    s.replace("ü", "\\\"{u}");
    s.replace("û", "\\^{u}");
    s.replace("ù", "\\`{u}");
    s.replace("ú", "\\'{u}");
    s.replace("ÿ", "\\\"{y}");
    s.replace("ž", "\\v{z}");

    s.replace("Å", "{\\AA}");
    s.replace("å", "{\\aa}");
    s.replace("Æ", "{\\AE}");
    s.replace("æ", "{\\ae}");
    s.replace("Œ", "{\\OE}");
    s.replace("œ)", "{\\oe}");
    s.replace("Ø", "{\\O}");
    s.replace("ø", "{\\o}");
    s.replace("ß", "{\\ss}");

    return s;
}

void TextProcessing::replaceCycle(QString * str, QString * what1, QString * what2, QString * to) {
    for (char ch = 'A'; ch <= 'Z'; ch++) {
        what1->replace(3, 1, ch);
        to->replace(1, 1, ch);
        str->replace(*what1, *to);
    }
    for (char ch = 'a'; ch <= 'z'; ch++) {
        what1->replace(3, 1, ch);
        to->replace(1, 1, ch);
        str->replace(*what1, *to);
    }
    if (what2) {
        for (char ch = 'A'; ch <= 'Z'; ch++) {
            what2->replace(2, 1, ch);
            to->replace(1, 1, ch);
            str->replace(*what2, *to);
        }
        for (char ch = 'a'; ch <= 'z'; ch++) {
            what2->replace(2, 1, ch);
            to->replace(1, 1, ch);
            str->replace(*what2, *to);
        }
    }
}

QString TextProcessing::replacePlaceholders(Model * model, const QString &str, const BibEntry &entry, bool surnameFirst, bool authorsNaturalOrder, bool abbreviate) {
    QString s = str;

    s.replace("{{bibkey}}", entry.getField(BibEntry::BIBKEY));
    s.replace("{{entrytype}}", BibEntry::BIBENTRY_TYPE[entry.getEntryType()]);
    s.replace("{{address}}", entry.getField(BibEntry::ADDRESS));
    s.replace("{{annote}}", entry.getField(BibEntry::ANNOTE));
    s.replace("{{author}}", processAuthors(entry.getField(BibEntry::AUTHOR), surnameFirst, authorsNaturalOrder, abbreviate));
    s.replace("{{booktitle}}", entry.getField(BibEntry::BOOKTITLE));
    s.replace("{{chapter}}", entry.getField(BibEntry::CHAPTER));
    s.replace("{{crossref}}", entry.getField(BibEntry::CROSSREF));
    s.replace("{{edition}}", entry.getField(BibEntry::EDITION));
    s.replace("{{editor}}", processAuthors(entry.getField(BibEntry::EDITOR), surnameFirst, authorsNaturalOrder, abbreviate));
    s.replace("{{howpublished}}", entry.getField(BibEntry::HOWPUBLISHED));
    s.replace("{{institution}}", entry.getField(BibEntry::INSTITUTION));
    s.replace("{{journal}}", entry.getField(BibEntry::JOURNAL));
    s.replace("{{key}}", entry.getField(BibEntry::KEY));
    s.replace("{{month}}", entry.getField(BibEntry::MONTH));
    s.replace("{{note}}", entry.getField(BibEntry::NOTE));
    s.replace("{{number}}", entry.getField(BibEntry::NUMBER));
    s.replace("{{organization}}", entry.getField(BibEntry::ORGANIZATION));
    s.replace("{{pages}}", entry.getField(BibEntry::PAGES));
    s.replace("{{publisher}}", entry.getField(BibEntry::PUBLISHER));
    s.replace("{{school}}", entry.getField(BibEntry::SCHOOL));
    s.replace("{{series}}", entry.getField(BibEntry::SERIES));
    s.replace("{{title}}", entry.getField(BibEntry::TITLE));
    s.replace("{{type}}", entry.getField(BibEntry::TYPE));
    s.replace("{{volume}}", entry.getField(BibEntry::VOLUME));
    s.replace("{{year}}", entry.getField(BibEntry::YEAR));
    s.replace("{{doi}}", entry.getField(BibEntry::DOI));
    s.replace("{{issn}}", entry.getField(BibEntry::ISSN));
    s.replace("{{isbn}}", entry.getField(BibEntry::ISBN));
    s.replace("{{url}}", entry.getField(BibEntry::URL));
    s.replace("{{date}}", entry.getField(BibEntry::DATE));
    s.replace("{{timestamp}}", entry.getField(BibEntry::TIMESTAMP));
    s.replace("{{language}}", entry.getField(BibEntry::LANGUAGE));
    s.replace("{{owner}}", entry.getField(BibEntry::OWNER));
    s.replace("{{editedby}}", entry.getField(BibEntry::EDITEDBY));
    s.replace("{{editedat}}", entry.getField(BibEntry::EDITEDAT));
    s.replace("{{abstract}}", entry.getField(BibEntry::ABSTRACT));

    if (s.indexOf("{{shelfs}}") >= 0) {
        const QList<ShelfEntry> shelfs = model->getShelfsForEntry(entry.getID());
        QString sshelfs = "<ol>";
        foreach (auto & shelf, shelfs) {
            sshelfs += "<li>" + shelf.name + "</li>";
        }
        sshelfs += "</ol>";
        s.replace("{{shelfs}}", sshelfs);
    }

    return TextProcessing::TeX2Normal(s);
}

TextProcessing::AuthorName TextProcessing::splitAuthorName(const QString & author) {
    AuthorName result = {"", ""};
    if (author.simplified().length() == 0) {
        return result;
    }

#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
        QStringList commaSplitted = author.split(',', QString::SkipEmptyParts);
#else
        QStringList commaSplitted = author.split(',', Qt::SkipEmptyParts);
#endif
    if (commaSplitted.size() == 1) {
        // no comma, natural order of name
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
        QStringList name = commaSplitted[0].split(QRegExp("\\s+"), QString::SkipEmptyParts);
#elif QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        QStringList name = commaSplitted[0].split(QRegExp("\\s+"), Qt::SkipEmptyParts);
#else
        QStringList name = commaSplitted[0].split(QRegularExpression("\\s+"), Qt::SkipEmptyParts);
#endif
        result.lastName = name.last().simplified();
        for (int i = 0; i < name.length() - 1; i ++) {
            result.firstNames += name[i].simplified() + " ";
        }
    } else {
        // "surname, name" ordering
        result.lastName = commaSplitted[0].simplified();
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
        QStringList name = commaSplitted[1].split(QRegExp("\\s+"), QString::SkipEmptyParts);
#elif QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        QStringList name = commaSplitted[1].split(QRegExp("\\s+"), Qt::SkipEmptyParts);
#else
        QStringList name = commaSplitted[1].split(QRegularExpression("\\s+"), Qt::SkipEmptyParts);
#endif
        for (int i = 0; i < name.length(); i ++) {
            result.firstNames += name[i].simplified() + " ";
        }
    }

    result.firstNames = result.firstNames.trimmed();
    result.lastName = result.lastName.trimmed();
    return result;
}

QString TextProcessing::abbreviateName(const QString & name) {
    QStringList res;
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
        res = name.split(QRegExp("\\s+"), QString::SkipEmptyParts);
#elif QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        res = name.split(QRegExp("\\s+"), Qt::SkipEmptyParts);
#else
    res = name.split(QRegularExpression("\\s+"), Qt::SkipEmptyParts);
#endif
    for (int i = 0; i < res.size(); i ++) {
        res[i] = res[i].trimmed();
        if (res[i].toLower() == "von" || res[i].toLower() == "der" || res[i].toLower() == "de" || res[i].toLower() == "den" || res[i].toLower() == "du" || res[i].toLower() == "van") {
            continue;
        }
        res[i] = res[i].at(0);
        res[i] += '.';
    }

    QString abbreviated = "";
    if (res.size() > 0) {
        abbreviated = res[0];
    }
    for (int i = 1; i < res.size(); i ++) {
        abbreviated += " " + res[i];
    }

    return abbreviated;
}

QString TextProcessing::prepareName(const QString &author, bool surnameFirst, bool abbreviate) {
    AuthorName name = splitAuthorName(author);
    QString final = "";

    if (surnameFirst) {
        if (abbreviate) {
            final += name.lastName + ", " + abbreviateName(name.firstNames);
        } else {
            final += name.lastName + ", " + name.firstNames;
        }
    } else {
        if (abbreviate) {
            final += abbreviateName(name.firstNames) + " " + name.lastName;
        } else {
            final += name.firstNames + " " + name.lastName;
        }
    }

    return final;
}

QString TextProcessing::processAuthors(const QString &authors, bool surnameFirst, bool authorsNaturalOrder, bool abbreviate) {
    if (authors.length() == 0) {
        return "";
    }

    QString s = authors;
    QString final = "";

    QStringList author = s.split(" and ");
    if (author.size() > 0) {
        final = prepareName(author[0], surnameFirst, abbreviate);
    }

    for (int i = 1; i < author.size() - 1; i ++) {
        if (authorsNaturalOrder) {
            final += ", ";
        } else {
            final += " and ";
        }
        final += prepareName(author[i], surnameFirst, abbreviate);
    }

    if (author.size() >= 2) {
        final += " and ";
        final += prepareName(author[author.size() - 1], surnameFirst, abbreviate);
    }

    return final;
}

































