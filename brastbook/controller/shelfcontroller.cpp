#include "shelfcontroller.h"

#include "visitor/syncwithgit.h"

ShelfController::ShelfController(Model * model):
    model(model)
{

}

void ShelfController::addShelf(const ShelfEntry &shelfEntry) {
    model->addShelf(shelfEntry);
}

void ShelfController::deleteShelf(const ShelfEntry shelf) {
    model->deleteShelf(shelf);
}

void ShelfController::clearShelf(const ShelfEntry shelf) {
    model->clearShelf(shelf);
}

void ShelfController::setCurrentShelf(const ShelfEntry entry) {
     model->setCurrentShelf(entry);
}

void ShelfController::updateShelf(ShelfEntry shelf) {
    model->updateShelf(shelf);
}

void ShelfController::updateShelfView() {
    ShelfEntry shelf(model->getRootShelfId());

    model->updateShelfView(shelf);
}


void ShelfController::updateShelfView(const ShelfEntry shelf, bool useLastQuery) {
    model->updateShelfView(shelf, useLastQuery);
}

void ShelfController::syncShelf(ShelfEntry shelfEntry, const SyncInfo &syncInfo, const ShelfEntry & shelfInView) {
    SyncWithGit sync(model, shelfInView, syncInfo);
    if (shelfEntry.shared) {
        sync.processShelf(shelfEntry);
        emit shelfIsSynced(shelfEntry);
    }
}
