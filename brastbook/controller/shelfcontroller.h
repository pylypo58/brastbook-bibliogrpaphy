#ifndef SHELFCONTROLLER_H
#define SHELFCONTROLLER_H

#include "../model/model.h"
#include "visitor/syncinfo.h"

class ShelfController : public QObject
{
    Q_OBJECT
public:
    ShelfController(Model * model);

    void setModel(Model * model) { this->model = model; }

    void addShelf(const ShelfEntry &shelfEntry);
    void deleteShelf(const ShelfEntry shelf);
    void clearShelf(const ShelfEntry shelf);

    void setCurrentShelf(const ShelfEntry entry);
    void updateShelf(ShelfEntry shelf);

    void updateShelfView();
    void updateShelfView(const ShelfEntry shelf, bool useLastQuery = false);

    void syncShelf(ShelfEntry shelfEntry, const SyncInfo & syncInfo, const ShelfEntry & shelfInView);

signals:
    void shelfIsSynced(ShelfEntry shelf);

private:
    Model * model;
};

#endif // SHELFCONTROLLER_H
