#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGridLayout>
#include <QPushButton>

#include "model/model.h"
#include "controller/controller.h"
#include "view/lastopenedmenu.h"
#include "view/currentshelfview.h"
#include "view/shelfstree.h"
#include "view/dockshelfs/dockshelfswidget.h"
#include "view/dockinfo/dockinfowidget.h"
#include "view/docksearch/docksearch.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

signals:
    void bibEntrySelected(BibEntry entry);

public slots:
    void setupEntriesViewColumns();

private slots:
    void processSelectedBibItem(const QItemSelection & selected, const QItemSelection & previous);

    void shelfViewColumnsResize(int logicalIndex, int oldSize, int newSize);

    void openRecent();

    void on_actionNew_triggered();

    void on_actionOpen_triggered();

    void on_action_bib_JabRef_triggered();

    void on_actionShelfs_triggered();

    void on_actionInfo_triggered();

    void closeAppSlot();

    void on_actionClose_database_triggered();

    void on_actionExit_triggered();

    void on_actionPreferences_triggered();

//    void processSelectedBibItem(const QModelIndex & modelIndex);

    void on_actionOpen_URL_or_DOI_triggered();

    void on_actionOpen_file_triggered();

    void on_actionModify_triggered();

    void on_actionAdd_triggered();

    void on_actionAdd_manually_triggered();

    void on_actionEdit_triggered();

//    void on_actionRemove_triggered();

//    void on_actionDelete_2_triggered();

//    void on_actionDelete_triggered();

    void on_actionDeleteShelf_triggered();

    void on_actionDeleteEntry_triggered();

    void on_actionUpdate_all_external_triggered();

    void on_actionUpdate_projects_triggered();

    void on_actionImport_from_file_triggered();

    void on_actionGenerate_BibKey_triggered();

    void on_actionSearch_triggered();

    void on_actionImport_by_DOI_triggered();

    void on_actionUpdate_all_external_projects_triggered();

    void on_actionSync_all_shared_shelfs_triggered();

    void on_actionAbout_triggered();

private:
    void freeMVC();
    void initMVC(QString path2database, bool isNew = false);
    void updateLastOpened(const QString & fileName);

    Ui::MainWindow *ui;

    CurrentShelfView * tableView;

    DockShelfsWidget * dockShelfsWidget;
    DockInfoWidget * dockInfoWidget;
    DockSearchWidget * dockSearchWidget;

    Controller * controller;
    Model * model;

    QGridLayout * shelfsLayout;
    QTreeWidget * shelfsTree;
    QPushButton * button;

    bool columnWidthControl;
    QMap<QString, int> columnWidths;
    int currentShelfViewScrollPosition;

    const static int MAX_LAST_DATABASES = 10;
    QList<LastOpenedMenu *> qmLastDB;
};
#endif // MAINWINDOW_H
