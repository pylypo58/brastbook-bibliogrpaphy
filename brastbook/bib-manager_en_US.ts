<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>About</name>
    <message>
        <location filename="view/about/about.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/about/about.ui" line="26"/>
        <source>BraStBook: Qt/SQLite-based bibliography manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/about/about.ui" line="39"/>
        <source>Developing team:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/about/about.ui" line="52"/>
        <source>Oleksandr V. Pylypovskyi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/about/about.ui" line="65"/>
        <source>Special thanks to:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/about/about.ui" line="78"/>
        <source>Artem V. Tomilo, Nina Elkina, Christian Hueser</source>
        <oldsource>Artem V. Tomilo, Nina Elkina</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/about/about.ui" line="91"/>
        <source>Icon image: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/about/about.ui" line="104"/>
        <source>Photo of Lip Kee (https://www.flickr.com/photos/lipkee/3252920731/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/about/about.ui" line="117"/>
        <source>Distributed under CC BY-SA 2.0 terms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/about/about.ui" line="130"/>
        <source>https://creativecommons.org/licenses/by-sa/2.0/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/about/about.ui" line="143"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CompareEntriesDialog</name>
    <message>
        <location filename="view/compareentries/compareentriesdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/compareentries/compareentriesdialog.cpp" line="34"/>
        <source>Merge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/compareentries/compareentriesdialog.cpp" line="36"/>
        <source>Leave two</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/compareentries/compareentriesdialog.cpp" line="38"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/compareentries/compareentriesdialog.cpp" line="43"/>
        <source>Take all LOCAL entries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/compareentries/compareentriesdialog.cpp" line="44"/>
        <source>Take all REMOTE entries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/compareentries/compareentriesdialog.cpp" line="112"/>
        <source>Add all from left &gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/compareentries/compareentriesdialog.cpp" line="114"/>
        <source>&lt; Add all from right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/compareentries/compareentriesdialog.cpp" line="282"/>
        <source>Files:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/compareentries/compareentriesdialog.cpp" line="314"/>
        <source>Shelfs:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/compareentries/compareentriesdialog.cpp" line="322"/>
        <source>Compare entries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/compareentries/compareentriesdialog.cpp" line="643"/>
        <location filename="view/compareentries/compareentriesdialog.cpp" line="651"/>
        <source>Syncronization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/compareentries/compareentriesdialog.cpp" line="643"/>
        <source>Do you want to use the only LOCAL values for meging?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/compareentries/compareentriesdialog.cpp" line="651"/>
        <source>Do you want to use the only REMOTE values for meging?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Controller</name>
    <message>
        <location filename="controller/controller.cpp" line="47"/>
        <source>Importing entries...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="controller/controller.cpp" line="47"/>
        <source>Abort import</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CurrentShelfView</name>
    <message>
        <location filename="view/currentshelfview.cpp" line="26"/>
        <source>&amp;Copy as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/currentshelfview.cpp" line="27"/>
        <source>&amp;BibTeX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/currentshelfview.cpp" line="29"/>
        <source>&amp;preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/currentshelfview.cpp" line="32"/>
        <source>Open &amp;file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/currentshelfview.cpp" line="34"/>
        <source>Open &amp;URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/currentshelfview.cpp" line="37"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DockInfoWidget</name>
    <message>
        <location filename="view/dockinfo/dockinfowidget.cpp" line="23"/>
        <source>Current entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockinfo/dockinfowidget.cpp" line="31"/>
        <source>Bibtex Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockinfo/dockinfowidget.cpp" line="33"/>
        <source>Click to copy into Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockinfo/dockinfowidget.cpp" line="37"/>
        <source>Associated files:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockinfo/dockinfowidget.cpp" line="41"/>
        <source>Double click on file to open in the default viewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockinfo/dockinfowidget.cpp" line="45"/>
        <source>DOI/URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockinfo/dockinfowidget.cpp" line="47"/>
        <source>Opens DOI (or URL) in browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockinfo/dockinfowidget.cpp" line="51"/>
        <source>Copy BibTeX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockinfo/dockinfowidget.cpp" line="53"/>
        <source>Copies entry in BibTeX format to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockinfo/dockinfowidget.cpp" line="57"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockinfo/dockinfowidget.cpp" line="170"/>
        <source>Modify entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockinfo/dockinfowidget.cpp" line="183"/>
        <location filename="view/dockinfo/dockinfowidget.cpp" line="201"/>
        <source>Unpredicted exception during editing the entry.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockinfo/dockinfowidget.cpp" line="189"/>
        <source>Edit entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockinfo/dockinfowidget.cpp" line="207"/>
        <source>Delete entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockinfo/dockinfowidget.cpp" line="207"/>
        <source>Delete selected entry?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DockSearchWidget</name>
    <message>
        <location filename="view/docksearch/docksearch.cpp" line="11"/>
        <location filename="view/docksearch/docksearch.cpp" line="24"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/docksearch/docksearch.cpp" line="17"/>
        <source>Search text:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/docksearch/docksearch.cpp" line="28"/>
        <source>Skip special symbols</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/docksearch/docksearch.cpp" line="29"/>
        <source>Filters out symbols like {, _, \, etc (but slows down search)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/docksearch/docksearch.cpp" line="33"/>
        <source>All checked and all unchecked means the same</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/docksearch/docksearch.cpp" line="37"/>
        <source>Set all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/docksearch/docksearch.cpp" line="41"/>
        <source>Unset all</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DockShelfsWidget</name>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="23"/>
        <source>Shelfs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="29"/>
        <source>Expand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="30"/>
        <source>Expand all shelfs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="34"/>
        <source>Collapse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="35"/>
        <source>Collapse all shelfs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="52"/>
        <source>Sync</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="53"/>
        <source>Syncronize by Git</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="54"/>
        <source>Proj.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="55"/>
        <source>Update the associated database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="67"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="95"/>
        <source>All shelfs (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="102"/>
        <source>%1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="127"/>
        <source>Shelf properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="137"/>
        <source>Unpredicted exception during selection of the shelf.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="151"/>
        <source>New shelf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="162"/>
        <source>Unpredicted exception during adding of a new shelf.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="173"/>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="184"/>
        <source>Delete entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="173"/>
        <source>Delete shelf &apos;%1&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="184"/>
        <source>Remove all entries from shelf &apos;%1&apos;?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="208"/>
        <source>Unpredicted exception during updating external DB for &apos;%1&apos; shelf.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="214"/>
        <source>External project has been updated!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="245"/>
        <source>Shelf has been syncronized!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="249"/>
        <source>Unpredicted exception during shelf &apos;%1&apos; syncronization.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="265"/>
        <source>Select bibliographical entry first!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/dockshelfswidget.cpp" line="270"/>
        <source>Select shelf first!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EntryProperties</name>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="42"/>
        <source>Entry type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="56"/>
        <source>article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="61"/>
        <source>book</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="66"/>
        <source>booklet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="71"/>
        <source>conference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="76"/>
        <source>inbook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="81"/>
        <source>incollection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="86"/>
        <source>inproceedings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="91"/>
        <source>manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="96"/>
        <source>masterthesis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="101"/>
        <source>misc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="106"/>
        <source>phdthesis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="111"/>
        <source>proceedings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="116"/>
        <source>techreport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="121"/>
        <source>unpublished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="139"/>
        <source>Main</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="167"/>
        <source>Additional</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="195"/>
        <source>Abstract</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="200"/>
        <source>Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="205"/>
        <source>Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="227"/>
        <source>Add file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="240"/>
        <location filename="view/entryproperties/entryproperties.cpp" line="938"/>
        <location filename="view/entryproperties/entryproperties.cpp" line="953"/>
        <source>Remove file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="253"/>
        <source>Delete from disk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="259"/>
        <source>Shelfs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="281"/>
        <source>Current shelfs:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="294"/>
        <source>All shelfs:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="307"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Unassign the shelf selected at the RHS from the entry&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="310"/>
        <source>&lt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="323"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Assign the shelf selected at the LHS to the entry&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="326"/>
        <source>&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="351"/>
        <source>Entry properties:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.ui" line="364"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="75"/>
        <source>Hash: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="218"/>
        <source>BibTex key:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="219"/>
        <source>Address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="220"/>
        <source>Annote:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="221"/>
        <source>Author(s):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="222"/>
        <source>Book title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="223"/>
        <source>Chapter:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="224"/>
        <source>Crossref:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="225"/>
        <source>Edition:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="226"/>
        <source>Editor:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="227"/>
        <source>How published:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="228"/>
        <source>Institution:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="229"/>
        <source>Journal:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="231"/>
        <source>Month:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="232"/>
        <source>Number:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="233"/>
        <source>Organization:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="234"/>
        <source>Pages:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="235"/>
        <source>Publisher:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="236"/>
        <source>School:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="237"/>
        <source>Series:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="238"/>
        <source>Title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="239"/>
        <source>Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="240"/>
        <source>Volume:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="241"/>
        <source>Year:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="242"/>
        <source>DOI:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="243"/>
        <source>ISSN:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="244"/>
        <source>ISBN:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="245"/>
        <source>URL:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="246"/>
        <source>Date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="247"/>
        <source>Timestamp:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="248"/>
        <source>Language:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="249"/>
        <source>Owner:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="250"/>
        <source>Edited by:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="251"/>
        <source>Edited at:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="919"/>
        <source>Open File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="919"/>
        <source>PDF documents (*.pdf);;DJVU documents (*.djv, *.djvu);;MS/OO/LO Office (*.doc, *.docx, *.odt);;All files (*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="938"/>
        <source>Remove file from list?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/entryproperties/entryproperties.cpp" line="953"/>
        <source>Delete file from disk?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportByMetadata</name>
    <message>
        <location filename="view/importbymetadata/importbymetadata.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/importbymetadata/importbymetadata.cpp" line="19"/>
        <source>Import by metadata</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/importbymetadata/importbymetadata.cpp" line="24"/>
        <source>Select service:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/importbymetadata/importbymetadata.cpp" line="33"/>
        <source>Identifier:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/importbymetadata/importbymetadata.cpp" line="42"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/importbymetadata/importbymetadata.cpp" line="45"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/importbymetadata/importbymetadata.cpp" line="63"/>
        <source>Please add your e-mail in Service-&gt;Preferences-&gt;Personal to use Crossref!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/importbymetadata/importbymetadata.cpp" line="94"/>
        <source>Response from server has not been received :(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/importbymetadata/importbymetadata.cpp" line="106"/>
        <source>Wrong response from the server!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/importbymetadata/importbymetadata.cpp" line="122"/>
        <source>Unpredicted exception during adding the entry.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="34"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="50"/>
        <source>Entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="64"/>
        <source>Service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="76"/>
        <source>Shelf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="84"/>
        <source>Windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="92"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="106"/>
        <source>New database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="109"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="114"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="117"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="122"/>
        <source>Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="127"/>
        <source>Close database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="132"/>
        <source>Save As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="137"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="142"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="145"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="150"/>
        <source>*.bib (JabRef)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="153"/>
        <source>Ctrl+I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="158"/>
        <source>Shelfs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="163"/>
        <source>Projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="168"/>
        <source>Current entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="173"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="178"/>
        <source>Import from file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="181"/>
        <source>F2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="186"/>
        <source>Import by metadata</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="189"/>
        <source>F3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="194"/>
        <source>Add manually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="197"/>
        <source>Shift+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="202"/>
        <location filename="mainwindow.cpp" line="691"/>
        <source>Open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="205"/>
        <source>F5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="210"/>
        <source>Open DOI or URL</source>
        <oldsource>Open URL or DOI</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="213"/>
        <source>F6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="218"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="221"/>
        <source>F7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="226"/>
        <location filename="mainwindow.ui" line="244"/>
        <location filename="mainwindow.ui" line="247"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="229"/>
        <source>F8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="234"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="239"/>
        <source>Modify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="252"/>
        <location filename="mainwindow.ui" line="283"/>
        <source>Update all external projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="255"/>
        <source>Ctrl+U</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="260"/>
        <location filename="mainwindow.ui" line="291"/>
        <source>Sync all shared shelfs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="265"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="270"/>
        <source>Generate BibKey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="273"/>
        <source>Ctrl+B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="278"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="286"/>
        <source>Ctrl+E</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="294"/>
        <source>Ctrl+G</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="114"/>
        <source>Please accept settings state before program starts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="133"/>
        <source>Cannot load previously opened database again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="347"/>
        <location filename="mainwindow.cpp" line="373"/>
        <source>SQLite files for bibliography (*.%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="354"/>
        <location filename="mainwindow.cpp" line="380"/>
        <source>Library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="354"/>
        <location filename="mainwindow.cpp" line="380"/>
        <source>Something wrong with saving library: too may files selected!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="363"/>
        <location filename="mainwindow.cpp" line="389"/>
        <location filename="mainwindow.cpp" line="408"/>
        <source>Unpredicted exception during creation of the sysem.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="443"/>
        <location filename="mainwindow.cpp" line="515"/>
        <location filename="mainwindow.cpp" line="527"/>
        <location filename="mainwindow.cpp" line="539"/>
        <location filename="mainwindow.cpp" line="551"/>
        <location filename="mainwindow.cpp" line="576"/>
        <location filename="mainwindow.cpp" line="620"/>
        <location filename="mainwindow.cpp" line="633"/>
        <location filename="mainwindow.cpp" line="651"/>
        <location filename="mainwindow.cpp" line="674"/>
        <location filename="mainwindow.cpp" line="687"/>
        <location filename="mainwindow.cpp" line="715"/>
        <location filename="mainwindow.cpp" line="752"/>
        <location filename="mainwindow.cpp" line="857"/>
        <location filename="mainwindow.cpp" line="883"/>
        <source>Please open/create a library first!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="448"/>
        <source>JabRef files (*.bib</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="448"/>
        <source>All files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="465"/>
        <source>Unpredicted exception during importing the database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="557"/>
        <source>New shelf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="568"/>
        <source>Unpredicted exception during adding of a new shelf.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="581"/>
        <source>Add new entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="607"/>
        <location filename="mainwindow.cpp" line="644"/>
        <location filename="mainwindow.cpp" line="663"/>
        <source>Unpredicted exception during editing the entry.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="691"/>
        <source>All files (*);;BibTeX files (*.bib *.bibtex);;RIS files (*.ris)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="704"/>
        <source>Unknown file type!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="724"/>
        <source>Replace BibKey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="724"/>
        <source>Replace existing BibKey?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="797"/>
        <location filename="mainwindow.cpp" line="799"/>
        <source>Similar entries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="797"/>
        <source>Possible duplicates found. Do you want to check/merge?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="799"/>
        <source>Import anyway?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="820"/>
        <source>Local</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="820"/>
        <source>By DOI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="849"/>
        <source>Unpredicted exception during adding the entry.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="868"/>
        <source>Unpredicted exception during updating external DB for &apos;%1&apos; shelf.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="875"/>
        <source>External projects have been updated!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="905"/>
        <source>Unpredicted exception during shelf &apos;%1&apos; syncronization.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="910"/>
        <source>Shelfs have been syncronized!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Model</name>
    <message>
        <location filename="model/model.cpp" line="117"/>
        <source>Root</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PreferencesWindow</name>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="17"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="49"/>
        <location filename="view/preferences/preferenceswindow.ui" line="247"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="54"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="59"/>
        <source>Default shelf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="74"/>
        <source>Shelf view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="102"/>
        <source>Order and appearance of columns:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="111"/>
        <source>Preview presets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="124"/>
        <source>article</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="129"/>
        <source>book</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="134"/>
        <source>booklet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="139"/>
        <source>conference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="144"/>
        <source>inbook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="149"/>
        <source>incollection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="154"/>
        <source>inproceedings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="159"/>
        <source>manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="164"/>
        <source>masterthesis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="169"/>
        <source>misc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="174"/>
        <source>phdthesis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="179"/>
        <source>proceedings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="184"/>
        <source>techreport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="189"/>
        <source>unpublished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="203"/>
        <source>Entry types:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="216"/>
        <source>Preset (use HTML/CSS and bibtex fields like {{author}}):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="235"/>
        <source>Personal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="259"/>
        <source>Global file path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="272"/>
        <source>Owner:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="305"/>
        <location filename="view/preferences/preferenceswindow.ui" line="367"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="328"/>
        <source>e-mail (for Crossref):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="342"/>
        <source>Git</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="354"/>
        <source>Git path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="400"/>
        <source>Commit:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="423"/>
        <source>Pull:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="449"/>
        <source>Push:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.ui" line="472"/>
        <source>Commit message:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.cpp" line="14"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.cpp" line="47"/>
        <source>BibTeX key template:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.cpp" line="47"/>
        <source>Use the following placeholders for BibTeX keys:
{{author}}: surname of the first author
{{yyyy}}: full year like 2016
{{yy}}: short year like 16
{{JRN}}: automatic journal abbreviation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.cpp" line="50"/>
        <source>Open last used database at startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.cpp" line="54"/>
        <source>Display &apos;Lastname, Firstname&apos; in detailed information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.cpp" line="58"/>
        <source>Use natural authors view (author1, author2 and author3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.cpp" line="62"/>
        <source>Abbreviate authors names</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.cpp" line="66"/>
        <source>Replace some UTF letters when import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.cpp" line="70"/>
        <source>Silent update of external projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.cpp" line="74"/>
        <source>Automatically update external project when entry is added</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.cpp" line="88"/>
        <source>Export list of files (aka JabRef format for *.bib)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.cpp" line="90"/>
        <source>Export &apos;owner&apos;/&apos;edited by&apos; as fields</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.cpp" line="92"/>
        <source>Export &apos;timestamp&apos;/&apos;edited at&apos; as fields</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.cpp" line="94"/>
        <source>Export abstract as a field</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.cpp" line="96"/>
        <source>Duplicate field &apos;number&apos; as &apos;issue&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.cpp" line="98"/>
        <source>In absence of URL, use DOI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.cpp" line="100"/>
        <source>Export &apos;language&apos; as field</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.cpp" line="325"/>
        <source>Select folder for files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/preferenceswindow.cpp" line="334"/>
        <source>Select path to Git</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="controller/import/doiimport.cpp" line="30"/>
        <source>Cannot parse Crossref response!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="controller/import/doiimport.cpp" line="236"/>
        <source>Cannot parse Crossref result!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="controller/import/importer.cpp" line="17"/>
        <source>Calling of &apos;getParsed&apos; before &apos;parse&apos;!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="controller/import/jabrefimport.cpp" line="16"/>
        <location filename="controller/import/risimport.cpp" line="18"/>
        <source>Cannot process &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="controller/import/jabrefimport.cpp" line="30"/>
        <source>Jabref/BibTeX Import: %1 entries are imported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="controller/import/risimport.cpp" line="33"/>
        <source>RIS Import: %1 entries are imported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="controller/visitor/exportbibtex.cpp" line="26"/>
        <location filename="controller/visitor/syncwithgit.cpp" line="164"/>
        <location filename="controller/visitor/syncwithgit.cpp" line="180"/>
        <source>Cannot open external library &apos;%1&apos; for writing!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="controller/visitor/syncwithgit.cpp" line="35"/>
        <source>The shelf &apos;%1&apos; is not shared!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="controller/visitor/syncwithgit.cpp" line="38"/>
        <source>Syncing with remote...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="controller/visitor/syncwithgit.cpp" line="38"/>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="controller/visitor/syncwithgit.cpp" line="100"/>
        <source>Removed entry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="controller/visitor/syncwithgit.cpp" line="100"/>
        <source>Entry with bibkey %1 is removed from the shelf &apos;%2&apos; remotely. Confirm?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="controller/visitor/syncwithgit.cpp" line="143"/>
        <source>Internal error: Wrong structure of the git file!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/bibentry.cpp" line="227"/>
        <location filename="model/bibentry.cpp" line="235"/>
        <source>Internal error: Wrong column number %1!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/bibentry.cpp" line="243"/>
        <source>Internal error: Wrong file number %1!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/bibentry.cpp" line="251"/>
        <source>Internal error: Wrong entry type %1!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="32"/>
        <source>Cannot load SQLite driver!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="156"/>
        <source>Cannot initialize bibitems table!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="171"/>
        <location filename="model/model.cpp" line="177"/>
        <source>Cannot initialize shelfs table!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="187"/>
        <location filename="model/model.cpp" line="219"/>
        <location filename="model/model.cpp" line="251"/>
        <location filename="model/model.cpp" line="360"/>
        <location filename="model/model.cpp" line="760"/>
        <location filename="model/model.cpp" line="810"/>
        <source>Cannot initialize entry_shelf table!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="198"/>
        <source>Cannot initialize files table!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="207"/>
        <source>Cannot open the database &apos;%1&apos;!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="214"/>
        <source>Internal error: Empty hash!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="233"/>
        <location filename="model/model.cpp" line="244"/>
        <location filename="model/model.cpp" line="267"/>
        <location filename="model/model.cpp" line="302"/>
        <location filename="model/model.cpp" line="317"/>
        <location filename="model/model.cpp" line="333"/>
        <location filename="model/model.cpp" line="346"/>
        <location filename="model/model.cpp" line="376"/>
        <location filename="model/model.cpp" line="385"/>
        <location filename="model/model.cpp" line="785"/>
        <location filename="model/model.cpp" line="794"/>
        <location filename="model/model.cpp" line="896"/>
        <location filename="model/model.cpp" line="907"/>
        <location filename="model/model.cpp" line="924"/>
        <location filename="model/model.cpp" line="936"/>
        <location filename="model/model.cpp" line="952"/>
        <location filename="model/model.cpp" line="963"/>
        <location filename="model/model.cpp" line="1031"/>
        <location filename="model/model.cpp" line="1041"/>
        <location filename="model/model.cpp" line="1051"/>
        <location filename="model/model.cpp" line="1061"/>
        <location filename="model/model.cpp" line="1092"/>
        <location filename="model/model.cpp" line="1102"/>
        <location filename="model/model.cpp" line="1112"/>
        <location filename="model/model.cpp" line="1137"/>
        <location filename="model/model.cpp" line="1147"/>
        <source>Database rollback failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="236"/>
        <location filename="model/model.cpp" line="899"/>
        <source>Cannot save entry!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="246"/>
        <location filename="model/model.cpp" line="348"/>
        <location filename="model/model.cpp" line="387"/>
        <location filename="model/model.cpp" line="796"/>
        <location filename="model/model.cpp" line="965"/>
        <location filename="model/model.cpp" line="1063"/>
        <location filename="model/model.cpp" line="1114"/>
        <location filename="model/model.cpp" line="1149"/>
        <source>Database commit failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="336"/>
        <location filename="model/model.cpp" line="955"/>
        <source>Cannot add &apos;new&apos; associated groups for the entry!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="379"/>
        <source>Cannot add shelf! Do you already have one with the same name?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="414"/>
        <source>Cannot get group&apos;s &apos;%1&apos; id!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="432"/>
        <source>Cannot retrive number of entries!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="450"/>
        <location filename="model/model.cpp" line="466"/>
        <source>Cannot retrive number of entries in shelf &apos;%1&apos;!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="487"/>
        <source>Cannot retrive number of entries in shelf with id&apos;%1&apos;!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="505"/>
        <source>Cannot fetch shelf with id &apos;%1&apos;!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="534"/>
        <source>Cannot fetch entry with id &apos;%1&apos;!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="584"/>
        <source>Cannot fetch entry with hash &apos;%1&apos;!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="788"/>
        <source>Cannot update shelf with id &apos;%1&apos;!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="910"/>
        <source>Cannot remove &apos;old&apos; associated files for the entry!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="927"/>
        <source>Cannot add &apos;new&apos; associated files for the entry!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="939"/>
        <source>Cannot remove &apos;old&apos; associated shelfs for the entry!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="1022"/>
        <location filename="model/model.cpp" line="1083"/>
        <location filename="model/model.cpp" line="1127"/>
        <source>Cannot start transaction!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="model/model.cpp" line="1034"/>
        <location filename="model/model.cpp" line="1044"/>
        <location filename="model/model.cpp" line="1054"/>
        <location filename="model/model.cpp" line="1095"/>
        <location filename="model/model.cpp" line="1105"/>
        <location filename="model/model.cpp" line="1140"/>
        <source>Cannot delete entry!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QShelfsTreeView</name>
    <message>
        <location filename="view/dockshelfs/qshelfstreeview.cpp" line="20"/>
        <source>&amp;Add current entry to ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/qshelfstreeview.cpp" line="22"/>
        <source>&amp;New subshelf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/qshelfstreeview.cpp" line="24"/>
        <source>&amp;Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/qshelfstreeview.cpp" line="26"/>
        <source>&amp;Update external DB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/qshelfstreeview.cpp" line="28"/>
        <source>&amp;Syncronize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/qshelfstreeview.cpp" line="30"/>
        <source>&amp;Delete shelf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/dockshelfs/qshelfstreeview.cpp" line="32"/>
        <source>&amp;Clear shelf from entries</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShelfProperties</name>
    <message>
        <location filename="view/preferences/shelfproperties.ui" line="14"/>
        <location filename="view/shelfproperties/shelfproperties.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/shelfproperties.cpp" line="34"/>
        <location filename="view/shelfproperties/shelfproperties.cpp" line="34"/>
        <source>&amp;Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/shelfproperties.cpp" line="37"/>
        <location filename="view/preferences/shelfproperties.cpp" line="52"/>
        <location filename="view/shelfproperties/shelfproperties.cpp" line="37"/>
        <location filename="view/shelfproperties/shelfproperties.cpp" line="52"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/shelfproperties.cpp" line="45"/>
        <location filename="view/shelfproperties/shelfproperties.cpp" line="45"/>
        <source>&amp;Associated database:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/shelfproperties.cpp" line="47"/>
        <location filename="view/shelfproperties/shelfproperties.cpp" line="47"/>
        <source>&amp;Shared shelf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/shelfproperties.cpp" line="58"/>
        <location filename="view/shelfproperties/shelfproperties.cpp" line="58"/>
        <source>&amp;File in Git repository:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/shelfproperties.cpp" line="72"/>
        <location filename="view/shelfproperties/shelfproperties.cpp" line="72"/>
        <source>Parent shelf:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/shelfproperties.cpp" line="181"/>
        <location filename="view/shelfproperties/shelfproperties.cpp" line="181"/>
        <source>Save external database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/shelfproperties.cpp" line="181"/>
        <location filename="view/shelfproperties/shelfproperties.cpp" line="181"/>
        <source>BibTex Files (*.bib)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/shelfproperties.cpp" line="189"/>
        <location filename="view/shelfproperties/shelfproperties.cpp" line="189"/>
        <source>Choose new/existing file in Git repository</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="view/preferences/shelfproperties.cpp" line="189"/>
        <location filename="view/shelfproperties/shelfproperties.cpp" line="189"/>
        <source>Shelf sync files (*.ssf)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
