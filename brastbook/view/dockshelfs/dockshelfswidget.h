#ifndef DOCKSHELFSWIDGET_H
#define DOCKSHELFSWIDGET_H

#include <QDockWidget>
#include <QWidget>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QListView>
#include <QTreeView>
#include <QLabel>
#include <QPushButton>
#include <QSqlQueryModel>

#include "qshelfstreeview.h"
#include "../../controller/controller.h"
#include "../../controller/shelfcontroller.h"
#include "../../model/model.h"
#include "../../model/shelfentry.h"
#include "../dockinfo/dockinfowidget.h"

class DockShelfsWidget : public QDockWidget
{
    Q_OBJECT
public:
    DockShelfsWidget(QWidget *parent, Model * model, Controller * controller, ShelfController * shelfController, QAbstractItemModel * shelfsModel);

    ~DockShelfsWidget();

    ShelfEntry getSelectedShelf() { return selectedShelf; }

    void setDockInfoWidget(DockInfoWidget * widget) { dockInfoWidget = widget; };

signals:
    void bibentryIsChanged();

public slots:
    void processSelectedShelf(const QItemSelection & selected, const QItemSelection & previous);
    void showPropertiesDialog();
    void deleteCurrentShelf();
    void updateExternalDB();
    void exportShelf();
    void exportShelf(ShelfEntry & shelf2export);
    void clearShelf();
    void syncShelf();
    void updateExternalDB(ShelfEntry & shelf2export);
    void syncShelf(ShelfEntry & shelf2sync);
    void expandShelfs();
    void collapseShelfs();
    void saveExpansionState2Settings() { view->saveExpansionState2Settings(); };

    void addToShelf();
    void newSubShelf();

private:

    QWidget * baseWidget;
    QGridLayout * layout;
    QShelfsTreeView * view;

    Model * model;
    Controller * controller;
    ShelfController * shelfController;
    QAbstractItemModel * shelfsModel;

    QLabel *l1;
    QPushButton * pbSync;
    QPushButton * pbProj;
    QPushButton * pbShelfProperties;

    QPushButton * pbExpand;
    QPushButton * pbCollapse;

    ShelfEntry selectedShelf;
    DockInfoWidget * dockInfoWidget;
};

#endif // DOCKSHELFSWIDGET_H
