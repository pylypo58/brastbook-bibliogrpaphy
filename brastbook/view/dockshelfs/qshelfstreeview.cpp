#include <QSettings>
#include <QDebug>

#include "qshelfstreeview.h"
#include "../../model/qshelfmodel.h"

QShelfsTreeView::QShelfsTreeView(QAbstractItemModel * shelfsModel, int rootShelfId, QWidget * parent) :
    QTreeView(parent)
  , shelfsModel(shelfsModel)
  , rootShelfId(rootShelfId)
{
    setModel(shelfsModel);

    setSelectionMode(QAbstractItemView::SingleSelection);
    setHeaderHidden(true);
    expandAll();
    show();

    menu = new QMenu(this);

    aAdd = new QAction(tr("&Add current entry to ..."), this);
    connect(aAdd, SIGNAL(triggered()), this, SLOT(slotAddToShelf()));
    aNew = new QAction(tr("&New subshelf"), this);
    connect(aNew, SIGNAL(triggered()), this, SLOT(slotNewSubshelf()));
    aProperties = new QAction(tr("&Properties"), this);
    connect(aProperties, SIGNAL(triggered()), this, SLOT(slotModifyShelf()));
    aUpdate = new QAction(tr("&Update external DB"), this);
    connect(aUpdate, SIGNAL(triggered()), this, SLOT(slotUpdateDB()));
    aSync = new QAction(tr("&Syncronize"), this);
    connect(aSync, SIGNAL(triggered()), this, SLOT(slotSyncShelf()));
    aExport = new QAction(tr("&Export as..."), this);
    connect(aExport, SIGNAL(triggered()), this, SLOT(slotExportShelf()));
    aDelete = new QAction(tr("&Delete shelf"), this);
    connect(aDelete, SIGNAL(triggered()), this, SLOT(slotDeleteShelf()));
    aClear = new QAction(tr("&Clear shelf from entries"), this);
    connect(aClear, SIGNAL(triggered()), this, SLOT(slotClearShelf()));

    menu->addAction(aAdd);
    menu->addSeparator();
    menu->addAction(aUpdate);
    menu->addAction(aSync);
    menu->addAction(aExport);
    menu->addSeparator();
    menu->addAction(aNew);
    menu->addAction(aProperties);
    menu->addSeparator();
    menu->addAction(aClear);
    menu->addAction(aDelete);

    ShelfEntry shelf(ShelfEntry::NOT_EXITS);
    emit setRightClickedShelf(shelf);

    connect(this, SIGNAL(expanded(const QModelIndex &)), this, SLOT(shelfsTraversal()));
    connect(this, SIGNAL(collapsed(const QModelIndex &)), this, SLOT(shelfsTraversal()));

    QSettings settings;
    settings.beginGroup("ShelfsExpansionState");
    QStringList groupIDs = settings.allKeys();
    for (int i = 0; i < groupIDs.size(); i ++) {
        QString sgroupID = groupIDs[i].mid(4);
        int groupId = sgroupID.toInt();
        expansionState.insert(groupId, settings.value(groupIDs[i]).toBool());
    }
    settings.endGroup();
    restoringState = false;
    shelfsTraversal(true);
}


void QShelfsTreeView::contextMenuEvent(QContextMenuEvent *event) {
    try {
        ShelfEntry shelf = this->indexAt(event->pos()).data(Qt::UserRole).value<ShelfEntry>();
        emit setRightClickedShelf(shelf);

        if (shelf.getID() == rootShelfId) {
            aAdd->setDisabled(true);
            aProperties->setDisabled(true);
            aUpdate->setDisabled(true);
            aSync->setDisabled(true);
            aDelete->setDisabled(true);
            aClear->setDisabled(true);
        } else {
            aAdd->setEnabled(true);
            aProperties->setEnabled(true);
            aUpdate->setEnabled(true);
            aSync->setEnabled(true);
            aDelete->setEnabled(true);
            aClear->setEnabled(true);
        }
        menu->exec(event->globalPos());
    }  catch (...) {

    }

}

void QShelfsTreeView::mousePressEvent(QMouseEvent *event) {
    if (event->button() != Qt::RightButton)
    {
       QTreeView::mousePressEvent(event);
    }
}

//ShelfEntry QShelfsTreeView::getRightClickedShelf() {
//    ShelfEntry tmp = rightClickedShelf;

//    ShelfEntry tmp2 = emit setRightClickedShelf(shelf);
//    rightClickedShelf.setID(ShelfEntry::NOT_EXITS);

//    return tmp;
//}

void QShelfsTreeView::shelfsTraversal(QModelIndex & index, bool restore) {
    for (int i = 0; i < this->model()->rowCount(index); i ++) {
        QModelIndex idx = this->model()->index(i, 0, index);
        ShelfEntry shelf = idx.data(Qt::UserRole).value<ShelfEntry>();
        if (restore) {
            if (expansionState.contains(shelf.getID())) {
                this->setExpanded(idx, expansionState.value(shelf.getID()));
            } else {
                this->setExpanded(idx, true);
                expansionState.insert(shelf.getID(), true);
            }
        } else {
            expansionState.insert(shelf.getID(), this->isExpanded(idx));
        }
        shelfsTraversal(idx, restore);
    }
}

void QShelfsTreeView::shelfsTraversal(bool restore) {
    if (restoringState) {
        return;
    }

    if (restore && !restoringState) {
        restoringState = true;
    }

    if (!restore) {
        expansionState.clear();
    }

    // traversal over all shelfs
    for (int i = 0; i < this->model()->rowCount(); i ++) {
        QModelIndex idx = this->model()->index(i, 0);
        ShelfEntry shelf = idx.data(Qt::UserRole).value<ShelfEntry>();
        if (restore) {
            this->setExpanded(idx, expansionState.value(shelf.getID()));
        } else {
            expansionState.insert(shelf.getID(), this->isExpanded(idx));
        }
        shelfsTraversal(idx, restore);
    }
}

void QShelfsTreeView::restoreExpansionState() {
    restoringState = false;
    shelfsTraversal(true);
}

void QShelfsTreeView::saveExpansionState2Settings() {
    restoringState = false;
    shelfsTraversal();
    QSettings settings;
    settings.beginGroup("ShelfsExpansionState");
    for (auto i = expansionState.cbegin(), end = expansionState.cend(); i != end; i++) {
        settings.setValue("shlf" + QString::number(i.key()), i.value());
    }
    settings.endGroup();
}
