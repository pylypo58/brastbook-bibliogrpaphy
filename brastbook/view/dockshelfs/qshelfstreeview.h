#ifndef QSHELFSTREEVIEW_H
#define QSHELFSTREEVIEW_H

#include <QTreeView>
#include <QMenu>
#include <QContextMenuEvent>
#include <QAbstractItemModel>
#include <QMap>
#include <iostream>

#include "../../model/shelfentry.h"

class QShelfsTreeView : public QTreeView
{
    Q_OBJECT
public:
    QShelfsTreeView(QAbstractItemModel * shelfsModel, int rootShelfId, QWidget * parent = nullptr);

//    ShelfEntry getRightClickedShelf();

protected:
    void contextMenuEvent(QContextMenuEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);

signals:
    void addToShelf();
    void newSubshelf();
    void modifyShelf();
    void updateDB();
    void syncShelf();
    void exportShelf();
    void deleteShelf();
    void clearShelf();

    void setRightClickedShelf(ShelfEntry shelf);

public slots:
    void slotAddToShelf() { emit addToShelf(); };
    void slotNewSubshelf() { emit newSubshelf(); };
    void slotModifyShelf() { emit modifyShelf(); };
    void slotUpdateDB() { emit updateDB(); };
    void slotExportShelf() { emit exportShelf(); };
    void slotSyncShelf() { emit syncShelf(); };
    void slotDeleteShelf() { emit deleteShelf(); };
    void slotClearShelf() { emit clearShelf(); };
    void restoreExpansionState();
    void saveExpansionState2Settings();

private slots:
    void shelfsTraversal(bool restore = false);

private:
    void shelfsTraversal(QModelIndex & index, bool restore = false);

    QAbstractItemModel * shelfsModel;
    int rootShelfId;

    QMenu * menu;
    QAction * aAdd;
    QAction * aNew;
    QAction * aProperties;
    QAction * aUpdate;
    QAction * aSync;
    QAction * aExport;
    QAction * aDelete;
    QAction * aClear;

    QMap<int, bool> expansionState;
    bool restoringState;

//    ShelfEntry rightClickedShelf;
};

#endif // QSHELFSTREEVIEW_H
