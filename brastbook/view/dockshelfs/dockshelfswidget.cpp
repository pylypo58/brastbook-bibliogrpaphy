#include <QMessageBox>
#include <QFileInfo>
#include <QFileDialog>
#include <QDateTime>
#include <QSettings>

#include "dockshelfswidget.h"
#include "../../global.h"
#include "../shelfproperties/shelfproperties.h"
#include "../../controller/visitor/syncwithgit.h"

DockShelfsWidget::DockShelfsWidget(QWidget *parent, Model * model, Controller * controller, ShelfController * shelfController, QAbstractItemModel * shelfsModel) :
    QDockWidget(parent)
  , layout(nullptr)
  , view(nullptr)
  , model(model)
  , controller(controller)
  , shelfController(shelfController)
  , shelfsModel(shelfsModel)
  , selectedShelf(-1)
  , dockInfoWidget(nullptr)
{
    baseWidget = new QWidget(this);
    this->setAllowedAreas(Qt::AllDockWidgetAreas);
    this->setWindowTitle(tr("Shelfs"));
    setObjectName("dock-shelfs");

    layout = new QGridLayout(baseWidget);
    baseWidget->setLayout(layout);

    pbExpand = new QPushButton(tr("Expand"), this);
    pbExpand->setToolTip(tr("Expand all shelfs"));
    connect(pbExpand, SIGNAL(clicked()), this, SLOT(expandShelfs()));
    layout->addWidget(pbExpand, 0, 0);

    pbCollapse = new QPushButton(tr("Collapse"), this);
    pbCollapse->setToolTip(tr("Collapse all shelfs"));
    connect(pbCollapse, SIGNAL(clicked()), this, SLOT(collapseShelfs()));
    layout->addWidget(pbCollapse, 0, 1);

    view = new QShelfsTreeView(shelfsModel, model->getRootShelfId(), this);
    this->setWidget(view);
    layout->addWidget(view);
    connect(view, SIGNAL(addToShelf()), this, SLOT(addToShelf()));
    connect(view, SIGNAL(newSubshelf()), this, SLOT(newSubShelf()));
    connect(view, SIGNAL(modifyShelf()), this, SLOT(showPropertiesDialog()));
    connect(view, SIGNAL(updateDB()), this, SLOT(updateExternalDB()));
    connect(view, SIGNAL(syncShelf()), this, SLOT(syncShelf()));
    connect(view, SIGNAL(exportShelf()), this, SLOT(exportShelf()));
    connect(view, SIGNAL(deleteShelf()), this, SLOT(deleteCurrentShelf()));
    connect(view, SIGNAL(clearShelf()), this, SLOT(clearShelf()));
    connect(view, SIGNAL(setRightClickedShelf(ShelfEntry)), model, SLOT(setRightClickedShelf(ShelfEntry)));

    l1 = new QLabel("", this);
    pbSync = new QPushButton(tr("Sync"), this);
    pbSync->setToolTip(tr("Syncronize by Git"));
    pbProj = new QPushButton(tr("Proj."), this);
    pbProj->setToolTip(tr("Update the associated database"));
    layout->addWidget(view, 1, 0, 1, -1);
    layout->addWidget(l1, 2, 0, 1, -1);
    layout->addWidget(pbSync, 3, 0);
    layout->addWidget(pbProj, 3, 1);
    l1->show();
    pbSync->show();
    pbProj->show();
    pbSync->setDisabled(true);
    pbProj->setDisabled(true);
    connect(pbProj, SIGNAL(clicked()), this, SLOT(updateExternalDB()));
    connect(pbSync, SIGNAL(clicked()), this, SLOT(syncShelf()));
    pbShelfProperties = new QPushButton(tr("Properties"), this);
    layout->addWidget(pbShelfProperties, 4, 0, 1, -1);
    pbShelfProperties->show();
    pbShelfProperties->setDisabled(true);
    connect(pbShelfProperties, SIGNAL(clicked()), this, SLOT(showPropertiesDialog()));

    this->setMinimumWidth(200);
    this->setWidget(baseWidget);

//     connect(view, SIGNAL(clicked(const QModelIndex)), this, SLOT(processSelectedShelf(const QModelIndex &)));
//    connect(view, SIGNAL(activated(const QModelIndex)), this, SLOT(processSelectedShelf(const QModelIndex &)));
    connect(view->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)), this, SLOT(processSelectedShelf(const QItemSelection &, const QItemSelection &)));
    connect(model, SIGNAL(allShelfsAreChanged()), view, SLOT(restoreExpansionState()));
}

DockShelfsWidget::~DockShelfsWidget() {

}

void DockShelfsWidget::processSelectedShelf(const QItemSelection & selected, const QItemSelection & previous) {
    QModelIndex modelIndex = selected.indexes().at(0);

    selectedShelf = modelIndex.data(Qt::UserRole).value<ShelfEntry>();
    shelfController->setCurrentShelf(selectedShelf);

    if (selectedShelf.hash == ShelfEntry::ROOT_HASH) {
        shelfController->updateShelfView();

        l1->setText(tr("All shelfs (%1)").arg(model->getAllBibEntriesNum()));
        pbSync->setDisabled(true);
        pbProj->setDisabled(true);
        pbShelfProperties->setDisabled(true);
    } else {
        shelfController->updateShelfView(selectedShelf);

        QString shelfName = selectedShelf.name;
        if (shelfName.length() > 50) {
            shelfName = shelfName.mid(0, 40) + "..." + shelfName.mid(shelfName.length()-5);
        }
        l1->setText(tr("%1 (%2)").arg(shelfName).arg(model->getBibEntriesNumAtShelf(selectedShelf)));
        if (selectedShelf.shared) {
           pbSync->setEnabled(true);
        } else {
           pbSync->setDisabled(true);
        }
        if (selectedShelf.external_db_path.length() > 0) {
           pbProj->setEnabled(true);
        } else {
           pbProj->setDisabled(true);
        }
        pbShelfProperties->setDisabled(false);
    }
}

void DockShelfsWidget::showPropertiesDialog() {
    ShelfEntry shelf2process = model->getRightClickedShelf();
    if (shelf2process.getID() == ShelfEntry::NOT_EXITS) {
        shelf2process = selectedShelf;
    }

    if (shelf2process.getID() <= 0) {
        return;
    }

    ShelfProperties shelfProps(shelf2process, model->getShelfsModel(), DBVars::getInstance().getFilePath(), this, tr("Shelf properties"));
    shelfProps.exec();

    if (shelfProps.result() == QDialog::Accepted) {
        try {
            shelf2process = shelfProps.getShelfState();
            shelfController->updateShelf(shelf2process);
        } catch (std::exception & e) {
            QMessageBox::critical(this, APP_NAME, e.what());
        } catch (...) {
            QMessageBox::critical(this, APP_NAME, tr("Unpredicted exception during selection of the shelf."));
        }
    }
}

void DockShelfsWidget::newSubShelf() {
    ShelfEntry shelf2process = model->getRightClickedShelf();
    if (shelf2process.getID() < 0) {
        return;
    }

    ShelfEntry newShelf(ShelfEntry::DEFAULT);

    newShelf.parent = shelf2process.getID();
    ShelfProperties shelfProps(newShelf, model->getShelfsModel(), DBVars::getInstance().getFilePath(), this, tr("New shelf"));
    shelfProps.exec();

    if (shelfProps.result() == QDialog::Accepted) {
        try {
            newShelf = shelfProps.getShelfState();
            newShelf.setHash();
            shelfController->addShelf(newShelf);
        } catch (std::exception & e) {
            QMessageBox::critical(this, APP_NAME, e.what());
        } catch (...) {
            QMessageBox::critical(this, APP_NAME, tr("Unpredicted exception during adding of a new shelf."));
        }
    }
}

void DockShelfsWidget::deleteCurrentShelf() {
    ShelfEntry shelf2process = model->getRightClickedShelf();
    if (shelf2process.getID() == ShelfEntry::NOT_EXITS) {
        shelf2process = selectedShelf;
    }

    if (QMessageBox::question(this, tr("Delete entry"), tr("Delete shelf '%1'?").arg(shelf2process.name), QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes) {
        shelfController->deleteShelf(shelf2process);
    }
}

void DockShelfsWidget::clearShelf() {
    ShelfEntry shelf2process = model->getRightClickedShelf();
    if (shelf2process.getID() == ShelfEntry::NOT_EXITS) {
        return;
    }

    if (QMessageBox::question(this, tr("Delete entry"), tr("Remove all entries from shelf '%1'?").arg(shelf2process.name), QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes) {
        shelfController->clearShelf(shelf2process);
    }
}

void DockShelfsWidget::exportShelf() {
    ShelfEntry tmp(ShelfEntry::NOT_EXITS);
    exportShelf(tmp);
}

void DockShelfsWidget::exportShelf(ShelfEntry & shelf2export) {
    ShelfEntry shelf2process = shelf2export;
    if (shelf2process.getID() == ShelfEntry::NOT_EXITS) {
        shelf2process = model->getRightClickedShelf();
        if (shelf2process.getID() == ShelfEntry::NOT_EXITS) {
            shelf2process = selectedShelf;
        }
    }

    QString fileName = QFileDialog::getSaveFileName(this, tr("Export Shelf"), "", tr("BibTeX [*.bib] (*.bib)"));

    QDateTime date = QDateTime::currentDateTime();
    QString header =  "Group \"" + shelf2process.name + "\"\nExported at " + date.toString("yyyy.MM.dd hh:mm:ss");
    controller->exportShelf2File(shelf2process, fileName, header);
}

void DockShelfsWidget::updateExternalDB() {
    ShelfEntry tmp(ShelfEntry::NOT_EXITS);
    updateExternalDB(tmp);
}

void DockShelfsWidget::updateExternalDB(ShelfEntry & shelf2export) {
    ShelfEntry shelf2process = shelf2export;
    if (shelf2process.getID() == ShelfEntry::NOT_EXITS) {
        shelf2process = model->getRightClickedShelf();
        if (shelf2process.getID() == ShelfEntry::NOT_EXITS) {
            shelf2process = selectedShelf;
        }
    }

    try {
        controller->updateAssociatedDB(shelf2process);
    } catch (std::exception & e) {
        QMessageBox::critical(this, APP_NAME, e.what());
    } catch (...) {
        QMessageBox::critical(this, APP_NAME, tr("Unpredicted exception during updating external DB for '%1' shelf.").arg(shelf2process.name));
    }

    QSettings settings;
    settings.beginGroup("General");
    if (!settings.value("SilentExtUpd", false).toBool()) {
        QMessageBox::information(this, APP_NAME, tr("External project has been updated!"));
    }
    settings.endGroup();
}

void DockShelfsWidget::syncShelf() {
    ShelfEntry tmp(ShelfEntry::NOT_EXITS);
    syncShelf(tmp);
}

void DockShelfsWidget::syncShelf(ShelfEntry & shelf2sync) {
    ShelfEntry shelf2process = shelf2sync;
    if (shelf2process.getID() == ShelfEntry::NOT_EXITS) {
        shelf2process = model->getRightClickedShelf();
        if (shelf2process.getID() == ShelfEntry::NOT_EXITS) {
            shelf2process = selectedShelf;
        }
    }

    QSettings settings;
    settings.beginGroup("Git");
    SyncInfo syncInfo;
    syncInfo.command = settings.value("gitPath", "git.exe").toString();
    syncInfo.commit = settings.value("commitCommand", "commit -a").toString();
    syncInfo.commitMsg = settings.value("commitMessage", "shelf update").toString();
    syncInfo.pull = settings.value("pullCommand", "pull -X theirs").toString();
    syncInfo.push = settings.value("pushCommand", "push").toString();
    settings.endGroup();

    try {
        shelfController->syncShelf(shelf2process, syncInfo, selectedShelf);
        QMessageBox::information(this, APP_NAME, tr("Shelf has been syncronized!"));
    } catch (std::exception & e) {
        QMessageBox::critical(this, APP_NAME, e.what());
    } catch (...) {
        QMessageBox::critical(this, APP_NAME, tr("Unpredicted exception during shelf '%1' syncronization.").arg(shelf2process.name));
    }

}

void DockShelfsWidget::expandShelfs() {
    view->expandAll();
}

void DockShelfsWidget::collapseShelfs() {
    view->collapseAll();
    view->expandToDepth(0);
}

void DockShelfsWidget::addToShelf() {
    if (!dockInfoWidget) {
        QMessageBox::information(this, APP_NAME, tr("Select bibliographical entry first!"));
        return;
    }
    ShelfEntry shelf2process = model->getRightClickedShelf();
    if (shelf2process.getID() == ShelfEntry::NOT_EXITS) {
        QMessageBox::information(this, APP_NAME, tr("Select shelf first!"));
        return;
    }

    BibEntry entry = dockInfoWidget->getCurrentEntry();
    entry.addShelfID(shelf2process.getID());
    model->updateBibEntry(entry, selectedShelf);
    dockInfoWidget->setBibEntry(entry);
    emit bibentryIsChanged();


    QSettings settings;
    settings.beginGroup("General");
    if (settings.value("AutoExtUpd", false).toBool()) {
        updateExternalDB(shelf2process);
    }
    settings.endGroup();
}
