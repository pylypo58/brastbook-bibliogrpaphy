#ifndef CURRENTSHELFVIEW_H
#define CURRENTSHELFVIEW_H

#include <QTableView>
#include <QHeaderView>
#include <QMenu>
#include <QContextMenuEvent>
#include <QAbstractItemModel>

#include "../model/model.h"
#include "../model/bibentry.h"

class CurrentShelfView : public QTableView
{
    Q_OBJECT
public:
    CurrentShelfView(Model * mvc_model, QWidget * parent = nullptr);

protected:
    void contextMenuEvent(QContextMenuEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);

signals:
    void modify(BibEntry entry);

protected slots:
    void copyAsBibTeX();
    void copyAsPreset();
    void openFile();
    void openURL();
    void slotModify();

private:
    Model * mvc_model;

    QMenu * menu;
    QMenu * mCopy;
    QAction * aCopyBibTeX;
    QAction * aCopyPreset;
    QAction * aOpenFile;
    QAction * aOpenURL;
    QAction * aModify;

    BibEntry rightClickedEntry;
};

#endif // CURRENTSHELFVIEW_H
