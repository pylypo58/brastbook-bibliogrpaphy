#include <QFileInfo>
#include "lastopenedmenu.h"

LastOpenedMenu::LastOpenedMenu(QString filePath, QWidget *parent) :
    QAction(parent)
  , filePath(filePath)
{
    QFileInfo fi(filePath);
    this->setText(fi.baseName());
//    this->setTitle(fi.baseName());
    this->setToolTip(filePath);
}
