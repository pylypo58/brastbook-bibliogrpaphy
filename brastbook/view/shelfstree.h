#ifndef SHELFSTREE_H
#define SHELFSTREE_H

#include <QTreeWidget>
#include <QObject>

class ShelfsTree : public QTreeWidget
{
    Q_OBJECT
public:
    ShelfsTree(QWidget *parent = nullptr);
};

#endif // SHELFSTREE_H
