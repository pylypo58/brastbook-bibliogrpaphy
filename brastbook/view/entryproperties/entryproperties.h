#ifndef ENTRYPROPERTIES_H
#define ENTRYPROPERTIES_H

#include <QDialog>
#include <QFormLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QTextEdit>
#include <QLineEdit>
#include <QSqlQueryModel>

#include "../../model/bibentry.h"
#include "../../model/model.h"

namespace Ui {
class EntryProperties;
}

class EntryProperties : public QDialog
{
    Q_OBJECT

public:
    explicit EntryProperties(BibEntry & entry, Model * model, QWidget *parent = nullptr, QString title = "");
    ~EntryProperties();

    void resizeEvent(QResizeEvent *event) override;

public slots:
    void setupLayouts(int entrytype);
    BibEntry getEntryState();

    void accept() override;
    void reject() override;

private slots:
    void on_lwEntryTypes_currentRowChanged(int currentRow);

    void on_pbAddFile_clicked();

    void on_pbRemoveFile_clicked();

    void on_pbDeleteFromDisk_clicked();

    void on_pbAddToShelf_clicked();

    void on_pbRemoveFromShelf_clicked();

private:
    const int dist = 10;

    bool isShelfAlreadyAssigned(const int shelf_id);

    Ui::EntryProperties *ui;

    BibEntry entry;
    QList<ShelfEntry> usedShelfs;

    Model * model;

    QFormLayout * mainLayout;
    QFormLayout * additionalLayout;

    QTextEdit * teAbstract;
    QTextEdit * teNote;

    QWidget * parentWidget;

    QLineEdit * leBibkey;
    QLineEdit * leAddress;
    QLineEdit * leAnnote;
    QTextEdit * teAuthor;
    QTextEdit * teBooktitle;
    QTextEdit * teChapter;
    QLineEdit * leCrossref;
    QLineEdit * leEdition;
    QLineEdit * leEditor;
    QLineEdit * leHowpublished;
    QLineEdit * leInstitution;
    QLineEdit * leJournal;
//    QLineEdit * leKey;
    QLineEdit * leMonth;
    QLineEdit * leNumber;
    QLineEdit * leOrganization;
    QLineEdit * lePages;
    QLineEdit * lePublisher;
    QLineEdit * leSchool;
    QLineEdit * leSeries;
    QTextEdit * teTitle;
    QLineEdit * leType;
    QLineEdit * leVolume;
    QLineEdit * leYear;
    QLineEdit * leDOI;
    QLineEdit * leISSN;
    QLineEdit * leISBN;
    QLineEdit * leURL;
    QLineEdit * leDate;
    QLineEdit * leTimestamp;
    QLineEdit * leLanguage;
    QLineEdit * leOwner;
    QLineEdit * leEditedby;
    QLineEdit * leEditedat;

    QLabel * lBibkey;
    QLabel * lAddress;
    QLabel * lAnnote;
    QLabel * lAuthor;
    QLabel * lBooktitle;
    QLabel * lChapter;
    QLabel * lCrossref;
    QLabel * lEdition;
    QLabel * lEditor;
    QLabel * lHowpublished;
    QLabel * lInstitution;
    QLabel * lJournal;
//    QLabel * lKey;
    QLabel * lMonth;
    QLabel * lNumber;
    QLabel * lOrganization;
    QLabel * lPages;
    QLabel * lPublisher;
    QLabel * lSchool;
    QLabel * lSeries;
    QLabel * lTitle;
    QLabel * lType;
    QLabel * lVolume;
    QLabel * lYear;
    QLabel * lDOI;
    QLabel * lISSN;
    QLabel * lISBN;
    QLabel * lURL;
    QLabel * lDate;
    QLabel * lTimestamp;
    QLabel * lLanguage;
    QLabel * lOwner;
    QLabel * lEditedby;
    QLabel * lEditedat;
};

#endif // ENTRYPROPERTIES_H
