#include <QDateTime>
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>

#include "entryproperties.h"
#include "ui_entryproperties.h"

EntryProperties::EntryProperties(BibEntry & entry, Model * model, QWidget *parent, QString title) :
    QDialog(parent),
    ui(new Ui::EntryProperties)
  , entry(entry)
  , model(model)
  , mainLayout(nullptr)
  , additionalLayout(nullptr)
  , parentWidget(nullptr)
{
    ui->setupUi(this);

    this->setWindowTitle(title);

    QSettings settings;
    settings.beginGroup("EntryPropertiesWindow");
    resize(settings.value("size", QSize(742, 423)).toSize());
    move(settings.value("position", QPoint(100, 100)).toPoint());
    settings.endGroup();

    QVBoxLayout * vbAbsLayout = new QVBoxLayout(ui->tabAbstract);
    teAbstract = new QTextEdit(entry.getField(BibEntry::ABSTRACT), this);
    vbAbsLayout->addWidget(teAbstract);
//    ui->tabAbstract->setLayout(vbAbsLayout);

    QVBoxLayout * vbNoteLayout = new QVBoxLayout(ui->tabNote);
    teNote = new QTextEdit(entry.getField(BibEntry::NOTE), this);
    vbNoteLayout->addWidget(teNote);
//    ui->tabNote->setLayout(vbNoteLayout);

    ui->lwEntryTypes->setCurrentRow(entry.getEntryType());

    ui->lwFiles->clear();
    const QStringList files = entry.getFiles();
    for (int i = 0; i < files.length(); i ++) {
        ui->lwFiles->addItem(files[i]);
    }
    ui->lwFiles->setDragDropMode(QAbstractItemView::InternalMove);

    ui->tvShelfs->setModel(model->getShelfsModel());
    ui->tvShelfs->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tvShelfs->setHeaderHidden(true);
    ui->tvShelfs->expandAll();

    ui->lwUsedShelfs->clear();
    usedShelfs = model->getShelfsForEntry(entry.getID());
    foreach (auto shelf, usedShelfs) {
        auto * item = new QListWidgetItem(shelf.name, ui->lwUsedShelfs);
        item->setData(Qt::UserRole, QVariant::fromValue(shelf));
        ui->lwUsedShelfs->addItem(item);
    }
    if (entry.getID() == BibEntry::NOT_EXISTS && model->getDefaultShelfId() != model->getRootShelfId()) {
        ShelfEntry shelf = model->getShelfEntry(model->getDefaultShelfId());
        auto * item = new QListWidgetItem(shelf.name, ui->lwUsedShelfs);
        item->setData(Qt::UserRole, QVariant::fromValue(shelf));
        ui->lwUsedShelfs->addItem(item);
        usedShelfs.append(shelf);

        ShelfEntry currentShelf = model->getCurrentShelf();
        if (currentShelf.getID() != model->getRootShelfId() && !usedShelfs.contains(currentShelf)) {
            auto * item = new QListWidgetItem(currentShelf.name, ui->lwUsedShelfs);
            item->setData(Qt::UserRole, QVariant::fromValue(currentShelf));
            ui->lwUsedShelfs->addItem(item);
            usedShelfs.append(currentShelf);
        }
    }

    ui->lHash->setText(tr("Hash: ") + entry.getField(BibEntry::HASH));

    resizeEvent(nullptr);
}

EntryProperties::~EntryProperties()
{
    delete ui;
}

void EntryProperties::setupLayouts(int entrytype) {
    if (mainLayout) {
        QLayoutItem * item;
        while ((item = mainLayout->takeAt(0)) != nullptr)
            delete item;
        while ((item = additionalLayout->takeAt(0)) != nullptr)
            delete item;
        delete leBibkey;
        delete leAddress;
        delete leAnnote;
        delete teAuthor;
        delete teBooktitle;
        delete teChapter;
        delete leCrossref;
        delete leEdition;
        delete leEditor;
        delete leHowpublished;
        delete leInstitution;
        delete leJournal;
//        delete leKey;
        delete leMonth;
        delete leNumber;
        delete leOrganization;
        delete lePages;
        delete lePublisher;
        delete leSchool;
        delete leSeries;
        delete teTitle;
        delete leType;
        delete leVolume;
        delete leYear;
        delete leDOI;
        delete leISSN;
        delete leISBN;
        delete leURL;
        delete leDate;
        delete leTimestamp;
        delete leLanguage;
        delete leOwner;
        delete leEditedby;
        delete leEditedat;
        delete parentWidget;

        delete lBibkey;
        delete lAddress;
        delete lAnnote;
        delete lAuthor;
        delete lBooktitle;
        delete lChapter;
        delete lCrossref;
        delete lEdition;
        delete lEditor;
        delete lHowpublished;
        delete lInstitution;
        delete lJournal;
//        delete lKey;
        delete lMonth;
        delete lNumber;
        delete lOrganization;
        delete lPages;
        delete lPublisher;
        delete lSchool;
        delete lSeries;
        delete lTitle;
        delete lType;
        delete lVolume;
        delete lYear;
        delete lDOI;
        delete lISSN;
        delete lISBN;
        delete lURL;
        delete lDate;
        delete lTimestamp;
        delete lLanguage;
        delete lOwner;
        delete lEditedby;
        delete lEditedat;

        delete mainLayout;
        delete additionalLayout;
    }

    mainLayout = new QFormLayout(ui->scrollAreaWidgetContents);
    additionalLayout = new QFormLayout(ui->scrollAreaWidgetContents_2);

    parentWidget = new QWidget(this);
    leBibkey = new QLineEdit(entry.getField(BibEntry::BIBKEY), parentWidget);
    leAddress = new QLineEdit(entry.getField(BibEntry::ADDRESS), parentWidget);
    leAnnote = new QLineEdit(entry.getField(BibEntry::ANNOTE), parentWidget);
    teAuthor = new QTextEdit(entry.getField(BibEntry::AUTHOR), parentWidget);
    teAuthor->setFixedHeight(60);
    teBooktitle = new QTextEdit(entry.getField(BibEntry::BOOKTITLE), parentWidget);
    teBooktitle->setFixedHeight(60);
    teChapter = new QTextEdit(entry.getField(BibEntry::CHAPTER), parentWidget);
    teChapter->setFixedHeight(60);
    leCrossref = new QLineEdit(entry.getField(BibEntry::CROSSREF), parentWidget);
    leEdition = new QLineEdit(entry.getField(BibEntry::EDITION), parentWidget);
    leEditor = new QLineEdit(entry.getField(BibEntry::EDITOR), parentWidget);
    leHowpublished = new QLineEdit(entry.getField(BibEntry::HOWPUBLISHED), parentWidget);
    leInstitution = new QLineEdit(entry.getField(BibEntry::INSTITUTION), parentWidget);
    leJournal = new QLineEdit(entry.getField(BibEntry::JOURNAL), parentWidget);
//    leKey = new QLineEdit(entry.key, parentWidget);
    leMonth = new QLineEdit(entry.getField(BibEntry::MONTH), parentWidget);
    leNumber = new QLineEdit(entry.getField(BibEntry::NUMBER), parentWidget);
    leOrganization = new QLineEdit(entry.getField(BibEntry::ORGANIZATION), parentWidget);
    lePages = new QLineEdit(entry.getField(BibEntry::PAGES), parentWidget);
    lePublisher = new QLineEdit(entry.getField(BibEntry::PUBLISHER), parentWidget);
    leSchool = new QLineEdit(entry.getField(BibEntry::SCHOOL), parentWidget);
    leSeries = new QLineEdit(entry.getField(BibEntry::SERIES), parentWidget);
    teTitle = new QTextEdit(entry.getField(BibEntry::TITLE), parentWidget);
    teTitle->setFixedHeight(60);
    leType = new QLineEdit(entry.getField(BibEntry::TYPE), parentWidget);
    leVolume = new QLineEdit(entry.getField(BibEntry::VOLUME), parentWidget);
    leYear = new QLineEdit(entry.getField(BibEntry::YEAR), parentWidget);
    leDOI = new QLineEdit(entry.getField(BibEntry::DOI), parentWidget);
    leISSN = new QLineEdit(entry.getField(BibEntry::ISSN), parentWidget);
    leISBN = new QLineEdit(entry.getField(BibEntry::ISBN), parentWidget);
    leURL = new QLineEdit(entry.getField(BibEntry::URL), parentWidget);
    leDate = new QLineEdit(entry.getField(BibEntry::DATE), parentWidget);
    leTimestamp = new QLineEdit(entry.getField(BibEntry::TIMESTAMP), parentWidget);
    if (entry.getField(BibEntry::TIMESTAMP).length() == 0) {
        leTimestamp->setText(QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss"));
    }
    leLanguage = new QLineEdit(entry.getField(BibEntry::LANGUAGE), parentWidget);
    leOwner = new QLineEdit(entry.getField(BibEntry::OWNER), parentWidget);
    if (entry.getField(BibEntry::OWNER).length() == 0) {
        leOwner->setText(DBVars::getInstance().getOwner());
    }
    leEditedby = new QLineEdit(DBVars::getInstance().getOwner(), parentWidget);
    leEditedat = new QLineEdit(QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss"), parentWidget);



    lBibkey = new QLabel(tr("BibTex key:"));
    lAddress = new QLabel(tr("Address:"));
    lAnnote = new QLabel(tr("Annote:"));
    lAuthor = new QLabel(tr("Author(s):"));
    lBooktitle = new QLabel(tr("Book title:"));
    lChapter = new QLabel(tr("Chapter:"));
    lCrossref = new QLabel(tr("Crossref:"));
    lEdition = new QLabel(tr("Edition:"));
    lEditor = new QLabel(tr("Editor:"));
    lHowpublished = new QLabel(tr("How published:"));
    lInstitution = new QLabel(tr("Institution:"));
    lJournal = new QLabel(tr("Journal:"));
//    lKey = new QLabel(tr("Key:"));
    lMonth = new QLabel(tr("Month:"));
    lNumber = new QLabel(tr("Number:"));
    lOrganization = new QLabel(tr("Organization:"));
    lPages = new QLabel(tr("Pages:"));
    lPublisher = new QLabel(tr("Publisher:"));
    lSchool = new QLabel(tr("School:"));
    lSeries = new QLabel(tr("Series:"));
    lTitle = new QLabel(tr("Title:"));
    lType = new QLabel(tr("Type:"));
    lVolume = new QLabel(tr("Volume:"));
    lYear = new QLabel(tr("Year:"));
    lDOI = new QLabel(tr("DOI:"));
    lISSN = new QLabel(tr("ISSN:"));
    lISBN = new QLabel(tr("ISBN:"));
    lURL = new QLabel(tr("URL:"));
    lDate = new QLabel(tr("Date:"));
    lTimestamp = new QLabel(tr("Timestamp:"));
    lLanguage = new QLabel(tr("Language:"));
    lOwner = new QLabel(tr("Owner:"));
    lEditedby = new QLabel(tr("Edited by:"));
    lEditedat = new QLabel(tr("Edited at:"));


//    switch (entrytype) {
    if (entrytype == BibEntry::BET_ARTICLE) {
        mainLayout->addRow(lBibkey, leBibkey);
        mainLayout->addRow(lAuthor, teAuthor);
        mainLayout->addRow(lTitle, teTitle);
        mainLayout->addRow(lJournal, leJournal);
        mainLayout->addRow(lYear, leYear);
        mainLayout->addRow(lVolume, leVolume);
        mainLayout->addRow(lNumber, leNumber);
        mainLayout->addRow(lPages, lePages);
        mainLayout->addRow(lURL, leURL);
        mainLayout->addRow(lDOI, leDOI);

        QWidget::setTabOrder(leBibkey, teAuthor);
        QWidget::setTabOrder(teAuthor, teTitle);
        QWidget::setTabOrder(teTitle, leJournal);
        QWidget::setTabOrder(leJournal, leYear);
        QWidget::setTabOrder(leYear, leVolume);
        QWidget::setTabOrder(leVolume, leNumber);
        QWidget::setTabOrder(leNumber, lePages);
        QWidget::setTabOrder(lePages, leURL);
        QWidget::setTabOrder(leURL, leDOI);

        additionalLayout->addRow(lAddress, leAddress);
        additionalLayout->addRow(lAnnote, leAnnote);
        additionalLayout->addRow(lBooktitle, teBooktitle);
        additionalLayout->addRow(lChapter, teChapter);
        additionalLayout->addRow(lCrossref, leCrossref);
        additionalLayout->addRow(lDate, leDate);
        additionalLayout->addRow(lEdition, leEdition);
        additionalLayout->addRow(lEditor, leEditor);
        additionalLayout->addRow(lHowpublished, leHowpublished);
        additionalLayout->addRow(lISBN, leISBN);
        additionalLayout->addRow(lISSN, leISSN);
        additionalLayout->addRow(lInstitution, leInstitution);
        additionalLayout->addRow(lLanguage, leLanguage);
        additionalLayout->addRow(lMonth, leMonth);
        additionalLayout->addRow(lOrganization, leOrganization);
        additionalLayout->addRow(lPublisher, lePublisher);
        additionalLayout->addRow(lSchool, leSchool);
        additionalLayout->addRow(lSeries, leSeries);
        additionalLayout->addRow(lType, leType);

        additionalLayout->addRow(lOwner, leOwner);
        additionalLayout->addRow(lTimestamp, leTimestamp);
        additionalLayout->addRow(lEditedby, leEditedby);
        additionalLayout->addRow(lEditedat, leEditedat);

        QWidget::setTabOrder(leAddress, leAnnote);
        QWidget::setTabOrder(leAnnote, teBooktitle);
        QWidget::setTabOrder(teBooktitle, teChapter);
        QWidget::setTabOrder(teChapter, leCrossref);
        QWidget::setTabOrder(leCrossref, leDate);
        QWidget::setTabOrder(leDate, leEdition);
        QWidget::setTabOrder(leEdition, leEditor);
        QWidget::setTabOrder(leEditor, leHowpublished);
        QWidget::setTabOrder(leHowpublished, leISBN);
        QWidget::setTabOrder(leISBN, leISSN);
        QWidget::setTabOrder(leISSN, leInstitution);
        QWidget::setTabOrder(leInstitution, leLanguage);
        QWidget::setTabOrder(leLanguage, leMonth);
        QWidget::setTabOrder(leMonth, leOrganization);
        QWidget::setTabOrder(leOrganization, lePublisher);
        QWidget::setTabOrder(lePublisher, leSchool);
        QWidget::setTabOrder(leSchool, leSeries);
        QWidget::setTabOrder(leSeries, leType);
        QWidget::setTabOrder(leType, leOwner);
        QWidget::setTabOrder(leOwner, leTimestamp);
        QWidget::setTabOrder(leTimestamp, leEditedby);
        QWidget::setTabOrder(leEditedby, leEditedat);

    } else if (entrytype == BibEntry::BET_BOOK) {
        mainLayout->addRow(lBibkey, leBibkey);
        mainLayout->addRow(lAuthor, teAuthor);
        mainLayout->addRow(lEditor, leEditor);
        mainLayout->addRow(lTitle, teTitle);
        mainLayout->addRow(lPublisher, lePublisher);
        mainLayout->addRow(lYear, leYear);
        mainLayout->addRow(lVolume, leVolume);
        mainLayout->addRow(lNumber, leNumber);
        mainLayout->addRow(lSeries, leSeries);
        mainLayout->addRow(lAddress, leAddress);
        mainLayout->addRow(lEdition, leEdition);
        mainLayout->addRow(lMonth, leMonth);
        mainLayout->addRow(lURL, leURL);
        mainLayout->addRow(lDOI, leDOI);

        QWidget::setTabOrder(leBibkey, teAuthor);
        QWidget::setTabOrder(teAuthor, leEditor);
        QWidget::setTabOrder(leEditor, teTitle);
        QWidget::setTabOrder(teTitle, lePublisher);
        QWidget::setTabOrder(lePublisher, leYear);
        QWidget::setTabOrder(leYear, leVolume);
        QWidget::setTabOrder(leVolume, leNumber);
        QWidget::setTabOrder(leNumber, leSeries);
        QWidget::setTabOrder(leSeries, leAddress);
        QWidget::setTabOrder(leAddress, leEdition);
        QWidget::setTabOrder(leEdition, leMonth);
        QWidget::setTabOrder(leMonth, leURL);
        QWidget::setTabOrder(leURL, leDOI);

        additionalLayout->addRow(lAnnote, leAnnote);
        additionalLayout->addRow(lBooktitle, teBooktitle);
        additionalLayout->addRow(lChapter, teChapter);
        additionalLayout->addRow(lCrossref, leCrossref);
        additionalLayout->addRow(lDate, leDate);
        additionalLayout->addRow(lHowpublished, leHowpublished);
        additionalLayout->addRow(lISBN, leISBN);
        additionalLayout->addRow(lISSN, leISSN);
        additionalLayout->addRow(lInstitution, leInstitution);
        additionalLayout->addRow(lJournal, leJournal);
        additionalLayout->addRow(lLanguage, leLanguage);
        additionalLayout->addRow(lOrganization, leOrganization);
        additionalLayout->addRow(lPages, lePages);
        additionalLayout->addRow(lSchool, leSchool);
        additionalLayout->addRow(lType, leType);

        additionalLayout->addRow(lOwner, leOwner);
        additionalLayout->addRow(lTimestamp, leTimestamp);
        additionalLayout->addRow(lEditedby, leEditedby);
        additionalLayout->addRow(lEditedat, leEditedat);

        QWidget::setTabOrder(leAnnote, teBooktitle);
        QWidget::setTabOrder(teBooktitle, teChapter);
        QWidget::setTabOrder(teChapter, leCrossref);
        QWidget::setTabOrder(leCrossref, leDate);
        QWidget::setTabOrder(leDate, leHowpublished);
        QWidget::setTabOrder(leHowpublished, leISBN);
        QWidget::setTabOrder(leISBN, leISSN);
        QWidget::setTabOrder(leISSN, leInstitution);
        QWidget::setTabOrder(leInstitution, leJournal);
        QWidget::setTabOrder(leJournal, leLanguage);
        QWidget::setTabOrder(leLanguage, leOrganization);
        QWidget::setTabOrder(leOrganization, lePages);
        QWidget::setTabOrder(lePages, leSchool);
        QWidget::setTabOrder(leSchool, leType);
        QWidget::setTabOrder(leType, leOwner);
        QWidget::setTabOrder(leOwner, leTimestamp);
        QWidget::setTabOrder(leTimestamp, leEditedby);
        QWidget::setTabOrder(leEditedby, leEditedat);

    } else if (entrytype == BibEntry::BET_BOOKLET) {
        mainLayout->addRow(lBibkey, leBibkey);
        mainLayout->addRow(lTitle, teTitle);
        mainLayout->addRow(lAuthor, teAuthor);
        mainLayout->addRow(lHowpublished, leHowpublished);
        mainLayout->addRow(lAddress, leAddress);
        mainLayout->addRow(lMonth, leMonth);
        mainLayout->addRow(lYear, leYear);
        mainLayout->addRow(lURL, leURL);
        mainLayout->addRow(lDOI, leDOI);

        additionalLayout->addRow(lAnnote, leAnnote);
        additionalLayout->addRow(lBooktitle, teBooktitle);
        additionalLayout->addRow(lChapter, teChapter);
        additionalLayout->addRow(lCrossref, leCrossref);
        additionalLayout->addRow(lDate, leDate);
        additionalLayout->addRow(lEdition, leEdition);
        additionalLayout->addRow(lEditor, leEditor);
        additionalLayout->addRow(lISBN, leISBN);
        additionalLayout->addRow(lISSN, leISSN);
        additionalLayout->addRow(lInstitution, leInstitution);
        additionalLayout->addRow(lJournal, leJournal);
        additionalLayout->addRow(lLanguage, leLanguage);
        additionalLayout->addRow(lNumber, leNumber);
        additionalLayout->addRow(lOrganization, leOrganization);
        additionalLayout->addRow(lPages, lePages);
        additionalLayout->addRow(lPublisher, lePublisher);
        additionalLayout->addRow(lSchool, leSchool);
        additionalLayout->addRow(lSeries, leSeries);
        additionalLayout->addRow(lType, leType);
        additionalLayout->addRow(lVolume, leVolume);

        additionalLayout->addRow(lOwner, leOwner);
        additionalLayout->addRow(lTimestamp, leTimestamp);
        additionalLayout->addRow(lEditedby, leEditedby);
        additionalLayout->addRow(lEditedat, leEditedat);

    } else if (entrytype == BibEntry::BET_CONFERENCE) {
        mainLayout->addRow(lBibkey, leBibkey);
        mainLayout->addRow(lAuthor, teAuthor);
        mainLayout->addRow(lTitle, teTitle);
        mainLayout->addRow(lBooktitle, teBooktitle);
        mainLayout->addRow(lYear, leYear);
        mainLayout->addRow(lEditor, leEditor);
        mainLayout->addRow(lVolume, leVolume);
        mainLayout->addRow(lNumber, leNumber);
        mainLayout->addRow(lSeries, leSeries);
        mainLayout->addRow(lPages, lePages);
        mainLayout->addRow(lAddress, leAddress);
        mainLayout->addRow(lMonth, leMonth);
        mainLayout->addRow(lOrganization, leOrganization);
        mainLayout->addRow(lPublisher, lePublisher);

        mainLayout->addRow(lURL, leURL);
        mainLayout->addRow(lDOI, leDOI);

        additionalLayout->addRow(lAnnote, leAnnote);
        additionalLayout->addRow(lChapter, teChapter);
        additionalLayout->addRow(lCrossref, leCrossref);
        additionalLayout->addRow(lDate, leDate);
        additionalLayout->addRow(lEdition, leEdition);
        additionalLayout->addRow(lHowpublished, leHowpublished);
        additionalLayout->addRow(lISBN, leISBN);
        additionalLayout->addRow(lISSN, leISSN);
        additionalLayout->addRow(lInstitution, leInstitution);
        additionalLayout->addRow(lJournal, leJournal);
        additionalLayout->addRow(lLanguage, leLanguage);
        additionalLayout->addRow(lSchool, leSchool);
        additionalLayout->addRow(lType, leType);

        additionalLayout->addRow(lOwner, leOwner);
        additionalLayout->addRow(lTimestamp, leTimestamp);
        additionalLayout->addRow(lEditedby, leEditedby);
        additionalLayout->addRow(lEditedat, leEditedat);

    } else if (entrytype == BibEntry::BET_INBOOK) {
        mainLayout->addRow(lBibkey, leBibkey);
        mainLayout->addRow(lEditor, leEditor);
        mainLayout->addRow(lTitle, teTitle);
        mainLayout->addRow(lAuthor, teAuthor);
        mainLayout->addRow(lChapter, teChapter);
        mainLayout->addRow(lPages, lePages);
        mainLayout->addRow(lPublisher, lePublisher);
        mainLayout->addRow(lYear, leYear);
        mainLayout->addRow(lURL, leURL);
        mainLayout->addRow(lDOI, leDOI);

        additionalLayout->addRow(lAddress, leAddress);
        additionalLayout->addRow(lAnnote, leAnnote);
        additionalLayout->addRow(lBooktitle, teBooktitle);
        additionalLayout->addRow(lCrossref, leCrossref);
        additionalLayout->addRow(lDate, leDate);
        additionalLayout->addRow(lEdition, leEdition);
        additionalLayout->addRow(lHowpublished, leHowpublished);
        additionalLayout->addRow(lISBN, leISBN);
        additionalLayout->addRow(lISSN, leISSN);
        additionalLayout->addRow(lInstitution, leInstitution);
        additionalLayout->addRow(lJournal, leJournal);
        additionalLayout->addRow(lLanguage, leLanguage);
        additionalLayout->addRow(lMonth, leMonth);
        additionalLayout->addRow(lNumber, leNumber);
        additionalLayout->addRow(lOrganization, leOrganization);
        additionalLayout->addRow(lSchool, leSchool);
        additionalLayout->addRow(lSeries, leSeries);
        additionalLayout->addRow(lType, leType);
        additionalLayout->addRow(lVolume, leVolume);

        additionalLayout->addRow(lOwner, leOwner);
        additionalLayout->addRow(lTimestamp, leTimestamp);
        additionalLayout->addRow(lEditedby, leEditedby);
        additionalLayout->addRow(lEditedat, leEditedat);

    } else if (entrytype == BibEntry::BET_INCOLLECTION) {
        mainLayout->addRow(lBibkey, leBibkey);
        mainLayout->addRow(lAuthor, teAuthor);
        mainLayout->addRow(lTitle, teTitle);
        mainLayout->addRow(lBooktitle, teBooktitle);
        mainLayout->addRow(lPublisher, lePublisher);
        mainLayout->addRow(lYear, leYear);
        mainLayout->addRow(lEditor, leEditor);
        mainLayout->addRow(lVolume, leVolume);
        mainLayout->addRow(lNumber, leNumber);
        mainLayout->addRow(lSeries, leSeries);
        mainLayout->addRow(lPages, lePages);
        mainLayout->addRow(lAddress, leAddress);
        mainLayout->addRow(lEdition, leEdition);
        mainLayout->addRow(lMonth, leMonth);
        mainLayout->addRow(lURL, leURL);
        mainLayout->addRow(lDOI, leDOI);

        additionalLayout->addRow(lAnnote, leAnnote);
        additionalLayout->addRow(lChapter, teChapter);
        additionalLayout->addRow(lCrossref, leCrossref);
        additionalLayout->addRow(lDate, leDate);
        additionalLayout->addRow(lHowpublished, leHowpublished);
        additionalLayout->addRow(lISBN, leISBN);
        additionalLayout->addRow(lISSN, leISSN);
        additionalLayout->addRow(lInstitution, leInstitution);
        additionalLayout->addRow(lJournal, leJournal);
        additionalLayout->addRow(lLanguage, leLanguage);
        additionalLayout->addRow(lOrganization, leOrganization);
        additionalLayout->addRow(lSchool, leSchool);
        additionalLayout->addRow(lType, leType);

        additionalLayout->addRow(lOwner, leOwner);
        additionalLayout->addRow(lTimestamp, leTimestamp);
        additionalLayout->addRow(lEditedby, leEditedby);
        additionalLayout->addRow(lEditedat, leEditedat);

    } else if (entrytype == BibEntry::BET_INPROCEEDINGS) {
        mainLayout->addRow(lBibkey, leBibkey);
        mainLayout->addRow(lAuthor, teAuthor);
        mainLayout->addRow(lTitle, teTitle);
        mainLayout->addRow(lBooktitle, teBooktitle);
        mainLayout->addRow(lYear, leYear);
        mainLayout->addRow(lEditor, leEditor);
        mainLayout->addRow(lVolume, leVolume);
        mainLayout->addRow(lNumber, leNumber);
        mainLayout->addRow(lSeries, leSeries);
        mainLayout->addRow(lPages, lePages);
        mainLayout->addRow(lAddress, leAddress);
        mainLayout->addRow(lMonth, leMonth);
        mainLayout->addRow(lOrganization, leOrganization);
        mainLayout->addRow(lPublisher, lePublisher);

        mainLayout->addRow(lURL, leURL);
        mainLayout->addRow(lDOI, leDOI);

        additionalLayout->addRow(lAnnote, leAnnote);
        additionalLayout->addRow(lChapter, teChapter);
        additionalLayout->addRow(lCrossref, leCrossref);
        additionalLayout->addRow(lDate, leDate);
        additionalLayout->addRow(lEdition, leEdition);
        additionalLayout->addRow(lHowpublished, leHowpublished);
        additionalLayout->addRow(lISBN, leISBN);
        additionalLayout->addRow(lISSN, leISSN);
        additionalLayout->addRow(lInstitution, leInstitution);
        additionalLayout->addRow(lJournal, leJournal);
        additionalLayout->addRow(lLanguage, leLanguage);
        additionalLayout->addRow(lSchool, leSchool);
        additionalLayout->addRow(lType, leType);

        additionalLayout->addRow(lOwner, leOwner);
        additionalLayout->addRow(lTimestamp, leTimestamp);
        additionalLayout->addRow(lEditedby, leEditedby);
        additionalLayout->addRow(lEditedat, leEditedat);

    } else if (entrytype == BibEntry::BET_MANUAL) {
        mainLayout->addRow(lBibkey, leBibkey);
        mainLayout->addRow(lTitle, teTitle);
        mainLayout->addRow(lAuthor, teAuthor);
        mainLayout->addRow(lOrganization, leOrganization);
        mainLayout->addRow(lAddress, leAddress);
        mainLayout->addRow(lEdition, leEdition);
        mainLayout->addRow(lMonth, leMonth);
        mainLayout->addRow(lYear, leYear);
        mainLayout->addRow(lURL, leURL);
        mainLayout->addRow(lDOI, leDOI);

        additionalLayout->addRow(lAnnote, leAnnote);
        additionalLayout->addRow(lBooktitle, teBooktitle);
        additionalLayout->addRow(lChapter, teChapter);
        additionalLayout->addRow(lCrossref, leCrossref);
        additionalLayout->addRow(lDate, leDate);
        additionalLayout->addRow(lEditor, leEditor);
        additionalLayout->addRow(lHowpublished, leHowpublished);
        additionalLayout->addRow(lISBN, leISBN);
        additionalLayout->addRow(lISSN, leISSN);
        additionalLayout->addRow(lInstitution, leInstitution);
        additionalLayout->addRow(lJournal, leJournal);
        additionalLayout->addRow(lLanguage, leLanguage);
        additionalLayout->addRow(lNumber, leNumber);
        additionalLayout->addRow(lPages, lePages);
        additionalLayout->addRow(lPublisher, lePublisher);
        additionalLayout->addRow(lSchool, leSchool);
        additionalLayout->addRow(lSeries, leSeries);
        additionalLayout->addRow(lType, leType);
        mainLayout->addRow(lVolume, leVolume);

        additionalLayout->addRow(lOwner, leOwner);
        additionalLayout->addRow(lTimestamp, leTimestamp);
        additionalLayout->addRow(lEditedby, leEditedby);
        additionalLayout->addRow(lEditedat, leEditedat);

    } else if (entrytype == BibEntry::BET_MASTERTHESIS) {
        mainLayout->addRow(lBibkey, leBibkey);
        mainLayout->addRow(lAuthor, teAuthor);
        mainLayout->addRow(lTitle, teTitle);
        mainLayout->addRow(lSchool, leSchool);
        mainLayout->addRow(lYear, leYear);
        mainLayout->addRow(lType, leType);
        mainLayout->addRow(lAddress, leAddress);
        mainLayout->addRow(lMonth, leMonth);
        mainLayout->addRow(lURL, leURL);
        mainLayout->addRow(lDOI, leDOI);

        additionalLayout->addRow(lAnnote, leAnnote);
        additionalLayout->addRow(lBooktitle, teBooktitle);
        additionalLayout->addRow(lChapter, teChapter);
        additionalLayout->addRow(lCrossref, leCrossref);
        additionalLayout->addRow(lDate, leDate);
        additionalLayout->addRow(lEdition, leEdition);
        additionalLayout->addRow(lEditor, leEditor);
        additionalLayout->addRow(lHowpublished, leHowpublished);
        additionalLayout->addRow(lISBN, leISBN);
        additionalLayout->addRow(lISSN, leISSN);
        additionalLayout->addRow(lInstitution, leInstitution);
        additionalLayout->addRow(lJournal, leJournal);
        additionalLayout->addRow(lLanguage, leLanguage);
        additionalLayout->addRow(lNumber, leNumber);
        additionalLayout->addRow(lOrganization, leOrganization);
        additionalLayout->addRow(lPages, lePages);
        additionalLayout->addRow(lPublisher, lePublisher);
        additionalLayout->addRow(lSeries, leSeries);
        additionalLayout->addRow(lVolume, leVolume);

        additionalLayout->addRow(lOwner, leOwner);
        additionalLayout->addRow(lTimestamp, leTimestamp);
        additionalLayout->addRow(lEditedby, leEditedby);
        additionalLayout->addRow(lEditedat, leEditedat);

    } else if (entrytype == BibEntry::BET_MISC) {
        mainLayout->addRow(lBibkey, leBibkey);
        mainLayout->addRow(lAuthor, teAuthor);
        mainLayout->addRow(lTitle, teTitle);
        mainLayout->addRow(lYear, leYear);
        mainLayout->addRow(lURL, leURL);
        mainLayout->addRow(lDOI, leDOI);

        additionalLayout->addRow(lAddress, leAddress);
        additionalLayout->addRow(lAnnote, leAnnote);
        additionalLayout->addRow(lBooktitle, teBooktitle);
        additionalLayout->addRow(lChapter, teChapter);
        additionalLayout->addRow(lCrossref, leCrossref);
        additionalLayout->addRow(lDate, leDate);
        additionalLayout->addRow(lEdition, leEdition);
        additionalLayout->addRow(lEditor, leEditor);
        additionalLayout->addRow(lHowpublished, leHowpublished);
        additionalLayout->addRow(lISBN, leISBN);
        additionalLayout->addRow(lISSN, leISSN);
        additionalLayout->addRow(lInstitution, leInstitution);
        additionalLayout->addRow(lJournal, leJournal);
        additionalLayout->addRow(lLanguage, leLanguage);
        additionalLayout->addRow(lMonth, leMonth);
        additionalLayout->addRow(lNumber, leNumber);
        additionalLayout->addRow(lOrganization, leOrganization);
        additionalLayout->addRow(lPages, lePages);
        additionalLayout->addRow(lPublisher, lePublisher);
        additionalLayout->addRow(lSchool, leSchool);
        additionalLayout->addRow(lSeries, leSeries);
        additionalLayout->addRow(lType, leType);
        additionalLayout->addRow(lVolume, leVolume);

        additionalLayout->addRow(lOwner, leOwner);
        additionalLayout->addRow(lTimestamp, leTimestamp);
        additionalLayout->addRow(lEditedby, leEditedby);
        additionalLayout->addRow(lEditedat, leEditedat);

    } else if (entrytype == BibEntry::BET_PHDTHESIS) {
        mainLayout->addRow(lBibkey, leBibkey);
        mainLayout->addRow(lAuthor, teAuthor);
        mainLayout->addRow(lTitle, teTitle);
        additionalLayout->addRow(lSchool, leSchool);
        mainLayout->addRow(lYear, leYear);
        mainLayout->addRow(lType, leType);
        mainLayout->addRow(lAddress, leAddress);
        mainLayout->addRow(lMonth, leMonth);
        mainLayout->addRow(lURL, leURL);
        mainLayout->addRow(lDOI, leDOI);

        additionalLayout->addRow(lAnnote, leAnnote);
        additionalLayout->addRow(lBooktitle, teBooktitle);
        additionalLayout->addRow(lChapter, teChapter);
        additionalLayout->addRow(lCrossref, leCrossref);
        additionalLayout->addRow(lDate, leDate);
        additionalLayout->addRow(lEdition, leEdition);
        additionalLayout->addRow(lEditor, leEditor);
        additionalLayout->addRow(lHowpublished, leHowpublished);
        additionalLayout->addRow(lISBN, leISBN);
        additionalLayout->addRow(lISSN, leISSN);
        additionalLayout->addRow(lInstitution, leInstitution);
        additionalLayout->addRow(lJournal, leJournal);
        additionalLayout->addRow(lLanguage, leLanguage);
        additionalLayout->addRow(lNumber, leNumber);
        additionalLayout->addRow(lOrganization, leOrganization);
        additionalLayout->addRow(lPages, lePages);
        additionalLayout->addRow(lPublisher, lePublisher);
        additionalLayout->addRow(lSeries, leSeries);
        additionalLayout->addRow(lVolume, leVolume);

        additionalLayout->addRow(lOwner, leOwner);
        additionalLayout->addRow(lTimestamp, leTimestamp);
        additionalLayout->addRow(lEditedby, leEditedby);
        additionalLayout->addRow(lEditedat, leEditedat);

    } else if (entrytype == BibEntry::BET_PROCEEDINGS) {
        mainLayout->addRow(lBibkey, leBibkey);
        mainLayout->addRow(lTitle, teTitle);
        mainLayout->addRow(lYear, leYear);
        mainLayout->addRow(lEditor, leEditor);
        mainLayout->addRow(lVolume, leVolume);
        mainLayout->addRow(lNumber, leNumber);
        mainLayout->addRow(lSeries, leSeries);
        mainLayout->addRow(lAddress, leAddress);
        mainLayout->addRow(lMonth, leMonth);
        mainLayout->addRow(lOrganization, leOrganization);
        mainLayout->addRow(lPublisher, lePublisher);
        mainLayout->addRow(lURL, leURL);
        mainLayout->addRow(lDOI, leDOI);

        additionalLayout->addRow(lAnnote, leAnnote);
        additionalLayout->addRow(lAuthor, teAuthor);
        additionalLayout->addRow(lBooktitle, teBooktitle);
        additionalLayout->addRow(lChapter, teChapter);
        additionalLayout->addRow(lCrossref, leCrossref);
        additionalLayout->addRow(lDate, leDate);
        additionalLayout->addRow(lEdition, leEdition);
        additionalLayout->addRow(lHowpublished, leHowpublished);
        additionalLayout->addRow(lISBN, leISBN);
        additionalLayout->addRow(lISSN, leISSN);
        additionalLayout->addRow(lInstitution, leInstitution);
        additionalLayout->addRow(lJournal, leJournal);
        additionalLayout->addRow(lLanguage, leLanguage);
        additionalLayout->addRow(lPages, lePages);
        additionalLayout->addRow(lSchool, leSchool);
        additionalLayout->addRow(lType, leType);

        additionalLayout->addRow(lOwner, leOwner);
        additionalLayout->addRow(lTimestamp, leTimestamp);
        additionalLayout->addRow(lEditedby, leEditedby);
        additionalLayout->addRow(lEditedat, leEditedat);

    } else if (entrytype == BibEntry::BET_TECHREPORT) {
        mainLayout->addRow(lBibkey, leBibkey);
        mainLayout->addRow(lAuthor, teAuthor);
        mainLayout->addRow(lTitle, teTitle);
        mainLayout->addRow(lInstitution, leInstitution);
        mainLayout->addRow(lYear, leYear);
        mainLayout->addRow(lType, leType);
        mainLayout->addRow(lAddress, leAddress);
        mainLayout->addRow(lNumber, leNumber);
        mainLayout->addRow(lMonth, leMonth);
        mainLayout->addRow(lURL, leURL);
        mainLayout->addRow(lDOI, leDOI);

        additionalLayout->addRow(lAnnote, leAnnote);
        additionalLayout->addRow(lBooktitle, teBooktitle);
        additionalLayout->addRow(lChapter, teChapter);
        additionalLayout->addRow(lCrossref, leCrossref);
        additionalLayout->addRow(lDate, leDate);
        additionalLayout->addRow(lEdition, leEdition);
        additionalLayout->addRow(lEditor, leEditor);
        additionalLayout->addRow(lHowpublished, leHowpublished);
        additionalLayout->addRow(lISBN, leISBN);
        additionalLayout->addRow(lISSN, leISSN);
        additionalLayout->addRow(lJournal, leJournal);
        additionalLayout->addRow(lLanguage, leLanguage);
        additionalLayout->addRow(lOrganization, leOrganization);
        additionalLayout->addRow(lPages, lePages);
        additionalLayout->addRow(lPublisher, lePublisher);
        additionalLayout->addRow(lSchool, leSchool);
        additionalLayout->addRow(lSeries, leSeries);
        additionalLayout->addRow(lVolume, leVolume);

        additionalLayout->addRow(lOwner, leOwner);
        additionalLayout->addRow(lTimestamp, leTimestamp);
        additionalLayout->addRow(lEditedby, leEditedby);
        additionalLayout->addRow(lEditedat, leEditedat);

    } else if (entrytype == BibEntry::BET_UNPUBLISHED) {
        mainLayout->addRow(lBibkey, leBibkey);
        mainLayout->addRow(lAuthor, teAuthor);
        mainLayout->addRow(lTitle, teTitle);
        mainLayout->addRow(lYear, leYear);

        additionalLayout->addRow(lAddress, leAddress);
        additionalLayout->addRow(lAnnote, leAnnote);
        additionalLayout->addRow(lBooktitle, teBooktitle);
        additionalLayout->addRow(lChapter, teChapter);
        additionalLayout->addRow(lCrossref, leCrossref);
        additionalLayout->addRow(lDate, leDate);
        additionalLayout->addRow(lDOI, leDOI);
        additionalLayout->addRow(lEdition, leEdition);
        additionalLayout->addRow(lEditor, leEditor);
        additionalLayout->addRow(lHowpublished, leHowpublished);
        additionalLayout->addRow(lISBN, leISBN);
        additionalLayout->addRow(lISSN, leISSN);
        additionalLayout->addRow(lInstitution, leInstitution);
        additionalLayout->addRow(lJournal, leJournal);
        additionalLayout->addRow(lLanguage, leLanguage);
        additionalLayout->addRow(lMonth, leMonth);
        additionalLayout->addRow(lNumber, leNumber);
        additionalLayout->addRow(lOrganization, leOrganization);
        additionalLayout->addRow(lPages, lePages);
        additionalLayout->addRow(lPublisher, lePublisher);
        additionalLayout->addRow(lSchool, leSchool);
        additionalLayout->addRow(lSeries, leSeries);
        additionalLayout->addRow(lType, leType);
        additionalLayout->addRow(lURL, leURL);
        additionalLayout->addRow(lVolume, leVolume);

        additionalLayout->addRow(lOwner, leOwner);
        additionalLayout->addRow(lTimestamp, leTimestamp);
        additionalLayout->addRow(lEditedby, leEditedby);
        additionalLayout->addRow(lEditedat, leEditedat);
    }

//    ui->scrollAreaWidgetContents->setLayout(mainLayout);
//    ui->scrollAreaWidgetContents_2->setLayout(additionalLayout);
//    ui->saMain->setLayout(mainLayout);
//    ui->saAdditional->setLayout(additionalLayout);
}

void EntryProperties::on_lwEntryTypes_currentRowChanged(int currentRow)
{
    setupLayouts(currentRow);
}

BibEntry EntryProperties::getEntryState() {
    entry.setField(BibEntry::BIBKEY       , leBibkey->text());
    entry.setField(BibEntry::ADDRESS      , leAddress->text());
    entry.setField(BibEntry::ANNOTE       , leAnnote->text());
    entry.setField(BibEntry::AUTHOR       , teAuthor->toPlainText());
    entry.setField(BibEntry::BOOKTITLE    , teBooktitle->toPlainText());
    entry.setField(BibEntry::CHAPTER      , teChapter->toPlainText());
    entry.setField(BibEntry::CROSSREF     , leCrossref->text());
    entry.setField(BibEntry::EDITION      , leEdition->text());
    entry.setField(BibEntry::EDITOR       , leEditor->text());
    entry.setField(BibEntry::HOWPUBLISHED , leHowpublished->text());
    entry.setField(BibEntry::INSTITUTION  , leInstitution->text());
    entry.setField(BibEntry::JOURNAL      , leJournal->text());
    entry.setField(BibEntry::MONTH        , leMonth->text());
    entry.setField(BibEntry::NUMBER       , leNumber->text());
    entry.setField(BibEntry::ORGANIZATION , leOrganization->text());
    entry.setField(BibEntry::PAGES        , lePages->text());
    entry.setField(BibEntry::PUBLISHER    , lePublisher->text());
    entry.setField(BibEntry::SCHOOL       , leSchool->text());
    entry.setField(BibEntry::SERIES       , leSeries->text());
    entry.setField(BibEntry::TITLE        , teTitle->toPlainText());
    entry.setField(BibEntry::TYPE         , leType->text());
    entry.setField(BibEntry::VOLUME       , leVolume->text());
    entry.setField(BibEntry::YEAR         , leYear->text());
    entry.setField(BibEntry::DOI          , leDOI->text());
    entry.setField(BibEntry::ISSN         , leISSN->text());
    entry.setField(BibEntry::ISBN         , leISBN->text());
    entry.setField(BibEntry::URL          , leURL->text());
    entry.setField(BibEntry::DATE         , leDate->text());
    entry.setField(BibEntry::TIMESTAMP    , leTimestamp->text());
    entry.setField(BibEntry::LANGUAGE     , leLanguage->text());
    entry.setField(BibEntry::OWNER        , leOwner->text());
    entry.setField(BibEntry::EDITEDBY     , leEditedby->text());
    entry.setField(BibEntry::EDITEDAT     , leEditedat->text());
    entry.setField(BibEntry::ABSTRACT     , teAbstract->toPlainText());
    entry.setField(BibEntry::NOTE         , teNote->toPlainText());

    entry.clearFiles();
    for (int i = 0; i < ui->lwFiles->count(); i ++) {
        entry.addFile(ui->lwFiles->item(i)->text());
    }

    entry.clearShelfIDs();
    for (int i = 0; i < ui->lwUsedShelfs->count(); i ++) {
        ShelfEntry sentry = ui->lwUsedShelfs->item(i)->data(Qt::UserRole).value<ShelfEntry>();
        int shelf_id = ui->lwUsedShelfs->item(i)->data(Qt::UserRole).value<ShelfEntry>().getID();
        entry.addShelfID(shelf_id);
    }

    if (model->getDefaultShelfId() != model->getRootShelfId()) {
        entry.addShelfID(model->getDefaultShelfId());
    }

    QList<QListWidgetItem *> selectedEntryTypes = ui->lwEntryTypes->selectedItems();
    if (selectedEntryTypes.length() > 0) {
        entry.setEntryType(ui->lwEntryTypes->currentRow());
//        entry.entrytype = selectedEntryTypes.at(0)->data(Qt::DisplayRole).toString();
    } else {
        entry.setEntryType(BibEntry::BET_ARTICLE);
    }

    return entry;
}

void EntryProperties::on_pbAddFile_clicked()
{
    QStringList fileNames = QFileDialog::getOpenFileNames(this, tr("Open File"), DBVars::getInstance().getFilePath(), tr("PDF documents (*.pdf);;DJVU documents (*.djv, *.djvu);;MS/OO/LO Office (*.doc, *.docx, *.odt);;All files (*.*)"));

    for (int i = 0; i < fileNames.length(); i ++) {
        QString fileName = fileNames[i];
        if (!fileName.isEmpty() && !fileName.isNull()) {
            QDir dir(DBVars::getInstance().getFilePath());
            QString relFileName = dir.relativeFilePath(fileName);
            //        entry.files.append(relFileName);
            ui->lwFiles->addItem(relFileName);
        }
    }
}

void EntryProperties::on_pbRemoveFile_clicked()
{
    if (entry.getFiles().length() == 0) {
        return;
    }

    int pos = ui->lwFiles->currentRow();

    if (pos >= 0) {
        if (QMessageBox::question(this, tr("Remove file"), tr("Remove file from list?"), QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes) {
            ui->lwFiles->model()->removeRow(pos);
        }
    }
}

void EntryProperties::on_pbDeleteFromDisk_clicked()
{
    if (entry.getFiles().length() == 0) {
        return;
    }

    int pos = ui->lwFiles->currentRow();

    if (pos >= 0) {
        if (QMessageBox::question(this, tr("Remove file"), tr("Delete file from disk?"), QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes) {
            QFile::remove(QDir::cleanPath(DBVars::getInstance().getFilePath() + QDir::separator() + entry.getFile(pos)));
            ui->lwFiles->model()->removeRow(pos);
        }
    }
}

void EntryProperties::on_pbAddToShelf_clicked()
{
    QModelIndexList modelIndexList = ui->tvShelfs->selectionModel()->selectedRows();

    if (modelIndexList.length() > 0) {
        ShelfEntry selectedShelf = modelIndexList.at(0).data(Qt::UserRole).value<ShelfEntry>();

        if (!isShelfAlreadyAssigned(selectedShelf.getID())) {
            auto * item = new QListWidgetItem(selectedShelf.name, ui->lwUsedShelfs);
            item->setData(Qt::UserRole, QVariant::fromValue(selectedShelf));
            ui->lwUsedShelfs->addItem(item);
        }
    }
}

void EntryProperties::on_pbRemoveFromShelf_clicked()
{
    QList<QListWidgetItem *> selectedItems = ui->lwUsedShelfs->selectedItems();
    if (selectedItems.length() > 0) {
        delete ui->lwUsedShelfs->takeItem(ui->lwUsedShelfs->row(selectedItems.at(0)));
    }
}

bool EntryProperties::isShelfAlreadyAssigned(const int shelf_id) {
    for (int i = 0; i < ui->lwUsedShelfs->count(); i ++) {
        int ss = ui->lwUsedShelfs->item(i)->data(Qt::UserRole).value<ShelfEntry>().getID();
        if (shelf_id == ui->lwUsedShelfs->item(i)->data(Qt::UserRole).value<ShelfEntry>().getID()) {
            return true;
        }
    }

    return false;
}


void EntryProperties::resizeEvent(QResizeEvent *event) {
    QDialog::resizeEvent(event);

    QSize winSize = this->size();
    QWidget * widget;
    QPoint pos;

    widget = ui->buttonBox;
    widget->move(winSize.width()-widget->width()-dist, winSize.height()-widget->height()-dist);

    widget = ui->buttonBox;
    pos = widget->pos();
    widget->move(winSize.width() - widget->width() - dist, winSize.height() - widget->height() - dist);

    widget = ui->lHash;
    pos = widget->pos();
    widget->move(pos.x(), winSize.height() - widget->height() - dist);

    widget = ui->lwEntryTypes;
    pos = widget->pos();
    widget->resize(widget->width(), winSize.height() - widget->y() - dist - ui->buttonBox->height() - dist);

    QSize tabSize = ui->tabWidget->size();
    QSize tabBarSize = ui->tabWidget->tabBar()->size();
    widget = ui->tabWidget;
    pos = widget->pos();
    widget->resize(winSize.width() - widget->x() - dist, ui->lwEntryTypes->height());

    widget = ui->saMain;
    pos = widget->pos();
    widget->resize(tabSize.width() - pos.x() - 2*dist, tabSize.height() - tabBarSize.height() - pos.y() - 2*dist);

    widget = ui->saAdditional;
    pos = widget->pos();
    widget->resize(tabSize.width() - pos.x() - 2*dist, tabSize.height() - tabBarSize.height() - pos.y() - 2*dist);

    widget = ui->pbAddFile;
    pos = widget->pos();
    widget->move(tabSize.width() - widget->width() - 2*dist, widget->y());
    widget = ui->pbRemoveFile;
    pos = widget->pos();
    widget->move(tabSize.width() - widget->width() - 2*dist, widget->y());
    widget = ui->pbDeleteFromDisk;
    pos = widget->pos();
    widget->move(tabSize.width() - widget->width() - 2*dist, widget->y());

    widget = ui->lwFiles;
    pos = widget->pos();
    widget->resize(tabSize.width() - ui->pbAddFile->width() - 4*dist, tabSize.height() - tabBarSize.height() - pos.y() - 2*dist);

    widget = ui->pbAddToShelf;
    pos = widget->pos();
    widget->move((tabSize.width() - widget->width())/2, (tabSize.height() - tabBarSize.height() - widget->height() - 4*dist)/2 - 2*dist);

    widget = ui->pbRemoveFromShelf;
    pos = widget->pos();
    widget->move((tabSize.width() - widget->width())/2, (tabSize.height() - tabBarSize.height() + widget->height() - 4*dist)/2 + 2*dist);

    widget = ui->tvShelfs;
    pos = widget->pos();
    widget->resize(ui->pbAddToShelf->pos().x() - 2*dist, tabSize.height() - tabBarSize.height() - pos.y() - 2*dist);

    widget = ui->lwUsedShelfs;
    pos = widget->pos();
    widget->move(ui->pbAddToShelf->x() + ui->pbAddToShelf->width() + dist, widget->y());
    widget->resize(ui->tvShelfs->width(), ui->tvShelfs->height());


    widget = ui->label;
    pos = widget->pos();
    widget->move(ui->lwUsedShelfs->x(), widget->y());

}

void EntryProperties::accept() {
    QSettings settings;
    settings.beginGroup("EntryPropertiesWindow");
    settings.setValue("size", size());
    settings.setValue("position", pos());
    settings.endGroup();

    QDialog::accept();
}

void EntryProperties::reject() {
    QSettings settings;
    settings.beginGroup("EntryPropertiesWindow");
    settings.setValue("size", size());
    settings.setValue("position", pos());
    settings.endGroup();

    QDialog::reject();
}
