#include <QDateTime>
#include <QListWidgetItem>
#include <QMessageBox>

#include "compareentriesdialog.h"
#include "ui_compareentriesdialog.h"

CompareEntriesDialog::CompareEntriesDialog(const BibEntry & left, const BibEntry & right, Model * model, Action action, SyncAction saction, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CompareEntriesDialog)
  , left(left)
  , right(right)
  , model(model)
  , action(action)
  , saction(saction)
{
    ui->setupUi(this);

    leLeft = new QLineEdit * [BibEntry::COL_NUM];
    leRight = new QLineEdit * [BibEntry::COL_NUM];
    leChosen = new QLineEdit * [BibEntry::COL_NUM];
    teLeft = new QTextEdit * [BibEntry::COL_NUM];
    teRight = new QTextEdit * [BibEntry::COL_NUM];
    teChosen = new QTextEdit * [BibEntry::COL_NUM];
    pbFromLeft = new QPushButton * [BibEntry::COL_NUM];
    pbFromRight = new QPushButton * [BibEntry::COL_NUM];
    toChoose = new bool[BibEntry::COL_NUM];

    mainLayout = new QGridLayout(this);
    scrollArea = new QScrollArea(this);
    scrollArea->setWidgetResizable(true);
    mainLayout->addWidget(new QLabel("Fill the central column as the merged version:", this), 0, 0, 1, -1);
    mainLayout->addWidget(scrollArea, 1, 0, 1, -1);
    pbAccept = new QPushButton(tr("Merge"), this);
    if (action == LEAVE_TWO) {
        pbCancel = new QPushButton(tr("Leave two"), this);
    } else if (action == CANCEL) {
        pbCancel = new QPushButton(tr("Cancel"), this);
    }
    connect(pbAccept, SIGNAL(clicked()), this, SLOT(acceptPressed()));
    connect(pbCancel, SIGNAL(clicked()), this, SLOT(cancelPressed()));
    if (saction != NO_SYNC) {
        pbTakeLocal = new QPushButton(tr("Take all LOCAL entries"), this);
        pbTakeRemote = new QPushButton(tr("Take all REMOTE entries"), this);
        connect(pbTakeLocal, SIGNAL(clicked()), this, SLOT(takeLocalPressed()));
        connect(pbTakeRemote, SIGNAL(clicked()), this, SLOT(takeRemotePressed()));
        mainLayout->addWidget(pbTakeLocal, 2, 0);
        mainLayout->addWidget(pbTakeRemote, 2, 1);
        mainLayout->addWidget(pbAccept, 3, 0);
        mainLayout->addWidget(pbCancel, 3, 1);
    } else {
        mainLayout->addWidget(pbAccept, 2, 0);
        mainLayout->addWidget(pbCancel, 2, 1);
    }
    saction = NO_SYNC;

    scrollLayout = new QGridLayout(scrollArea);

    for (int i = 0; i < BibEntry::COL_NUM; i ++) {
        toChoose[i]     = false;
        leLeft[i]       = new QLineEdit(this);
        leRight[i]      = new QLineEdit(this);
        leChosen[i]     = new QLineEdit(this);
        teLeft[i]       = new QTextEdit(this);
        teRight[i]      = new QTextEdit(this);
        teChosen[i]     = new QTextEdit(this);
        pbFromLeft[i]   = new QPushButton(">", this);
        pbFromRight[i]  = new QPushButton("<", this);

        leLeft[i]->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        leLeft[i]->setMinimumHeight(15);
        leChosen[i]->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        leChosen[i]->setMinimumHeight(15);
        leRight[i]->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        leRight[i]->setMinimumHeight(15);

        teLeft[i]->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Ignored);
        teChosen[i]->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Ignored);
        teRight[i]->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Ignored);

        pbFromLeft[i]->setObjectName(QString::number(i));
        pbFromRight[i]->setObjectName(QString::number(i));
        connect(pbFromLeft[i], SIGNAL(clicked()), this, SLOT(moveFromLeft()));
        connect(pbFromRight[i], SIGNAL(clicked()), this, SLOT(moveFromRight()));

        leLeft[i]->setVisible(false);
        leRight[i]->setVisible(false);
        leChosen[i]->setVisible(false);
        teLeft[i]->setVisible(false);
        teRight[i]->setVisible(false);
        teChosen[i]->setVisible(false);
        pbFromLeft[i]->setVisible(false);
        pbFromRight[i]->setVisible(false);
    }

    int fieldId, scrollAreaRow = 0;

    scrollLayout->addWidget(&leftTitle, scrollAreaRow, 0);
    scrollLayout->addWidget(&rightTitle, scrollAreaRow, 4);
    scrollAreaRow ++;

    scrollLayout->addWidget(new QLabel(BibEntry::BIBENTRY_TYPE[left.getEntryType()], this), scrollAreaRow, 0);
    scrollLayout->addWidget(new QLabel(BibEntry::BIBENTRY_TYPE[right.getEntryType()], this), scrollAreaRow, 4);
    cbEntrytypeC = new QComboBox(this);
    for (int i = 0; i < BibEntry::BIBENTRYTYPES_NUM; i ++) {
        cbEntrytypeC->addItem(BibEntry::BIBENTRY_TYPE[i]);
    }
    cbEntrytypeC->setCurrentIndex(left.getEntryType());
    scrollLayout->addWidget(cbEntrytypeC, scrollAreaRow, 3);
    scrollAreaRow ++;

    pbAllFromLeft = new QPushButton(tr("Add all from left >"), this);
    connect(pbAllFromLeft, SIGNAL(clicked()), this, SLOT(moveAllFromLeft()));
    pbAllFromRight = new QPushButton(tr("< Add all from right"), this);
    connect(pbAllFromRight, SIGNAL(clicked()), this, SLOT(moveAllFromRight()));
    scrollLayout->addWidget(pbAllFromLeft, scrollAreaRow, 1, 1, 2);
    scrollLayout->addWidget(pbAllFromRight, scrollAreaRow, 4, 1, 2);
    scrollAreaRow ++;


    palette.setColor(QPalette::Base, QColor(255, 202, 143));
    palette.setColor(QPalette::Text,Qt::black);
    fieldId = BibEntry::BIBKEY;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow, true);
    }
    fieldId = BibEntry::AUTHOR;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addTextEdit(fieldId, &scrollAreaRow, true);
    }
    fieldId = BibEntry::TITLE;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addTextEdit(fieldId, &scrollAreaRow, true);
    }
    fieldId = BibEntry::CHAPTER;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addTextEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::BOOKTITLE;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addTextEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::EDITOR;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::YEAR;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow, true);
    }
    fieldId = BibEntry::JOURNAL;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow, true);
    }
    fieldId = BibEntry::ADDRESS;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::ANNOTE;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::CROSSREF;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::EDITION;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::HOWPUBLISHED;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::INSTITUTION;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::MONTH;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::NOTE;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::NUMBER;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::ORGANIZATION;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::PAGES;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::PUBLISHER;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::SCHOOL;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::SERIES;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::TYPE;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::VOLUME;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::DOI;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow, true);
    }
    fieldId = BibEntry::ISSN;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::ISBN;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::URL;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::DATE;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::LANGUAGE;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addLineEdit(fieldId, &scrollAreaRow);
    }
    fieldId = BibEntry::ABSTRACT;
    if (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) {
        addTextEdit(fieldId, &scrollAreaRow);
    }

    scrollLayout->addWidget(new QLabel(BibEntry::BIBENTRY_FIELD[BibEntry::OWNER]), scrollAreaRow, 0);
    scrollLayout->addWidget(new QLabel(left.getField(BibEntry::OWNER), this), scrollAreaRow, 1);
    chosenOwner = new QLabel("", this);
    scrollLayout->addWidget(chosenOwner, scrollAreaRow, 3);
    scrollLayout->addWidget(new QLabel(right.getField(BibEntry::OWNER), this), scrollAreaRow, 5);
    scrollAreaRow ++;
    scrollLayout->addWidget(new QLabel(BibEntry::BIBENTRY_FIELD[BibEntry::TIMESTAMP]), scrollAreaRow, 0);
    scrollLayout->addWidget(new QLabel(left.getField(BibEntry::TIMESTAMP), this), scrollAreaRow, 1);
    chosenTimestamp = new QLabel("", this);
    scrollLayout->addWidget(chosenTimestamp, scrollAreaRow, 3);
    scrollLayout->addWidget(new QLabel(right.getField(BibEntry::TIMESTAMP), this), scrollAreaRow, 5);
    scrollAreaRow ++;

    const QStringList filesLeft = left.getFiles();
    const QStringList filesRight = right.getFiles();
    lwFilesLeft = new QListWidget(this);
    lwFilesRight = new QListWidget(this);
    lwFilesChosen = new QListWidget(this);
    lwFilesLeft->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    lwFilesChosen->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    lwFilesRight->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    if (filesLeft.size() > 0 || filesRight.size() > 0) {
        for (int i = 0; i < filesLeft.size(); i ++) {
            lwFilesLeft->addItem(filesLeft[i]);
        }
        connect(lwFilesLeft, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(addFileFromLeft(QListWidgetItem *)));

        for (int i = 0; i < filesRight.size(); i ++) {
            lwFilesRight->addItem(filesRight[i]);
        }
        connect(lwFilesRight, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(addFileFromRight(QListWidgetItem *)));

        connect(lwFilesChosen, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(removeFile(QListWidgetItem *)));

        scrollLayout->addWidget(new QLabel(tr("Files:"), this), scrollAreaRow, 0);
        scrollLayout->addWidget(lwFilesLeft, scrollAreaRow, 1);
        scrollLayout->addWidget(lwFilesChosen, scrollAreaRow, 3);
        scrollLayout->addWidget(lwFilesRight, scrollAreaRow, 5);
        scrollAreaRow ++;
    }

    const auto shelfsLeft = model->getShelfsForEntry(left);
    const auto shelfsRight = model->getShelfsForEntry(right);
    lwShelfsLeft = new QListWidget(this);
    lwShelfsRight = new QListWidget(this);
    lwShelfsChosen = new QListWidget(this);
    lwShelfsLeft->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    lwShelfsRight->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    lwShelfsChosen->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    if (shelfsLeft.size() > 0 || shelfsRight.size() > 0) {
        for (int i = 0; i < shelfsLeft.size(); i ++) {
            auto * item = new QListWidgetItem(shelfsLeft[i].name, lwShelfsLeft);
            item->setData(Qt::UserRole, QVariant::fromValue(shelfsLeft[i]));
            lwShelfsLeft->addItem(item);
        }
        connect(lwShelfsLeft, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(addShelfFromLeft(QListWidgetItem *)));

        for (int i = 0; i < shelfsRight.size(); i ++) {
            auto * item = new QListWidgetItem(shelfsRight[i].name, lwShelfsRight);
            item->setData(Qt::UserRole, QVariant::fromValue(shelfsRight[i]));
            lwShelfsRight->addItem(item);
        }
        connect(lwShelfsRight, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(addShelfFromRight(QListWidgetItem *)));

        connect(lwShelfsChosen, SIGNAL(itemDoubleClicked(QListWidgetItem *)), this, SLOT(removeShelf(QListWidgetItem *)));

        scrollLayout->addWidget(new QLabel(tr("Shelfs:"), this), scrollAreaRow, 0);
        scrollLayout->addWidget(lwShelfsLeft, scrollAreaRow, 1);
        scrollLayout->addWidget(lwShelfsChosen, scrollAreaRow, 3);
        scrollLayout->addWidget(lwShelfsRight, scrollAreaRow, 5);
        scrollAreaRow ++;
    }

//    this->setLayout(mainLayout);
    this->setWindowTitle(tr("Compare entries"));
//    scrollArea->setLayout(scrollLayout);


    scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    scrollArea->setWidgetResizable(true);
    scrollArea->setGeometry(scrollArea->rect());

    takenFromLeft = 0;
    takenFromRight = 0;

    pbAccept->setDisabled(true);

    this->setWindowState(Qt::WindowMaximized);
}

CompareEntriesDialog::~CompareEntriesDialog()
{
    delete [] leLeft;
    delete [] leRight;
    delete [] leChosen;
    delete [] teLeft;
    delete [] teRight;
    delete [] teChosen;
    delete [] pbFromLeft;
    delete [] pbFromRight;
    delete [] toChoose;
    delete ui;
}

void CompareEntriesDialog::addLineEdit(int fieldId, int *row, bool alwaysShow) {
    leLeft[fieldId]->setText(left.getField(fieldId));
    leRight[fieldId]->setText(right.getField(fieldId));

    if (left.getField(fieldId).simplified() != right.getField(fieldId).simplified()) {
        leLeft[fieldId]->setPalette(palette);
        leRight[fieldId]->setPalette(palette);
    }

//    if (alwaysShow || (left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0)) {
    if (alwaysShow || ((left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) && (left.getField(fieldId).simplified() != right.getField(fieldId).simplified()))) {
        leLeft[fieldId]->setReadOnly(true);
        leRight[fieldId]->setReadOnly(true);
        leLeft[fieldId]->setVisible(true);
        leRight[fieldId]->setVisible(true);
        leChosen[fieldId]->setVisible(true);
        pbFromLeft[fieldId]->setVisible(true);
        pbFromRight[fieldId]->setVisible(true);

        scrollLayout->addWidget(new QLabel(BibEntry::BIBENTRY_FIELD[fieldId]), *row, 0);
        scrollLayout->addWidget(leLeft[fieldId], *row, 1);
        scrollLayout->addWidget(pbFromLeft[fieldId], *row, 2);
        scrollLayout->addWidget(leChosen[fieldId], *row, 3);
        scrollLayout->addWidget(pbFromRight[fieldId], *row, 4);
        scrollLayout->addWidget(leRight[fieldId], *row, 5);

        toChoose[fieldId] = true;

    }

    (*row) ++;
}

void CompareEntriesDialog::addTextEdit(int fieldId, int *row, bool alwaysShow) {
    teLeft[fieldId]->setText(left.getField(fieldId));
    teRight[fieldId]->setText(right.getField(fieldId));

    teLeft[fieldId]->setReadOnly(true);
    teRight[fieldId]->setReadOnly(true);

    if (left.getField(fieldId).simplified() != right.getField(fieldId).simplified()) {
        teLeft[fieldId]->setPalette(palette);
        teRight[fieldId]->setPalette(palette);
    }

    if (alwaysShow || ((left.getField(fieldId).length() > 0 || right.getField(fieldId).length() > 0) && (left.getField(fieldId).simplified() != right.getField(fieldId).simplified()))) {
        teLeft[fieldId]->setVisible(true);
        teRight[fieldId]->setVisible(true);
        teChosen[fieldId]->setVisible(true);
        pbFromLeft[fieldId]->setVisible(true);
        pbFromRight[fieldId]->setVisible(true);

        scrollLayout->addWidget(new QLabel(BibEntry::BIBENTRY_FIELD[fieldId]), *row, 0);
        scrollLayout->addWidget(teLeft[fieldId], *row, 1);
        scrollLayout->addWidget(pbFromLeft[fieldId], *row, 2);
        scrollLayout->addWidget(teChosen[fieldId], *row, 3);
        scrollLayout->addWidget(pbFromRight[fieldId], *row, 4);
        scrollLayout->addWidget(teRight[fieldId], *row, 5);

        toChoose[fieldId] = true;
    }


    (*row) ++;
}


void CompareEntriesDialog::moveFromLeft() {
    QObject* obj = sender();
    int fieldId = obj->objectName().toInt();

    if (fieldId == BibEntry::AUTHOR || fieldId == BibEntry::TITLE || fieldId == BibEntry::CHAPTER || fieldId == BibEntry::BOOKTITLE) {
        teChosen[fieldId]->setText(teLeft[fieldId]->toPlainText());
    } else {
        leChosen[fieldId]->setText(leLeft[fieldId]->text());
    }

    takenFromLeft ++;
    if (takenFromLeft > takenFromRight) {
        chosenOwner->setText(left.getField(BibEntry::OWNER));
        chosenTimestamp->setText(left.getField(BibEntry::TIMESTAMP));
    }
    pbAccept->setEnabled(true);
}

void CompareEntriesDialog::moveFromRight() {
    QObject* obj = sender();
    int fieldId = obj->objectName().toInt();

    if (fieldId == BibEntry::AUTHOR || fieldId == BibEntry::TITLE || fieldId == BibEntry::CHAPTER || fieldId == BibEntry::BOOKTITLE) {
        teChosen[fieldId]->setText(teRight[fieldId]->toPlainText());
    } else {
        leChosen[fieldId]->setText(leRight[fieldId]->text());
    }

    takenFromRight ++;
    if (takenFromRight > takenFromLeft) {
        chosenOwner->setText(right.getField(BibEntry::OWNER));
        chosenTimestamp->setText(right.getField(BibEntry::TIMESTAMP));
    }
    pbAccept->setEnabled(true);
}

void CompareEntriesDialog::acceptPressed() {
    int fieldId;

//    chosen.setID(right.getID());

    chosen.setEntryType(cbEntrytypeC->currentIndex());

//    for (int i = 0; i < BibEntry::COL_NUM; i ++) {
//        if (toChoose[i]) {
//            chosen.setField(i, left.getField(i));
//        }
//    }

    if (takenFromLeft > takenFromRight) {
        chosen.setField(BibEntry::OWNER, left.getField(BibEntry::OWNER));
        chosen.setField(BibEntry::TIMESTAMP, left.getField(BibEntry::TIMESTAMP));
    } else {
        chosen.setField(BibEntry::OWNER, right.getField(BibEntry::OWNER));
        chosen.setField(BibEntry::TIMESTAMP, right.getField(BibEntry::TIMESTAMP));
    }
    if (chosen.getField(BibEntry::OWNER).length() == 0) {
        chosen.setField(BibEntry::OWNER, DBVars::getInstance().getOwner());
    }
    chosen.setField(BibEntry::EDITEDBY, DBVars::getInstance().getOwner());
    chosen.setField(BibEntry::EDITEDAT, QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss"));

    chosen.clearFiles();
    for (int i = 0; i < lwFilesChosen->count(); i ++) {
        chosen.addFile(lwFilesChosen->item(i)->text());
    }

    for (int i = 0; i < lwShelfsChosen->count(); i ++) {
        ShelfEntry shelf = lwShelfsChosen->item(i)->data(Qt::UserRole).value<ShelfEntry>();
        chosen.addShelfID(shelf.getID());
    }

    fieldId = BibEntry::BIBKEY;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::AUTHOR;
    chosen.setField(fieldId, teChosen[fieldId]->toPlainText());
    fieldId = BibEntry::TITLE;
    chosen.setField(fieldId, teChosen[fieldId]->toPlainText());
    fieldId = BibEntry::CHAPTER;
    chosen.setField(fieldId, teChosen[fieldId]->toPlainText());
    fieldId = BibEntry::BOOKTITLE;
    chosen.setField(fieldId, teChosen[fieldId]->toPlainText());
    fieldId = BibEntry::EDITOR;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::YEAR;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::JOURNAL;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::ADDRESS;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::ANNOTE;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::CROSSREF;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::EDITION;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::HOWPUBLISHED;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::INSTITUTION;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::MONTH;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::NOTE;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::NUMBER;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::ORGANIZATION;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::PAGES;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::PUBLISHER;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::SCHOOL;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::SERIES;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::TYPE;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::VOLUME;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::DOI;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::ISSN;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::ISBN;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::URL;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::DATE;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::LANGUAGE;
    chosen.setField(fieldId, leChosen[fieldId]->text());
    fieldId = BibEntry::ABSTRACT;
    chosen.setField(fieldId, teChosen[fieldId]->toPlainText());

    emit accept();
}

void CompareEntriesDialog::cancelPressed() {
    emit reject();
}


void CompareEntriesDialog::addFileFromLeft(QListWidgetItem * item) {
    QListWidgetItem * newItem = new QListWidgetItem(*item);
    lwFilesChosen->addItem(newItem);
    pbAccept->setEnabled(true);
}

void CompareEntriesDialog::addShelfFromLeft(QListWidgetItem * item) {
    QListWidgetItem * newItem = new QListWidgetItem(*item);
    lwShelfsChosen->addItem(newItem);
    pbAccept->setEnabled(true);
}

void CompareEntriesDialog::addFileFromRight(QListWidgetItem * item) {
    QListWidgetItem * newItem = new QListWidgetItem(*item);
    lwFilesChosen->addItem(newItem);
    pbAccept->setEnabled(true);
}

void CompareEntriesDialog::addShelfFromRight(QListWidgetItem * item) {
    QListWidgetItem * newItem = new QListWidgetItem(*item);
    newItem->setData(Qt::UserRole, item->data(Qt::UserRole));
    lwShelfsChosen->addItem(newItem);
    pbAccept->setEnabled(true);
}

void CompareEntriesDialog::removeFile(QListWidgetItem * item) {
    delete lwFilesChosen->takeItem(lwFilesChosen->row(item));
}

void CompareEntriesDialog::removeShelf(QListWidgetItem * item) {
    delete lwShelfsChosen->takeItem(lwShelfsChosen->row(item));
}

void CompareEntriesDialog::moveAllFromLeft() {
    for (int fieldId = 0; fieldId < BibEntry::COL_NUM; fieldId ++) {
        if (fieldId == BibEntry::AUTHOR || fieldId == BibEntry::TITLE || fieldId == BibEntry::CHAPTER || fieldId == BibEntry::BOOKTITLE) {
            teChosen[fieldId]->setText(teLeft[fieldId]->toPlainText());
        } else {
            leChosen[fieldId]->setText(leLeft[fieldId]->text());
        }
    }

    for (int i = 0; i < lwFilesLeft->count(); i ++) {
        addFileFromLeft(lwFilesLeft->item(i));
    }
    for (int i = 0; i < lwShelfsLeft->count(); i ++) {
        addShelfFromLeft(lwShelfsLeft->item(i));
    }


    takenFromLeft = BibEntry::COL_NUM;
    chosenOwner->setText(left.getField(BibEntry::OWNER));
    chosenTimestamp->setText(left.getField(BibEntry::TIMESTAMP));
    pbAccept->setEnabled(true);
}


void CompareEntriesDialog::moveAllFromRight() {
    for (int fieldId = 0; fieldId < BibEntry::COL_NUM; fieldId ++) {
        if (fieldId == BibEntry::AUTHOR || fieldId == BibEntry::TITLE || fieldId == BibEntry::CHAPTER || fieldId == BibEntry::BOOKTITLE) {
            teChosen[fieldId]->setText(teRight[fieldId]->toPlainText());
        } else {
            leChosen[fieldId]->setText(leRight[fieldId]->text());
        }
    }

    for (int i = 0; i < lwFilesRight->count(); i ++) {
        addFileFromLeft(lwFilesRight->item(i));
    }
    for (int i = 0; i < lwShelfsRight->count(); i ++) {
        addShelfFromLeft(lwShelfsRight->item(i));
    }


    takenFromRight = BibEntry::COL_NUM;
    chosenOwner->setText(right.getField(BibEntry::OWNER));
    chosenTimestamp->setText(right.getField(BibEntry::TIMESTAMP));
    pbAccept->setEnabled(true);
}

void CompareEntriesDialog::takeLocalPressed() {
    if (QMessageBox::question(this, tr("Syncronization"), tr("Do you want to use the only LOCAL values for meging?"), QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes) {
        saction = TAKE_ALL_LOCAL;
        emit accept();
    }
}


void CompareEntriesDialog::takeRemotePressed() {
    if (QMessageBox::question(this, tr("Syncronization"), tr("Do you want to use the only REMOTE values for meging?"), QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes) {
        saction = TAKE_ALL_REMOTE;
        emit accept();
    }
}
