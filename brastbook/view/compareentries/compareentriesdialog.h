#ifndef COMPAREENTRIESDIALOG_H
#define COMPAREENTRIESDIALOG_H

#include <QDialog>
#include <QGridLayout>
#include <QLineEdit>
#include <QLabel>
#include <QTextEdit>
#include <QPushButton>
#include <QComboBox>
#include <QScrollArea>
#include <QListWidget>

#include "../../model/model.h"
#include "../../model/bibentry.h"
#include "../../model/shelfentry.h"

namespace Ui {
class CompareEntriesDialog;
}

class CompareEntriesDialog : public QDialog
{
    Q_OBJECT

public:
    enum Action {
        LEAVE_TWO
        , CANCEL
    };
    enum SyncAction {
        TAKE_ALL_LOCAL
        , TAKE_ALL_REMOTE
        , NO_SYNC
        , SHOW_SYNC_ACITONS
    };
    explicit CompareEntriesDialog(const BibEntry & left, const BibEntry & right, Model * model, Action action, SyncAction saction = NO_SYNC, QWidget *parent = nullptr);
    ~CompareEntriesDialog();

    const BibEntry & getChosenEntry() { return chosen; }
    void setTitles(const QString left, const QString right) { leftTitle.setText(left); rightTitle.setText(right); }

    SyncAction getSyncAction() { return saction; }

private slots:
    void moveFromLeft();
    void moveFromRight();
    void moveAllFromLeft();
    void moveAllFromRight();
    void addFileFromLeft(QListWidgetItem * item);
    void addShelfFromLeft(QListWidgetItem * item);
    void addFileFromRight(QListWidgetItem * item);
    void addShelfFromRight(QListWidgetItem * item);
    void removeFile(QListWidgetItem * item);
    void removeShelf(QListWidgetItem * item);
    void acceptPressed();
    void cancelPressed();
    void takeLocalPressed();
    void takeRemotePressed();

private:
    void addLineEdit(int fieldId, int * row, bool alwaysShow = false);
    void addTextEdit(int fieldId, int * row, bool alwaysShow = false);
    Ui::CompareEntriesDialog *ui;

    QGridLayout * mainLayout;
    QGridLayout * scrollLayout;
    QScrollArea * scrollArea;

    QPushButton * pbAccept, * pbCancel;
    QPushButton * pbTakeLocal, * pbTakeRemote;

    BibEntry left, right, chosen;
    Model * model;
    Action action;
    SyncAction saction;

    int takenFromLeft, takenFromRight;

    QLineEdit ** leLeft, ** leRight, ** leChosen;
    QTextEdit ** teLeft, ** teRight, ** teChosen;
    QPushButton ** pbFromLeft, ** pbFromRight;
    QPushButton * pbAllFromLeft, * pbAllFromRight;
    QComboBox * cbEntrytypeL, * cbEntrytypeR, * cbEntrytypeC;

    QLabel leftTitle, rightTitle;
    QLabel * chosenOwner, * chosenTimestamp;
    bool * toChoose;


    QListWidget * lwFilesLeft, * lwFilesRight, * lwFilesChosen;
    QListWidget * lwShelfsLeft, * lwShelfsRight, * lwShelfsChosen;

    QPalette palette;
};

#endif // COMPAREENTRIESDIALOG_H
