#include <QDebug>
#include <QSettings>
#include <QClipboard>
#include <QGuiApplication>
#include <QTextEdit>
#include <QFileInfo>
#include <QDir>
#include <QDesktopServices>

#include "currentshelfview.h"
#include "../controller/visitor/bibentrytobibtex.h"
#include "../controller/textprocessing.h"

CurrentShelfView::CurrentShelfView(Model * mvc_model, QWidget * parent) :
    QTableView(parent)
  , mvc_model(mvc_model)
{
    setModel(mvc_model->getBibEntriesModel());
    setSelectionBehavior(QAbstractItemView::SelectRows);
    setSelectionMode(QAbstractItemView::SingleSelection);
    setEditTriggers(QAbstractItemView::NoEditTriggers);
    setSortingEnabled(true);
    show();

    menu = new QMenu(this);
    mCopy = menu->addMenu(tr("&Copy as"));
    aCopyBibTeX = mCopy->addAction(tr("&BibTeX"));
    connect(aCopyBibTeX, SIGNAL(triggered()), this, SLOT(copyAsBibTeX()));
    aCopyPreset = mCopy->addAction(tr("&preset"));
    connect(aCopyPreset, SIGNAL(triggered()), this, SLOT(copyAsPreset()));
    menu->addSeparator();
    aOpenFile = menu->addAction(tr("Open &file"));
    connect(aOpenFile, SIGNAL(triggered()), this, SLOT(openFile()));
    aOpenURL = menu->addAction(tr("Open &URL"));
    connect(aOpenURL, SIGNAL(triggered()), this, SLOT(openURL()));
    menu->addSeparator();
    aModify = menu->addAction(tr("&Edit"));
    connect(aModify, SIGNAL(triggered()), this, SLOT(slotModify()));

    rightClickedEntry.setID(BibEntry::NOT_EXISTS);
}


void CurrentShelfView::contextMenuEvent(QContextMenuEvent *event) {
    try {
        int bibid = this->indexAt(event->pos()).siblingAtColumn(0).data(Qt::DisplayRole).toInt();
        rightClickedEntry = mvc_model->getBibEntry(bibid);
        mvc_model->setRightClickedBibEntry(rightClickedEntry);

        menu->exec(event->globalPos());
    }  catch (...) {

    };
}

void CurrentShelfView::mousePressEvent(QMouseEvent *event) {
    if (event->button() != Qt::RightButton)
    {
       QTableView::mousePressEvent(event);
    }
}

void CurrentShelfView::copyAsBibTeX() {
    BibEntryToBibTeX converter;

    QSettings settings;
    settings.beginGroup("BibTeX");
    converter.setExprortFiles(settings.value("exportFiles", true).toBool());
    converter.setExportOwner(settings.value("exportOwner", true).toBool());
    converter.setExportTimestamp(settings.value("exportTimestamp", true).toBool());
    converter.setExportAbstract(settings.value("exportAbstract", true).toBool());
    converter.setNumberIssue(settings.value("makeNumberIssue", true).toBool());
    converter.setDOI2URL(settings.value("makeDOI2URL", true).toBool());
    converter.setExportLanguage(settings.value("exportLanguage", true).toBool());
    settings.endGroup();

    converter.processBibEntry(rightClickedEntry);

    QClipboard * cb = QGuiApplication::clipboard();
    cb->setText(converter.toString());
}

void CurrentShelfView::copyAsPreset() {
    QStringList presets = DBVars::getInstance().getPresets();

//    QClipboard * cb = QGuiApplication::clipboard();

    QTextEdit * te = new QTextEdit(this);


    QSettings settings;
    settings.beginGroup("General");
    bool namesAfter = settings.value("NamesAfter", false).toBool();
    bool splitComma = settings.value("SplitComma", false).toBool();
    bool abbreviate = settings.value("Abbreviate", false).toBool();
    settings.endGroup();

    te->setHtml(TextProcessing::replacePlaceholders(mvc_model, presets[rightClickedEntry.getEntryType()], rightClickedEntry, namesAfter, splitComma, abbreviate));
    te->selectAll();
    te->copy();

//    cb->setText(te->toPlainText());
}

void CurrentShelfView::openFile() {
    if (!mvc_model) {
        return;
    }

    auto files = rightClickedEntry.getFiles();

    if (files.count() > 0) {
        QString path = QDir::cleanPath("file://" + QDir::toNativeSeparators(DBVars::getInstance().getFilePath() + QDir::separator() + files[0]));
        QDesktopServices::openUrl(QUrl(path));
    }
}

void CurrentShelfView::openURL() {
    if (!mvc_model) {
        return;
    }

    if (rightClickedEntry.getField(BibEntry::URL).length() > 0) {
        QDesktopServices::openUrl(QUrl(rightClickedEntry.getField(BibEntry::URL)));
    } else if (rightClickedEntry.getField(BibEntry::DOI).length() > 0) {
        QDesktopServices::openUrl(QUrl("https://doi.org/" + rightClickedEntry.getField(BibEntry::DOI)));
    }
}

void CurrentShelfView::slotModify() {
    BibEntry entry = mvc_model->getRightClickedEntry();

    if (entry.getID() == BibEntry::NOT_EXISTS) {
        return;
    }

    emit modify(entry);
}
