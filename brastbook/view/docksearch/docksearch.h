#ifndef DOCKSEARCH_H
#define DOCKSEARCH_H

#include <QDockWidget>
#include <QObject>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QListWidget>
#include <QLineEdit>
#include <QCheckBox>

#include "../../model/model.h"
#include "../../controller/controller.h"
#include "../../model/bibentry.h"
#include "../../model/shelfentry.h"

class DockSearchWidget : public QDockWidget
{
    Q_OBJECT
public:
    DockSearchWidget(QWidget *parent, Model * model, Controller * controller);

signals:
    void searchIsPerformed();

public slots:
    void setCurrentShelf(ShelfEntry shelf);
    void setAll();
    void unsetAll();
    void search();

private:
    void initCheckboxes();
    QWidget * baseWidget;
    QGridLayout * mainLayout;
    QLineEdit * leSearch;
    QPushButton * pbSearch;
    QListWidget * lwColumns;
    QPushButton * pbSetAll;
    QPushButton * pbUnsetAll;

    QCheckBox * cbSkipSpecial;

    Model * model;
    Controller * controller;

    ShelfEntry currentShelf;
};

#endif // DOCKSEARCH_H
