#include "docksearch.h"

DockSearchWidget::DockSearchWidget(QWidget *parent, Model * model, Controller * controller) :
    QDockWidget(parent)
  , model(model)
  , controller(controller)
  , currentShelf(-1)
{    
    baseWidget = new QWidget(this);
    this->setAllowedAreas(Qt::AllDockWidgetAreas);
    this->setWindowTitle(tr("Search"));
    this->setWidget(baseWidget);
    setObjectName("dock-search");

    mainLayout = new QGridLayout(baseWidget);

    QLabel * l1 = new QLabel(tr("Search text:"), this);
    mainLayout->addWidget(l1, 0, 0, 1, -1);

    leSearch = new QLineEdit(this);
    mainLayout->addWidget(leSearch, 1, 0, 1, -1);
    connect(leSearch, SIGNAL(returnPressed()), this, SLOT(search()));

    pbSearch = new QPushButton(tr("Search"), this);
    mainLayout->addWidget(pbSearch, 2, 0, 1, -1);
    connect(pbSearch, SIGNAL(clicked()), this, SLOT(search()));

    cbSkipSpecial = new QCheckBox(tr("Skip special symbols"), this);
    cbSkipSpecial->setToolTip(tr("Filters out symbols like {, _, \\, etc (but slows down search)"));
    mainLayout->addWidget(cbSkipSpecial, 3, 0, 1, -1);

    lwColumns = new QListWidget(this);
    lwColumns->setToolTip(tr("All checked and all unchecked means the same"));
    mainLayout->addWidget(lwColumns, 4, 0, 1, -1);
    initCheckboxes();

    pbSetAll = new QPushButton(tr("Set all"), this);
    mainLayout->addWidget(pbSetAll, 5, 0);
    connect(pbSetAll, SIGNAL(clicked()), this, SLOT(setAll()));

    pbUnsetAll = new QPushButton(tr("Unset all"), this);
    mainLayout->addWidget(pbUnsetAll, 5, 1);
    connect(pbUnsetAll, SIGNAL(clicked()), this, SLOT(unsetAll()));

//    baseWidget->setLayout(mainLayout);
}

void DockSearchWidget::setCurrentShelf(ShelfEntry shelf) {
    currentShelf = shelf;
}

void DockSearchWidget::setAll() {
    for (int i = 0; i < lwColumns->count(); i ++) {
        lwColumns->item(i)->setCheckState(Qt::Checked);
    }
}

void DockSearchWidget::unsetAll() {
    for (int i = 0; i < lwColumns->count(); i ++) {
        lwColumns->item(i)->setCheckState(Qt::Unchecked);
    }
}

void DockSearchWidget::initCheckboxes() {
    QListWidgetItem * lwi = new QListWidgetItem("author", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("editor", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("title", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("booktitle", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("chapter", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("journal", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
//    lwi = new QListWidgetItem("entrytype", lwColumns);
//    lwi->setCheckState(Qt::Unchecked);
//    lwColumns->addItem(lwi);

    lwi = new QListWidgetItem("bibkey", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("address", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("annote", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("crossref", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("edition", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("howpublished", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("institution", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("month", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("note", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("number", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("organization", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("pages", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("publisher", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("school", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("series", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("type", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("volume", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("year", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("doi", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("issn", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("isbn", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("url", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("date", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("language", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("owner", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("editedby", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("timestamp", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("editedat", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
    lwi = new QListWidgetItem("abstract", lwColumns);
    lwi->setCheckState(Qt::Unchecked);
    lwColumns->addItem(lwi);
}

void DockSearchWidget::search() {
    if (leSearch->text().trimmed().length() > 0) {
        QString searchCondition = "";
        QStringList columns;

        for (int i = 0; i < lwColumns->count(); i ++) {
            if (lwColumns->item(i)->checkState() == Qt::Checked) {
                columns.append(lwColumns->item(i)->text());
            }
        }
        if (columns.length() == 0) {
            for (int i = 0; i < lwColumns->count(); i ++) {
                columns.append(lwColumns->item(i)->text());
            }
        }

        int shelf_id = currentShelf.getID();
        if (shelf_id < 0) {
            shelf_id = model->getDefaultShelfId();
        }
        controller->search(leSearch->text(), columns, cbSkipSpecial->checkState() == Qt::Checked, shelf_id);
        emit searchIsPerformed();
    }
}
