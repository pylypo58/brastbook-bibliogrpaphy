#include <QLayout>
#include "about.h"
#include "ui_about.h"

About::About(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::About)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint);
    this->setFixedSize(this->width(), this->height());
}

About::~About()
{
    delete ui;
}

void About::on_pushButton_clicked()
{
    close();
}
