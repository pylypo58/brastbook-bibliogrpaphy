#ifndef SHELFPROPERTIES_H
#define SHELFPROPERTIES_H

#include <QDialog>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QCheckBox>
#include <QDialogButtonBox>
#include <QTreeView>

#include "../../model/shelfentry.h"

namespace Ui {
class ShelfProperties;
}

/**
 * @brief The ShelfProperties class
 *
 * Part of View
 *
 * This class represents a dialog window to edit new or existing shelf properties,
 * such as name, linked external database, parent etc.
 */
class ShelfProperties : public QDialog
{
    Q_OBJECT

public:
    /**
     * Constructor
     * @param entry Shelf entry to edit
     * @param bookcaseModel Model class to work with shelfs
     * @param filePath global file path
     * @param parent Qt parent
     * @param title Window title (e.g. "Add new shelf" or "Edit shelf")
     */
    explicit ShelfProperties(ShelfEntry & entry, QAbstractItemModel * bookcaseModel, QString filePath, QWidget *parent = nullptr, QString title = "", const ShelfEntry * parentEntry = nullptr);

    ~ShelfProperties();

    /**
     * @brief getShelfState
     * @return Returns shelf object with parameters, defined by user
     */
    ShelfEntry getShelfState();

    /**
     * @brief setupParentShelf
     * @param parentShelf
     *
     * Setups parent shelf in the respective view
     */
    void setParentShelf(const ShelfEntry & parentShelf);

private slots:
    /**
     * @brief showSelectExtDBpathDialog
     *
     * Opens dialog to select file of external database project
     */
    void showSelectExtDBpathDialog();

    /**
     * @brief showSelectGitFilesPathDialog
     *
     * Opens dialog to select file for the syncronization
     */
    void showSelectGitFilesPathDialog();

    /**
     * @brief setSharedState
     * @param checked
     *
     * Helper to enable/disable sharing
     */
    void setSharedState(bool checked);

    /**
     * @brief processSelectedShelf
     * @param selected
     * @param previous
     *
     * Helper to select the parent shelf in tree view
     */
    void processSelectedShelf(const QItemSelection & selected, const QItemSelection & previous);

private:
    /**
     * @brief selectParent
     *
     * Find parent shelf in the tree view
     */
    void selectParent();
    /**
     * @brief selectParent
     *
     * Helper for finding parent shelf
     * @param index QModelIndex object, children of which should be checked
     * @return true if the shelf is found
     *
     * @see selectParent
     */
    bool selectParent(QModelIndex & index);


    /**
     * @brief selectShelf
     * @param entry shelf to find
     *
     * Find shelf in the tree view
     */
    void selectShelf(const int entry_id);
    /**
     * @brief selectShelf
     *
     * Helper for finding shelf
     * @param index QModelIndex object, children of which should be checked
     * @param entry shelf to find
     * @return true if the shelf is found
     *
     * @see selectShelf
     */
    bool selectShelf(QModelIndex & index, const int entry_id);

    Ui::ShelfProperties *ui;

    ShelfEntry entry;
    QAbstractItemModel * bookcaseModel;
    QString filePath;

    QFormLayout * mainLayout;
    QGridLayout * mainLayout1;

    QLineEdit * leName;
    QLineEdit * leextDBpath;
    QCheckBox * cbShared;
    QLineEdit * leRepoFile;
    QPushButton * pbGitpath;

    QTreeView * parentShelfView;
    ShelfEntry parentShelf;

//    bool selectPrevious1, selectPrevious2;
//    QItemSelection previousParent;
};

#endif // SHELFPROPERTIES_H
