#ifndef IMPORTBYMETADATA_H
#define IMPORTBYMETADATA_H

#include <QDialog>
#include <QLineEdit>
#include <QLabel>
#include <QComboBox>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTimer>

#include "../../model/bibentry.h"
#include "../../controller/import/importer.h"

namespace Ui {
class ImportByMetadata;
}

/**
 * @brief The ImportByMetadata class
 *
 * Part of View
 *
 * This class represents a dialog window to import bibliographical entry
 * from remote servers like Crossref, ArXiv etc.
 */
class ImportByMetadata : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief ImportByMetadata Costructor
     * @param email QString value with e-mail of user. Is needed for DOI import and should be replaced by a structure with different parameters
     * @param parent QWidget parent
     */
    explicit ImportByMetadata(QString email, QWidget *parent = nullptr);
    ~ImportByMetadata();

    /**
     * @brief getEntries
     * @return list of BibEntry objects which have been parsed from the external servise
     */
    QList<BibEntry>  getEntries() { return entries; }

public slots:
    /**
     * @brief acceptChanges
     *
     * Accept slot of the dialog window
     */
    void acceptChanges();

    /**
     * @brief addByMetadataPressed
     *
     * Action for the button to add the data from remote service
     */
    void addByMetadataPressed();

    /**
     * @brief cancel
     *
     * Cancel slot of the dialog window
     */
    void cancel();

private slots:
    void on_buttonBox_accepted();

    /**
     * @brief whenTimerIsStopped
     *
     * For waiting of response from server
     */
    void whenTimerIsStopped();

    /**
     * @brief whenTimerIsStopped
     * @param result bool indicates success of the response from the remote service
     *
     * For waiting result from server
     */
    void whenTimerIsStopped(bool result);

private:
    static const int WAIT_INTERVAL = 5000; /**< How long to wait the response, in ms */

    // List of string constants for the dropdown menu with remote services
    static const QString DOIService; /**< Crossref by DOI */

    Ui::ImportByMetadata *ui;

    QVBoxLayout * mainLayout;
    QHBoxLayout * hblButtons;
    QLineEdit * leIdentifier;
    QComboBox * cbService;
    QPushButton * pbAdd, * pbCancel;

    QList<BibEntry> entries;
    QString email;
    bool resultIsObtained;

    QTimer * timer;
    Importer * importer;
};

#endif // IMPORTBYMETADATA_H
