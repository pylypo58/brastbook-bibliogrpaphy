#include <QMessageBox>
#include "importbymetadata.h"
#include "ui_importbymetadata.h"

#include "../../global.h"
#include "../../controller/import/doiimport.h"

const QString ImportByMetadata::DOIService = "DOI";

ImportByMetadata::ImportByMetadata(QString email, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ImportByMetadata)
  , email(email)
{
    importer = nullptr;

    ui->setupUi(this);

    this->setWindowTitle(tr("Import by metadata"));
    connect(this, SIGNAL(accepted()), this, SLOT(acceptChanges()));

    mainLayout = new QVBoxLayout(this);

    QLabel * l1 = new QLabel(tr("Select service:"), this);
    mainLayout->addWidget(l1);

    cbService = new QComboBox(this);
    cbService->addItem(ImportByMetadata::DOIService);
//    cbService->setItemData(0, );
//    cbService->addItem("ArXiv");
    mainLayout->addWidget(cbService);

    QLabel * l2 = new QLabel(tr("Identifier:"), this);
    mainLayout->addWidget(l2);

    leIdentifier = new QLineEdit(this);
    mainLayout->addWidget(leIdentifier);
    leIdentifier->setFocus();
    connect(leIdentifier, SIGNAL(returnPressed()), this, SLOT(addByMetadataPressed()));

    hblButtons = new QHBoxLayout(this);
    pbAdd = new QPushButton(tr("Add"), this);
    connect(pbAdd, SIGNAL(clicked()), this, SLOT(addByMetadataPressed()));
    hblButtons->addWidget(pbAdd);
    pbCancel = new QPushButton(tr("Cancel"), this);
    connect(pbCancel, SIGNAL(clicked()), this, SLOT(cancel()));
    hblButtons->addWidget(pbCancel);
    mainLayout->addLayout(hblButtons);

    ui->buttonBox->setVisible(false);

//    this->setLayout(mainLayout);
}

ImportByMetadata::~ImportByMetadata()
{
    delete ui;
}

void ImportByMetadata::addByMetadataPressed() {
    if (cbService->currentText() == DOIService) {
        if (email.trimmed().length() == 0) {
            QMessageBox::critical(this, APP_NAME, tr("Please add your e-mail in Service->Preferences->Personal to use Crossref!"));
            return;
        }
        importer = new DOIImport(leIdentifier->text(), email);
    } else {

    }

    timer = new QTimer(this);
    disconnect(timer, SIGNAL(timeout()), 0, 0);
    disconnect(importer, SIGNAL(responseIsObtained(bool)), 0, 0);
    connect(timer, SIGNAL(timeout()), this, SLOT(whenTimerIsStopped()));
    connect(importer, SIGNAL(responseIsObtained(bool)), this, SLOT(whenTimerIsStopped(bool)));
    timer->start(WAIT_INTERVAL);
    importer->process();
}

void ImportByMetadata::cancel() {
    emit QDialog::reject();
}

void ImportByMetadata::acceptChanges() {

}

void ImportByMetadata::on_buttonBox_accepted()
{

}

void ImportByMetadata::whenTimerIsStopped() {
    QMessageBox::critical(this, APP_NAME, tr("Response from server has not been received :("));
//    importer->deleteLater();
    emit QDialog::reject();
}


void ImportByMetadata::whenTimerIsStopped(bool result) {
    timer->stop();
    delete timer;
    resultIsObtained = result;

    if (!result) {
        QMessageBox::critical(this, APP_NAME, tr("Wrong response from the server!"));

        emit QDialog::reject();
        return;
    }

    try {
        entries = importer->getProcessed();

        importer->deleteLater();

        emit QDialog::accept();
    } catch (std::exception & e) {
        QMessageBox::critical(this, APP_NAME, e.what());
        emit QDialog::reject();
    } catch (...) {
        QMessageBox::critical(this, APP_NAME, tr("Unpredicted exception during adding the entry."));
        emit QDialog::reject();
    }
}
