#include <QDesktopServices>
#include <QGuiApplication>
#include <QClipboard>
#include <QFileInfo>
#include <QDir>
#include <QMessageBox>
#include <QList>
#include <QSettings>

#include "../../global.h"
#include "dockinfowidget.h"
#include "../entryproperties/entryproperties.h"
#include "../../controller/textprocessing.h"
#include "../../controller/visitor/bibentrytobibtex.h"

DockInfoWidget::DockInfoWidget(QWidget *parent, Model * model) :
    QDockWidget(parent)
  , model(model)
  , currentShelf(ShelfEntry::DEFAULT)
{    
    baseWidget = new QWidget(this);
    this->setAllowedAreas(Qt::AllDockWidgetAreas);
    this->setWindowTitle(tr("Current entry"));
    setObjectName("dock-info");

    this->setMinimumHeight(DockInfoWidget::MIN_WIDGET_HEIGHT);
    this->setWidget(baseWidget);

    mainLayout = new QGridLayout(baseWidget);

    pbBibtexKey = new QPushButton(tr("Bibtex Key"));
    pbBibtexKey->setDisabled(true);
    pbBibtexKey->setToolTip(tr("Click to copy into Clipboard"));
    mainLayout->addWidget(pbBibtexKey, 0, 0, 1, 2);
    connect(pbBibtexKey, SIGNAL(clicked()), this, SLOT(copyBibtexKey()));

    QLabel * l1 = new QLabel(tr("Associated files:"), baseWidget);
    mainLayout->addWidget(l1, 1, 0, 1, 2);

    lwFiles = new QListWidget(baseWidget);
    lwFiles->setToolTip(tr("Double click on file to open in the default viewer"));
    mainLayout->addWidget(lwFiles, 2, 0, 1, 2);
    connect(lwFiles, SIGNAL(itemActivated(QListWidgetItem * )), this, SLOT(openFile(QListWidgetItem * )));

    pbDOIURL = new QPushButton(tr("DOI/URL"), baseWidget);
    pbDOIURL->setDisabled(true);
    pbDOIURL->setToolTip(tr("Opens DOI (or URL) in browser"));
    mainLayout->addWidget(pbDOIURL, 3, 0);
    connect(pbDOIURL, SIGNAL(clicked()), this, SLOT(openDOIorURL()));

    pbCopyBibTeX = new QPushButton(tr("Copy BibTeX"), baseWidget);
    pbCopyBibTeX->setDisabled(true);
    pbCopyBibTeX->setToolTip(tr("Copies entry in BibTeX format to clipboard"));
    mainLayout->addWidget(pbCopyBibTeX, 3, 1);
    connect(pbCopyBibTeX, SIGNAL(clicked()), this, SLOT(copyBibTeX()));

    pbEdit = new QPushButton(tr("Edit"), baseWidget);
    pbEdit->setDisabled(true);
    mainLayout->addWidget(pbEdit, 4, 0, 1, 2);
    connect(pbEdit, SIGNAL(clicked()), this, SLOT(openEditWindow()));

    teInfo = new QTextEdit(baseWidget);
    teInfo->setReadOnly(true);
    mainLayout->addWidget(teInfo, 0, 2, -1, 1);

//    baseWidget->setLayout(mainLayout);
}

void DockInfoWidget::setBibEntry(BibEntry selectedEntry) {
    model->setCurrentBibEntry(selectedEntry);

    this->currentBibEntry = selectedEntry;

    pbBibtexKey->setText(currentBibEntry.getField(BibEntry::BIBKEY));
    pbBibtexKey->setEnabled(true);

    if (currentBibEntry.getField(BibEntry::URL).length() > 0 || currentBibEntry.getField(BibEntry::DOI).length() > 0) {
        pbDOIURL->setEnabled(true);
    } else{
        pbDOIURL->setDisabled(true);
    }

    pbCopyBibTeX->setEnabled(true);

    pbEdit->setEnabled(true);
    updateHTMLPreview();

    QFileInfo fi;
    lwFiles->clear();
    foreach (auto filepath, currentBibEntry.getFiles()) {
        fi.setFile(filepath);
        lwFiles->addItem(fi.fileName());
    }
}

void DockInfoWidget::updateHTMLPreview() {
    QString s;
    QStringList presets = DBVars::getInstance().getPresets();

    QSettings settings;
    settings.beginGroup("General");
    bool namesAfter = settings.value("NamesAfter", false).toBool();
    bool splitComma = settings.value("SplitComma", false).toBool();
    bool abbreviate = settings.value("Abbreviate", false).toBool();
    settings.endGroup();

    teInfo->setHtml(TextProcessing::replacePlaceholders(model, presets[currentBibEntry.getEntryType()], currentBibEntry, namesAfter, splitComma, abbreviate));
}


void DockInfoWidget::openURL() {
    QDesktopServices::openUrl(QUrl(currentBibEntry.getField(BibEntry::URL)));
}

void DockInfoWidget::openDoi() {
    QDesktopServices::openUrl(QUrl("https://doi.org/" + currentBibEntry.getField(BibEntry::DOI)));
}

void DockInfoWidget::openDOIorURL() {
    if (currentBibEntry.getField(BibEntry::DOI).length() > 0) {
        openDoi();
    } else if (currentBibEntry.getField(BibEntry::URL).length() > 0) {
        openURL();
    }
}

void DockInfoWidget::copyBibtexKey() {
    QClipboard * cb = QGuiApplication::clipboard();
    cb->setText(pbBibtexKey->text());
}

void DockInfoWidget::copyBibTeX() {
    BibEntryToBibTeX converter;

    QSettings settings;
    settings.beginGroup("BibTeX");
    converter.setExprortFiles(settings.value("exportFiles", true).toBool());
    converter.setExportOwner(settings.value("exportOwner", true).toBool());
    converter.setExportTimestamp(settings.value("exportTimestamp", true).toBool());
    converter.setExportAbstract(settings.value("exportAbstract", true).toBool());
    converter.setNumberIssue(settings.value("makeNumberIssue", true).toBool());
    converter.setDOI2URL(settings.value("makeDOI2URL", true).toBool());
    converter.setExportLanguage(settings.value("exportLanguage", true).toBool());
    settings.endGroup();

    converter.processBibEntry(currentBibEntry);

    QClipboard * cb = QGuiApplication::clipboard();
    cb->setText(converter.toString());
}

void DockInfoWidget::openFile(QListWidgetItem *item) {
    if (item == nullptr) {
        return;
    }

    int row = lwFiles->row(item);
    QString path = QDir::cleanPath("file://" + QDir::toNativeSeparators(DBVars::getInstance().getFilePath() + QDir::separator() + currentBibEntry.getFiles()[row]));
    QDesktopServices::openUrl(QUrl(path));
}

void DockInfoWidget::openFile() {
    if (lwFiles->count() > 0) {
        QString path = QDir::cleanPath("file://" + QDir::toNativeSeparators(DBVars::getInstance().getFilePath() + QDir::separator() + currentBibEntry.getFiles()[0]));
        QDesktopServices::openUrl(QUrl(path));
    }
}

void DockInfoWidget::openEditWindow() {
    EntryProperties props(currentBibEntry, model, this, tr("Modify entry"));

    props.exec();

    if (props.result() == QDialog::Accepted) {
        try {
            currentBibEntry = props.getEntryState();
            model->updateBibEntry(currentBibEntry, currentShelf);
            setBibEntry(currentBibEntry);
            emit entryIsModified(currentBibEntry);
        } catch (std::exception & e) {
            QMessageBox::critical(this, APP_NAME, e.what());
        } catch (...) {
            QMessageBox::critical(this, APP_NAME, tr("Unpredicted exception during editing the entry."));
        }
    }
}

void DockInfoWidget::openEditWindow(BibEntry entry) {
    EntryProperties props(entry, model, this, tr("Edit entry"));

    props.exec();

    if (props.result() == QDialog::Accepted) {
        try {
            entry = props.getEntryState();
            model->updateBibEntry(entry, currentShelf);
            emit entryIsModified(entry);
        } catch (std::exception & e) {
            QMessageBox::critical(this, APP_NAME, e.what());
        } catch (...) {
            QMessageBox::critical(this, APP_NAME, tr("Unpredicted exception during editing the entry."));
        }
    }
}

void DockInfoWidget::deleteCurrentEntry() {
    if (QMessageBox::question(this, tr("Delete entry"), tr("Delete selected entry?"), QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes) {
        model->deleteBibEntry(currentBibEntry, currentShelf);
        model->setCurrentBibEntry(BibEntry(BibEntry::NOT_EXISTS));
    }
}
