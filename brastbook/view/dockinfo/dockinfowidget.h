#ifndef DOCKINFOWIDGET_H
#define DOCKINFOWIDGET_H

#include <QDockWidget>
#include <QWidget>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QListWidget>
#include <QLabel>
#include <QPushButton>
//#include <QSqlQueryModel>
#include <QTextEdit>

#include "../../model/model.h"
#include "../../model/bibentry.h"
#include "../../model/shelfentry.h"

/**
 * @brief The DockInfoWidget class
 *
 * Part of View
 *
 * This class represents a dockable widget to show information about the
 * bibliogrpahical entry, which is selected elsewhere and assigned to this
 * widget. The main functionality to work with the entry is provided.
 */
class DockInfoWidget : public QDockWidget
{
    Q_OBJECT
public:
    /**
     * @brief DockInfoWidget constructor
     * @param parent parent widget, can be nullptr
     * @param model pointer to the model used for the data storage. Cannot be nullptr
     */
    DockInfoWidget(QWidget *parent, Model * model);

    /**
     * Opens the first file assigned to the entry, if present
     */
    void openFile();

    /**
     * Provides an access to the entry, which is displayed. In absence of entry,
     * the return value has id `BibEntry::DEFAULT`
     * @return Current bibliographical entry
     */
    BibEntry getCurrentEntry() { return currentBibEntry; }

signals:
    /**
     * Signal that the curent entry has been modified
     * @param Current bibliographical entry
     */
    void entryIsModified(BibEntry entry);

public slots:    
    /**
     * Opens URL of the given entry. If URL is absent, opens DOI.
     * If both are absent, does nothing.
     *
     *
     */
    void openDOIorURL();

    /**
     * Setup current shelf, which is needed to update current shelf view
     * @param shelf current shelf
     */
    void setCurrentShelf(const ShelfEntry & shelf) { currentShelf = shelf; };

    /**
     * Setup the given entry to display
     * @param selectedEntry bibliogrraphical entry
     */
    void setBibEntry(BibEntry selectedEntry);

    /**
     * Copies bibtexkey to clipboard
     */
    void copyBibtexKey();

    /**
     * Opens URL in browser, if present
     */
    void openURL();

    /**
     * Opens DOI in browser, if present
     */
    void openDoi();

    /**
     * Opens dialog window to edit the current bibliographical entry
     */
    void openEditWindow();

    /**
     * Copies BibTeX representation of the entry to clipboard
     */
    void copyBibTeX();

    /**
     * Opens dialog window to edit the current bibliographical entry
     */
    void openEditWindow(BibEntry entry);

    /**
     * Deletes current entry asking a confirmation
     */
    void deleteCurrentEntry();

private slots:
    /**
     * Opens selected file, which is associated with the given bibliographical entry
     * @param item item of QListWidget with the list of associated files
     */
    void openFile(QListWidgetItem *item);

private:
    /**
     * Processing of the bibliographical entry's information to display it in QTextWidget
     */
    void updateHTMLPreview();

    static const int MIN_WIDGET_HEIGHT = 150;

    QWidget * baseWidget;

    QGridLayout * mainLayout;

    QPushButton * pbBibtexKey;
    QListWidget * lwFiles;
    QPushButton * pbDOIURL;
    QPushButton * pbCopyBibTeX;
    QTextEdit * teInfo;

    QPushButton * pbEdit;

    Model * model;

    BibEntry currentBibEntry;
    ShelfEntry currentShelf;
};

#endif // DOCKINFOWIDGET_H
