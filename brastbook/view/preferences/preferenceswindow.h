#ifndef PREFERENCESWINDOW_H
#define PREFERENCESWINDOW_H

#include <QDialog>
#include <QSettings>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QLabel>
#include <QLineEdit>
#include <QFormLayout>
#include <QTreeView>

#include "../../model/model.h"

namespace Ui {
class PreferencesWindow;
}

class PreferencesWindow : public QDialog
{
    Q_OBJECT

public:
    explicit PreferencesWindow(Model * model, QWidget *parent = nullptr);
    ~PreferencesWindow();

    QStringList getPresets() { return presets; }
    QString getFilePath();
    QString getOwner();
    QString getEmail();
    QString getEntriesViewColumnsState() { return entriesViewColumnsState; };

    void resizeEvent(QResizeEvent *event) override;

public slots:
    void acceptChanges();

private slots:
    void on_teEntryPreset_textChanged();

    void on_lwEntryTypes_currentRowChanged(int currentRow);

    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void processSelectedShelf(const QItemSelection & selected, const QItemSelection & previous);

    void on_pbAddReplacementRule_clicked();

private:
    void setupTabGeneral(QSettings & settings);
    void setupTabExport(QSettings & settings);
    void setupTabDefaultShelf(QSettings & settings);
    void setupTabShelfView(QSettings & settings);
    void setupTabPreviewPresets(QSettings & settings);
    void setupTabReplacements(QSettings & settings);
    void setupTabPersonal(QSettings & settings);

    Ui::PreferencesWindow *ui;

    /**
     * @brief selectDefaultShelf
     *
     * Find default shelf in the tree view
     */
    void selectDefaultShelf();
    /**
     * @brief selectDefaultShelf
     *
     * Helper for finding default shelf
     * @param index QModelIndex object, children of which should be checked
     * @return true if the shelf is found
     *
     * @see selectDefaultShelf
     */
    bool selectDefaultShelf(QModelIndex & index);


    void initCheckboxes(QString checked);
//    void initCheckbox(QSettings & settings, QString s, QString defval = "");
//    void saveCheckbox(QSettings & settings, QString s, Qt::CheckState cs);

    Model * model;

    QStringList presets;
    QString entriesViewColumnsState;
    QFormLayout * tabGeneralLayout;
    QFormLayout * tabExportLayout;
    QLineEdit * leBibtexkeyPreset;
    QCheckBox * cbFile, * cbOwner, * cbTimestamp, * cbAbstract, * cbIssue, * cbDOI2URL, * cbLanguage;
    QCheckBox * cbNamesAfter, * cbSplitComma, * cbAbbreviate;
    QCheckBox * cbOpenLastDB;
    QCheckBox * cbReplaceUTF;
    QCheckBox * cbSilentExtUpd;
    QCheckBox * cbAutoExtUpd;

    ShelfEntry defaultShelf;
};

#endif // PREFERENCESWINDOW_H
