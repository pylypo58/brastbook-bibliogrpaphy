#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>
#include <QDir>

#include "shelfproperties.h"
#include "ui_shelfproperties.h"
#include "../../global.h"

ShelfProperties::ShelfProperties(ShelfEntry & entry, QAbstractItemModel * bookcaseModel, QString filePath, QWidget *parent, QString title, const ShelfEntry * parentEntry) :
    QDialog(parent),
    ui(new Ui::ShelfProperties)
  , entry(entry)
  , bookcaseModel(bookcaseModel)
  , filePath(filePath)
//  , selectPrevious1(false)
//  , selectPrevious2(false)
{
    ui->setupUi(this);

    this->setWindowTitle(title);

    QSettings settings;
    settings.beginGroup("ShelfPropertiesWindow");
    resize(settings.value("size", QSize(900, 600)).toSize());
    move(settings.value("position", QPoint(100, 100)).toPoint());
    settings.endGroup();

    mainLayout1 = new QGridLayout(this);
    QWidget * dummyWidget = new QWidget(this);
    mainLayout = new QFormLayout(this);

    leName = new QLineEdit(entry.name, this);
    mainLayout->addRow(tr("&Name:"), leName);

    QWidget * extDBwidget = new QWidget(this);
    QPushButton * pbDBpath = new QPushButton(tr("..."), extDBwidget);
    connect(pbDBpath, SIGNAL(clicked()), this, SLOT(showSelectExtDBpathDialog()));
    QHBoxLayout * extDBLayout = new QHBoxLayout(extDBwidget);
    extDBLayout->setContentsMargins(0, 0, 0, 0);
    extDBwidget->setLayout(extDBLayout);
    leextDBpath = new QLineEdit(entry.external_db_path, this);
    extDBLayout->addWidget(leextDBpath);
    extDBLayout->addWidget(pbDBpath);
    mainLayout->addRow(tr("&Associated database:"), extDBwidget);

    cbShared = new QCheckBox(tr("&Shared shelf"), this);
    mainLayout->addRow(cbShared);

    QWidget * gitPathWidget = new QWidget(this);
    leRepoFile = new QLineEdit(entry.repo_file, gitPathWidget);
    pbGitpath = new QPushButton(tr("..."), gitPathWidget);
    connect(pbGitpath, SIGNAL(clicked()), this, SLOT(showSelectGitFilesPathDialog()));
    QHBoxLayout * gitFilePathLayout = new QHBoxLayout(gitPathWidget);
    gitFilePathLayout->setContentsMargins(0, 0, 0, 0);
    gitFilePathLayout->addWidget(leRepoFile);
    gitFilePathLayout->addWidget(pbGitpath);
    mainLayout->addRow(tr("&File in Git repository:"), gitPathWidget);

    connect(cbShared, SIGNAL(toggled(bool)), this, SLOT(setSharedState(bool)));
    if (entry.shared) {
        cbShared->setChecked(true);
        setSharedState(true);
    } else {
        cbShared->setChecked(false);
        setSharedState(false);
    }

    dummyWidget->setLayout(mainLayout);

    mainLayout1->addWidget(dummyWidget, 0, 0, 2, 1);
    mainLayout1->addWidget(new QLabel(tr("Parent shelf:"), this), 0, 1);
    parentShelfView = new QTreeView;
    parentShelfView->setModel(bookcaseModel);
    parentShelfView->setSelectionMode(QAbstractItemView::SingleSelection);
    parentShelfView->setHeaderHidden(true);
    parentShelfView->expandAll();

//    if (parentEntry == nullptr) {
//        selectParent();
//    } else {
//        selectShelf(*parentEntry);
//        parentShelf = *parentEntry;
//    }
    connect(parentShelfView->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)), this, SLOT(processSelectedShelf(const QItemSelection &, const QItemSelection &)));
    mainLayout1->addWidget(parentShelfView, 1, 1);
    parentShelf.setID(entry.parent);
    selectShelf(parentShelf.getID());

    QDialogButtonBox * buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    mainLayout1->addWidget(buttonBox, 2, 0, 1, -1);
    connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

    setLayout(mainLayout1);
}

ShelfProperties::~ShelfProperties()
{
    delete ui;
}

bool ShelfProperties::selectParent(QModelIndex & index) {
    for (int i = 0; i < parentShelfView->model()->rowCount(index); i ++) {
        QModelIndex idx = parentShelfView->model()->index(i, 0, index);
        ShelfEntry shelf = idx.data(Qt::UserRole).value<ShelfEntry>();
        if (shelf.getID() == entry.parent) {
            parentShelfView->selectionModel()->select(idx, QItemSelectionModel::Select | QItemSelectionModel::Rows);
            return true;
        } else {
            if (selectParent(idx)) {
                return true;
            }
        }
    }
    return false;
}

void ShelfProperties::selectParent() {
    if (entry.parent <= 0) {
        // no parent is defined => parent is root
        QModelIndex idx = parentShelfView->model()->index(0, 0);
        parentShelfView->selectionModel()->select(idx, QItemSelectionModel::Select | QItemSelectionModel::Rows);
    } else {
        // traversal over all shelfs to find the parent in the tree view and select it
        for (int i = 0; i < parentShelfView->model()->rowCount(); i ++) {
            QModelIndex idx = parentShelfView->model()->index(i, 0);
            ShelfEntry shelf = idx.data(Qt::UserRole).value<ShelfEntry>();
            if (shelf.getID() == entry.parent) {
                parentShelfView->selectionModel()->select(idx, QItemSelectionModel::Select | QItemSelectionModel::Rows);
            } else {
                if (selectParent(idx)) {
                    return;
                }
            }
        }
    }
}

bool ShelfProperties::selectShelf(QModelIndex & index, const int entry_id) {
    for (int i = 0; i < parentShelfView->model()->rowCount(index); i ++) {
        QModelIndex idx = parentShelfView->model()->index(i, 0, index);
        ShelfEntry shelf = idx.data(Qt::UserRole).value<ShelfEntry>();
        if (shelf.getID() == entry_id) {
            parentShelfView->selectionModel()->select(idx, QItemSelectionModel::Select | QItemSelectionModel::Rows);
            return true;
        } else {
            if (selectShelf(idx, entry_id)) {
                return true;
            }
        }
    }
    return false;
}

void ShelfProperties::selectShelf(const int entry_id) {
    if (entry_id <= 0) {
        QModelIndex idx = parentShelfView->model()->index(0, 0);
        parentShelfView->selectionModel()->select(idx, QItemSelectionModel::Select | QItemSelectionModel::Rows);
    } else {
        // traversal over all shelfs to find the parent in the tree view and select it
        for (int i = 0; i < parentShelfView->model()->rowCount(); i ++) {
            QModelIndex idx = parentShelfView->model()->index(i, 0);
            ShelfEntry shelf = idx.data(Qt::UserRole).value<ShelfEntry>();
            if (shelf.getID() == entry_id) {
                parentShelfView->selectionModel()->select(idx, QItemSelectionModel::Select | QItemSelectionModel::Rows);
            } else {
                if (selectShelf(idx, entry_id)) {
                    return;
                }
            }
        }
    }
}

void ShelfProperties::setParentShelf(const ShelfEntry & parentShelf) {
    selectShelf(parentShelf.getID());
}

void ShelfProperties::showSelectExtDBpathDialog() {
    QString filename = QFileDialog::getSaveFileName(this, tr("Save external database"), leextDBpath->text(), tr("BibTex Files (*.bib)"));

    if (!filename.isEmpty() && !filename.isNull()) {
        leextDBpath->setText(filename);
    }
}

void ShelfProperties::showSelectGitFilesPathDialog() {
    QString filename = QFileDialog::getSaveFileName(this, tr("Choose new/existing file in Git repository"), leextDBpath->text(), tr("Shelf sync files (*.ssf)"), nullptr, QFileDialog::DontConfirmOverwrite);

    if (!filename.isEmpty() && !filename.isNull()) {
        leRepoFile->setText(filename);
    }
}

void ShelfProperties::setSharedState(bool checked) {
    if (checked) {
        leRepoFile->setEnabled(true);
        pbGitpath->setEnabled(true);
    } else {
        leRepoFile->setEnabled(false);
        pbGitpath->setEnabled(false);
    }
}

ShelfEntry ShelfProperties::getShelfState() {
    QDir dir(filePath);

    entry.external_db_path = dir.relativeFilePath(leextDBpath->text().trimmed());
    entry.name = leName->text().trimmed();
    entry.repo_file = dir.relativeFilePath(leRepoFile->text().trimmed());
    entry.shared = cbShared->isChecked();
    if (parentShelf.getID() == ShelfEntry::NOT_EXITS) {
        entry.parent = ShelfEntry::DEFAULT;
    } else {
        entry.parent = parentShelf.getID();
    }

    QSettings settings;
    settings.beginGroup("ShelfPropertiesWindow");
    settings.setValue("size", this->size());
    settings.setValue("position", this->pos());
    settings.endGroup();

    return entry;
}

void ShelfProperties::processSelectedShelf(const QItemSelection & selected, const QItemSelection & previous) {
//    if (selectPrevious1) {
//        selectPrevious1 = false;
//        selectPrevious2 = true;
//        parentShelfView->selectionModel()->select(previousParent.indexes().at(0), QItemSelectionModel::Select | QItemSelectionModel::Rows);
//    }
//    if (selectPrevious2) {
//        return;
//    }

    QModelIndex modelIndex = selected.indexes().at(0);

//    if (QMessageBox::question(this, tr("Change parent"), tr("Are you sure to change parent?"), QMessageBox::Yes|QMessageBox::No) == QMessageBox::No) {
//        selectPrevious1 = true;
//        previousParent = previous;
//        parentShelfView->selectionModel()->clear();
//        return;
//    }

    parentShelf = modelIndex.data(Qt::UserRole).value<ShelfEntry>();
}
