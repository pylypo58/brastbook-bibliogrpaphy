#include <QFileDialog>
#include "preferenceswindow.h"
#include "ui_preferenceswindow.h"
#include "../../global.h"
#include "../../model/bibentry.h"


PreferencesWindow::PreferencesWindow(Model * model, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PreferencesWindow)
  , model(model)
{
    ui->setupUi(this);
    this->setWindowTitle(tr("Preferences"));

    connect(this, SIGNAL(accepted()), this, SLOT(acceptChanges()));

    QSettings settings;
    settings.beginGroup("PreferencesWindow");
    resize(settings.value("size", QSize(660, 555)).toSize());
    move(settings.value("position", QPoint(100, 100)).toPoint());
    settings.endGroup();

    setupTabGeneral(settings);
    setupTabExport(settings);
    setupTabDefaultShelf(settings);
    setupTabShelfView(settings);
    setupTabPreviewPresets(settings);
    setupTabReplacements(settings);
    setupTabPersonal(settings);

    ui->pbAddReplacementRule->move(ui->pbAddReplacementRule->pos().x(), ui->tabReplacements->height()-ui->pbAddReplacementRule->height()-10);
    ui->tabWidget->setCurrentIndex(0);

}

PreferencesWindow::~PreferencesWindow()
{
    delete ui;
}

void PreferencesWindow::setupTabGeneral(QSettings &settings) {
    settings.beginGroup("General");
    tabGeneralLayout = new QFormLayout(ui->tabGeneral);

    leBibtexkeyPreset = new QLineEdit(this);
    tabGeneralLayout->addRow(tr("BibTeX key template:"), new QLabel(tr("Use the following placeholders for BibTeX keys:\n{{author}}: surname of the first author\n{{yyyy}}: full year like 2016\n{{yy}}: short year like 16\n{{JRN}}: automatic journal abbreviation"), this));
    tabGeneralLayout->addWidget(leBibtexkeyPreset);

    cbOpenLastDB = new QCheckBox(tr("Open last used database at startup"), this);
    tabGeneralLayout->addWidget(cbOpenLastDB);
    cbOpenLastDB->setChecked(settings.value("openLastDBatStartup", true).toBool());

    cbNamesAfter = new QCheckBox(tr("Display 'Lastname, Firstname' in detailed information"));
    tabGeneralLayout->addWidget(cbNamesAfter);
    cbNamesAfter->setChecked(settings.value("NamesAfter", true).toBool());

    cbSplitComma = new QCheckBox(tr("Use natural authors view (author1, author2 and author3)"));
    tabGeneralLayout->addWidget(cbSplitComma);
    cbSplitComma->setChecked(settings.value("SplitComma", true).toBool());

    cbAbbreviate = new QCheckBox(tr("Abbreviate authors names"));
    tabGeneralLayout->addWidget(cbAbbreviate);
    cbAbbreviate->setChecked(settings.value("Abbreviate", true).toBool());

    cbReplaceUTF = new QCheckBox(tr("Replace some UTF letters when import"));
    tabGeneralLayout->addWidget(cbReplaceUTF);
    cbReplaceUTF->setChecked(settings.value("ReplaceUTF", true).toBool());

    cbSilentExtUpd = new QCheckBox(tr("Silent update of external projects"));
    tabGeneralLayout->addWidget(cbSilentExtUpd);
    cbSilentExtUpd->setChecked(settings.value("SilentExtUpd", true).toBool());

    cbAutoExtUpd = new QCheckBox(tr("Automatically update external project when entry is added"));
    tabGeneralLayout->addWidget(cbAutoExtUpd);
    cbAutoExtUpd->setChecked(settings.value("AutoExtUpd", true).toBool());


    settings.endGroup();

//    ui->tabGeneral->setLayout(tabGeneralLayout);

}

void PreferencesWindow::setupTabExport(QSettings &settings) {
    tabExportLayout = new QFormLayout(ui->exportTab);

    cbFile = new QCheckBox(tr("Export list of files (aka JabRef format for *.bib)"), this);
    tabExportLayout->addWidget(cbFile);
    cbOwner = new QCheckBox(tr("Export 'owner'/'edited by' as fields"), this);
    tabExportLayout->addWidget(cbOwner);
    cbTimestamp = new QCheckBox(tr("Export 'timestamp'/'edited at' as fields"), this);
    tabExportLayout->addWidget(cbTimestamp);
    cbAbstract = new QCheckBox(tr("Export abstract as a field"), this);
    tabExportLayout->addWidget(cbAbstract);
    cbIssue = new QCheckBox(tr("Duplicate field 'number' as 'issue'"), this);
    tabExportLayout->addWidget(cbIssue);
    cbDOI2URL = new QCheckBox(tr("In absence of URL, use DOI"), this);
    tabExportLayout->addWidget(cbDOI2URL);
    cbLanguage = new QCheckBox(tr("Export 'language' as field"), this);
    tabExportLayout->addWidget(cbLanguage);

//    ui->exportTab->setLayout(tabExportLayout);

    settings.beginGroup("BibTeX");
    leBibtexkeyPreset->setText(settings.value("bibtexkeyTemplate", "{{author}}{{yy}}").toString());
    cbFile->setChecked(settings.value("exportFiles", false).toBool());
    cbOwner->setChecked(settings.value("exportOwner", false).toBool());
    cbTimestamp->setChecked(settings.value("exportTimestamp", true).toBool());
    cbAbstract->setChecked(settings.value("exportAbstract", false).toBool());
    cbIssue->setChecked(settings.value("makeNumberIssue", true).toBool());
    cbDOI2URL->setChecked(settings.value("makeDOI2URL", true).toBool());
    cbLanguage->setChecked(settings.value("exportLanguage", false).toBool());
    settings.endGroup();
}

void PreferencesWindow::setupTabDefaultShelf(QSettings &settings) {
    if (model) {
        ui->twDefaultShelf->setModel(model->getShelfsModel());
    }
    ui->twDefaultShelf->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->twDefaultShelf->setHeaderHidden(true);
    ui->twDefaultShelf->expandAll();
    connect(ui->twDefaultShelf->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)), this, SLOT(processSelectedShelf(const QItemSelection &, const QItemSelection &)));

    settings.beginGroup("DefaultShelf");
    if (model) {
        try {
            defaultShelf = model->getShelfEntry(settings.value("defaultShelfId", model->getRootShelfId()).toInt());
        }  catch (...) {
            defaultShelf = model->getShelfEntry(model->getRootShelfId());
        }
        selectDefaultShelf();
    }
    settings.endGroup();
}

void PreferencesWindow::setupTabShelfView(QSettings & settings) {
    settings.beginGroup("ShelfView");
    QString entriesViewColumnsOrder = settings.value("entriesViewColumnsOrder", "bibkey_c,entrytype_c,author_c,editor_c,title_c,journal_c,volume_c,year_c,owner_c,timestamp_c,address,annote,booktitle,chapter,crossref,edition,howpublished,institution,key,month,note,number,organization,pages,publisher,school,series,type,doi,issn,isbn,url,date,language,editedby_c,editedat_c,abstract").toString();
    initCheckboxes(entriesViewColumnsOrder);
    settings.endGroup();
}

void PreferencesWindow::setupTabPreviewPresets(QSettings &settings) {
    settings.beginReadArray("Presets");
    for (int i = 0; i < BibEntry::BIBENTRYTYPES_NUM; i ++) {
        settings.setArrayIndex(i);
        presets.append(settings.value("entryPreset", BibEntry::DEFAULT_PRESETS[i]).toString());
    }
    settings.endArray();
    ui->lwEntryTypes->setCurrentRow(0);
    this->on_lwEntryTypes_currentRowChanged(0);
}

void PreferencesWindow::setupTabReplacements(QSettings & settings) {
    ui->tabWidget->setTabText(5, "Replacement rules");
    settings.beginGroup("Replacements");

    QStringList headers;
    headers << tr("String") << tr("Replacement");
    ui->tableReplacements->setColumnCount(2);
    ui->tableReplacements->setShowGrid(true);
    ui->tableReplacements->setHorizontalHeaderLabels(headers);
    ui->tableReplacements->setColumnWidth(0, ui->tableReplacements->width()/2 - 5);
    ui->tableReplacements->setColumnWidth(1, ui->tableReplacements->width()/2 - 5);

    int repStringsNum = settings.value("repStringsNum").toInt();
    ui->tableReplacements->setRowCount(repStringsNum);
    for (int i = 0; i < repStringsNum; i ++) {
        ui->tableReplacements->setItem(i, 0, new QTableWidgetItem(settings.value("string2replace" + QString::number(i)).toString()));
        ui->tableReplacements->setItem(i, 1, new QTableWidgetItem(settings.value("stringReplacement" + QString::number(i)).toString()));
    }
    ui->tableReplacements->sortItems(0);

    ui->tableReplacements->setRowCount(repStringsNum+1);
    ui->tableReplacements->setToolTip(tr("Pairs of string -> replacement for automatic replacement of substrings in book/article captions when imported"));
    settings.endGroup();
}

void PreferencesWindow::setupTabPersonal(QSettings & settings) {
    settings.beginGroup("Owner");
    ui->ownerName->setText(settings.value("ownerName", "Owner").toString());
    ui->leEmail->setText(settings.value("ownerEmail", "").toString());
    ui->globalFilePath->setText(settings.value("globalFilePath", "").toString());
    settings.endGroup();

    settings.beginGroup("Git");
    ui->gitPath->setText(settings.value("gitPath", "git.exe").toString());
    ui->commitCommand->setText(settings.value("commitCommand", "commit -a").toString());
    ui->commitMessage->setText(settings.value("commitMessage", "shelf update").toString());
    ui->pullCommand->setText(settings.value("pullCommand", "pull -X theirs").toString());
    ui->pushCommand->setText(settings.value("pushCommand", "push").toString());
    settings.endGroup();
}

void PreferencesWindow::resizeEvent(QResizeEvent *event) {
    QDialog::resizeEvent(event);

    int dist = 10;
    QSize winSize = this->size();
    QWidget * widget;
    QPoint pos;

    widget = ui->buttonBox;
    widget->move(winSize.width()-widget->width()-dist, winSize.height()-widget->height()-dist);

    widget = ui->tabWidget;
    pos = widget->pos();
    widget->resize(winSize.width()-pos.x()-dist, winSize.height()-pos.y()-ui->buttonBox->height()-2*dist);

    QSize tabSize = ui->tabWidget->size();
    QSize tabBarSize = ui->tabWidget->tabBar()->size();
    widget = ui->shelfViewColumns;
    pos = widget->pos();
    widget->resize(tabSize.width()-pos.x()-2*dist, tabSize.height()-tabBarSize.height()-pos.y()-2*dist);

    widget = ui->lwEntryTypes;
    pos = widget->pos();
    widget->resize(widget->width(), tabSize.height()-tabBarSize.height()-pos.y()-2*dist);

    widget = ui->teEntryPreset;
    pos = widget->pos();
    widget->resize(tabSize.width()-pos.x()-2*dist, tabSize.height()-tabBarSize.height()-pos.y()-2*dist);

    widget = ui->tableReplacements;
    pos = widget->pos();
    widget->resize(tabSize.width()-pos.x()-2*dist, tabSize.height()-tabBarSize.height()-pos.y()-ui->pbAddReplacementRule->height()-2*dist);
    ui->pbAddReplacementRule->move(ui->pbAddReplacementRule->pos().x(), ui->tabReplacements->height()-ui->pbAddReplacementRule->height()-dist);

    widget = ui->twDefaultShelf;
    pos = widget->pos();
    widget->resize(tabSize.width()-pos.x()-2*dist, tabSize.height()-tabBarSize.height()-pos.y()-2*dist);
}

void PreferencesWindow::initCheckboxes(QString checked) {
    QStringList states = checked.split(',');
    foreach (auto state, states) {
        QListWidgetItem * lwi = new QListWidgetItem("", ui->shelfViewColumns);
        if (state.right(2) == INI_CHECKED_STATE) {
            lwi->setCheckState(Qt::Checked);
            lwi->setText(state.mid(0, state.length() - 2));
        } else {
            lwi->setCheckState(Qt::Unchecked);
            lwi->setText(state);
        }
        ui->shelfViewColumns->addItem(lwi);
    }
}

void PreferencesWindow::acceptChanges() {
    QSettings settings;

    settings.beginGroup("PreferencesWindow");
    settings.setValue("size", size());
    settings.setValue("position", pos());
    settings.endGroup();

    settings.beginGroup("Owner");
    settings.setValue("ownerName", ui->ownerName->text());
    settings.setValue("ownerEmail", ui->leEmail->text());
    settings.setValue("globalFilePath", ui->globalFilePath->text());
    settings.endGroup();

    settings.beginGroup("Git");
    settings.setValue("gitPath", ui->gitPath->text());
    settings.setValue("commitCommand", ui->commitCommand->text());
    settings.setValue("commitMessage", ui->commitMessage->text());
    settings.setValue("pullCommand", ui->pullCommand->text());
    settings.setValue("pushCommand", ui->pushCommand->text());
    settings.endGroup();

    settings.beginGroup("ShelfView");
    entriesViewColumnsState = "";
    QString entriesViewColumnsOrder = "";
    if (ui->shelfViewColumns->item(0)->checkState() == Qt::Checked) {
        entriesViewColumnsOrder = ui->shelfViewColumns->item(0)->text() + INI_CHECKED_STATE;
        entriesViewColumnsState = ui->shelfViewColumns->item(0)->text();
    } else {
        entriesViewColumnsOrder = ui->shelfViewColumns->item(0)->text();
    }
    for(int i = 1; i < ui->shelfViewColumns->count(); i++) {
        if (ui->shelfViewColumns->item(i)->checkState() == Qt::Checked) {
            entriesViewColumnsOrder += "," + ui->shelfViewColumns->item(i)->text() + INI_CHECKED_STATE;
            entriesViewColumnsState += "," + ui->shelfViewColumns->item(i)->text();
        } else {
            entriesViewColumnsOrder += "," + ui->shelfViewColumns->item(i)->text();
        }
    }
    settings.setValue("entriesViewColumnsOrder", entriesViewColumnsOrder);
    settings.setValue("entriesViewColumnsState", entriesViewColumnsState);
    settings.endGroup();

    settings.beginWriteArray("Presets");
    for (int i = 0; i < BibEntry::BIBENTRYTYPES_NUM; i ++) {
        settings.setArrayIndex(i);
        settings.setValue("entryPreset", presets.at(i));
    }
    settings.endArray();

    settings.beginGroup("BibTeX");
    settings.setValue("bibtexkeyTemplate", leBibtexkeyPreset->text());
    settings.setValue("exportFiles", cbFile->isChecked());
    settings.setValue("exportOwner", cbOwner->isChecked());
    settings.setValue("exportTimestamp", cbTimestamp->isChecked());
    settings.setValue("exportAbstract", cbAbstract->isChecked());
    settings.setValue("makeNumberIssue", cbIssue->isChecked());
    settings.setValue("makeDOI2URL", cbDOI2URL->isChecked());
    settings.setValue("exportLanguage", cbLanguage->isChecked());
    settings.endGroup();

    settings.beginGroup("DefaultShelf");
    if (model) {
        model->setDefaultShelfId(defaultShelf.getID());
        settings.setValue("defaultShelfId", model->getDefaultShelfId());
    }
    settings.endGroup();

    settings.beginGroup("General");
    settings.setValue("openLastDBatStartup", cbOpenLastDB->isChecked());
    settings.setValue("NamesAfter", cbNamesAfter->isChecked());
    settings.setValue("SplitComma", cbSplitComma->isChecked());
    settings.setValue("Abbreviate", cbAbbreviate->isChecked());
    settings.setValue("ReplaceUTF", cbReplaceUTF->isChecked());
    settings.setValue("SilentExtUpd", cbSilentExtUpd->isChecked());
    settings.setValue("AutoExtUpd", cbAutoExtUpd->isChecked());
    settings.endGroup();


    settings.beginGroup("Replacements");



    int repStringsNum = 0;
    for (int i = 0; i < ui->tableReplacements->rowCount(); i ++) {
        if (ui->tableReplacements->item(i, 0)) {
            QString key = ui->tableReplacements->item(i, 0)->text().trimmed();
            QString value = ui->tableReplacements->item(i, 1)->text().trimmed();
            if (key.length() > 0) {
                settings.setValue("string2replace" + QString::number(repStringsNum), key);
                settings.setValue("stringReplacement" + QString::number(repStringsNum), value);
                repStringsNum ++;
            }
        }
    }
    settings.setValue("repStringsNum", repStringsNum);
    settings.endGroup();
}

void PreferencesWindow::on_teEntryPreset_textChanged()
{
    presets.replace(ui->lwEntryTypes->currentRow(), ui->teEntryPreset->toPlainText());
}

void PreferencesWindow::on_lwEntryTypes_currentRowChanged(int currentRow)
{
    ui->teEntryPreset->setPlainText(presets.at(currentRow));
    ui->teEntryPreset->show();
}

QString PreferencesWindow::getFilePath() {
    return ui->globalFilePath->text();
}

QString PreferencesWindow::getOwner() {
    return ui->ownerName->text();
}

QString PreferencesWindow::getEmail() {
    return ui->leEmail->text();
}

void PreferencesWindow::on_pushButton_clicked()
{
    QString filename =  QFileDialog::getExistingDirectory(this, tr("Select folder for files"), ui->globalFilePath->text(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    if (!filename.isEmpty() && !filename.isNull()) {
        ui->globalFilePath->setText(filename);
    }
}

void PreferencesWindow::on_pushButton_3_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, tr("Select path to Git"), ui->globalFilePath->text());

    if (!filename.isEmpty() && !filename.isNull()) {
        ui->gitPath->setText(filename);
    }
}

bool PreferencesWindow::selectDefaultShelf(QModelIndex & index) {
    for (int i = 0; i < ui->twDefaultShelf->model()->rowCount(index); i ++) {
        QModelIndex idx = ui->twDefaultShelf->model()->index(i, 0, index);
        ShelfEntry shelf = idx.data(Qt::UserRole).value<ShelfEntry>();
        if (shelf.getID() == defaultShelf.getID()) {
            ui->twDefaultShelf->selectionModel()->select(idx, QItemSelectionModel::Select | QItemSelectionModel::Rows);
            return true;
        } else {
            if (selectDefaultShelf(idx)) {
                return true;
            }
        }
    }
    return false;
}

void PreferencesWindow::selectDefaultShelf() {
    if (defaultShelf.getID() <= 0) {
        QModelIndex idx = ui->twDefaultShelf->model()->index(0, 0);
        ui->twDefaultShelf->selectionModel()->select(idx, QItemSelectionModel::Select | QItemSelectionModel::Rows);
    } else {
        // traversal over all shelfs to find the parent in the tree view and select it
        for (int i = 0; i < ui->twDefaultShelf->model()->rowCount(); i ++) {
            QModelIndex idx = ui->twDefaultShelf->model()->index(i, 0);
            ShelfEntry shelf = idx.data(Qt::UserRole).value<ShelfEntry>();
            if (shelf.getID() == defaultShelf.getID()) {
                ui->twDefaultShelf->selectionModel()->select(idx, QItemSelectionModel::Select | QItemSelectionModel::Rows);
            } else {
                if (selectDefaultShelf(idx)) {
                    return;
                }
            }
        }
    }
}

void PreferencesWindow::processSelectedShelf(const QItemSelection & selected, const QItemSelection & previous) {
    QModelIndex modelIndex = selected.indexes().at(0);

    defaultShelf = modelIndex.data(Qt::UserRole).value<ShelfEntry>();
}

void PreferencesWindow::on_pbAddReplacementRule_clicked()
{
    ui->tableReplacements->setRowCount(ui->tableReplacements->rowCount()+1);
}

