#ifndef LASTOPENEDMENU_H
#define LASTOPENEDMENU_H

#include <QMenu>
#include <QAction>
#include <QObject>

class LastOpenedMenu : public QAction
{
    Q_OBJECT
public:
    LastOpenedMenu(QString filePath, QWidget *parent);

    QString getFilePath() { return filePath; }

private:
    QString filePath;
};

#endif // LASTOPENEDMENU_H
