#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QSettings>
#include <QDateTime>
#include <QScrollBar>

#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "global.h"
#include "model/bibentry.h"
#include "controller/import/jabrefimport.h"
#include "controller/import/risimport.h"
#include "controller/visitor/syncinfo.h"
#include "view/preferences/preferenceswindow.h"
#include "view/shelfproperties/shelfproperties.h"
#include "view/entryproperties/entryproperties.h"
#include "view/importbymetadata/importbymetadata.h"
#include "view/compareentries/compareentriesdialog.h"
#include "view/about/about.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , tableView(nullptr)
    , dockShelfsWidget(nullptr)
    , dockInfoWidget(nullptr)
    , dockSearchWidget(nullptr)
    , controller(nullptr)
    , model(nullptr)
    , shelfsTree(nullptr)
{
    ui->setupUi(this);

    this->setWindowTitle(APP_NAME);

    QCoreApplication::setOrganizationName("HZDR");
//    QCoreApplication::setOrganizationDomain("hzdr.de");
    QCoreApplication::setApplicationName(APP_NAME);

    QSettings settings;
    settings.beginGroup("MainWindow");
    resize(settings.value("size", QSize(1000, 900)).toSize());
    move(settings.value("position", QPoint(100, 100)).toPoint());

    int lastOpened = settings.value("lastOpenedNum", 0).toInt();
    settings.beginReadArray("LastOpened");
    for (int i = 0; i < lastOpened; i ++) {
        settings.setArrayIndex(i);
        QString path = settings.value("filePath", "").toString();
        LastOpenedMenu * menu = new LastOpenedMenu(path, this);
        connect(menu, SIGNAL(triggered()), this, SLOT(openRecent()));
        qmLastDB.append(menu);
    }
    QList<QAction *> actions;
    for (int i = 0; i < qmLastDB.size(); i ++) {
        actions.append(qmLastDB.at(i));
    }
    ui->actionRecent->setMenu(new QMenu(this));
    ui->actionRecent->menu()->addActions(actions);
    settings.endArray();


    int size = settings.beginReadArray("columnWidths");
    if (size > 0) {
        for (int i = 0; i < size; i ++) {
            settings.setArrayIndex(i);
            QString colTitle = settings.value("columnTitle", "").toString();
            int colWidth = settings.value("columnWidth", 200).toInt();
            columnWidths.insert(colTitle, colWidth);
        }
    } else {
        columnWidths.insert("id", 0);
        columnWidths.insert("bibkey", 50);
        columnWidths.insert("entrytype", 50);
        columnWidths.insert("address", 50);
        columnWidths.insert("author", 100);
        columnWidths.insert("booktitle", 100);
        columnWidths.insert("chapter", 250);
        columnWidths.insert("crossref", 50);
        columnWidths.insert("edition", 50);
        columnWidths.insert("editor", 100);
        columnWidths.insert("howpublished", 50);
        columnWidths.insert("institution", 50);
        columnWidths.insert("key", 50);
        columnWidths.insert("month", 50);
        columnWidths.insert("note", 50);
        columnWidths.insert("number", 50);
        columnWidths.insert("organization", 50);
        columnWidths.insert("pages", 50);
        columnWidths.insert("publisher", 50);
        columnWidths.insert("school", 50);
        columnWidths.insert("title", 250);
        columnWidths.insert("type", 50);
        columnWidths.insert("year", 50);
        columnWidths.insert("doi", 50);
        columnWidths.insert("issn", 50);
        columnWidths.insert("isbn", 50);
        columnWidths.insert("url", 50);
        columnWidths.insert("date", 50);
        columnWidths.insert("timestamp", 50);
        columnWidths.insert("language", 50);
        columnWidths.insert("owner", 50);
        columnWidths.insert("editedby", 50);
        columnWidths.insert("editedat", 50);
        columnWidths.insert("abstract", 50);
    }
    columnWidthControl = true;
    settings.endArray();
    settings.endGroup();

    QString version = settings.value("version", "").toString();
    if (version != CURRENT_VERSION) {
        QMessageBox::information(this, APP_NAME, tr("Please accept settings state before program starts."));
        on_actionPreferences_triggered();
    }
    settings.setValue("version", CURRENT_VERSION);

    settings.beginGroup("General");
    bool flag = settings.value("openLastDBatStartup", false).toBool();
    if (flag && qmLastDB.size() > 0) {
        QFile file(qmLastDB.at(0)->getFilePath());
        if(QFileInfo::exists(qmLastDB.at(0)->getFilePath())) {
            initMVC(qmLastDB.at(0)->getFilePath());
        } else {
            qmLastDB.pop_front();
            ui->actionRecent->menu()->clear();
            QList<QAction *> actions;
            for (int i = 0; i < qmLastDB.size(); i ++) {
                actions.append(qmLastDB.at(i));
            }
            ui->actionRecent->menu()->addActions(actions);
            QMessageBox::critical(this, APP_NAME, tr("Cannot load previously opened database again!"));
        }
    }
    settings.endGroup();

//    Q_INIT_RESOURCE(resources);
    setWindowIcon(QIcon(":/icons/app.jpg"));
    setCorner(Qt::TopLeftCorner, Qt::LeftDockWidgetArea);
    setCorner(Qt::TopRightCorner, Qt::RightDockWidgetArea);
    setCorner(Qt::BottomLeftCorner, Qt::LeftDockWidgetArea);
    setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);
}

void MainWindow::processSelectedBibItem(const QItemSelection & selected, const QItemSelection & previous) {    
    if (selected.indexes().length() > 0) {
        int bibid = selected.indexes().at(0).siblingAtColumn(0).data(Qt::DisplayRole).toInt();
        BibEntry entry = model->getBibEntry(bibid);
        model->setCurrentBibEntry(entry);
        emit bibEntrySelected(entry);
    }


    currentShelfViewScrollPosition = tableView->verticalScrollBar()->value();
//    currentShelfViewScrollPosition = tableView->verticalScrollBar()->value();
}

MainWindow::~MainWindow()
{
    freeMVC();
    delete ui;
}

void MainWindow::closeAppSlot() {
    QSettings settings;
    settings.beginGroup("MainWindow");
    settings.setValue("size", this->size());
    settings.setValue("position", this->pos());
    settings.setValue("geometry", saveGeometry());
    settings.setValue("state", saveState());
    if (dockInfoWidget) {
        settings.setValue("dockinfoGeometryB", true);
        settings.setValue("dockinfoGeometry", dockInfoWidget->saveGeometry());
    }
    if (dockShelfsWidget) {
        dockShelfsWidget->saveExpansionState2Settings();
        settings.setValue("dockshelfsGeometryB", true);
        settings.setValue("dockshelfsGeometry", dockShelfsWidget->saveGeometry());
    }
    if (dockSearchWidget) {
        settings.setValue("docksearchGeometryB", true);
        settings.setValue("docksearchGeometry", dockSearchWidget->saveGeometry());
    }

    settings.setValue("lastOpenedNum", qmLastDB.size());
    settings.beginWriteArray("LastOpened");
    for (int i = 0; i < qmLastDB.size(); i ++) {
        settings.setArrayIndex(i);
        settings.setValue("filePath", qmLastDB.at(i)->getFilePath());
    }
    settings.endArray();

    settings.beginWriteArray("columnWidths");
    for (int i = 0; i < columnWidths.size(); i ++) {
        settings.setArrayIndex(i);
        settings.setValue("columnTitle", columnWidths.keys().at(i));
        settings.setValue("columnWidth", columnWidths.values().at(i));
    }
    settings.endArray();
    settings.endGroup();
}

void MainWindow::freeMVC() {

    if (tableView) {
        if (tableView->selectionModel()) {
            disconnect(tableView->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)), 0, 0);
        }
        delete tableView;
        tableView = nullptr;
    }

    if (dockShelfsWidget) {
        disconnect(model, SIGNAL(shelfIsChanged(ShelfEntry)), 0, 0);
        disconnect(controller->getShelfController(), SIGNAL(shelfIsSynced(ShelfEntry)), 0, 0);
        delete dockShelfsWidget;
        dockShelfsWidget = nullptr;
    }
    if (dockInfoWidget) {
        disconnect(this, SIGNAL(bibEntrySelected(BibEntry)), 0, 0);
        disconnect(dockInfoWidget, SIGNAL(entryIsModified(BibEntry)), 0, 0);
        delete dockInfoWidget;
        dockInfoWidget = nullptr;
    }
    if (dockSearchWidget) {
        disconnect(dockSearchWidget, SIGNAL(searchIsPerformed()), 0, 0);
        delete dockSearchWidget;
        dockSearchWidget = nullptr;
    }
    if (shelfsTree) {
        delete shelfsTree;
        shelfsTree = nullptr;
    }
    if (!controller) {
        delete controller;
        controller = nullptr;
    }
}

void MainWindow::initMVC(QString path2database, bool isNew) {
    freeMVC();

    QSettings settings;
    settings.beginGroup("ShelfView");
    QString entriesViewColumnsState = settings.value("entriesViewColumnsState", "bibkey,entrytype,author,editor,title,journal,volume,year,owner,timestamp,address,annote,booktitle,chapter,crossref,edition,howpublished,institution,key,month,note,number,organization,pages,publisher,school,series,type,doi,issn,isbn,url,date,language,editedby,editedat,abstract").toString();
    settings.endGroup();
    settings.beginGroup("Owner");
    QString owner = settings.value("ownerName", "").toString();
    QString email = settings.value("ownerEmail", "").toString();
    QString filePath = settings.value("globalFilePath", "").toString();
    settings.endGroup();
    settings.beginReadArray("Presets");
    QStringList presets;
    for (int i = 0; i < BibEntry::BIBENTRYTYPES_NUM; i ++) {
        settings.setArrayIndex(i);
        presets.append(settings.value("entryPreset", BibEntry::DEFAULT_PRESETS[i]).toString());
    }
    settings.endArray();

    controller = new Controller();
    controller->initComponents(path2database, entriesViewColumnsState, isNew);
    model = controller->getModel();
    model->setOwner(owner);
    model->setEmail(email);
    model->setFilePath(filePath);
    model->setPresets(presets);

    settings.beginGroup("DefaultShelf");
    model->setDefaultShelfId(settings.value("defaultShelfId", model->getRootShelfId()).toInt());
    settings.endGroup();

    connect(model, SIGNAL(shelfViewIsChanged()), this, SLOT(setupEntriesViewColumns()));

    tableView = new CurrentShelfView(model, this);
    this->setCentralWidget(tableView);
    setupEntriesViewColumns();
    connect(tableView->selectionModel(), SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection &)), this, SLOT(processSelectedBibItem(const QItemSelection &, const QItemSelection &)));
    connect(tableView->horizontalHeader(), SIGNAL(sectionResized(int, int, int)), this, SLOT(shelfViewColumnsResize(int, int, int)));

    settings.beginGroup("ShelfView");
    this->setCorner(Qt::Corner::BottomLeftCorner, Qt::LeftDockWidgetArea);
    dockShelfsWidget = new DockShelfsWidget(this, controller->getModel(), controller, controller->getShelfController(), controller->getModel()->getShelfsModel());
    this->addDockWidget(Qt::LeftDockWidgetArea, dockShelfsWidget);
    bool pos;
    pos = settings.value("dockshelfGeometryB", false).toBool();
    if (pos) {
        dockShelfsWidget->restoreGeometry(settings.value("dockshelfGeometry").toByteArray());
    }
    connect(model, SIGNAL(shelfIsChanged(ShelfEntry)), this, SLOT(setupEntriesViewColumns()));
    connect(controller->getShelfController(), SIGNAL(shelfIsSynced(ShelfEntry)), this, SLOT(setupEntriesViewColumns()));
    connect(dockShelfsWidget, SIGNAL(bibentryIsChanged()), this, SLOT(setupEntriesViewColumns()));    

    dockInfoWidget = new DockInfoWidget(this, controller->getModel());
    this->addDockWidget(Qt::BottomDockWidgetArea, dockInfoWidget);
    pos = settings.value("dockinfoGeometryB", false).toBool();
    if (pos) {
        dockShelfsWidget->restoreGeometry(settings.value("dockinfoGeometry").toByteArray());
    }
    connect(this, SIGNAL(bibEntrySelected(BibEntry)), dockInfoWidget, SLOT(setBibEntry(BibEntry)));
    connect(dockInfoWidget, SIGNAL(entryIsModified(BibEntry)), this, SLOT(setupEntriesViewColumns()));
    connect(tableView, SIGNAL(modify(BibEntry)), dockInfoWidget, SLOT(openEditWindow(BibEntry)));
    dockShelfsWidget->setDockInfoWidget(dockInfoWidget);

    dockSearchWidget = new DockSearchWidget(this, controller->getModel(), controller);
//    this->addDockWidget(Qt::LeftDockWidgetArea, dockSearchWidget);
    this->addDockWidget(Qt::BottomDockWidgetArea, dockSearchWidget);
    pos = settings.value("docksearchGeometryB", false).toBool();
    if (pos) {
        dockShelfsWidget->restoreGeometry(settings.value("docksearchGeometry").toByteArray());
    }
    connect(model, SIGNAL(shelfIsChanged(ShelfEntry)), dockSearchWidget, SLOT(setCurrentShelf(ShelfEntry)));
    connect(model, SIGNAL(shelfIsChanged(ShelfEntry)), model, SLOT(setCurrentShelf(ShelfEntry)));
    connect(dockSearchWidget, SIGNAL(searchIsPerformed()), this, SLOT(setupEntriesViewColumns()));
    settings.endGroup();

    model->setCurrentShelf(model->getShelfEntry(model->getRootShelfId()));
//    QMainWindow::tabifyDockWidget(dockSearchWidget, dockShelfsWidget);
    settings.beginGroup("MainWindow");
//    resize(settings.value("size", QSize(1000, 900)).toSize());
//    move(settings.value("position", QPoint(100, 100)).toPoint());
    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("state").toByteArray());
    settings.endGroup();
}

void MainWindow::setupEntriesViewColumns() {
    columnWidthControl = false;

    for (int i = 0; i < tableView->model()->columnCount(); i++) {
        QString colTitle = tableView->model()->headerData(i, Qt::Horizontal).toString();
        if (columnWidths.keys().contains(colTitle)) {
            tableView->setColumnWidth(i, columnWidths[colTitle]);
        }
    }
    tableView->setColumnWidth(BibEntry::ID, 0); // hide id column
    columnWidthControl = true;

    if (currentShelfViewScrollPosition < tableView->verticalScrollBar()->maximum()) {
        tableView->verticalScrollBar()->setValue(currentShelfViewScrollPosition);
    }
}


void MainWindow::on_actionNew_triggered()
{
    QFileDialog dialog(this);
    dialog.setNameFilter(tr("SQLite files for bibliography (*.%1)").arg(FILE_EXTENSION));
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setDefaultSuffix(FILE_EXTENSION);

    if (dialog.exec()) {
        QStringList fileName = dialog.selectedFiles();
        if (fileName.length() > 1) {
            QMessageBox::warning(this, tr("Library"), tr("Something wrong with saving library: too may files selected!"));
        } else {
            try {
                freeMVC();
                initMVC(fileName[0], true);
            } catch (std::exception & e) {
                QMessageBox::critical(this, APP_NAME, e.what());
                return;
            } catch (...) {
                QMessageBox::critical(this, APP_NAME, tr("Unpredicted exception during creation of the sysem."));
                return;
            }
        }
    }
}

void MainWindow::on_actionOpen_triggered()
{
    QFileDialog dialog(this);
    dialog.setNameFilter(tr("SQLite files for bibliography (*.%1)").arg(FILE_EXTENSION));
    dialog.setAcceptMode(QFileDialog::AcceptOpen);
    dialog.setDefaultSuffix(FILE_EXTENSION);

    if (dialog.exec()) {
        QStringList fileName = dialog.selectedFiles();
        if (fileName.length() > 1) {
            QMessageBox::warning(this, tr("Library"), tr("Something wrong with saving library: too may files selected!"));
        } else {
            try {
                initMVC(fileName[0]);
                updateLastOpened(fileName[0]);
            } catch (std::exception & e) {
                QMessageBox::critical(this, APP_NAME, e.what());
                return;
            } catch (...) {
                QMessageBox::critical(this, APP_NAME, tr("Unpredicted exception during creation of the sysem."));
                return;
            }
        }
    }
}

void MainWindow::openRecent() {
    LastOpenedMenu * menu = qobject_cast<LastOpenedMenu*>(sender());
    if(menu)
    {
        QString filePath = menu->getFilePath();
        try {
            initMVC(filePath);
            updateLastOpened(filePath);
        } catch (std::exception & e) {
            QMessageBox::critical(this, APP_NAME, e.what());
            return;
        } catch (...) {
            QMessageBox::critical(this, APP_NAME, tr("Unpredicted exception during creation of the sysem."));
            return;
        }
    }
}

void MainWindow::updateLastOpened(const QString & fileName) {
    LastOpenedMenu * menu = new LastOpenedMenu(fileName, this);
    connect(menu, SIGNAL(triggered()), this, SLOT(openRecent()));
    qmLastDB.insert(0, menu);

    int i = 1;
    while (i < qmLastDB.size()) {
        if (qmLastDB.at(i)->getFilePath() == menu->getFilePath()) {
            delete (LastOpenedMenu*)qmLastDB.takeAt(i);
        } else {
            i++;
        }
    }


    if (qmLastDB.length() >= MainWindow::MAX_LAST_DATABASES) {
        delete (LastOpenedMenu*)qmLastDB.takeLast();
    }
    ui->actionRecent->menu()->clear();
    QList<QAction *> actions;
    for (int i = 0; i < qmLastDB.size(); i ++) {
        actions.append(qmLastDB.at(i));
    }
    ui->actionRecent->menu()->addActions(actions);
}

void MainWindow::on_action_bib_JabRef_triggered()
{
    if (!controller || !model) {
        QMessageBox::critical(this, APP_NAME, tr("Please open/create a library first!"));
        return;
    }

    QFileDialog dialog(this);
    const QStringList filters({tr("JabRef files (*.bib"), tr("All files (*)")});
    dialog.setNameFilters(filters);
    dialog.setAcceptMode(QFileDialog::AcceptOpen);

    if (dialog.exec()) {
        QStringList fileNames = dialog.selectedFiles();
        QSettings settings;
        QString preset = settings.value("BibTeX/bibtexkeyTemplate", "").toString();

        foreach (const QString &fileName, fileNames) {
            try {
                JabrefImport jabimp(fileName);
                controller->importData(jabimp, preset);
                setupEntriesViewColumns();
            } catch (std::exception & e) {
                QMessageBox::critical(this, APP_NAME, e.what());
            } catch (...) {
                QMessageBox::critical(this, APP_NAME, tr("Unpredicted exception during importing the database."));
            }
        }
    }
}

void MainWindow::on_actionShelfs_triggered()
{
    this->dockShelfsWidget->show();
}

void MainWindow::on_actionInfo_triggered()
{
    this->dockInfoWidget->show();
}

void MainWindow::on_actionClose_database_triggered()
{
}

void MainWindow::on_actionExit_triggered()
{
    QApplication::quit();
}

void MainWindow::on_actionPreferences_triggered()
{
    PreferencesWindow prefswin(model, this);
    prefswin.exec();

    if (prefswin.result() == QDialog::Accepted) {
        if (dockInfoWidget) {
            model->setOwner(prefswin.getOwner());
            model->setEmail(prefswin.getEmail());
            model->setFilePath(prefswin.getFilePath());
            model->setPresets(prefswin.getPresets());
        }
        if (model) {
            auto cols = prefswin.getEntriesViewColumnsState();
            model->setEntryViewColumns(cols);
            controller->getShelfController()->updateShelfView(model->getCurrentShelf().getID());
            setupEntriesViewColumns();
        }

    }
}

void MainWindow::on_actionOpen_URL_or_DOI_triggered()
{
    if (!controller || !model) {
        QMessageBox::critical(this, APP_NAME, tr("Please open/create a library first!"));
        return;
    }

    if (dockInfoWidget) {
        dockInfoWidget->openDOIorURL();
    }
}

void MainWindow::on_actionOpen_file_triggered()
{
    if (!controller || !model) {
        QMessageBox::critical(this, APP_NAME, tr("Please open/create a library first!"));
        return;
    }

    if (dockInfoWidget) {
        dockInfoWidget->openFile();
    }
}

void MainWindow::on_actionModify_triggered()
{
    if (!controller || !model) {
        QMessageBox::critical(this, APP_NAME, tr("Please open/create a library first!"));
        return;
    }

    if (dockShelfsWidget) {
        dockShelfsWidget->showPropertiesDialog();
    }
}

void MainWindow::on_actionAdd_triggered()
{
    if (!controller || !model) {
        QMessageBox::critical(this, APP_NAME, tr("Please open/create a library first!"));
        return;
    }

    ShelfEntry newShelf(-1);

    ShelfProperties shelfProps(newShelf, model->getShelfsModel(), DBVars::getInstance().getFilePath(), this, tr("New shelf"));
    shelfProps.exec();

    if (shelfProps.result() == QDialog::Accepted) {
        try {
            newShelf = shelfProps.getShelfState();
            newShelf.setHash();
            controller->getShelfController()->addShelf(newShelf);
        } catch (std::exception & e) {
            QMessageBox::critical(this, APP_NAME, e.what());
        } catch (...) {
            QMessageBox::critical(this, APP_NAME, tr("Unpredicted exception during adding of a new shelf."));
        }
    }
}

void MainWindow::on_actionAdd_manually_triggered()
{
    if (!controller || !model) {
        QMessageBox::critical(this, APP_NAME, tr("Please open/create a library first!"));
        return;
    }

    BibEntry newEntry(BibEntry::NOT_EXISTS);
    EntryProperties props(newEntry, model, this, tr("Add new entry"));

    props.exec();

    if (props.result() == QDialog::Accepted) {
        try {
            newEntry = props.getEntryState();
//            newEntry.setField(BibEntry::HASH, controller->getHash(newEntry));
            newEntry.setHash();

            controller->replaceUTF(&newEntry);
            newEntry.setField(BibEntry::BOOKTITLE, controller->replaceSymbolsByList(newEntry.getField(BibEntry::BOOKTITLE)));
            newEntry.setField(BibEntry::TITLE, controller->replaceSymbolsByList(newEntry.getField(BibEntry::TITLE)));
            model->addBibEntry(newEntry, dockShelfsWidget->getSelectedShelf().getID());

            QSettings settings;
            settings.beginGroup("General");
            if (settings.value("AutoExtUpd", false).toBool()) {
                const auto shelfsIds = newEntry.getShelfIDs();
                foreach (auto shelfId, shelfsIds) {
                    ShelfEntry shelf = model->getShelfEntry(shelfId);
                    controller->updateAssociatedDB(shelf);
                }
            }
            settings.endGroup();
        } catch (std::exception & e) {
            QMessageBox::critical(this, APP_NAME, e.what());
        } catch (...) {
            QMessageBox::critical(this, APP_NAME, tr("Unpredicted exception during editing the entry."));
        }
    }    
    setupEntriesViewColumns();
}

//void MainWindow::on_actionDelete_triggered() {}
//void MainWindow::on_actionDelete_2_triggered() {}
//void MainWindow::on_actionRemove_triggered() {}

void MainWindow::on_actionEdit_triggered()
{
    if (!controller || !model) {
        QMessageBox::critical(this, APP_NAME, tr("Please open/create a library first!"));
        return;
    }

    if (dockInfoWidget) {
        dockInfoWidget->openEditWindow();
    }
}


void MainWindow::on_actionDeleteShelf_triggered()
{
    if (!controller || !model) {
        QMessageBox::critical(this, APP_NAME, tr("Please open/create a library first!"));
        return;
    }

    try {
        if (dockShelfsWidget) {
            dockShelfsWidget->deleteCurrentShelf();
        }
    } catch (std::exception & e) {
        QMessageBox::critical(this, APP_NAME, e.what());
    } catch (...) {
        QMessageBox::critical(this, APP_NAME, tr("Unpredicted exception during editing the entry."));
    }
}

void MainWindow::on_actionDeleteEntry_triggered()
{
    if (!controller || !model) {
        QMessageBox::critical(this, APP_NAME, tr("Please open/create a library first!"));
        return;
    }

    try {
        if (dockInfoWidget) {
            dockInfoWidget->deleteCurrentEntry();
            setupEntriesViewColumns();
        }
    } catch (std::exception & e) {
        QMessageBox::critical(this, APP_NAME, e.what());
    } catch (...) {
        QMessageBox::critical(this, APP_NAME, tr("Unpredicted exception during editing the entry."));
    }
}

void MainWindow::on_actionUpdate_all_external_triggered()
{
}

void MainWindow::on_actionUpdate_projects_triggered()
{
    if (!controller || !model) {
        QMessageBox::critical(this, APP_NAME, tr("Please open/create a library first!"));
        return;
    }

    QList<ShelfEntry> shelfEntries = model->getAllShelfs();
    foreach (auto shelf, shelfEntries) {
        dockShelfsWidget->updateExternalDB(shelf);
    }
}

void MainWindow::on_actionImport_from_file_triggered()
{
    if (!controller || !model) {
        QMessageBox::critical(this, APP_NAME, tr("Please open/create a library first!"));
        return;
    }

    QString fileName = QFileDialog::getOpenFileName(this, tr("Open file"), "",  tr("All files (*);;BibTeX files (*.bib *.bibtex);;RIS files (*.ris)"));

    if (!fileName.isEmpty() || fileName.length() > 0) {
        QSettings settings;
        QString preset = settings.value("BibTeX/bibtexkeyTemplate", "").toString();

        Importer * importer;
        QFileInfo fi(fileName);
        if (fi.suffix().toLower() == "bib" || fi.suffix().toLower() == "bibtex") {
            importer = new JabrefImport(fileName);
        } else if (fi.suffix().toLower() == "ris") {
            importer = new RISImport(fileName);
        } else {
            QMessageBox::critical(this, APP_NAME, tr("Unknown file type!"));
            return;
        }
        controller->importData(*importer, preset, dockShelfsWidget->getSelectedShelf().getID());
        setupEntriesViewColumns();
    }
}

void MainWindow::on_actionGenerate_BibKey_triggered()
{
    if (!controller || !model) {
        QMessageBox::critical(this, APP_NAME, tr("Please open/create a library first!"));
        return;
    }

    QSettings settings;
    QString preset = settings.value("BibTeX/bibtexkeyTemplate", "").toString();
    BibEntry bibEntry = dockInfoWidget->getCurrentEntry();
    ShelfEntry shelfEntry = dockShelfsWidget->getSelectedShelf();
    if (bibEntry.getField(BibEntry::BIBKEY).length() > 0) {
        if (QMessageBox::question(this, tr("Replace BibKey"), tr("Replace existing BibKey?"), QMessageBox::Yes|QMessageBox::No) == QMessageBox::No) {
            return;
        }
    }
    bibEntry.setField(BibEntry::BIBKEY, controller->getBibKey(bibEntry, preset));
    model->updateBibEntry(bibEntry, shelfEntry);
    setupEntriesViewColumns();
}

void MainWindow::shelfViewColumnsResize(int logicalIndex, int oldSize, int newSize) {
    if (!columnWidthControl) {
        return;
    }

    QString colTitle = tableView->model()->headerData(logicalIndex, Qt::Horizontal).toString();
    columnWidths[colTitle] = newSize;
}

void MainWindow::on_actionSearch_triggered()
{
    if (dockSearchWidget) {
        dockSearchWidget->show();
    }
}

void MainWindow::on_actionImport_by_DOI_triggered()
{
    if (!controller || !model) {
        QMessageBox::critical(this, APP_NAME, tr("Please open/create a library first!"));
        return;
    }

    ImportByMetadata metadataImport(DBVars::getInstance().getEmail(), this);

    metadataImport.exec();

    if (metadataImport.result() == QDialog::Accepted) {
        try {
            QSettings settings;
            QString bibkeyPreset = settings.value("BibTeX/bibtexkeyTemplate", "").toString();
            QList<BibEntry> entries = metadataImport.getEntries();
            BibEntry entry = entries.at(0);
            int shelf_id = dockShelfsWidget->getSelectedShelf().getID();
            if (shelf_id != model->getRootShelfId()) {
                entry.addShelfID(shelf_id);
            }
            if (model->getDefaultShelfId() != model->getRootShelfId()) {
                entry.addShelfID(model->getDefaultShelfId());
            }
            entry.setHash();
            entry.setField(BibEntry::BIBKEY, controller->getBibKey(entry, bibkeyPreset));
            entry.setField(BibEntry::TIMESTAMP, QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss"));
            entry.setField(BibEntry::OWNER, DBVars::getInstance().getOwner());

            QList<BibEntry> similar = controller->searchSimilar(entry);
            if (similar.size() == 0) {
                controller->replaceUTF(&entry);
                entry.setField(BibEntry::BOOKTITLE, controller->replaceSymbolsByList(entry.getField(BibEntry::BOOKTITLE)));
                entry.setField(BibEntry::TITLE, controller->replaceSymbolsByList(entry.getField(BibEntry::TITLE)));
                model->addBibEntry(entry, dockShelfsWidget->getSelectedShelf().getID());
                BibEntry finalEntry = model->getBibEntry(entry.getField(BibEntry::HASH));
                dockInfoWidget->setBibEntry(finalEntry);


                QSettings settings;
                settings.beginGroup("General");
                if (settings.value("AutoExtUpd", false).toBool()) {
                    const auto shelfsIds = finalEntry.getShelfIDs();
                    foreach (auto shelfId, shelfsIds) {
                        ShelfEntry shelf = model->getShelfEntry(shelfId);
                        controller->updateAssociatedDB(shelf);
                    }
                }
                settings.endGroup();
            } else {
                QMessageBox::StandardButton button = QMessageBox::question(this, tr("Similar entries"), tr("Possible duplicates found. Do you want to check/merge?"), QMessageBox::Yes|QMessageBox::No);
                if (button == QMessageBox::No) {
                    if (QMessageBox::question(this, tr("Similar entries"), tr("Import anyway?"), QMessageBox::Yes|QMessageBox::No) == QMessageBox::Yes) {
                        controller->replaceUTF(&entry);
                        entry.setField(BibEntry::BOOKTITLE, controller->replaceSymbolsByList(entry.getField(BibEntry::BOOKTITLE)));
                        entry.setField(BibEntry::TITLE, controller->replaceSymbolsByList(entry.getField(BibEntry::TITLE)));
                        model->addBibEntry(entry, dockShelfsWidget->getSelectedShelf().getID());
                        BibEntry finalEntry = model->getBibEntry(entry.getField(BibEntry::HASH));
                        dockInfoWidget->setBibEntry(finalEntry);


                        QSettings settings;
                        settings.beginGroup("General");
                        if (settings.value("AutoExtUpd", false).toBool()) {
                            const auto shelfsIds = finalEntry.getShelfIDs();
                            foreach (auto shelfId, shelfsIds) {
                                ShelfEntry shelf = model->getShelfEntry(shelfId);
                                controller->updateAssociatedDB(shelf);
                            }
                        }
                        settings.endGroup();
                    }
                } else {
                    for (int i = 0; i < similar.size(); i ++) {
                        CompareEntriesDialog compareentries(similar[i], entry, model, CompareEntriesDialog::Action::CANCEL);
                        compareentries.setTitles(tr("Local"), tr("By DOI"));
                        compareentries.exec();

                        if (compareentries.result() == QDialog::Accepted) {
                            entry = compareentries.getChosenEntry();
                            entry.setID(similar[i].getID());
                            entry.setField(BibEntry::HASH, similar[i].getField(BibEntry::HASH));
                            model->updateBibEntry(entry, dockShelfsWidget->getSelectedShelf());
                            BibEntry finalEntry = model->getBibEntry(entry.getID());
                            dockInfoWidget->setBibEntry(finalEntry);

                            QSettings settings;
                            settings.beginGroup("General");
                            if (settings.value("AutoExtUpd", false).toBool()) {
                                const auto shelfsIds = finalEntry.getShelfIDs();
                                foreach (auto shelfId, shelfsIds) {
                                    ShelfEntry shelf = model->getShelfEntry(shelfId);
                                    controller->updateAssociatedDB(shelf);
                                }
                            }
                            settings.endGroup();
                        }
                    }
                }
            }
            setupEntriesViewColumns();
        } catch (std::exception & e) {
            QMessageBox::critical(this, APP_NAME, e.what());
        } catch (...) {
            QMessageBox::critical(this, APP_NAME, tr("Unpredicted exception during adding the entry."));
        }
    }
}

void MainWindow::on_actionUpdate_all_external_projects_triggered()
{
    if (!controller || !model) {
        QMessageBox::critical(this, APP_NAME, tr("Please open/create a library first!"));
        return;
    }

    QList<ShelfEntry> shelfEntries = model->getAllShelfs();
    foreach (auto shelf, shelfEntries) {
        try {
            controller->updateAssociatedDB(shelf);
        } catch (std::exception & e) {
            QMessageBox::critical(this, APP_NAME, e.what());
        } catch (...) {
            QMessageBox::critical(this, APP_NAME, tr("Unpredicted exception during updating external DB for '%1' shelf.").arg(shelf.name));
        }
    }

    QSettings settings;
    settings.beginGroup("General");
    if (!settings.value("SilentExtUpd", false).toBool()) {
        QMessageBox::information(this, APP_NAME, tr("External projects have been updated!"));
    }
    settings.endGroup();
}

void MainWindow::on_actionSync_all_shared_shelfs_triggered()
{
    if (!controller || !model) {
        QMessageBox::critical(this, APP_NAME, tr("Please open/create a library first!"));
        return;
    }

    QSettings settings;
    settings.beginGroup("Git");
    SyncInfo syncInfo;
    syncInfo.command = settings.value("gitPath", "git.exe").toString();
    syncInfo.commit = settings.value("commitCommand", "commit -a").toString();
    syncInfo.commitMsg = settings.value("commitMessage", "shelf update").toString();
    syncInfo.pull = settings.value("pullCommand", "pull -X theirs").toString();
    syncInfo.push = settings.value("pushCommand", "push").toString();
    settings.endGroup();

    QList<ShelfEntry> shelfEntries = model->getAllShelfs();
    foreach (auto shelf, shelfEntries) {
        try {
            controller->getShelfController()->syncShelf(shelf, syncInfo, dockShelfsWidget->getSelectedShelf());
            setupEntriesViewColumns();
        } catch (std::exception & e) {
            QMessageBox::critical(this, APP_NAME, e.what());
        } catch (...) {
            QMessageBox::critical(this, APP_NAME, tr("Unpredicted exception during shelf '%1' syncronization.").arg(shelf.name));
        }
    }


    QMessageBox::information(this, APP_NAME, tr("Shelfs have been syncronized!"));
}

void MainWindow::on_actionAbout_triggered()
{
    About about;
    about.exec();
}
