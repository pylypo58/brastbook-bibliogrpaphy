# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

- Qt6 compatibility/Windows check

## [0.1.4]

- Replace substrings in book/paper titles
- Export group to file
- Save shelfs state (expanded/collapsed)

## [0.1.3]

### Fixed
- Problem with merging by DOI
- Better default settings
- LGPL v3 license for the project

## [0.1.2]
- Silent updates of external projects
- Search of strings with LaTeX commands
- Minor interface updates
- Program remembers dockable widget's positions
- Filter special symbols in search

## [0.1.1]
- Additional context menu items
- About menu + icon

## [0.1.0]
- Default shelf for new bibliographical entries
- Open last database at startup
- Shelfs view with context menu
- Paths to external projects and git repos are relative to the main folder
- Proper fetch of page via DOI for some Nature and APS journals
- Restoring of shelf's collapsing state when shelfs are updated
- Accept/Decline remote changes
- Replacement of some UTF symbols during import by metadata/bibfile/manually

### Fixed
- List of last opened files
- Fetching all bib entries immediately
- Search in the root shelf
- Bug in RIS import
- Parent shelf not always saved
- Bug in search within shelf
- Crash if last opened database was moved
- Bug with click on the empty space
- Default shelf appears in the dialog of adding a new entry
- Bug with remote sync

## [0.01]
The initial verstion of program, which can be public


[0.01] https://gitlab.com/engraver/bibliography-manager/-/tags/v0.01
