# Bibliography manager BraStBook


## Description
This program is designed for the management of large collections of books, papers and other publications  (good enough at least up to ~ 10k of records), mainly for science-oriented writing and collaborative work between small groups of researchers. Our development team was inspired by other prominent tools like JabRef or Mendeley and has been focused on a lightweight, self-sufficient tool, which can be combined with other technologies.

BraStBook uses SQLite format to store data, Qt GUI to be cross-platform and can be combined with cloud storage like DropBox/Google Drive and Git repositories for syncing between computers and accounts of different people.


## Installation
To compile `brastbook` from sources in Linux, perform the following operations in the folder where sources are unpacked:

```bash
$ mkdir ../brastbook
$ cd ../brastbook
$ cmake ../bibliography-manager/
$ make
```

Executable will be located in `brastbook/brastbook/` subfolder. `make` command can be run with multiple threads to speed up compilation, e.g. `make -j4` for four threads.

## Authors and acknowledgment
Developing team:
- [Oleksandr V. Pylypovskyi](engraver.github.io/), [Helmholtz-Zentrum Dresden-Rossendorf](https://hzdr.de)

OVP acknowleges contribution of Vitalii Kominko at the early stages of the project with the proof-of-concept testing and Tobias Huste for the consulting.

## License
[LGPL v3 license](https://www.gnu.org/licenses/lgpl-3.0.en.html).

## Project status
Under development

