#include "bibentrytest.h"

#include "../../brastbook/model/bibentry.h"

BibEntryTest::BibEntryTest()
{

}

BibEntryTest::~BibEntryTest()
{

}

void BibEntryTest::initTestCase()
{

}

void BibEntryTest::cleanupTestCase()
{

}



void BibEntryTest::test_ID_functionality() {
    BibEntry entry;

    QCOMPARE(entry.getID(), BibEntry::DEFAULT);

    int id = 123;
    entry.setID(id);
    QCOMPARE(entry.getID(), id);
}

void BibEntryTest::test_Field_functionality() {
    BibEntry entry;

    QCOMPARE(entry.bibField.size(), BibEntry::COL_NUM);

    for (int i = 0; i < BibEntry::COL_NUM; i ++) {
        entry.setField(i, QString::number(i) + QString::number(3 + 10*i));
    }

    for (int i = 0; i < BibEntry::COL_NUM; i ++) {
        QCOMPARE(entry.getField(i), QString::number(i) + QString::number(3 + 10*i));
    }

    try {
        entry.setField(-1, "test");
        QFAIL("Assignment of negative field index");
    }  catch (...) {}

    try {
        entry.setField(BibEntry::COL_NUM, "test");
        QFAIL("Assignment of the field index outside range");
    }  catch (...) {}

    try {
        entry.getField(-1);
        QFAIL("Extraction of negative field index");
    }  catch (...) {}

    try {
        entry.getField(BibEntry::COL_NUM);
        QFAIL("Extraction of the field index outside range");
    }  catch (...) {}

    entry.setHash();
    QString hash1 = entry.getField(BibEntry::HASH);
}

void BibEntryTest::test_EntryType_functionality() {
    BibEntry entry;


    for (int i = 0; i < BibEntry::BIBENTRYTYPES_NUM; i ++) {
        entry.setEntryType(i);
        QCOMPARE(entry.getEntryType(), i);
    }

    try {
        entry.setEntryType(-1);
        QFAIL("Assignment of negative entry type");
    }  catch (...) {}

    try {
        entry.setEntryType(BibEntry::BIBENTRYTYPES_NUM);
        QFAIL("Assignment of the entry type outside range");
    }  catch (...) {}
}

void BibEntryTest::test_Shelfs_functionality() {
    BibEntry entry;

    QCOMPARE(entry.shelfIDs.size(), 0);

    entry.addShelfID(1);
    entry.addShelfID(1);
    entry.addShelfID(1);
    // ids: 1
    QCOMPARE(entry.shelfIDs.size(), 1);

    entry.addShelfID(2);
    // ids: 1, 2
    QCOMPARE(entry.shelfIDs.size(), 2);

    entry.removeShelfID(1);
    // ids: 2
    QCOMPARE(entry.shelfIDs.size(), 1);

    entry.addShelfID(1);
    entry.removeShelfID(2);
    // ids: 1
    QCOMPARE(entry.shelfIDs.size(), 1);

    entry.addShelfID(2);
    entry.addShelfID(3);
    // ids: 1, 2, 3
    const QList<int> ids = entry.getShelfIDs();
    QCOMPARE(ids.size(), 3);
    QVERIFY(ids.contains(1));
    QVERIFY(ids.contains(2));
    QVERIFY(ids.contains(3));

    entry.clearShelfIDs();
    QCOMPARE(entry.shelfIDs.size(), 0);
}

void BibEntryTest::test_Files_functionality() {
    BibEntry entry;

    QCOMPARE(entry.files.size(), 0);

    entry.addFile("fid1");
    entry.addFile("fid2");
    // ids: fid1, fid2
    QCOMPARE(entry.files.size(), 2);

    QCOMPARE(entry.getFile(0), "fid1");
    QCOMPARE(entry.getFile(1), "fid2");

    try {
        entry.getFile(-1);
        QFAIL("Taking file outside allowed range");
    }  catch (...) {}

    try {
        entry.getFile(2);
        QFAIL("Taking file outside allowed range");
    }  catch (...) {}

    const QStringList fids = entry.getFiles();
    QCOMPARE(fids[0], "fid1");
    QCOMPARE(fids[1], "fid2");

    entry.clearFiles();
    QCOMPARE(entry.files.size(), 0);
}

void BibEntryTest::test_Assignment_functionality() {
    BibEntry entry1, entry2;

    QVERIFY(entry1 == entry2);

    entry1.setEntryType(BibEntry::BET_ARTICLE);
    entry2.setEntryType(BibEntry::BET_INBOOK);
    QVERIFY(entry1 != entry2);
    QVERIFY(!(entry1 == entry2));

    for (int i = 0; i < BibEntry::COL_NUM; i ++) {
        entry1.setField(i, QString::number(i) + QString::number(3 + 10*i));
        entry2.setField(i, QString::number(i) + QString::number(2 + 10*i));
    }
    QVERIFY(entry1 != entry2);
    QVERIFY(!(entry1 == entry2));


    for (int i = BibEntry::BIBKEY; i < BibEntry::COL_NUM; i ++) {
        entry1.setField(i, QString::number(i) + QString::number(3 + 10*i));
    }
    for (int i = BibEntry::BIBKEY; i < BibEntry::COL_NUM; i ++) {
        if (i == BibEntry::EDITEDBY || i == BibEntry::EDITEDAT)  {
            continue;
        }
        entry2 = entry1;
        entry2.setField(i, "test");
        QVERIFY(entry1 != entry2);
    }

}
