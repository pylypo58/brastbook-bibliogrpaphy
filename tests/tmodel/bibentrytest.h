#ifndef BIBENTRYTEST_H
#define BIBENTRYTEST_H

#include <QTest>

class BibEntryTest : public QObject
{
    Q_OBJECT

public:
    BibEntryTest();
    ~BibEntryTest();

private slots:
    void initTestCase();
    void cleanupTestCase();

    void test_ID_functionality();

    void test_Field_functionality();

    void test_EntryType_functionality();

    void test_Shelfs_functionality();

    void test_Files_functionality();

    void test_Assignment_functionality();
};

#endif // BIBENTRYTEST_H
