#include <QtTest>

#include "tmodel/bibentrytest.h"
#include "tcontroller/controllertest.h"
#include "tcontroller/textprocessingtest.h"
#include "tcontroller/jabrefimporttest.h"

int main(int argc, char * argv[]) {
    int status = 0;

    {
        BibEntryTest t;
        status |= QTest::qExec(&t, argc, argv);
    }

    {
        ControllerTest t;
        status |= QTest::qExec(&t, argc, argv);
    }

    {
        TextProcessingTest t;
        status |= QTest::qExec(&t, argc, argv);
    }

    {
        JabrefImportTest t;
        status |= QTest::qExec(&t, argc, argv);
    }

    return status;
}
