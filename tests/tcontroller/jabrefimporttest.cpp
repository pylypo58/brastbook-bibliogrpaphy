#include "jabrefimporttest.h"

#include "../../brastbook/controller/import/jabrefimport.h"

JabrefImportTest::JabrefImportTest()
{

}

JabrefImportTest::~JabrefImportTest()
{

}

void JabrefImportTest::initTestCase()
{

}

void JabrefImportTest::cleanupTestCase()
{

}

void JabrefImportTest::test_getNextEntry_OneRecord() {
    JabrefImport instance("");

    QString input = "     @article{Pylypovskyi21e,\n"
                    "author = {Oleksandr V. Pylypovskyi and Yelyzaveta A. Borysenko and Jürgen Fassbender and Denis D. Sheka and Denys Makarov},\n"
                    "doi = {10.1063/5.0048823},\n"
                    "editedat = {2022.01.01 18:48:36},\n"
                    "editedby = {engraver},\n"
                    "issue = {18},\n"
                    "journal = {Applied Physics Letters},\n"
                    "month = {may},\n"
                    "number = {18},\n"
                    "owner = {engraver},\n"
                    "pages = {182405},\n"
                    "publisher = {{AIP} Publishing},\n"
                    "timestamp = {2021.05.05 09:59:49},\n"
                    "title = {Curvature-driven homogeneous {D}zyaloshinskii--{M}oriya interaction and emergent weak ferromagnetism in anisotropic antiferromagnetic spin chains},\n"
                    "url = {https://doi.org/10.1063/5.0048823},\n"
                    "volume = {118},\n"
                    "year = {2021},\n"
                    "}       \n";

    QString expected = "@article{Pylypovskyi21e,\n"
                    "author = {Oleksandr V. Pylypovskyi and Yelyzaveta A. Borysenko and Jürgen Fassbender and Denis D. Sheka and Denys Makarov},\n"
                    "doi = {10.1063/5.0048823},\n"
                    "editedat = {2022.01.01 18:48:36},\n"
                    "editedby = {engraver},\n"
                    "issue = {18},\n"
                    "journal = {Applied Physics Letters},\n"
                    "month = {may},\n"
                    "number = {18},\n"
                    "owner = {engraver},\n"
                    "pages = {182405},\n"
                    "publisher = {{AIP} Publishing},\n"
                    "timestamp = {2021.05.05 09:59:49},\n"
                    "title = {Curvature-driven homogeneous {D}zyaloshinskii--{M}oriya interaction and emergent weak ferromagnetism in anisotropic antiferromagnetic spin chains},\n"
                    "url = {https://doi.org/10.1063/5.0048823},\n"
                    "volume = {118},\n"
                    "year = {2021},\n"
                    "}";

    QString result;
    int pos = 0;
    bool res;

    res = instance.getNextEntry(input, &pos, &result);

    QCOMPARE(res, true);
    QCOMPARE(result, expected);

    input = "article {}";
    res = instance.getNextEntry(input, &pos, &result);
    QCOMPARE(res, false);

}

void JabrefImportTest::test_getNextEntry_TwoRecords() {
    QString input = "     @article{Pylypovskyi21e,\n"
                    "author = {Oleksandr V. Pylypovskyi and Yelyzaveta A. Borysenko and Jürgen Fassbender and Denis D. Sheka and Denys Makarov},\n"
                    "doi = {10.1063/5.0048823},\n"
                    "editedat = {2022.01.01 18:48:36},\n"
                    "editedby = {engraver},\n"
                    "issue = {18},\n"
                    "journal = {Applied Physics Letters},\n"
                    "month = {may},\n"
                    "number = {18},\n"
                    "owner = {engraver},\n"
                    "pages = {182405},\n"
                    "publisher = {{AIP} Publishing},\n"
                    "timestamp = {2021.05.05 09:59:49},\n"
                    "title = {Curvature-driven homogeneous {D}zyaloshinskii--{M}oriya interaction and emergent weak ferromagnetism in anisotropic antiferromagnetic spin chains},\n"
                    "url = {https://doi.org/10.1063/5.0048823},\n"
                    "volume = {118},\n"
                    "year = {2021},\n"
                    "}       \n"
                    "   \n"
                    "\n"
                    "     @article{Pylypovskyi20a,\n"
                    "author = {Oleksandr V. Pylypovskyi and Volodymyr P. Kravchuk and Oleksii M. Volkov and Juergen Fassbender and Denis Sheka and Denys Makarov},\n"
                    "date = {2020-07-09},\n"
                    "doi = {10.1088/1361-6463/ab95bd},\n"
                    "editedat = {2022.01.01 18:47:30},\n"
                    "editedby = {engraver},\n"
                    "issue = {39},\n"
                    "journal = {Journal of Physics D: Applied Physics},\n"
                    "month = {may},\n"
                    "number = {39},\n"
                    "owner = {engraver},\n"
                    "pages = {395003},\n"
                    "publisher = {{IOP} Publishing},\n"
                    "timestamp = {2020.07.10 10:36:16},\n"
                    "title = {Unidirectional tilt of domain walls in equilibrium in biaxial stripes with Dzyaloshinskii-Moriya interaction},\n"
                    "url = {https://iopscience.iop.org/article/10.1088/1361-6463/ab95bd/meta},\n"
                    "volume = {53},\n"
                    "year = {2020},\n"
                    "}       \n";

    QString expected = "@article{Pylypovskyi20a,\n"
                    "author = {Oleksandr V. Pylypovskyi and Volodymyr P. Kravchuk and Oleksii M. Volkov and Juergen Fassbender and Denis Sheka and Denys Makarov},\n"
                    "date = {2020-07-09},\n"
                    "doi = {10.1088/1361-6463/ab95bd},\n"
                    "editedat = {2022.01.01 18:47:30},\n"
                    "editedby = {engraver},\n"
                    "issue = {39},\n"
                    "journal = {Journal of Physics D: Applied Physics},\n"
                    "month = {may},\n"
                    "number = {39},\n"
                    "owner = {engraver},\n"
                    "pages = {395003},\n"
                    "publisher = {{IOP} Publishing},\n"
                    "timestamp = {2020.07.10 10:36:16},\n"
                    "title = {Unidirectional tilt of domain walls in equilibrium in biaxial stripes with Dzyaloshinskii-Moriya interaction},\n"
                    "url = {https://iopscience.iop.org/article/10.1088/1361-6463/ab95bd/meta},\n"
                    "volume = {53},\n"
                    "year = {2020},\n"
                    "}";

    QString result;
    int pos = 0;
    bool res;
    JabrefImport instance("");

    res = instance.getNextEntry(input, &pos, &result);
    res = instance.getNextEntry(input, &pos, &result);

    QCOMPARE(res, true);
    QCOMPARE(result, expected);
}

void JabrefImportTest::test_parseEntry_Fields() {
    JabrefImport instance("");
    QString input;
    BibEntry result;

    input         = "@article{bibkey,\n"
                    "address = {address},\n"
                    "annote = {annote},\n"
                    "author = {author},\n"
                    "booktitle = {booktitle},\n"
                    "chapter = {chapter},\n"
                    "crossref = {crossref},\n"
                    "edition = {edition},\n"
                    "editor = {editor},\n"
                    "howpublished = {howpublished},\n"
                    "institution = {institution},\n"
                    "key = {key},\n"
                    "journal = {journal},\n"
                    "month = {month},\n"
                    "note = {note},\n"
                    "number = {number},\n"
                    "organization = {organization},\n"
                    "pages = {pages},\n"
                    "publisher = {publisher},\n"
                    "school = {school},\n"
                    "series = {series},\n"
                    "title = {title},\n"
                    "type = {type},\n"
                    "volume = {volume},\n"
                    "year = {year},\n"
                    "doi = {doi},\n"
                    "issn = {issn},\n"
                    "isbn = {isbn},\n"
                    "url = {url},\n"
                    "date = {date},\n"
                    "timestamp = {timestamp},\n"
                    "language = {language},\n"
                    "owner = {owner},\n"
                    "abstract = {abstract},\n"
                    "}";
    result = instance.parseEntry(input);
    result.setField(BibEntry::EDITEDBY, "editedby");
    result.setField(BibEntry::EDITEDAT, "editedat");
    QCOMPARE(result.getEntryType(), BibEntry::BET_ARTICLE);
    for (int i = BibEntry::BIBKEY; i < BibEntry::COL_NUM; i ++) {
        QCOMPARE(result.getField(i), QString(BibEntry::BIBENTRY_FIELD[i]));
    }

    input         = "@article{bibkey,\n"
                    "address     =   address    ,\n"
                    "annote =   \"  annote  \" ,\n"
                    "author = { author }    ,\n"
                    "booktitle = {booktitle},\n"
                    "chapter = \" chapter  \",\n"
                    "crossref = {crossref},\n"
                    "edition = {edition}    ,\n"
                    "editor = {editor},\n"
                    "howpublished = {howpublished},\n"
                    "institution = {institution},\n"
                    "key = {key},\n"
                    "journal = {journal},\n"
                    "month = {month},\n"
                    "note = {note},\n"
                    "number = {number},\n"
                    "organization = {organization},\n"
                    "pages = {pages},\n"
                    "publisher = {publisher},\n"
                    "school = {school},\n"
                    "series = {series},\n"
                    "title = {title},\n"
                    "type = {type},\n"
                    "volume = {volume},\n"
                    "year = {year},\n"
                    "doi = {doi},\n"
                    "issn = {issn},\n"
                    "isbn = {isbn},\n"
                    "url = {url},\n"
                    "date = {date},\n"
                    "timestamp = {timestamp},\n"
                    "language = {language},\n"
                    "owner = {owner},\n"
                    "abstract = {abstract},\n"
                    "}";
    result = instance.parseEntry(input);
    result.setField(BibEntry::EDITEDBY, "editedby");
    result.setField(BibEntry::EDITEDAT, "editedat");
    QCOMPARE(result.getEntryType(), BibEntry::BET_ARTICLE);
    for (int i = BibEntry::BIBKEY; i < BibEntry::COL_NUM; i ++) {
        QCOMPARE(result.getField(i), QString(BibEntry::BIBENTRY_FIELD[i]));
    }



    input = "@Article{D1SM00719J,"
            "author =\"Napoli, Gaetano and Pylypovskyi, Oleksandr V. and Sheka, Denis D. and Vergori, Luigi\","
            "title  =\"Nematic shells: new insights in topology- and curvature-induced effects\","
            "journal  =\"Soft Matter\","
            "year  =\"2021\","
            "volume  =\"17\","
            "issue  =\"45\","
            "pages  =\"10322-10333\","
            "publisher  =\"The Royal Society of Chemistry\","
            "doi  =\"10.1039/D1SM00719J\","
            "url  =\"http://dx.doi.org/10.1039/D1SM00719J\","
            "abstract  =\"Within the framework of continuum theory{,} we draw a parallel between ferromagnetic materials and nematic liquid crystals confined on curved surfaces{,} which are both characterized by local interaction and anchoring potentials. We show that the extrinsic curvature of the shell combined with the out-of-plane component of the director field gives rise to chirality effects. This interplay produces an effective energy term reminiscent of the chiral term in cholesteric liquid crystals{,} with the curvature tensor acting as a sort of anisotropic helicity. We discuss also how the different nature of the order parameter{,} a vector in ferromagnets and a tensor in nematics{,} yields different textures on surfaces with the same topology as the sphere. In particular{,} we show that the extrinsic curvature governs the ground state configuration on a nematic spherical shell{,} favouring two antipodal disclinations of charge +1 on small particles and four +1/2 disclinations of charge located at the vertices of a square inscribed in a great circle on larger particles.\"}";

    result = instance.parseEntry(input);
    QCOMPARE(result.getEntryType(), BibEntry::BET_ARTICLE);
    QCOMPARE(result.getField(BibEntry::AUTHOR), "Napoli, Gaetano and Pylypovskyi, Oleksandr V. and Sheka, Denis D. and Vergori, Luigi");
    QCOMPARE(result.getField(BibEntry::TITLE), "Nematic shells: new insights in topology- and curvature-induced effects");
    QCOMPARE(result.getField(BibEntry::JOURNAL), "Soft Matter");
    QCOMPARE(result.getField(BibEntry::YEAR), "2021");
    QCOMPARE(result.getField(BibEntry::VOLUME), "17");
    QCOMPARE(result.getField(BibEntry::NUMBER), "45");
    QCOMPARE(result.getField(BibEntry::PAGES), "10322-10333");
    QCOMPARE(result.getField(BibEntry::PUBLISHER), "The Royal Society of Chemistry");
    QCOMPARE(result.getField(BibEntry::DOI), "10.1039/D1SM00719J");
    QCOMPARE(result.getField(BibEntry::URL), "http://dx.doi.org/10.1039/D1SM00719J");
    QCOMPARE(result.getField(BibEntry::ABSTRACT), "Within the framework of continuum theory{,} we draw a parallel between ferromagnetic materials and nematic liquid crystals confined on curved surfaces{,} which are both characterized by local interaction and anchoring potentials. We show that the extrinsic curvature of the shell combined with the out-of-plane component of the director field gives rise to chirality effects. This interplay produces an effective energy term reminiscent of the chiral term in cholesteric liquid crystals{,} with the curvature tensor acting as a sort of anisotropic helicity. We discuss also how the different nature of the order parameter{,} a vector in ferromagnets and a tensor in nematics{,} yields different textures on surfaces with the same topology as the sphere. In particular{,} we show that the extrinsic curvature governs the ground state configuration on a nematic spherical shell{,} favouring two antipodal disclinations of charge +1 on small particles and four +1/2 disclinations of charge located at the vertices of a square inscribed in a great circle on larger particles.");

}

void JabrefImportTest::test_parseEntry_Fields_special() {
    JabrefImport instance("");
    QString input;
    BibEntry result;

    input         = "@article{bibkey,\n"
                    "address     =   address    \n"
                    "}";
    result = instance.parseEntry(input);
    QCOMPARE(result.getField(BibEntry::ADDRESS), "address");

    input         = "@article{bibkey,\n"
                    "address     =   address ,   \n"
                    "abstract = {abstract},\n"
                    "}";
    result = instance.parseEntry(input);
    QCOMPARE(result.getField(BibEntry::ADDRESS), "address");

    input         = "@article{bibkey,\n"
                    "address     =  { add\"  r   \" ess   } ,   \n"
                    "abstract = {abstract},\n"
                    "}";
    result = instance.parseEntry(input);
    QCOMPARE(result.getField(BibEntry::ADDRESS), "add\"  r   \" ess");

    input         = "@article{bibkey,\n"
                    "address     =  \" add{  r   } ess   \" ,   \n"
                    "abstract = {abstract},\n"
                    "}";
    result = instance.parseEntry(input);
    QCOMPARE(result.getField(BibEntry::ADDRESS), "add{  r   } ess");

    input         = "@article{bibkey,\n"
                    "address     =  { add\"  r,   \" ess   } ,   \n"
                    "abstract = {abstract},\n"
                    "}";
    result = instance.parseEntry(input);
    QCOMPARE(result.getField(BibEntry::ADDRESS), "add\"  r,   \" ess");

    input         = "@article{bibkey,\n"
                    "address     =  \" add{  r,   } ess   \" ,   \n"
                    "abstract = {abstract},\n"
                    "}";
    result = instance.parseEntry(input);
    QCOMPARE(result.getField(BibEntry::ADDRESS), "add{  r,   } ess");

    input         = "@article{bibkey,\n"
                    "address     =  \" add{  r,   } ess   \" ,   \n"
                    "abstract = abstract   \n"
                    "}";
    result = instance.parseEntry(input);
    QCOMPARE(result.getField(BibEntry::ADDRESS), "add{  r,   } ess");
    QCOMPARE(result.getField(BibEntry::ABSTRACT), "abstract");

    input         = "@article{bibkey,\n"
                    "}";
    result = instance.parseEntry(input);
    QCOMPARE(result.getID(), BibEntry::NOT_EXISTS);
}

void JabrefImportTest::test_parseEntry_Groups() {
    QString input = "@article{bibkey,\n"
                    "groups = {grp1, grp2,    grp3 },\n"
                    "}";

    JabrefImport instance("");

    BibEntry result = instance.parseEntry(input);

    QStringList grps = result.getTextNamedGroups();
    QCOMPARE(grps.length(), 3);
    QCOMPARE(grps[0], "grp1");
    QCOMPARE(grps[1], "grp2");
    QCOMPARE(grps[2], "grp3");
}

void JabrefImportTest::test_parseEntry_Files() {
    JabrefImport instance("");
    QString input;

    input = "@article{bibkey,\n"
                    "file = {:path/file.pdf:PDF},\n"
                    "}";
    BibEntry result = instance.parseEntry(input);
    QStringList files = result.getFiles();
    QCOMPARE(files.length(), 1);
    QCOMPARE(files[0], "path/file.pdf");

    input = "@article{bibkey,\n"
                    "file = {:path/to/file.pdf:PDF;:path/to/suppl.pdf:PDF},\n"
                    "}";
    result = instance.parseEntry(input);
    files = result.getFiles();
    QCOMPARE(files.length(), 2);
    QCOMPARE(files[0], "path/to/file.pdf");
    QCOMPARE(files[1], "path/to/suppl.pdf");

    input = "@article{bibkey,\n"
                    "file = {English version:path/to/file1.pdf:PDF;Russian version:path/to/file2.pdf:PDF},\n"
                    "}";
    result = instance.parseEntry(input);
    files = result.getFiles();
    QCOMPARE(files.length(), 2);
    QCOMPARE(files[0], "path/to/file1.pdf");
    QCOMPARE(files[1], "path/to/file2.pdf");
}
