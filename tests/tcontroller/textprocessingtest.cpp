#include "textprocessingtest.h"

#include "../../brastbook/controller/textprocessing.h"


void TextProcessingTest::test_processAuthors_basic() {
    QString authors = "";
    bool surnameFirst = false;
    bool authorsNaturalOrder = false;
    bool abbreviate = false;

    QCOMPARE(TextProcessing::processAuthors(authors, surnameFirst, authorsNaturalOrder, abbreviate), "");

    authors = "Aaa    Author1  and  Bbb   Author2";
    QCOMPARE(TextProcessing::processAuthors(authors, surnameFirst, authorsNaturalOrder, abbreviate), authors.simplified());

    authors = "Aaa   Author1";
    QCOMPARE(TextProcessing::processAuthors(authors, surnameFirst, authorsNaturalOrder, abbreviate), authors.simplified());

    authors = "Author1  ,   A.  A.";
    QCOMPARE(TextProcessing::processAuthors(authors, surnameFirst, authorsNaturalOrder, abbreviate), "A. A. Author1");

    authors = "Author1  ,   A.  A. and Author2 ,  B.    B.";
    QCOMPARE(TextProcessing::processAuthors(authors, surnameFirst, authorsNaturalOrder, abbreviate), "A. A. Author1 and B. B. Author2");
}

void TextProcessingTest::test_splitAuthorName() {
    QString author;
    TextProcessing::AuthorName name;

    author = "          ";
    name = TextProcessing::splitAuthorName(author);
    QCOMPARE(name.firstNames, "");
    QCOMPARE(name.lastName, "");

    author = "       Beatty   ";
    name = TextProcessing::splitAuthorName(author);
    QCOMPARE(name.firstNames, "");
    QCOMPARE(name.lastName, "Beatty");

    author = "   Michael    L.  Beatty   ";
    name = TextProcessing::splitAuthorName(author);
    QCOMPARE(name.firstNames, "Michael L.");
    QCOMPARE(name.lastName, "Beatty");

    author = "      Ulpu   Salo     ";
    name = TextProcessing::splitAuthorName(author);
    QCOMPARE(name.firstNames, "Ulpu");
    QCOMPARE(name.lastName, "Salo");

    author = "     Beatty , Michael    L.  ";
    name = TextProcessing::splitAuthorName(author);
    QCOMPARE(name.firstNames, "Michael L.");
    QCOMPARE(name.lastName, "Beatty");

    author = "      Salo  ,   Ulpu    ";
    name = TextProcessing::splitAuthorName(author);
    QCOMPARE(name.firstNames, "Ulpu");
    QCOMPARE(name.lastName, "Salo");

    author = "      Salo  ,   U.    ";
    name = TextProcessing::splitAuthorName(author);
    QCOMPARE(name.firstNames, "U.");
    QCOMPARE(name.lastName, "Salo");

}

void TextProcessingTest::test_abbreviateName() {
    QString author;

    author = "   Jacques      Eliacin       François    Marie  ";
    QCOMPARE(TextProcessing::abbreviateName(author), "J. E. F. M.");

    author = "Jacques    Eliacin   François   Marie de ";
    QCOMPARE(TextProcessing::abbreviateName(author), "J. E. F. M. de");

    author = "Jacques  Eliacin   François   Marie        van    den ";
    QCOMPARE(TextProcessing::abbreviateName(author), "J. E. F. M. van den");

    author = "   J.      E.       F.    M.  ";
    QCOMPARE(TextProcessing::abbreviateName(author), "J. E. F. M.");


}

void TextProcessingTest::test_processAuthors() {
    QString string, result;

    bool surnameFirst = false;
    bool authorsNaturalOrder = false;
    bool abbreviate = false;
    string = "      Frontino  Lachance     ";
    result = "Frontino Lachance";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);

    string = "      Frontino  Lachance   and  Grégoire       Deslauriers    ";
    result = "Frontino Lachance and Grégoire Deslauriers";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);

    string = "      Frontino  Lachance   and  Grégoire       Deslauriers    and Campbell        Jolicoeur  ";
    result = "Frontino Lachance and Grégoire Deslauriers and Campbell Jolicoeur";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);


    surnameFirst = true;
    authorsNaturalOrder = false;
    abbreviate = false;
    string = "      Frontino  Lachance     ";
    result = "Lachance, Frontino";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);

    string = "      Frontino  Lachance   and  Grégoire       Deslauriers    ";
    result = "Lachance, Frontino and Deslauriers, Grégoire";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);

    string = "      Frontino  Lachance   and  Grégoire       Deslauriers    and Campbell        Jolicoeur  ";
    result = "Lachance, Frontino and Deslauriers, Grégoire and Jolicoeur, Campbell";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);


    surnameFirst = false;
    authorsNaturalOrder = true;
    abbreviate = false;
    string = "      Frontino  Lachance     ";
    result = "Frontino Lachance";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);

    string = "      Frontino  Lachance   and  Grégoire       Deslauriers    ";
    result = "Frontino Lachance and Grégoire Deslauriers";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);

    string = "      Frontino  Lachance   and  Grégoire       Deslauriers    and Campbell        Jolicoeur  ";
    result = "Frontino Lachance, Grégoire Deslauriers and Campbell Jolicoeur";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);


    surnameFirst = false;
    authorsNaturalOrder = false;
    abbreviate = true;
    string = "      Frontino  Lachance     ";
    result = "F. Lachance";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);

    string = "      Frontino  Lachance   and  Grégoire       Deslauriers    ";
    result = "F. Lachance and G. Deslauriers";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);

    string = "      Frontino  Lachance   and  Grégoire       Deslauriers    and Campbell        Jolicoeur  ";
    result = "F. Lachance and G. Deslauriers and C. Jolicoeur";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);


    surnameFirst = true;
    authorsNaturalOrder = true;
    abbreviate = false;
    string = "      Frontino  Lachance     ";
    result = "Lachance, Frontino";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);

    string = "      Frontino  Lachance   and  Grégoire       Deslauriers    ";
    result = "Lachance, Frontino and Deslauriers, Grégoire";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);

    string = "      Frontino  Lachance   and  Grégoire       Deslauriers    and Campbell        Jolicoeur  ";
    result = "Lachance, Frontino, Deslauriers, Grégoire and Jolicoeur, Campbell";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);


    surnameFirst = true;
    authorsNaturalOrder = false;
    abbreviate = true;
    string = "      Frontino  Lachance     ";
    result = "Lachance, F.";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);

    string = "      Frontino  Lachance   and  Grégoire       Deslauriers    ";
    result = "Lachance, F. and Deslauriers, G.";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);

    string = "      Frontino  Lachance   and  Grégoire       Deslauriers    and Campbell        Jolicoeur  ";
    result = "Lachance, F. and Deslauriers, G. and Jolicoeur, C.";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);


    surnameFirst = false;
    authorsNaturalOrder = true;
    abbreviate = true;
    string = "      Frontino  Lachance     ";
    result = "F. Lachance";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);

    string = "      Frontino  Lachance   and  Grégoire       Deslauriers    ";
    result = "F. Lachance and G. Deslauriers";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);

    string = "      Frontino  Lachance   and  Grégoire       Deslauriers    and Campbell        Jolicoeur  ";
    result = "F. Lachance, G. Deslauriers and C. Jolicoeur";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);


    surnameFirst = true;
    authorsNaturalOrder = true;
    abbreviate = true;
    string = "      Frontino  Lachance     ";
    result = "Lachance, F.";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);

    string = "      Frontino  Lachance   and  Grégoire       Deslauriers    ";
    result = "Lachance, F. and Deslauriers, G.";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);

    string = "      Frontino  Lachance   and  Grégoire       Deslauriers    and Campbell        Jolicoeur  ";
    result = "Lachance, F., Deslauriers, G. and Jolicoeur, C.";
    QCOMPARE(TextProcessing::processAuthors(string, surnameFirst, authorsNaturalOrder, abbreviate), result);
}
