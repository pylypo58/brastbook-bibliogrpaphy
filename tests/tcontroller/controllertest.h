#ifndef CONTROLLERTEST_H
#define CONTROLLERTEST_H

#include <QTest>


class ControllerTest : public QObject
{
    Q_OBJECT

public:
    ControllerTest();
    ~ControllerTest();

private slots:
    void initTestCase();
    void cleanupTestCase();

    void test_getFirstAuthorSurname_data();
    void test_getFirstAuthorSurname();

    void test_journalAbbrev_data();
    void test_journalAbbrev();

};

#endif // CONTROLLERTEST_H
