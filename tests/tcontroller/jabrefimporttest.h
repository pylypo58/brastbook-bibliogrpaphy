#ifndef JABREFIMPORTTEST_H
#define JABREFIMPORTTEST_H

#include <QTest>


class JabrefImportTest : public QObject
{
    Q_OBJECT

public:
    JabrefImportTest();
    ~JabrefImportTest();

private slots:
    void initTestCase();
    void cleanupTestCase();

    void test_getNextEntry_OneRecord();
    void test_getNextEntry_TwoRecords();

    void test_parseEntry_Fields();
    void test_parseEntry_Fields_special();
    void test_parseEntry_Groups();
    void test_parseEntry_Files();
};

#endif // JABREFIMPORTTEST_H
