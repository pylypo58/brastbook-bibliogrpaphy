#ifndef TEXTPROCESSINGTEST_H
#define TEXTPROCESSINGTEST_H

#include <QTest>


class TextProcessingTest : public QObject
{
    Q_OBJECT

public:
    TextProcessingTest() {};
    ~TextProcessingTest() {};

private slots:
    void initTestCase() {};
    void cleanupTestCase() {};

    void test_processAuthors_basic();

    void test_splitAuthorName();

    void test_abbreviateName();

    void test_processAuthors();
};


#endif // TEXTPROCESSINGTEST_H
