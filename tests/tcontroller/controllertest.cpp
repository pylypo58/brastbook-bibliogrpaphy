#include "controllertest.h"

#include "../../brastbook/controller/controller.h"

ControllerTest::ControllerTest()
{

}

ControllerTest::~ControllerTest()
{

}

void ControllerTest::initTestCase()
{

}

void ControllerTest::cleanupTestCase()
{

}

void ControllerTest::test_getFirstAuthorSurname_data() {
    QTest::addColumn<QString>("string");
    QTest::addColumn<QString>("result");

    QTest::newRow("many authors") << "Beatriz Miranda-Silva and Pedro H. C. Taveira and Allison W. Teixeira and Jakson M. Fonseca and Ricardo G. Elías and Nicolas Vidal-Silva and Vagson L. Carvalho-Santos" << "Miranda-Silva";
    QTest::newRow("two authors") << "Michal Mruczkiewicz and Pawel Gruszecki" << "Mruczkiewicz";
    QTest::newRow("three authors with initials") << "Kostiantyn V. Yershov and Attila Kakay and Volodymyr P. Kravchuk" << "Yershov";
    QTest::newRow("surname-initial") << "Bargmann, V." << "Bargmann";
    QTest::newRow("initial-surname") << "N. Levinson" << "Levinson";
    QTest::newRow("surname-initial many") << "Williams, H. J. and Foster, F. G. and Wood, E. A." << "Williams";
    QTest::newRow("surname-name") << "Brown, Christopher Anthon" << "Brown";
}

void ControllerTest::test_getFirstAuthorSurname()
{
    QFETCH(QString, string);
    QFETCH(QString, result);

    QCOMPARE(Controller::getFirstAuthorSurname(string), result);
}

void ControllerTest::test_journalAbbrev_data() {
    QTest::addColumn<QString>("string");
    QTest::addColumn<QString>("result");

    QTest::newRow("two words") << "Physical Review" << "PR";
    QTest::newRow("four words") << "Bell System Technical Journal" << "BSTJ";
    QTest::newRow("with short words") << "Le Journal de Physique Colloques" << "JPC";
    QTest::newRow("abbreviated") << "Phys. Stat. Sol. (B)" << "PSSB";
    QTest::newRow("with single letters and short words") << "C R C Critical Reviews in Solid State Sciences" << "CRCCRSSS";
    QTest::newRow("one word") << "Nanotechnology"<< "Nan";
}

void ControllerTest::test_journalAbbrev() {

    QFETCH(QString, string);
    QFETCH(QString, result);

    QCOMPARE(Controller::getJournalAbbreviation(string), result);
}
